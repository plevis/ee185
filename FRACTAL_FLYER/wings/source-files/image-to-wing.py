#!/usr/bin/env python3

import cv2
import numpy as np
import sys

if len(sys.argv) != 2:
  print("usage: ", sys.argv[0], "FILE")
  sys.exit(1)

input = sys.argv[1]
print("Reading", input)
image = cv2.imread(input, cv2.IMREAD_COLOR)
(iheight, iwidth, idepth) = image.shape

tokens = input.split(".")
print(tokens)
leading = tokens[0:-1]
type = tokens[-1]

filename = ".".join(leading)
scaled_file = filename + "-scaled." + type
cropped_file = filename + "-cropped." + type

print("Will produce output", scaled_file, "and", cropped_file)
h = 501
w1 = 1375
w2 = 1659
aspect_ratio = float(h) / float(w2)

oheight = 0
owidth = 0
odepth = idepth
if aspect_ratio * iwidth > iheight:
  oheight = int(iheight)
  owidth = int(iheight / aspect_ratio)
else:
  owidth = int(iwidth)
  oheight = int(owidth * aspect_ratio)

wscale = owidth / iwidth
hscale = oheight / iheight

mid = float(w1) / float(w2)
midpoint = int(mid * owidth)

print("Source is " + str(iwidth) + "x" + str(iheight))
print("Output is " + str(owidth) + "x" + str(oheight))
print("Width scale is " + str(wscale) + ", height scale is " + str(hscale));
print("Midpoint is at", midpoint)

# Make the scaled output

output = np.ndarray([oheight, owidth, odepth])
output.fill(0) # Fill it black

for x in range(midpoint):
  fraction = float(x) / float(midpoint)
  stop = (fraction * oheight) + .0001
  samplex = int((float(x) / float(owidth)) * iwidth);
  for y in range(oheight):
    sampley = int((float(y) / stop) * iheight);
    if y < int(stop): 
      output[y, x] = image[sampley, samplex]

for x in range(midpoint, owidth):
  fraction = 1.0 - (float(x - midpoint) / float(owidth - midpoint))
  stop = fraction * oheight
  samplex = int((float(x) / float(owidth)) * iwidth);
  for y in range(oheight):
    sampley = int((float(y) / stop) * iheight);
    if y < int(stop):
      output[y, x] = image[sampley, samplex] 

cv2.imwrite(scaled_file, output)

# Make the cropped output

output = np.ndarray([oheight, owidth, odepth])
output.fill(0) # Fill it black

def image_to_bw(image, y, x):
  [r, g, b] = image[y, x]
  if r == 0 or g == 0 or b == 0:
    print(x, y, ":", r, g, b, "->", 255, 255, 255);
    return [255, 255, 255]
  for dx in range(-1, 2):
    for dy in range(-1, 2):
      newx = x + dx
      newy = y + dy
      if newx >= owidth or newy >= oheight or newx < 0 or newy < 0:
        print(x, y, newx, newy, "::", r, g, b, "->", 255, 255, 255);
        return [255, 255, 255]
      else:
        [newr, newg, newb] = image[newy, newx]
        if newr != 0 or newg != 0 or newb != 0:
          print(x, y, newx, newy, ":::", newr, newg, newb, "->", 0, 0, 0);
          return [0, 0, 0]
  return [255, 255, 255] 
  
for x in range(midpoint):
  fraction = float(x) / float(midpoint)
  stop = (fraction * oheight) + .0001
  for y in range(oheight):
    if y < int(stop): 
      output[y, x] = image_to_bw(image, y, x)

for x in range(midpoint, owidth):
  fraction = 1.0 - (float(x - midpoint) / float(owidth - midpoint))
  stop = fraction * oheight
  for y in range(oheight):
    if y < int(stop):
      output[y, x] = image_to_bw(image, y, x)

cv2.imwrite(cropped_file, output)

