Adamkiewicz,Michal	Electrical Engineering (BS)
	Electrical Engineering (BS)
Garimella,Mihir	Computer Science (MS)
	Computer Science (MS)
Huang,Claire	Computer Science (BS)
	Computer Science (BS)
Konz,Sean William	Electrical Engineering (BS)
	Electrical Engineering (BS)
Marom,Lee	Mechanical Engineer (MS)
	Mechanical Engineer (MS)
Mendoza,David	Electrical Engineering (MS)
	Electrical Engineering (MS)
Stein,Andrea Nari	Mechanical Engineer (MS)
	Mechanical Engineer (MS)
Thompson,Will Charles	Mechanical Engineer (MS)
	Mechanical Engineer (MS)
Vrakas,Tim Paul	Electrical Engineering (BS)
	Electrical Engineering (BS)
Woo,Kelly	Electrical Engineering -
	Electrical Engineering (PhD)
Marler,Sydney	Undergraduate Matriculated -
	Electrical Engineering (BS)
Tsao,Charles	Undergraduate Matriculated -
	Electrical Engineering (BS)
Colbert,Eric William	Mechanical Engineer -
	Mechanical Engineer (MS)
Ayers,Hudson Randal	Electrical Engineering -
	Electrical Engineering (PhD)/Electrical Engineering (MS)
Davenport,Carly Aubrie	Electrical Engineering -
	Electrical Engineering (MS)
Domingo,Ron	Electrical Engineering -
	Electrical Engineering (MS)
Dunham,Hallie	Electrical Engineering -
	Electrical Engineering (MS)
Huang,Claire	Undergraduate Matriculated -
	Computer Science (BS)
Marom,Lee	Mechanical Engineer -
	Mechanical Engineer (MS)
Moran,Courtney Noel	Electrical Engineering -
	Electrical Engineering (MS)
Nguyen,Vinh Quang	Electrical Engineering -
	Electrical Engineering (MS)
Palacios Orbe,Omar	Electrical Engineering -
	Electrical Engineering (MS)
Perera,Viraga	Electrical Engineering -
	Electrical Engineering (MS)
Stein,Andrea Nari	Mechanical Engineer -
	Mechanical Engineer (MS)
Thompson,Will Charles	Mechanical Engineer -
	Mechanical Engineer (MS)
Van,Erik Kevin	Electrical Engineering -
	Electrical Engineering (MS)
Chen,Ashley	Undergraduate Matriculated -
	Computer Science (BS)
Durvasula,Samsara Pappu	Undergraduate Matriculated -
	Computer Science (BS)
Malty,Andrew M	Computer Science -
	Computer Science (MS)
Mbuthia,Martin	Undergraduate Matriculated -
	Computer Science (BS)
Swai,Moses Andrew	Mechanical Engineer -
	Mechanical Engineer (MS)
Baruah,Nirvik	Undergraduate Matriculated -
	Computer Science (BS)/Classics (Min)
Flat,Jonathan Robert	Undergraduate Matriculated -
	Computer Science (BS)/Physics (BS)
Mehta,Bhagirath P	Undergraduate Matriculated -
	Computer Science (BS)/Linguistics (BA)
Mohamdy,Abdu	Undergraduate Matriculated -
	Computer Science (BS)/Mathematics (Min)
Audet,Abby Catherine	Undergraduate Matriculated -
	Electrical Engineering (BS)
Kim,Kyung-Tae J	Electrical Engineering -
	Electrical Engineering (MS)
Landa,Albert Singyu	Electrical Engineering -
	Electrical Engineering (MS)
Owusu-Akyaw,Akwasi Sarpong	Electrical Engineering -
	Electrical Engineering (MS)
Van Dyke,David Ho	Mechanical Engineer -
	Mechanical Engineer (MS)
Fu,Alex Wang	Computer Science -
	Computer Science (MS)
Gong,Jackie C	Undergraduate Matriculated -
	Undeclared (B)
Ke,Jake	Electrical Engineering -
	Electrical Engineering (MS)
Medina Perez,Antonio Rafael	Engineering -
	Engineering (MS)
Cheng,Christine	Electrical Engineering -
	Electrical Engineering (MS)
Hernandez Fernandez,Maria Paula	Electrical Engineering -
	Electrical Engineering (MS)
Jones,Blake Malcolm	Undergraduate Matriculated -
	Electrical Engineering (BS)
Leong,Anna Ya-Li	Electrical Engineering -
	Electrical Engineering (MS)
Mei,Yuchen	Electrical Engineering -
	Electrical Engineering (MS)
Pyarali,Maisam	Electrical Engineering -
	Electrical Engineering (MS)
Rodriguez,Sam Arioch	Mechanical Engineer -
	Mechanical Engineer (MS)
Srivastav,Vidisha	Electrical Engineering -
	Electrical Engineering (MS)
Gallo Dagir,Raul	Undergraduate Matriculated -
	Electrical Engineering (BS)
Miller,Becky	Undergraduate Matriculated -
	Mechanical Engineer (BS)
Queeglay,Owen Mialou	Undergraduate Matriculated -
	Undeclared (B)
Vesey,Annie Sinclair	Undergraduate Matriculated -
	Mechanical Engineer (BS)
Jih-Schiff, Ava Elizabeth
    Electrical Engineering (BS)
Zampa, Caterina Maria
    Electrical Engineering (BS)
Dressel, Dea Elizabeth
	Computer Science (BS)
Eicher,Seiji
	Computer Science (MS)
Moser, Joseph Luis
	Computer Science (BS)

