| #    | Category                 | Theme                                     | Words                                                        | Icon                               |
| ---- | ------------------------ | ----------------------------------------- | ------------------------------------------------------------ | ---------------------------------- |
|      |                          |                                           |                                                              |                                    |
| 1    | In Memory Of             | Harris  Ryan                              | Harris Ryan: Established Department                          | harris-crt                         |
| 2    |                          | Ward  Kindy                               | Ward Kindy: Power Generation                                 | transformer                        |
| 3    |                          | Hugh  Skilling                            | Hugh Skilling: Engineering Educator                          | book                               |
| 4    |                          | Frederick  Terman                         | Frederick Terman: Silicon Valley                             | silicon_valley                     |
| 5    |                          | John  Linvill                             | John Linvill: Transistorized Curriculum                      | transistor                         |
| 6    |                          | Gordon  Kino                              | Gordon Kino: Confocal Microscopy                             | confocal_microscope                |
| 7    |                          | Anthony  Siegman                          | Anthony Siegman: Lasers & Masers                             | laser                              |
| 8    |                          | Calvin  Quate                             | Calvin Quate: Nanoscience & Microscopy                       | atomic_force_microscopy            |
| 9    |                          | Edward  McCluskey                         | Edward McCluskey: Digital Design                             | multivalue_logic_circuits          |
| 10   |                          | Thomas  Cover                             | Thomas Cover: Superposing Signals                            | broadcast_channel                  |
| 11   |                          | Hector  Garcia-Molina                     | Hector Garcia-Molina: Mentor to All                          | stanford_digital_library_project   |
| 12   |                          | Owen Garriott                             | Owen Garriot: NASA Astronaut                                 | skylab_3                           |
| 13   |                          | Gerald  Pearson                           | Gerald Pearson: Photovoltaic cells                           | PVC                                |
| 14   |                          | Frederic Auten Combs Perrine              | Stanford's First Professor of Electrical Engineering         | pole_line_construction             |
| 15   |                          | James Spilker                             | Global Positioning System (GPS)                              | gps_icon                           |
| 16   |                          | Allen Peterson                            | Radio Physics Laboratory and Communications Laboratory       | over_the_horizon                   |
| 17   | Notable Alumni           | Martin  Hellman                           | Martin Hellman: Public Key Cryptography                      | public_key_cryptography            |
| 18   |                          | John  Hennessy                            | John Hennessy: Computer Architecture                         | mips_architecture                  |
| 19   |                          | Jen-Hsun  Huang                           | Jen-Hsun Huang: nVIDIA                                       | nVIDIA                             |
| 20   |                          | Jerry  Yang                               | Jerry Yang: Yahoo!                                           | yahoo                              |
| 21   |                          | David  Filo                               | David Filo: Yahoo!                                           | yahoo                              |
| 22   |                          | Vint  Cerf                                | Vint Cerf: Father of the Internet                            | internet                           |
| 23   |                          | Larry  Page                               | Larry Page: Google                                           | google                             |
| 24   |                          | Sergey  Brin                              | Sergey Brin: Google                                          | google                             |
| 25   |                          | Andy  Bechtolsheim                        | Andy Bechtolsheim: Sun Microsystems                          | sun_microsystems                   |
| 26   |                          | Sandy  Lerner                             | Sandy Lerner: Cisco Systems                                  | cisco                              |
| 27   |                          | John  Cioffi                              | John Cioffi: Father of DSL                                   | dsl                                |
| 28   |                          | Thomas  Kailath                           | Thomas Kailath: Control Theory                               | linear_systems                     |
| 29   |                          | Ray  Dolby                                | Ray Dolby: Dolby Laboratories                                | dolby                              |
| 30   |                          | Ted Hoff                                  | Ted Hoff: Microprocessor                                     | microprocessor                     |
| 31   |                          | Bill  Hewlett                             | Bill Hewlett: HP                                             | hp                                 |
| 32   |                          | David  Packard                            | David Packard: HP                                            | hp                                 |
| 33   |                          | Ellen Ochoa                               | Ellen Ochoa: First Latina to Space                           | NASA                               |
| 34   |                          | Stan Honey                                |Stan Honey: Navigator and Entrepreneur                        | sportvision                        |
| 35   |                          | Acha Leke                                 | Acha Leke: African Leadership Academy                        | ALA                                |
| 36   |                          | Kristina Johnson                          | Kristina Johnson: Ohio State University                      | ohio_state                         |
| 37   |                          | James Gibbons                             | James Gibbons: Semiconductor                                 | semiconductor_transistor           |
| 38   |                          | Rahul Panicker                            | Rahul Paniker: Entrepreneur                                  | Wadhwani                           |
| 39   |                          | Mark Dean                                 | Mark Dean: IBM                                               | IBM                                |
| 40   | Departmental Initiatives | Stanford  Prototyping Facility            | SPF: System Prototyping Facility                             | stanford_logo                      |
| 41   |                          | SystemX  Alliance                         | SystemX Alliance                                             | SystemXLogo_bw                     |
| 42   |                          | Computer  Forum                           | Computer Forum                                               | stanford_computer_forum            |
| 43   |                          | Q-Farm:  Stanford-SLAC Quantum Initiative | Q-Farm: Stanford-SLAC Quantum Initiative                     | Qfarm_logo_selection               |
| 44   |                          | Stanford  Student Space Initiative        | Stanford Student Space Initiative                            | sssi                               |
| 45   |                          | Stanford  Student Robotics Club           | Stanford Student Robotics Club                               | sfsrobotics_bw                     |
| 46   |                          | Lab64  Makerspace                         | Lab64 Makerspace                                             | Lab64_bw                           |
| 47   |                          | SNF:  Stanford Nanofabrication Facility   | SNF: Stanford Nanofabrication Facility                       | snf                                |
| 48   |                          | MIPS  Processor                           | MIPS Processor                                               | MIPSBW                             |
| 49   |                          | Stanford  Computational Imaging Lab       | Stanford Computational Imaging Lab                           | sci-logo                           |
| 50   | Current Faculty/ Staff   | Umran  Inan                               | Umran Inan: VLF Electromagnetic Radiation                    | radio-antenna                      |
| 51   |                          | Dorsa  Sadigh                             | Dorsa  Sadigh: Autonomous Systems                            | autonomous-smart-car               |
| 52   |                          | Chelsea  Finn                             | Chelsea  Finn: Robotic Intelligence                          | robotic-intelligence               |
| 53   |                          | Jeannette  Bohg                           | Jeannette Bohg: Robot Mesmeriser                             | bohg_icon                          |
| 54   |                          | John  Duchi                               | John  Duchi: Optimization and Beyond                         | convex_function_illustration       |
| 55   |                          | Juan  Rivas-Davila                        | Juan  Rivas-Davila: Electrical Power                         | resonant-converter                 |
| 56   |                          | Mary  Wootters                            | Mary  Wootters: Theoretical Aspects of Engineering           | algorithms                         |
| 57   |                          | H. -S.  Philip Wong                       | H.-S. Philip Wong: Nanoelectronics systems                   | Philip_Wong_Icon                   |
| 58   |                          | Joseph  Little                            | Joseph  Little: Any sufficiently advanced research is indistinguishable from magic | DiscWorld    |
| 59   |                          | Denise  Murphy                            | Denise Murphy: Departmental Memory                           | recordkeeping                      |
| 60   |                          | Sara Achour                               | Programming Analog Computing Platforms                       |                                    |
| 61   |                          | Dan Congeve                               | Optoelectronic Nanomaterials                                 |                                    |
| 62   |                          | Jonathan Fan                              | AI meets optics                                              |                                    |
| 63   |                          | Andrea Goldsmith                          | Wireless Communications Trailblazer, Educator, Mentor        |                                    |
| 64   |                          | Teresa Meng                               | WiFi Pioneer                                                 |                                    |
| 65   |                          | Charles Orgish                            | Manager of Distributed Systems                               |                                    |
| 66   |                          | Mert Pilanci                              | Optimization and Neural Networks                             | neural_network_icon                |
| 67   |                          | Priyanka Raina                            | Hardware Accelerators                                        |                                    |
| 68   |                          | Caroline Trippel                          | High Assurance Computer Architecture                         |                                    |
| 69   |                          | Jelena Vučković                           | Nanoscale and Quantum Photonics                              | wavelength_splitter                |
| 70   |                          | Gordon Wetzstein                          | Computational Imaging and Display                            | sci-logo_Wetzstein                 |
| 71   |                          | Pat Hanrahan                              | Computer Graphics and Visualization                          |                                    |
| 72   |                          | Abbas El Gamal                            | Communications, FPGAs, and CMOS imaging sensors              |                                    |
| 73   |                          | Arogyaswami Paulraj                       | MIMO Communications                                          |                                    |
| 74   |                          | Whitfield Diffie                          | New Directions in Cryptography                               |                                    |
| 75   | Department History       | The Construction of The Dish              | Stanford Research Institute Radio Antenna                    | satellite_dish                     |
| 76   |                          | Ryan High-Voltage Lab                     | High Voltage Phenomena Research                              | electricity_generator              |




Note: Those entries in *italics* are subject to change based on information to be received from the related individual
