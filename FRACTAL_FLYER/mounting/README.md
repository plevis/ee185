# Mounting Approaches

This directory stores all of the files, notes, and techincal documents for
the mounting approaches used to attach the Fractal Flyers to the Packard
stairwell.

The directory structure:
  - ceiling-hang-points.pdf: diagram of where wire loops are for hanging flyers from the Packard stairwell ceiling
  - first-design-spr22: documentation of the first mounting design (replaced by the one in mounting-design.pdf)
  - mounting-design.pdf: documentation of design decisions and technical components of the mounting approaches,
  - solidworks: CAD parts and assemblies of mounting approaches for staircase and front window, and displacement results from simulations
