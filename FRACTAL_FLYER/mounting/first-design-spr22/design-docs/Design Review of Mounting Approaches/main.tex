%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------
% Math equation/formula
% \begin{equation}
% 	I = \int_{a}^{b} f(x) \; \text{d}x.
% \end{equation}


\documentclass{article}
\usepackage{array}
\usepackage{indentfirst}
\usepackage{multicol}
\usepackage{float}
\usepackage{caption}
\usepackage{sectsty}
\subsubsectionfont{\fontsize{12}{15}\selectfont}
\subsubsectionfont{\fontsize{11}{15}\selectfont}

\input{structure.tex} % Include the file specifying the document structure and custom commands

%----------------------------------------------------------------------------------------
%	ASSIGNMENT INFORMATION
%----------------------------------------------------------------------------------------

\title{Design Review of Mounting Approaches} % Title of the assignment

\author{Dea Dressel \\
{\tt\small ddressel@stanford.edu}
\and
Ava Jih-Schiff\\
{\tt\small avaj@stanford.edu}} % Author name and email address


\date{Stanford University --- \today} % University, school and/or department name(s) and a date

%----------------------------------------------------------------------------------------

\begin{document}

\maketitle % Print the title

%----------------------------------------------------------------------------------------
%	INTRODUCTION
%----------------------------------------------------------------------------------------

\section*{Introduction} % Unnumbered section

This document provides insight into the design requirements and proposed options for the mounting mechanisms that will hold the fractal flyers in the Packard stairwell. Section 1 addresses the identified design and functional requirements for the mounting mechanisms. Section 2 offers multiple design options and addresses how each option fits within those requirements.

%----------------------------------------------------------------------------------------
%	Section 1
%----------------------------------------------------------------------------------------

\section{Requirements}

We have identified 12 total requirements for the mounting mechanism designs:
\begin{description}\begin{enumerate}
  \item Multiple Mounting Locations
  \item Material and Color
  \item Easy Installation and Removal of Mounting Mechanism
  \item Easy Attachment and Removal of Fractal Flyer 
  \item Adjustable
  \item Horizontal Extension
  \item Vertical Extension
  \item Weight Bearing
  \item Cost
  \item Earthquake Considerations
  \item Ethernet Cable Connection
  \item Corrosion Protection
\end{enumerate}\end{description}

The design options provided in this document focus on the top 7 of the stated requirements. The bottom 5 requirements are important to be aware of during this first step, but will be fully addressed later in the Mechanical Analysis of Mounting Mechanisms document. 

\subsection{Multiple Mounting Locations}

The artistic vision requires evenly dispersed fractal flyers throughout the entire three-story stairwell. Therefore, the chosen mounting locations must allow fractal flyers to be placed virtually anywhere in the space. This includes all areas other than the space directly above where a person would be walking up the stairs. Additionally, the chosen mounting locations should ideally allow the fractal flyers to interact with the sunlight that comes in through the front window and ceiling. Lastly, to simplify the designs, there should only be three mounting locations.

%------------------------------------------------

\subsection{Material and Color}

Firstly, the material of the mounting mechanisms cannot damage the building. The proposed designs must use materials that do not corrode, crack or deteriorate the building surfaces. Ideally, the proposed designs also do not fade or scratch the painted surfaces. The designs should also use a limited number of different materials to ensure coherency between the mounting mechanisms and the building.

Secondly, the color of the mounting mechanisms should work with the aesthetic goals of the project. As of right now, the goal is muted coloring that blends in with the industrial feel of the building. Considering that the flyer itself is the main focal point of the art installation, mounting options that serve as a background will allow the flyer to be the focus, appearing as though it is "flying". However, this may change. 

%------------------------------------------------
\subsection{Easy Installation and Removal of Mounting Mechanism}

The proposed mounting mechanism designs should be easy to install and remove. Installing the mounting mechanism should be a two person job that takes around 5 minutes. It may require scaffolding to reach high mounting points, but it should not require any other special tools. Removing the mounting mechanism should be similarly easy. The mounting mechanism should not require any drilling into the building. After removing the mounting mechanisms, it should be as if they never existed.

%------------------------------------------------
\subsection{Easy Installation and Removal of Fractal Flyer}

Installing and removing a fractal flyer to each mounting mechanism should be a one person job that takes roughly one minute. The one person should not need any special tools other than scaffolding to reach the mechanism. 

%------------------------------------------------
\subsection{Adjustable}

The mounting mechanisms should be adjustable in either horizontal or vertical extension. This will allow the fractal flyers to encompass a wider area within the stairwell, reaching better light and filling the space evenly. 

%------------------------------------------------
\subsection{Horizontal Extension}

Depending on the mounting location, the design options may need to have horizontal extension away from the mounting point. This horizontal extension must be greater than 24 inches away from any surface in order to safely hang the fractal flyer.

%------------------------------------------------
\subsection{Vertical Extension}

Every mounting mechanism should have some vertical extension, regardless of mounting location. There are no lower limits on the vertical extension.

%------------------------------------------------
\subsection{Weight Bearing}

The mounting mechanisms must bear the weight of a fractal flyer. The estimated weight of the fractal flyer can be broken down into its main components:\\

\begin{tabular}{ll}
    \textbf{Two Wings:}  & $2.5$ lbs\\ \\
    \textbf{Top Plate + Two Motors:}  & $3.2$ lbs\\ \\
    \textbf{Bottom Shell:}  & $0.5$ lbs\\ \\
    \rule{120}{1} & \rule{39}{1}\\ \\ 
    \textbf{Total Weight:}  & $6.2$ lbs\\ \\
\end{tabular}

Since we do not have access to a complete and finalized fractal flyer, we have decided to round up our overall estimate to 10 lbs. The components that we were unable to weigh consisted of the three turnbuckles, the hinges, and the finalized top plate with all electronics, mounts, and motors attached. The one we did weigh seemed to include most parts, but because most of the parts are custom made we were unable to get estimates online. Based on the weight of our other, much larger, metal pieces, we feel as though this is a safe estimate that provides a good deal of wiggle room.
%------------------------------------------------
\subsection{Cost}

The total cost of each mounting mechanism should be no more than \$50.

%------------------------------------------------
\subsection{Earthquake Considerations}

In the event of an earthquake, the mounting mechanisms must remain attached to the building and the fractal flyer must remain attached to the mounting mechanism.

%------------------------------------------------
\subsection{Ethernet Cable Considerations}

The mounting mechanisms must allow for the Ethernet cable to be safely run down to its port on the flyer board. This will include ensuring the attachment is secure and that the flyer can easily be disconnected form both the Ethernet cable and mounting mechanism in case of repairs. 

%------------------------------------------------
\subsection{Corrosion Protection}

 The choice of material for the mounting mechanisms must not corrode the building. 


%	Section 2
%----------------------------------------------------------------------------------------

\section{Design Options}

Satisfying our first requirement of multiple mounting locations, we offer design options for three different mounting locations: ceiling, staircase and front window. The design options directly address the top 7 of the previously mentioned requirements while keeping in mind the other 5 requirements. 

%------------------------------------------------

\subsection{Ceiling}

The first mounting location is the ceiling. We will be hanging fractal flyers from the circular, steel shafts that extend across the stairwell ceiling. The current plan has 12 fractal flyers hanging from this location.

The measurements and reference sketch of the circular ceiling shafts can be found below:\\

% \noindent
\begin{minipage}[c]{0.4\linewidth}
\begin{tabular}{ll}
    \textbf{Radius:}  & $4$ in\\ \\
      % \textbf{Length:}  & we should probably have the lengths of all the sections?\\ \\
\end{tabular}
\end{minipage} % no space if you would like to put them side by side
\begin{minipage}[c]{0.6\linewidth}
\includegraphics[scale=0.35]{Ceiling-Mounting-Location.png}
\end{minipage}\\


\subsubsection{Option 1: Wire Cable with Cable Gripper and Carabiner}

% \noindent\textbf{Option 1: Wire Cable with Cable Gripper and Carabiner}\\

The first option is a wire cable that loops around the circular ceiling shafts. The loose end of the wire cable is fastened by a cable gripper. The other end of the cable has a carabiner for easy attachment of a fractal flyer. A rough sketch of what this would look like can be seen below.\\

\begin{center}
\includegraphics[width=0.8\textwidth]{Ceiling-Option-1.png}
\end{center}


\noindent\textbf{Material and Color:} The wire cable material of this approach keeps to the industrial feel and coloring of the building.\\\\
\textbf{Easy Installation and Removal of Mounting Mechanism:} Installing and removing this design is a two person job that would take around five minutes. It does not require any special tools other than scaffolding to reach the locations. This design does not require any permanent changes to the building components.\\\\
\textbf{Easy Attachment and Removal of Fractal Flyer:} Attaching and removing a fractal flyer to this design is a two person job that would take one minute. We expect one person would need to lift the fractal flyer from below to loosen the connection of the turnbuckles with the carabiner, after which the other person would unclip the fractal flyer and lower it down. It does not require any special tools other than scaffolding to reach the locations. \\\\
\textbf{Adjustable:} The wire cable can be fed through the cable gripper until the desire length is reached.\\\\
\textbf{Horizontal Extension:} The ceiling-mounted fractal flyers do not need any horizontal extension from the mounting location.\\\\
\textbf{Vertical Extension:} The vertical distance from mounting location is variable and can be adjusted.

\subsubsection{Option 2: Rigging Sling, Shackle, and Double Sided Carabiner Wire}

The second option is a rigging sling that loops around the circular ceiling shafts with a bow shackle to which a wire cable is attached. The sling loops inside of itself creating a close cinch to the ceiling shafts. A rough sketch of what this would look like can be seen below.\\
\begin{center}
\includegraphics[width=0.8\textwidth]{Ceiling-Option-2.png}
\end{center}

\noindent\textbf{Material and Color:} The mixed materials in this approach make it less aesthetically pleasing. That being said, we hope to find a rigging sling that may be close in color to the ceiling shafts.\\\\
\textbf{Easy Installation and Removal of Mounting Mechanism:} Refer to option 1.\\\\
\textbf{Easy Attachment and Removal of Fractal Flyer:} Refer to option 1.\\\\
\textbf{Adjustable:} The wire cable can be clamped at any desired length.\\\\
\textbf{Horizontal Extension:} Refer to option 1.\\\\
\textbf{Vertical Extension:} Refer to option 1.

\subsubsection{Option 3: Rope with Carabiner}

The third option is a rope that loops around the circular ceiling shafts. The rope would loop through a knotted loop on one end which fastens it to the shaft. The other end of the rope has a carabiner for easy attachment of a fractal flyer. A rough sketch of what this would look like can be seen below.

\begin{center}
\includegraphics[width=0.8\textwidth]{Ceiling-Option-3.png}  
\end{center}

\noindent\textbf{Material and Color:} The rope material would not greatly scratch the paint on the ceiling shaft, and there are many options for rope color.\\\\
\textbf{Easy Installation and Removal of Mounting Mechanism:} Refer to option 1.\\\\
\textbf{Easy Attachment and Removal of Fractal Flyer:} Refer to option 1.\\\\
\textbf{Adjustable:} The rope can be cut and tied at any desired length.\\\\
\textbf{Horizontal Extension:} Refer to option 1.\\\\
\textbf{Vertical Extension:} Refer to option 1.

%------------------------------------------------

\subsection{Staircase}

The second mounting location is the staircase. We will be hanging fractal flyers off of both the inner and outer steel I-beams that line the edges of the staircase. The current plan has 35 outer edge flyers and 4 inner edge flyers.  

The measurements and reference sketch of the I-beam that lines the inner and outer edges of the stairwell can be found below: \\


\begin{minipage}[c]{0.37\linewidth}
    \begin{tabular}{ll}
        \textbf{Flange Width:}  & $7$ in\\ \\
        \textbf{Flange Thickness:}  & $\frac{3}{4}$ in\\ \\
        \textbf{Web Thickness:}  & $2$ in\\ \\
        \textbf{Slope:}  & $28$ deg\\ \\
        \textbf{Light Conduit Gap:}  & $1$ in\\ \\
    \end{tabular}
\end{minipage} % no space if you would like to put them side by side
\begin{minipage}[c]{0.63\linewidth}
\includegraphics[scale=0.44]{Staircase-Mounting-Location.png}
\end{minipage}

\subsubsection{Option 1: C-Clamp with Angled Beams}

The first option is a C-Clamp on the outer edge of the staircase I-beam, a straight, vertical beam, a second beam angled up and away from the stairs (exact angle has yet to be decided), and a wire cable hanging mechanism similar to that in the first design option for ceiling. Current options being explored for connecting the two beams are welding, a fixed elbow joint that screws in, or an open 3-way elbow joint with a cap closing off the end to avoid slippage. Whether the beams are circular or square will depend on the chosen connection type. \\

\begin{center}
\includegraphics[width=0.8\textwidth]{Staircase-Option-1.png}
\end{center}


\noindent\textbf{Material and Color:} Grey metal pieces would create a mount that blends in and mirrors the industrial, exposed steel theme of the Packard staircase. \\\\
\textbf{Easy Installation and Removal of Mounting Mechanism:} Installing and removing this design is a two person job that would take around five minutes. It does not require any special tools other than scaffolding to reach the locations. This design does not require any permanent changes to the building components.\\\\
\textbf{Easy Attachment and Removal of Fractal Flyer:} Attaching and removing a fractal flyer to this design is a two person job that would take one minute. We expect one person would need to lift the fractal flyer from below to loosen the connection of the turnbuckles with the carabiner, after which the other person would unclip the fractal flyer and lower it down. It does not require any special tools other than scaffolding to reach the locations. \\\\
\textbf{Adjustable:} The angle between the two beams will be set and non-adjustable. This design supports a few different, interchangeable shaft lengths and an adjustable wire cable.\\\\
\textbf{Horizontal Extension:} The angle between and length of both beams will provide the desired and required horizontal extension away from the staircase.\\\\
\textbf{Vertical Extension:} Two parts of this design will help us achieve the correct vertical drop: the vertical beam extension and the wire hanging component. They allow the flyer to be lowered to the correct hanging height. The exact contribution of each will be decided at a later time once calculations and mechanical analyses are done to create a structurally sound mount.

\subsubsection{Option 2: C-Clamp with Straight Beam}

The second option is a C-Clamp on the outer edge of the staircase I-beam, a horizontal beam with a half-circle piece on one end and an eye bolt on the extended end, and a wire cable hanging mechanism similar to that in "Option 1: Wire Cable with Cable Gripper and Carabiner" for the ceiling. 

\begin{center}
\includegraphics[width=0.8\textwidth]{Staircase-Option-2.png}
\end{center}

\noindent\textbf{Material and Color:} Refer to Option 1.\\\\
\textbf{Easy Installation and Removal of Mounting Mechanism:} Refer to Option 1.\\\\
\textbf{Easy Attachment and Removal of Fractal Flyer:} Refer to Option 1.\\\\
\textbf{Adjustable:} The horizontal beam could be a few different lengths and the wire cable mechanism can be adjusted to its desired length.\\\\
\textbf{Horizontal Extension:} The horizontal beam allows the fractal flyer to extend horizontally from the mounting location.\\\\
\textbf{Vertical Extension:} The wire cable mechanism will drop down from the horizontal beam allowing the fractal flyer to extend vertically from the mounting location.


%------------------------------------------------

\subsection{Front Windows}

The third mounting location is the front windows. We will be hanging fractal flyers from the horizontal, steel I-beams that make up the front window structure. The current plan has 25 fractal flyers hanging from this location. 

The measurements and reference sketch of the horizontal, front window I-beams can be found below:\\

\begin{minipage}[c]{0.5\linewidth}
    \begin{tabular}{ll}
    \textbf{Flange Width:}  & $7$ in\\ \\
    \textbf{Flange Thickness:}  & $\frac{3}{4}$ in\\ \\
    \textbf{Web Thickness:}  & $\frac{1}{2}$ in\\ \\
    \textbf{Gap between Beam and Window:}  & $2\frac{7}{8}$ in\\ \\
\end{tabular}
\end{minipage} % no space if you would like to put them side by side
\begin{minipage}[c]{0.5\linewidth}
\includegraphics[scale=0.4]{Front-Window-Mounting-Location.png}
\end{minipage}

\subsubsection{Option 1: C-Clamp with Angled Beams}

The first option is a c-clamp on the web of the horizontal T-beams. The clamp is attached to a straight, vertical beam, a second beam angled up and away from the window (exact angle has yet to be decided), and a wire cable hanging mechanism similar to that in the first design option for ceiling. Current options being explored for connecting the two beams are welding, a fixed elbow joint that screws in, or an open 3-way elbow joint with a cap closing off the end to avoid slippage. Whether the beams are circular or square will depend on the chosen connection type. \\

\begin{center}
    \includegraphics[width=0.8\textwidth]{Front-Window-Option-1.png}
\end{center}

\noindent\textbf{Material and Color:} Grey metal pieces would create a mount that blends in and mirrors the industrial, exposed steel theme of the Packard staircase. \\\\
\textbf{Easy Installation and Removal of Mounting Mechanism:} Installing and removing this design is a two person job that would take around five minutes. It does not require any special tools other than scaffolding to reach the locations. This design does not require any permanent changes to the building components.\\\\
\textbf{Easy Attachment and Removal of Fractal Flyer:} Attaching and removing a fractal flyer to this design is a two person job that would take one minute. We expect one person would need to lift the fractal flyer from below to loosen the connection of the turnbuckles with the carabiner, after which the other person would unclip the fractal flyer and lower it down. It does not require any special tools other than scaffolding to reach the locations. \\\\
\textbf{Adjustable:} The angle between the two beams will be set and non-adjustable. This design supports a few different, interchangeable shaft lengths and an adjustable wire cable.\\\\
\textbf{Horizontal Extension:} The angle between and length of both beams will provide the desired and required horizontal extension away from the staircase.\\\\
\textbf{Vertical Extension:} Two parts of this design will help us achieve the correct vertical drop: the vertical beam extension and the wire hanging component. They allow the flyer to be lowered to the correct hanging height. The exact contribution of each will be decided at a later time once calculations and mechanical analyses are done to create a structurally sound mount.

\subsubsection{Option 2: C-Clamp with Horizontal Beam}

The second option is a c-clamp on the web of the horizontal T-beams. The clamp is attached to a horizontal beam with a half-circle piece on one end and an eye bolt on the extended end, and a wire cable hanging mechanism. This option avoids having to connect two beams at a desired angle with an elbow piece.

\begin{center}
    \includegraphics[width=0.8\textwidth]{Front-Window-Option-2.png}
\end{center}

\noindent\textbf{Material and Color:} Refer to Option 1.\\\\
\textbf{Easy Installation and Removal of Mounting Mechanism:} Refer to Option 1.\\\\
\textbf{Easy Attachment and Removal of Fractal Flyer:} Refer to Option 1.\\\\
\textbf{Adjustable:} The horizontal beam could be a few different lengths and the wire cable mechanism can be adjusted to its desired length.\\\\
\textbf{Horizontal Extension:} The horizontal beam allows the fractal flyer to extend horizontally from the mounting location.\\\\
\textbf{Vertical Extension:} The wire cable mechanism will drop down from the horizontal beam allowing the fractal flyer to extend vertically from the mounting location.

%----------------------------------------------------------------------------------------

\end{document}
