# Headboard V2

## Quick Reference

This is a headboard design for testing 3 Fractal Flyers. It takes in 49V
and 3 USB-B cables, and converts these to the Ethernet pin configuration
a Flyer expects. It has a single-pole, single-throw switch that can reset
a Flyer; by default R+ is 3.3V and R- is at 4.5V. If the button is pushed,
R- is brought low to 0.4V or so, triggering a reset.

## Datasheets

There are 4 components on the board: the USB-B connectors, the Ethernet
jacks, the buttons, and the bannana plugs for power, as well as some
resistors. Datasheets for the USB-B, Ethernet, and buttons are in the
datasheets directory.
