EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr B 17000 11000
encoding utf-8
Sheet 1 5
Title "FLIGHT Tail Board, EE285"
Date "2020-11-01"
Rev "5.1"
Comp "Stanford University"
Comment1 "Schematic for FLIGHT art installation tail board."
Comment2 "Tail board is on a Fractal Flyer, provides power and control."
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L tail-rescue:RJ45-Connector J1
U 1 1 5E3E104D
P 2150 5350
F 0 "J1" H 1950 5900 50  0000 C CNN
F 1 "RJ45" H 2500 4900 50  0000 C CNN
F 2 "light_footprints:RJ45_SS-650810S-A-NF" V 2150 5375 50  0001 C CNN
F 3 "~" V 2150 5375 50  0001 C CNN
F 4 "SS-650810S-A-NF" H 2150 5350 50  0001 C CNN "Manufacturer PN"
F 5 "Stewart Connector" H 2150 5350 50  0001 C CNN "Manufacturer"
F 6 "380-1028-ND" H 2150 5350 50  0001 C CNN "Digikey PN"
F 7 "CONN MOD JACK 8P8C VERT SHLD" H 2150 5350 50  0001 C CNN "Description"
	1    2150 5350
	1    0    0    -1  
$EndComp
$Comp
L tail-rescue:Polyfuse-Device F1
U 1 1 5E3EC0E1
P 4000 4950
F 0 "F1" V 3800 5100 50  0000 C CNN
F 1 "Polyfuse" V 3900 5200 50  0000 C CNN
F 2 "light_footprints:Fuse_Bourns_MF_RX300_72" H 4050 4750 50  0001 L CNN
F 3 "~" H 4000 4950 50  0001 C CNN
F 4 "Bourns Inc." V 4000 4950 50  0001 C CNN "Manufacturer"
F 5 "MF-RX300/72-2" V 4000 4950 50  0001 C CNN "Manufacturer PN"
F 6 "MF-RX300/72-2CT-ND" V 4000 4950 50  0001 C CNN "Digikey PN"
F 7 "PTC RESET FUSE 72V 3A RADIAL" V 4000 4950 50  0001 C CNN "Description"
	1    4000 4950
	0    1    1    0   
$EndComp
$Comp
L tail-rescue:L-Device L1
U 1 1 5E3E24AE
P 4800 4950
F 0 "L1" V 4990 4950 50  0000 C CNN
F 1 "1uH" V 4899 4950 50  0000 C CNN
F 2 "light_footprints:L_Bourns-SRN5040TA-1R0M" H 4800 4950 50  0001 C CNN
F 3 "~" H 4800 4950 50  0001 C CNN
F 4 "" V 4800 4950 50  0001 C CNN "Manufacfurer PN"
F 5 "Bourns Inc." V 4800 4950 50  0001 C CNN "Manufacturer"
F 6 "SRN5040TA-1R0MCT-ND" V 4800 4950 50  0001 C CNN "Digikey PN"
F 7 "FIXED IND 1UH 5A 12 MOHM SMD" V 4800 4950 50  0001 C CNN "Description"
F 8 "SRN5040TA-1R0M" H 4800 4950 50  0001 C CNN "Manufacturer PN"
	1    4800 4950
	0    -1   -1   0   
$EndComp
$Comp
L tail-rescue:TestPoint-Connector TP1
U 1 1 5E3F0E1A
P 5150 4800
F 0 "TP1" H 5208 4918 50  0000 L CNN
F 1 "TestPoint" H 5208 4827 50  0000 L CNN
F 2 "TestPoint:TestPoint_THTPad_D2.0mm_Drill1.0mm" H 5350 4800 50  0001 C CNN
F 3 "~" H 5350 4800 50  0001 C CNN
	1    5150 4800
	1    0    0    -1  
$EndComp
Text Notes 4600 5100 0    50   ~ 0
40kHz cutoff
$Comp
L tail-rescue:640456-3-dk_Rectangular-Connectors-Headers-Male-Pins J3
U 1 1 5E4888FB
P 11730 6285
F 0 "J3" V 11580 6285 50  0000 C CNN
F 1 "BODY" V 12080 6185 50  0000 C CNN
F 2 "light_footprints:1969803-1" H 11930 6485 60  0001 L CNN
F 3 "" H 11930 6585 60  0001 L CNN
F 4 "CONN HEADER R/A 3POS 6.35MM" H 11930 7285 60  0001 L CNN "Description"
F 5 "Adam Tech" H 11930 7385 60  0001 L CNN "Manufacturer"
F 6 "2057-DME-M-03-RA-E-ND" H 11730 6285 50  0001 C CNN "Digikey PN"
F 7 "DME-M-03-RA-E" H 11730 6285 50  0001 C CNN "Manufacturer PN"
	1    11730 6285
	0    -1   1    0   
$EndComp
$Comp
L tail-rescue:640456-3-dk_Rectangular-Connectors-Headers-Male-Pins J4
U 1 1 5E48C02F
P 10090 7775
F 0 "J4" V 9940 7775 50  0000 C CNN
F 1 "LED LEFT" V 10440 7525 50  0000 C CNN
F 2 "light_footprints:1969803-1" H 10290 7975 60  0001 L CNN
F 3 "" H 10290 8075 60  0001 L CNN
F 4 "CONN HEADER VERT 3POS 2.5MM" H 10290 8775 60  0001 L CNN "Description"
F 5 "JST Sales America Inc." H 10290 8875 60  0001 L CNN "Manufacturer"
F 6 "455-2248-ND" H 10090 7775 50  0001 C CNN "Digikey PN"
F 7 "B3B-XH-A(LF)(SN)" H 10090 7775 50  0001 C CNN "Manufacturer PN"
	1    10090 7775
	0    -1   1    0   
$EndComp
$Comp
L tail-rescue:TestPoint-Connector MH1
U 1 1 5E584D96
P 7440 9445
F 0 "MH1" V 7440 9633 50  0000 L CNN
F 1 "Mounting Hole" V 7485 9633 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 7640 9445 50  0001 C CNN
F 3 "~" H 7640 9445 50  0001 C CNN
	1    7440 9445
	0    1    1    0   
$EndComp
$Comp
L tail-rescue:TestPoint-Connector MH2
U 1 1 5E5865A5
P 7440 9645
F 0 "MH2" V 7440 9833 50  0000 L CNN
F 1 "Mounting Hole" V 7485 9833 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 7640 9645 50  0001 C CNN
F 3 "~" H 7640 9645 50  0001 C CNN
	1    7440 9645
	0    1    1    0   
$EndComp
$Comp
L tail-rescue:TestPoint-Connector MH3
U 1 1 5E586AF5
P 7440 9845
F 0 "MH3" V 7440 10033 50  0000 L CNN
F 1 "Mounting Hole" V 7485 10033 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 7640 9845 50  0001 C CNN
F 3 "~" H 7640 9845 50  0001 C CNN
	1    7440 9845
	0    1    1    0   
$EndComp
$Comp
L tail-rescue:TestPoint-Connector MH4
U 1 1 5E586ED6
P 7440 10045
F 0 "MH4" V 7440 10233 50  0000 L CNN
F 1 "Mounting Hole" V 7485 10233 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 7640 10045 50  0001 C CNN
F 3 "~" H 7640 10045 50  0001 C CNN
	1    7440 10045
	0    1    1    0   
$EndComp
$Comp
L tail-rescue:GND-power #PWR06
U 1 1 5E58E9EF
P 7290 10145
F 0 "#PWR06" H 7290 9895 50  0001 C CNN
F 1 "GND" H 7295 9972 50  0000 C CNN
F 2 "" H 7290 10145 50  0001 C CNN
F 3 "" H 7290 10145 50  0001 C CNN
	1    7290 10145
	1    0    0    -1  
$EndComp
Wire Wire Line
	7440 10045 7290 10045
Wire Wire Line
	7290 10045 7290 10145
Wire Wire Line
	7440 9845 7290 9845
Wire Wire Line
	7290 9845 7290 10045
Connection ~ 7290 10045
Wire Wire Line
	7440 9645 7290 9645
Wire Wire Line
	7290 9645 7290 9845
Connection ~ 7290 9845
Wire Wire Line
	7440 9445 7290 9445
Wire Wire Line
	7290 9445 7290 9645
Connection ~ 7290 9645
$Comp
L Connector_Generic:Conn_01x02 J6
U 1 1 5E58058B
P 7945 3555
F 0 "J6" H 7895 3655 50  0000 L CNN
F 1 "S2B-ZR(LF)(SN)" H 7845 3355 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 7945 3555 50  0001 C CNN
F 3 "~" H 7945 3555 50  0001 C CNN
F 4 "455-2257-ND" H 7945 3555 50  0001 C CNN "Digikey PN"
F 5 "S2B-XH-A(LF)(SN)" H 7945 3555 50  0001 C CNN "Manufacturer PN"
F 6 "JST Sales America inc." H 7945 3555 50  0001 C CNN "Manufacturer"
F 7 "CONN HEADER R/A 2POS 2.5MM" H 7945 3555 50  0001 C CNN "Description"
	1    7945 3555
	1    0    0    -1  
$EndComp
$Comp
L tail-rescue:D_TVS-Device D1
U 1 1 5E3EB77A
P 3650 5350
F 0 "D1" V 3604 5429 50  0000 L CNN
F 1 "54V" V 3695 5429 50  0000 L CNN
F 2 "light_footprints:SMCJ54CA" H 3650 5350 50  0001 C CNN
F 3 "~" H 3650 5350 50  0001 C CNN
F 4 "SMCJ54CA" V 3650 5350 50  0001 C CNN "Manufacturer PN"
F 5 "Littelfuse Inc." V 3650 5350 50  0001 C CNN "Manufacturer"
F 6 "SMCJ54CALFCT-ND" V 3650 5350 50  0001 C CNN "Digikey PN"
F 7 "TVS DIODE 54V 87.1V DO214AB" V 3650 5350 50  0001 C CNN "Description"
	1    3650 5350
	0    1    1    0   
$EndComp
Text GLabel 7450 4950 2    50   Output ~ 0
+8V
$Comp
L Mechanical:Fiducial FID1
U 1 1 5E707DC6
P 8340 9445
F 0 "FID1" H 8425 9491 50  0000 L CNN
F 1 "Fiducial" H 8425 9400 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 8340 9445 50  0001 C CNN
F 3 "~" H 8340 9445 50  0001 C CNN
	1    8340 9445
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID2
U 1 1 5E708863
P 8340 9795
F 0 "FID2" H 8425 9841 50  0000 L CNN
F 1 "Fiducial" H 8425 9750 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 8340 9795 50  0001 C CNN
F 3 "~" H 8340 9795 50  0001 C CNN
	1    8340 9795
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID3
U 1 1 5E70F549
P 8340 10145
F 0 "FID3" H 8425 10191 50  0000 L CNN
F 1 "Fiducial" H 8425 10100 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 8340 10145 50  0001 C CNN
F 3 "~" H 8340 10145 50  0001 C CNN
	1    8340 10145
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID4
U 1 1 5E7161A0
P 9190 9445
F 0 "FID4" H 9275 9491 50  0000 L CNN
F 1 "Fiducial" H 9275 9400 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 9190 9445 50  0001 C CNN
F 3 "~" H 9190 9445 50  0001 C CNN
	1    9190 9445
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID5
U 1 1 5E71CF17
P 9190 9795
F 0 "FID5" H 9275 9841 50  0000 L CNN
F 1 "Fiducial" H 9275 9750 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 9190 9795 50  0001 C CNN
F 3 "~" H 9190 9795 50  0001 C CNN
	1    9190 9795
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID6
U 1 1 5E723BEE
P 9190 10145
F 0 "FID6" H 9275 10191 50  0000 L CNN
F 1 "Fiducial" H 9275 10100 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 9190 10145 50  0001 C CNN
F 3 "~" H 9190 10145 50  0001 C CNN
	1    9190 10145
	1    0    0    -1  
$EndComp
NoConn ~ 10655 3960
$Comp
L tail-rescue:Conn_01x12_feather_right-Feather_connectors J9
U 1 1 617F5809
P 11805 3510
F 0 "J9" H 12005 4210 50  0000 L CNN
F 1 "Conn_01x12" H 11805 2710 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x12_P2.54mm_Vertical" H 11805 3510 50  0001 C CNN
F 3 "" H 11805 3510 50  0001 C CNN
F 4 "CONN RCPT 12POS 0.1 GOLD PCB" H 11805 3510 50  0001 C CNN "Description"
F 5 "Molex" H 11805 3510 50  0001 C CNN "Manufacturer"
F 6 "23-0022182121-ND" H 11805 3510 50  0001 C CNN "Digikey PN"
F 7 "0022182121" H 11805 3510 50  0001 C CNN "Manufacturer PN"
	1    11805 3510
	-1   0    0    -1  
$EndComp
Text Label 10055 4110 0    50   ~ 0
MISO
Wire Wire Line
	10405 4110 10055 4110
Text Label 10055 4010 0    50   ~ 0
MOSI
$Comp
L tail-rescue:GND-power #PWR?
U 1 1 61808E5F
P 10115 3210
AR Path="/5E41758F/61808E5F" Ref="#PWR?"  Part="1" 
AR Path="/61808E5F" Ref="#PWR019"  Part="1" 
F 0 "#PWR019" H 10115 2960 50  0001 C CNN
F 1 "GND" V 10120 3082 50  0000 R CNN
F 2 "" H 10115 3210 50  0001 C CNN
F 3 "" H 10115 3210 50  0001 C CNN
	1    10115 3210
	0    1    1    0   
$EndComp
Text Label 10055 3910 0    50   ~ 0
SCK
Wire Wire Line
	10405 4010 10055 4010
Wire Wire Line
	10405 3910 10055 3910
Text Label 12505 3810 2    50   ~ 0
PWM_M1_B
Text Label 12505 3710 2    50   ~ 0
PWM_M2_A
Text Label 12505 3610 2    50   ~ 0
PWM_M2_B
Text Label 12505 3310 2    50   ~ 0
LED1_SIG
Text Label 12505 3410 2    50   ~ 0
LED2_SIG
Text Label 12505 3510 2    50   ~ 0
LED3_SIG
Text Label 10105 3510 0    50   ~ 0
CS_A2
Text Label 12495 4110 2    50   ~ 0
SDA
Text Label 12505 4010 2    50   ~ 0
SCL
Wire Wire Line
	12005 4110 12495 4110
Wire Wire Line
	12005 4010 12505 4010
Wire Wire Line
	12005 3810 12505 3810
Wire Wire Line
	12005 3710 12505 3710
Wire Wire Line
	12005 3610 12505 3610
Wire Wire Line
	12005 3310 12505 3310
Wire Wire Line
	12005 3410 12505 3410
Wire Wire Line
	12005 3510 12505 3510
Wire Wire Line
	10405 3310 10105 3310
Wire Wire Line
	10105 3410 10405 3410
Wire Wire Line
	10405 3510 10105 3510
Wire Wire Line
	10405 3210 10115 3210
Text GLabel 10065 3010 0    50   Output ~ 0
+3V3
Wire Wire Line
	10405 3010 10065 3010
Wire Wire Line
	10405 2910 10105 2910
NoConn ~ 10405 3110
NoConn ~ 10405 3710
NoConn ~ 10405 3810
NoConn ~ 10405 4210
NoConn ~ 10405 4310
NoConn ~ 10405 4410
NoConn ~ 12005 3010
NoConn ~ 12005 3110
NoConn ~ 12005 3210
Wire Wire Line
	12005 3910 12505 3910
Text Label 12505 3910 2    50   ~ 0
PWM_M1_A
NoConn ~ 10105 3310
NoConn ~ 10105 3410
$Comp
L power:PWR_FLAG #FLG02
U 1 1 61B36E27
P 10305 9840
F 0 "#FLG02" H 10305 9915 50  0001 C CNN
F 1 "PWR_FLAG" H 10305 10013 50  0000 C CNN
F 2 "" H 10305 9840 50  0001 C CNN
F 3 "~" H 10305 9840 50  0001 C CNN
	1    10305 9840
	-1   0    0    1   
$EndComp
$Comp
L tail-rescue:GND-power #PWR05
U 1 1 61B39744
P 11250 9820
F 0 "#PWR05" H 11250 9570 50  0001 C CNN
F 1 "GND" H 11255 9647 50  0000 C CNN
F 2 "" H 11250 9820 50  0001 C CNN
F 3 "" H 11250 9820 50  0001 C CNN
	1    11250 9820
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG01
U 1 1 61B39B27
P 11250 9820
F 0 "#FLG01" H 11250 9895 50  0001 C CNN
F 1 "PWR_FLAG" H 11250 9993 50  0000 C CNN
F 2 "" H 11250 9820 50  0001 C CNN
F 3 "~" H 11250 9820 50  0001 C CNN
	1    11250 9820
	1    0    0    -1  
$EndComp
Text Label 10220 3610 2    50   ~ 0
Enable_SIG
Wire Wire Line
	10405 3610 10220 3610
Text Label 10105 2910 0    50   ~ 0
Reset
$Comp
L Connector_Generic:Conn_01x06 J12
U 1 1 61A44D07
P 4615 8285
F 0 "J12" H 4515 8585 50  0000 L CNN
F 1 "Conn_01x06" H 4415 7885 50  0000 L CNN
F 2 "light_footprints:B6B-XH-ALFSN" H 4615 8285 50  0001 C CNN
F 3 "~" H 4615 8285 50  0001 C CNN
F 4 "B6B-XH-A(LF)(SN)" H 4615 8285 50  0001 C CNN "Manufacturer PN"
F 5 "JST Sales America Inc." H 4615 8285 50  0001 C CNN "Manufacturer"
F 6 "455-2271-ND" H 4615 8285 50  0001 C CNN "Digikey PN"
F 7 "CONN HEADER VERT 6POS 2.5MM" H 4615 8285 50  0001 C CNN "Description"
	1    4615 8285
	1    0    0    -1  
$EndComp
Text Notes 4705 8620 0    50   ~ 0
Wing
Text GLabel 4365 8085 0    50   Input ~ 0
+3V3
$Comp
L power:GND #PWR0101
U 1 1 61A53A08
P 4115 8585
F 0 "#PWR0101" H 4115 8335 50  0001 C CNN
F 1 "GND" V 4120 8457 50  0000 R CNN
F 2 "" H 4115 8585 50  0001 C CNN
F 3 "" H 4115 8585 50  0001 C CNN
	1    4115 8585
	1    0    0    -1  
$EndComp
Text Label 4365 8385 2    50   ~ 0
SCL
Text Label 4365 8485 2    50   ~ 0
SDA
$Comp
L Connector_Generic:Conn_01x06 J10
U 1 1 61A90EEE
P 4590 3380
F 0 "J10" H 4440 3730 50  0000 L CNN
F 1 "Conn_01x06" H 4390 2930 50  0000 L CNN
F 2 "light_footprints:B6B-XH-ALFSN" H 4590 3380 50  0001 C CNN
F 3 "~" H 4590 3380 50  0001 C CNN
F 4 "B6B-XH-A(LF)(SN)" H 4590 3380 50  0001 C CNN "Manufacturer PN"
F 5 "JST Sales America Inc." H 4590 3380 50  0001 C CNN "Manufacturer"
F 6 "455-2271-ND" H 4590 3380 50  0001 C CNN "Digikey PN"
F 7 "CONN HEADER VERT 6POS 2.5MM" H 4590 3380 50  0001 C CNN "Description"
	1    4590 3380
	1    0    0    -1  
$EndComp
Text Notes 4740 3730 0    50   ~ 0
Wing
Text GLabel 4240 3180 0    50   Input ~ 0
+3V3
Text Label 4290 3580 2    50   ~ 0
SDA
$Comp
L Connector_Generic:Conn_01x06 J11
U 1 1 61AA4595
P 11345 5550
F 0 "J11" H 11425 5542 50  0000 L CNN
F 1 "Conn_01x06" H 11425 5451 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x06_P2.54mm_Vertical" H 11345 5550 50  0001 C CNN
F 3 "~" H 11345 5550 50  0001 C CNN
F 4 "0022182061" H 11345 5550 50  0001 C CNN "Manufacturer PN"
F 5 "Molex" H 11345 5550 50  0001 C CNN "Manufacturer"
F 6 "23-0022182061-ND" H 11345 5550 50  0001 C CNN "Digikey PN"
F 7 "CONN RCPT 6POS 0.1 GOLD PCB" H 11345 5550 50  0001 C CNN "Description"
	1    11345 5550
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J13
U 1 1 61AAAF72
P 9880 5560
F 0 "J13" H 9960 5552 50  0000 L CNN
F 1 "Conn_01x02" H 9960 5461 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 9880 5560 50  0001 C CNN
F 3 "~" H 9880 5560 50  0001 C CNN
F 4 "0022182021" H 9880 5560 50  0001 C CNN "Manufacturer PN"
F 5 "Molex" H 9880 5560 50  0001 C CNN "Manufacturer"
F 6 "WM3233-ND" H 9880 5560 50  0001 C CNN "Digikey PN"
F 7 "CONN RCPT 2POS 0.1 GOLD PCB" H 9880 5560 50  0001 C CNN "Description"
	1    9880 5560
	1    0    0    -1  
$EndComp
Text Notes 11450 5840 0    50   ~ 0
Body Accelerometer 6 pin
Text Notes 9565 5805 0    50   ~ 0
Body Accelerometer 2 pin
Wire Wire Line
	9680 5560 9380 5560
Text Label 9380 5560 0    50   ~ 0
CS_A2
Wire Wire Line
	9680 5660 9380 5660
Text Label 9380 5660 0    50   ~ 0
MISO
Wire Wire Line
	10845 5350 11145 5350
Wire Wire Line
	11145 5650 10845 5650
Text GLabel 10845 5350 0    50   Input ~ 0
+3V3
$Comp
L power:GND #PWR0103
U 1 1 61AEC1A3
P 10795 5550
F 0 "#PWR0103" H 10795 5300 50  0001 C CNN
F 1 "GND" V 10800 5422 50  0000 R CNN
F 2 "" H 10795 5550 50  0001 C CNN
F 3 "" H 10795 5550 50  0001 C CNN
	1    10795 5550
	0    1    1    0   
$EndComp
Text Label 10845 5650 0    50   ~ 0
SCK
Wire Wire Line
	10795 5550 11145 5550
Wire Wire Line
	11145 5750 10845 5750
Text Label 10845 5750 0    50   ~ 0
MOSI
Text Label 4290 3480 2    50   ~ 0
SCL
$Comp
L power:GND #PWR0102
U 1 1 61A9133A
P 4040 3730
F 0 "#PWR0102" H 4040 3480 50  0001 C CNN
F 1 "GND" V 4045 3602 50  0000 R CNN
F 2 "" H 4040 3730 50  0001 C CNN
F 3 "" H 4040 3730 50  0001 C CNN
	1    4040 3730
	1    0    0    -1  
$EndComp
NoConn ~ 11145 5450
NoConn ~ 11145 5850
$Comp
L tail-rescue:GND-power #PWR0105
U 1 1 61A770DF
P 2650 6000
F 0 "#PWR0105" H 2650 5750 50  0001 C CNN
F 1 "GND" H 2655 5827 50  0000 C CNN
F 2 "" H 2650 6000 50  0001 C CNN
F 3 "" H 2650 6000 50  0001 C CNN
	1    2650 6000
	1    0    0    -1  
$EndComp
Text Label 2900 5150 0    50   ~ 0
R-
Text Label 2800 5450 0    50   ~ 0
R+
Text Label 2800 5550 0    50   ~ 0
D-
Text Label 2800 5650 0    50   ~ 0
D+
Wire Wire Line
	2650 6000 2650 5350
Wire Wire Line
	2650 5250 2550 5250
Wire Wire Line
	2650 5350 2550 5350
Connection ~ 2650 5350
Wire Wire Line
	2650 5350 2650 5250
$Comp
L tail-rescue:GND-power #PWR0106
U 1 1 61B02AFD
P 2350 6000
F 0 "#PWR0106" H 2350 5750 50  0001 C CNN
F 1 "GND" H 2355 5827 50  0000 C CNN
F 2 "" H 2350 6000 50  0001 C CNN
F 3 "" H 2350 6000 50  0001 C CNN
	1    2350 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 5850 2350 6000
Wire Wire Line
	2550 4950 2650 4950
Wire Wire Line
	2550 5050 2650 5050
Wire Wire Line
	2650 5050 2650 4950
Connection ~ 2650 4950
$Comp
L tail-rescue:GND-power #PWR0107
U 1 1 61B5642D
P 3650 5900
F 0 "#PWR0107" H 3650 5650 50  0001 C CNN
F 1 "GND" H 3655 5727 50  0000 C CNN
F 2 "" H 3650 5900 50  0001 C CNN
F 3 "" H 3650 5900 50  0001 C CNN
	1    3650 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 5500 3650 5900
Wire Wire Line
	5150 4800 5150 4950
Connection ~ 5150 4950
Wire Wire Line
	3650 5200 3650 4950
Connection ~ 3650 4950
Wire Wire Line
	3650 4950 3850 4950
Text GLabel 9730 6285 0    50   Input ~ 0
+8V
Wire Wire Line
	10030 6285 9730 6285
$Comp
L tail-rescue:GND-power #PWR0120
U 1 1 61CF969E
P 9830 6585
F 0 "#PWR0120" H 9830 6335 50  0001 C CNN
F 1 "GND" H 9835 6412 50  0000 C CNN
F 2 "" H 9830 6585 50  0001 C CNN
F 3 "" H 9830 6585 50  0001 C CNN
	1    9830 6585
	1    0    0    -1  
$EndComp
Wire Wire Line
	10030 6535 9830 6535
Wire Wire Line
	9830 6535 9830 6585
Text Notes 10380 6635 0    50   ~ 0
BODY\nLED\nPOWER
Text Label 11530 6385 2    50   ~ 0
LED2_SIG
Wire Wire Line
	11530 6385 11630 6385
$Comp
L tail-rescue:GND-power #PWR0121
U 1 1 61D551AE
P 11580 6585
F 0 "#PWR0121" H 11580 6335 50  0001 C CNN
F 1 "GND" H 11585 6412 50  0000 C CNN
F 2 "" H 11580 6585 50  0001 C CNN
F 3 "" H 11580 6585 50  0001 C CNN
	1    11580 6585
	1    0    0    -1  
$EndComp
Wire Wire Line
	11630 6485 11580 6485
Wire Wire Line
	11580 6485 11580 6585
Wire Wire Line
	10780 6285 11080 6285
Wire Wire Line
	11130 6185 11080 6185
Wire Wire Line
	11080 6185 11080 6285
Connection ~ 11080 6285
Wire Wire Line
	11080 6285 11630 6285
Text GLabel 8290 7825 0    50   Input ~ 0
+8V
Wire Wire Line
	8290 7825 8540 7825
$Comp
L tail-rescue:GND-power #PWR0122
U 1 1 61E0B8E2
P 8340 8175
F 0 "#PWR0122" H 8340 7925 50  0001 C CNN
F 1 "GND" H 8345 8002 50  0000 C CNN
F 2 "" H 8340 8175 50  0001 C CNN
F 3 "" H 8340 8175 50  0001 C CNN
	1    8340 8175
	1    0    0    -1  
$EndComp
Wire Wire Line
	8340 8175 8340 8075
Wire Wire Line
	8340 8075 8540 8075
Wire Wire Line
	9590 7525 9490 7525
Text Label 9890 7875 2    50   ~ 0
LED3_SIG
Wire Wire Line
	9890 7875 9990 7875
$Comp
L tail-rescue:GND-power #PWR0123
U 1 1 61E57F39
P 9940 8125
F 0 "#PWR0123" H 9940 7875 50  0001 C CNN
F 1 "GND" H 9945 7952 50  0000 C CNN
F 2 "" H 9940 8125 50  0001 C CNN
F 3 "" H 9940 8125 50  0001 C CNN
	1    9940 8125
	1    0    0    -1  
$EndComp
Text Notes 10240 8275 0    50   ~ 0
WING
Wire Wire Line
	9990 7975 9940 7975
Wire Wire Line
	9940 7975 9940 8125
Text Notes 8890 8225 0    50   ~ 0
LEFT\nWING\nLED\nPOWER
Wire Wire Line
	2550 5550 3150 5550
Wire Wire Line
	2550 5450 3050 5450
Wire Wire Line
	2550 5150 2950 5150
Wire Wire Line
	2550 5650 3250 5650
Text Label 3450 7300 2    50   ~ 0
Reset
Wire Wire Line
	3650 7300 3200 7300
Text Label 3450 7200 2    50   ~ 0
R-
Text Label 3450 7100 2    50   ~ 0
R+
Text Label 3450 7000 2    50   ~ 0
D-
Text Label 3450 6900 2    50   ~ 0
D+
Wire Wire Line
	2950 7200 2950 5150
Wire Wire Line
	3650 7200 2950 7200
Wire Wire Line
	3050 7100 3050 5450
Wire Wire Line
	3650 7100 3050 7100
Wire Wire Line
	3150 7000 3150 5550
Wire Wire Line
	3650 7000 3150 7000
Wire Wire Line
	3250 6900 3250 5650
Wire Wire Line
	3650 6900 3250 6900
Wire Wire Line
	3650 6700 3550 6700
Wire Wire Line
	3550 6800 3550 7400
Wire Wire Line
	3650 6800 3550 6800
Text GLabel 3550 6700 0    50   Input ~ 0
+8V
Text Notes 3950 7100 0    50   ~ 0
  TO\nSIGNALING\n  PCB
$Comp
L tail-rescue:GND-power #PWR?
U 1 1 618B48D9
P 3550 7400
AR Path="/5E41758F/618B48D9" Ref="#PWR?"  Part="1" 
AR Path="/618B48D9" Ref="#PWR020"  Part="1" 
F 0 "#PWR020" H 3550 7150 50  0001 C CNN
F 1 "GND" V 3555 7272 50  0000 R CNN
F 2 "" H 3550 7400 50  0001 C CNN
F 3 "" H 3550 7400 50  0001 C CNN
	1    3550 7400
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x07 J8
U 1 1 61887656
P 3850 7000
F 0 "J8" H 3750 7400 50  0000 L CNN
F 1 "7 Posn. Socket" H 3800 6550 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x07_P2.54mm_Vertical" H 3850 7000 50  0001 C CNN
F 3 "~" H 3850 7000 50  0001 C CNN
F 4 "Headers & Wire Housings TOP ENTRY PCB 7P " H 3850 7000 50  0001 C CNN "Description"
F 5 "Molex " H 3850 7000 50  0001 C CNN "Manufacturer"
F 6 "538-22-18-2071 " H 3850 7000 50  0001 C CNN "Mouser PN"
F 7 "22-18-2071" H 3850 7000 50  0001 C CNN "Manufacturer PN"
	1    3850 7000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 4950 5150 4950
Wire Wire Line
	5150 5750 5150 5900
$Comp
L tail-rescue:GND-power #PWR0110
U 1 1 61BF2CEF
P 5150 5900
F 0 "#PWR0110" H 5150 5650 50  0001 C CNN
F 1 "GND" H 5155 5727 50  0000 C CNN
F 2 "" H 5150 5900 50  0001 C CNN
F 3 "" H 5150 5900 50  0001 C CNN
	1    5150 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 5100 5150 4950
Wire Wire Line
	5150 5400 5150 5450
$Comp
L tail-rescue:CP-Device C2
U 1 1 5E3EF96A
P 5150 5600
F 0 "C2" H 5268 5646 50  0000 L CNN
F 1 "10uF" H 5268 5555 50  0000 L CNN
F 2 "light_footprints:CP_Elec_8x6.2_panasonic" H 5188 5450 50  0001 C CNN
F 3 "~" H 5150 5600 50  0001 C CNN
F 4 "EEE-2AA100UP" H 5150 5600 50  0001 C CNN "Manufacturer PN"
F 5 "Panasonic Electronic Components" H 5150 5600 50  0001 C CNN "Manufacturer"
F 6 "PCE3966CT-ND" H 5150 5600 50  0001 C CNN "Digikey PN"
F 7 "CAP ALUM 10UF 20% 100V SMD" H 5150 5600 50  0001 C CNN "Description"
	1    5150 5600
	1    0    0    -1  
$EndComp
$Comp
L tail-rescue:R-Device R1
U 1 1 5E3E47BD
P 5150 5250
F 0 "R1" H 5220 5296 50  0000 L CNN
F 1 "0.3" H 5220 5205 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5080 5250 50  0001 C CNN
F 3 "~" H 5150 5250 50  0001 C CNN
F 4 "ERJ-3BQFR30V" H 5150 5250 50  0001 C CNN "Manufacturer PN"
F 5 "Panasonic Electronic Components" H 5150 5250 50  0001 C CNN "Manufacturer"
F 6 "P17395CT-ND" H 5150 5250 50  0001 C CNN "Digikey PN"
F 7 "RES 0.3 OHM 1% 1/4W 0603" H 5150 5250 50  0001 C CNN "Description"
	1    5150 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 5450 4500 5900
Wire Wire Line
	4500 5150 4500 4950
$Comp
L tail-rescue:GND-power #PWR0108
U 1 1 61B92362
P 4500 5900
F 0 "#PWR0108" H 4500 5650 50  0001 C CNN
F 1 "GND" H 4505 5727 50  0000 C CNN
F 2 "" H 4500 5900 50  0001 C CNN
F 3 "" H 4500 5900 50  0001 C CNN
	1    4500 5900
	1    0    0    -1  
$EndComp
$Comp
L tail-rescue:CP-Device C1
U 1 1 5E3E09E5
P 4500 5300
F 0 "C1" H 4618 5346 50  0000 L CNN
F 1 "10uF" H 4618 5255 50  0000 L CNN
F 2 "light_footprints:CP_Elec_8x6.2_panasonic" H 4538 5150 50  0001 C CNN
F 3 "~" H 4500 5300 50  0001 C CNN
F 4 "EEE-2AA100UP" H 4500 5300 50  0001 C CNN "Manufacturer PN"
F 5 "Panasonic Electronic Components" H 4500 5300 50  0001 C CNN "Manufacturer"
F 6 "PCE3966CT-ND" H 4500 5300 50  0001 C CNN "Digikey PN"
F 7 "CAP ALUM 10UF 20% 100V SMD" H 4500 5300 50  0001 C CNN "Description"
	1    4500 5300
	1    0    0    -1  
$EndComp
Connection ~ 4500 4950
Wire Wire Line
	4500 4950 4650 4950
Wire Wire Line
	4150 4950 4500 4950
Wire Wire Line
	7250 5450 7650 5450
Text Label 7650 5450 2    50   ~ 0
PGOOD
Wire Wire Line
	7250 4950 7450 4950
$Comp
L tail-rescue:GND-power #PWR0124
U 1 1 620BD6E6
P 6050 5600
F 0 "#PWR0124" H 6050 5350 50  0001 C CNN
F 1 "GND" H 6055 5427 50  0000 C CNN
F 2 "" H 6050 5600 50  0001 C CNN
F 3 "" H 6050 5600 50  0001 C CNN
	1    6050 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 5450 6050 5450
Wire Wire Line
	6050 5450 6050 5600
Wire Wire Line
	5150 4950 6250 4950
Text Label 5750 5200 0    50   ~ 0
Enable_SIG
Wire Wire Line
	5750 5200 6250 5200
$Comp
L Device:C C?
U 1 1 62116FF7
P 6245 2905
AR Path="/5E4A2CB9/62116FF7" Ref="C?"  Part="1" 
AR Path="/62116FF7" Ref="C36"  Part="1" 
F 0 "C36" H 6360 2951 50  0000 L CNN
F 1 "0.1uF" H 6360 2860 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6283 2755 50  0001 C CNN
F 3 "~" H 6245 2905 50  0001 C CNN
F 4 "GMC10X7R104M16NT" H 6245 2905 50  0001 C CNN "Manufacturer PN"
F 5 "CAL-CHIP ELECTRONICS, INC." H 6245 2905 50  0001 C CNN "Manufacturer"
F 6 "2571-GMC10X7R104M16NTTR-ND" H 6245 2905 50  0001 C CNN "Digikey PN"
F 7 "CAP CER 0.1UF 16V X7R 0603" H 6245 2905 50  0001 C CNN "Description"
	1    6245 2905
	1    0    0    -1  
$EndComp
$Comp
L tail-rescue:DRV8210_DRL U?
U 1 1 6211700D
P 6995 3605
AR Path="/5E4A2CB9/6211700D" Ref="U?"  Part="1" 
AR Path="/6211700D" Ref="U5"  Part="1" 
F 0 "U5" H 6795 4005 50  0000 C CNN
F 1 "DRV8220_DRL" H 6895 3155 50  0000 C CNN
F 2 "motor12V:PDRV8220DRLR" H 6995 3605 50  0001 C CNN
F 3 "" H 6995 3605 50  0001 C CNN
F 4 "DRV8220DRLR" H 6995 3605 50  0001 C CNN "Manufacturer PN"
F 5 "Texas Instruments" H 6995 3605 50  0001 C CNN "Manufacturer"
F 6 "296-DRV8220DRLRTR-ND" H 6995 3605 50  0001 C CNN "Digikey PN"
F 7 "20-V, 1-A H-bridge motor driver" H 6995 3605 50  0001 C CNN "Description"
	1    6995 3605
	1    0    0    -1  
$EndComp
Text GLabel 6745 2755 2    50   Input ~ 0
+8V
Text Label 6345 3505 2    50   ~ 0
PWM_M2_A
Wire Wire Line
	6345 3505 6495 3505
Text Label 6345 3705 2    50   ~ 0
PWM_M2_B
Wire Wire Line
	6345 3705 6495 3705
$Comp
L tail-rescue:GND-power #PWR0125
U 1 1 62169C75
P 7345 4055
F 0 "#PWR0125" H 7345 3805 50  0001 C CNN
F 1 "GND" H 7350 3882 50  0000 C CNN
F 2 "" H 7345 4055 50  0001 C CNN
F 3 "" H 7345 4055 50  0001 C CNN
	1    7345 4055
	1    0    0    -1  
$EndComp
Text Label 7545 3505 0    50   ~ 0
M2A
Text Label 7695 3705 2    50   ~ 0
M2B
$Comp
L tail-rescue:GND-power #PWR0126
U 1 1 621888E8
P 6245 3055
F 0 "#PWR0126" H 6245 2805 50  0001 C CNN
F 1 "GND" H 6250 2882 50  0000 C CNN
F 2 "" H 6245 3055 50  0001 C CNN
F 3 "" H 6245 3055 50  0001 C CNN
	1    6245 3055
	1    0    0    -1  
$EndComp
Wire Wire Line
	6245 2755 6645 2755
Wire Wire Line
	6645 2755 6645 3205
Connection ~ 6645 2755
Wire Wire Line
	6645 2755 6745 2755
Wire Wire Line
	7745 3705 7745 3655
Wire Wire Line
	7495 3705 7745 3705
Wire Wire Line
	7745 3505 7745 3555
Wire Wire Line
	7495 3505 7745 3505
Wire Wire Line
	5640 7060 5740 7060
Connection ~ 5640 7060
Wire Wire Line
	5640 7510 5640 7060
Wire Wire Line
	5290 7060 5640 7060
Text GLabel 5740 7060 2    50   Input ~ 0
+8V
$Comp
L tail-rescue:GND-power #PWR0127
U 1 1 622725C8
P 5290 7360
F 0 "#PWR0127" H 5290 7110 50  0001 C CNN
F 1 "GND" H 5295 7187 50  0000 C CNN
F 2 "" H 5290 7360 50  0001 C CNN
F 3 "" H 5290 7360 50  0001 C CNN
	1    5290 7360
	1    0    0    -1  
$EndComp
Wire Wire Line
	6740 8010 6740 7960
Wire Wire Line
	6490 8010 6740 8010
Wire Wire Line
	6740 7810 6740 7860
Wire Wire Line
	6490 7810 6740 7810
Wire Wire Line
	5340 8010 5490 8010
Text Label 5340 8010 2    50   ~ 0
PWM_M1_B
Wire Wire Line
	5340 7810 5490 7810
Text Label 5340 7810 2    50   ~ 0
PWM_M1_A
$Comp
L tail-rescue:GND-power #PWR0128
U 1 1 622117CC
P 6340 8360
F 0 "#PWR0128" H 6340 8110 50  0001 C CNN
F 1 "GND" H 6345 8187 50  0000 C CNN
F 2 "" H 6340 8360 50  0001 C CNN
F 3 "" H 6340 8360 50  0001 C CNN
	1    6340 8360
	1    0    0    -1  
$EndComp
$Comp
L tail-rescue:DRV8210_DRL U?
U 1 1 6220262F
P 5990 7910
AR Path="/5E4A2CB9/6220262F" Ref="U?"  Part="1" 
AR Path="/6220262F" Ref="U6"  Part="1" 
F 0 "U6" H 5840 8310 50  0000 C CNN
F 1 "DRV8220_DRL" H 5890 7460 50  0000 C CNN
F 2 "motor12V:PDRV8220DRLR" H 5990 7910 50  0001 C CNN
F 3 "" H 5990 7910 50  0001 C CNN
	1    5990 7910
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 6220261E
P 5290 7210
AR Path="/5E4A2CB9/6220261E" Ref="C?"  Part="1" 
AR Path="/6220261E" Ref="C37"  Part="1" 
F 0 "C37" H 5405 7256 50  0000 L CNN
F 1 "0.1uF" H 5405 7165 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5328 7060 50  0001 C CNN
F 3 "~" H 5290 7210 50  0001 C CNN
F 4 "GMC10X7R104M16NT" H 5290 7210 50  0001 C CNN "Manufacturer PN"
F 5 "CAL-CHIP ELECTRONICS, INC." H 5290 7210 50  0001 C CNN "Manufacturer"
F 6 "2571-GMC10X7R104M16NTTR-ND" H 5290 7210 50  0001 C CNN "Digikey PN"
F 7 "CAP CER 0.1UF 16V X7R 0603" H 5290 7210 50  0001 C CNN "Description"
	1    5290 7210
	1    0    0    -1  
$EndComp
Text Label 6540 8010 0    50   ~ 0
M1B
Text Label 6540 7810 0    50   ~ 0
M1A
$Comp
L Connector_Generic:Conn_01x02 J5
U 1 1 5E6566C9
P 6940 7860
F 0 "J5" H 6890 7960 50  0000 L CNN
F 1 "S2B-ZR(LF)(SN)" H 6840 7660 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 6940 7860 50  0001 C CNN
F 3 "~" H 6940 7860 50  0001 C CNN
F 4 "455-2257-ND" H 6940 7860 50  0001 C CNN "Digikey PN"
F 5 "S2B-XH-A(LF)(SN)" H 6940 7860 50  0001 C CNN "Manufacturer PN"
F 6 "JST Sales America inc." H 6940 7860 50  0001 C CNN "Manufacturer"
F 7 "CONN HEADER R/A 2POS 2.5MM" H 6940 7860 50  0001 C CNN "Description"
	1    6940 7860
	1    0    0    -1  
$EndComp
Wire Wire Line
	9290 7775 9490 7775
Wire Wire Line
	9490 7525 9490 7775
Connection ~ 9490 7775
Wire Wire Line
	9490 7775 9990 7775
Wire Wire Line
	4415 8385 4365 8385
Wire Wire Line
	4415 8485 4365 8485
NoConn ~ 4415 8585
NoConn ~ 4415 8185
Wire Wire Line
	4365 8085 4415 8085
Wire Wire Line
	4415 8285 4115 8285
Wire Wire Line
	4115 8285 4115 8585
Wire Wire Line
	4390 3580 4290 3580
Wire Wire Line
	4390 3480 4290 3480
Wire Wire Line
	4390 3380 4040 3380
Wire Wire Line
	4040 3380 4040 3730
NoConn ~ 4390 3680
NoConn ~ 4390 3280
Wire Wire Line
	4240 3180 4390 3180
Text Notes 6650 5350 0    50   ~ 0
+48VDC\n   to\n+8VDC\nREGULATOR
Wire Wire Line
	2650 4950 3650 4950
Text GLabel 11130 6185 2    50   Input ~ 0
+5V_LED_2
Text GLabel 9590 7525 2    50   Input ~ 0
+5V_LED_3
$Sheet
S 10030 6135 750  650 
U 5E4888F1
F0 "led_power_2" 50
F1 "AOZ2255LQI-31_2.sch" 50
F2 "8Vin" U L 10030 6285 50 
F3 "5Vout2" U R 10780 6285 50 
F4 "GND" U L 10030 6535 50 
$EndSheet
$Sheet
S 8540 7675 750  650 
U 5E48C025
F0 "led_power_3" 50
F1 "AOZ2255LQI-31_3.sch" 50
F2 "8Vin" U L 8540 7825 50 
F3 "5Vout3" U R 9290 7775 50 
F4 "GND" U L 8540 8075 50 
$EndSheet
$Comp
L power:+48V #PWR0136
U 1 1 61BAACC4
P 3650 4850
F 0 "#PWR0136" H 3650 4700 50  0001 C CNN
F 1 "+48V" H 3665 5023 50  0000 C CNN
F 2 "" H 3650 4850 50  0001 C CNN
F 3 "" H 3650 4850 50  0001 C CNN
	1    3650 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 4850 3650 4950
$Comp
L power:+48V #PWR01
U 1 1 61C0A531
P 10305 9740
F 0 "#PWR01" H 10305 9590 50  0001 C CNN
F 1 "+48V" H 10320 9913 50  0000 C CNN
F 2 "" H 10305 9740 50  0001 C CNN
F 3 "" H 10305 9740 50  0001 C CNN
	1    10305 9740
	1    0    0    -1  
$EndComp
Wire Wire Line
	10305 9740 10305 9840
$Comp
L power:+8V #PWR02
U 1 1 61C13840
P 10755 9740
F 0 "#PWR02" H 10755 9590 50  0001 C CNN
F 1 "+8V" H 10770 9913 50  0000 C CNN
F 2 "" H 10755 9740 50  0001 C CNN
F 3 "" H 10755 9740 50  0001 C CNN
	1    10755 9740
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG05
U 1 1 61C1480B
P 10755 9840
F 0 "#FLG05" H 10755 9915 50  0001 C CNN
F 1 "PWR_FLAG" H 10755 10013 50  0000 C CNN
F 2 "" H 10755 9840 50  0001 C CNN
F 3 "~" H 10755 9840 50  0001 C CNN
	1    10755 9840
	-1   0    0    1   
$EndComp
Wire Wire Line
	10755 9740 10755 9840
$Sheet
S 6250 4800 1000 900 
U 5E4325FD
F0 "input_power" 50
F1 "input_power v2.sch" 50
F2 "48Vin" U L 6250 4950 50 
F3 "8Vout" U R 7250 4950 50 
F4 "GND" U L 6250 5450 50 
F5 "5VPGOODout" O R 7250 5450 50 
F6 "MCU_EN" I L 6250 5200 50 
$EndSheet
$Sheet
S 8090 4465 750  650 
U 5E46B36B
F0 "led_power_1" 50
F1 "AOZ2255LQI-31_1.sch" 50
F2 "8Vin" U L 8090 4565 50 
F3 "5Vout1" U R 8840 4565 50 
F4 "GND" U L 8090 4865 50 
$EndSheet
Text Notes 8440 5015 0    50   ~ 0
RIGHT\nWING\nLED\nPOWER
Connection ~ 9140 4565
Wire Wire Line
	8840 4565 9140 4565
Text Notes 9790 5065 0    50   ~ 0
WING
Wire Wire Line
	7890 4865 8090 4865
$Comp
L tail-rescue:GND-power #PWR0104
U 1 1 619D4FFF
P 7890 4865
F 0 "#PWR0104" H 7890 4615 50  0001 C CNN
F 1 "GND" H 7895 4692 50  0000 C CNN
F 2 "" H 7890 4865 50  0001 C CNN
F 3 "" H 7890 4865 50  0001 C CNN
	1    7890 4865
	1    0    0    -1  
$EndComp
Wire Wire Line
	7840 4565 8090 4565
Text GLabel 7840 4565 0    50   Input ~ 0
+8V
Wire Wire Line
	9490 4665 9540 4665
Text Label 9490 4665 2    50   ~ 0
LED1_SIG
Wire Wire Line
	9140 4565 9540 4565
Wire Wire Line
	9140 4265 9140 4565
Wire Wire Line
	9190 4265 9140 4265
Text GLabel 9190 4265 2    50   Input ~ 0
+5V_LED_1
Wire Wire Line
	9490 4765 9490 4865
Wire Wire Line
	9540 4765 9490 4765
$Comp
L tail-rescue:Conn_01x16_feather_left-Feather_connectors J7
U 1 1 617F1E00
P 10605 3710
F 0 "J7" H 10755 4610 50  0000 L CNN
F 1 "Conn_01x16" H 10605 2810 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x16_P2.54mm_Vertical" H 10605 3710 50  0001 C CNN
F 3 "" H 10605 3710 50  0001 C CNN
F 4 "CONN RCPT 4POS 0.1 GOLD PCB" H 10605 3710 50  0001 C CNN "Description"
F 5 "Molex" H 10605 3710 50  0001 C CNN "Manufacturer"
F 6 "23-0022182041-ND" H 10605 3710 50  0001 C CNN "Digikey PN"
F 7 "0022182041" H 10605 3710 50  0001 C CNN "Manufacturer PN"
	1    10605 3710
	1    0    0    -1  
$EndComp
$Comp
L tail-rescue:GND-power #PWR012
U 1 1 5E62732A
P 9490 4865
F 0 "#PWR012" H 9490 4615 50  0001 C CNN
F 1 "GND" H 9495 4692 50  0000 C CNN
F 2 "" H 9490 4865 50  0001 C CNN
F 3 "" H 9490 4865 50  0001 C CNN
	1    9490 4865
	1    0    0    -1  
$EndComp
$Comp
L tail-rescue:640456-3-dk_Rectangular-Connectors-Headers-Male-Pins J2
U 1 1 5E610559
P 9640 4565
F 0 "J2" V 9490 4565 50  0000 C CNN
F 1 "LED RIGHT" V 9990 4315 50  0000 C CNN
F 2 "light_footprints:1969803-1" H 9840 4765 60  0001 L CNN
F 3 "" H 9840 4865 60  0001 L CNN
F 4 "CONN HEADER VERT 3POS 6.35MM" H 9840 5565 60  0001 L CNN "Description"
F 5 "TE Connectivity AMP" H 9840 5665 60  0001 L CNN "Manufacturer"
F 6 "1969803-1-ND" H 9640 4565 50  0001 C CNN "Digikey PN"
F 7 "1969803-1" H 9640 4565 50  0001 C CNN "Manufacturer PN"
	1    9640 4565
	0    -1   1    0   
$EndComp
$EndSCHEMATC
