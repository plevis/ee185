"""
This is the testing framework written by the Fall 2024 team

We have written this in addition to the Arduino testing framework because:
1) the feathers come with circuitpython already installed which is simple 2)
we do not need all of the advanced lighting features as the arduino testing framework
3) we only seek to test the correct voltage levels/pinouts/gpios work properly

Thus, this testing framework serves as a "simplified" version of tailboard_testing_framework.ino 
"""

import adafruit_lis3dh
import pwmio
import board
import time
import digitalio
import busio
import neopixel

""" 
Test LED enable
"""

# Set up A4 as a digital output
pin_A4 = digitalio.DigitalInOut(board.A4)
pin_A4.direction = digitalio.Direction.OUTPUT


# Set the pin high (3.3V)
pin_A4.value = False# or False to set it low (0V) --> test the enable input on the LED driver (right LEDs)


# Set up A5 as a digital output
pin_A5 = digitalio.DigitalInOut(board.A5)
pin_A5.direction = digitalio.Direction.OUTPUT


# Set the pin high (3.3V)
pin_A5.value = False# or False to set it low (0V) --> test the enable input on the LED driver (Left LEDs)


# Set up D4 as a digital output
pin_D4 = digitalio.DigitalInOut(board.D4)
pin_D4.direction = digitalio.Direction.OUTPUT


# Set the pin high (3.3V)
pin_D4.value = False# or False to set it low (0V) --> test the enable input on the LED driver (Body LEDs)

"""
Test LED signal functions
"""

# Create PWM output on pin 13 (D13) with 3.3V signal
pwm_pin_13 = pwmio.PWMOut(board.D13, frequency=5000, duty_cycle=65535)  # 100% duty cycle (3.3V)


# Create PWM output on pin 12 (D12) with 0V signal
pwm_pin_12 = pwmio.PWMOut(board.D12, frequency=5000, duty_cycle=65535)  # 0% duty cycle (0V)


# Create PWM output on pin 11 (D11) with 3.3V signal
pwm_pin_11 = pwmio.PWMOut(board.D11, frequency=5000, duty_cycle=65535)  # 100% duty cycle (3.3V)


# Keep the PWM running indefinitely
# while True:
#    time.sleep(1)

"""
Test motors
"""

# Create PWM output on pin 5 (D5) with 3.3V signal
pwm_pin_5 = pwmio.PWMOut(board.D5, frequency=5000, duty_cycle=65535)  # 100% duty cycle (3.3V)


# Create PWM output on pin 6 (D6) with 0V signal
pwm_pin_6 = pwmio.PWMOut(board.D6, frequency=5000, duty_cycle=0)  # 0% duty cycle (0V)


# Create PWM output on pin 5 (D5) with 3.3V signal
pwm_pin_9 = pwmio.PWMOut(board.D9, frequency=5000, duty_cycle=65535)  # 100% duty cycle (3.3V)

# Create PWM output on pin 6 (D6) with 0V signal
pwm_pin_10 = pwmio.PWMOut(board.D10, frequency=5000, duty_cycle=0)  # 0% duty cycle (0V)

"""
Disable dummy load
"""
# Set up A3 as a digital output
pin_A3 = digitalio.DigitalInOut(board.A3)
pin_A3.direction = digitalio.Direction.OUTPUT


# Set the pin high (3.3V)
pin_A3.value = False # or False to set it low (0V)

"""
Flash lights on flyer
"""
pixel_pin_RW = board.D13
num_pixels_RW = 75
pixels_RW = neopixel.NeoPixel(pixel_pin_RW, num_pixels_RW, brightness=5, auto_write=False)


for i in range(num_pixels_RW):
   pixels_RW[i] = (255, 0, 0)


#pixels_RW[0] = (255, 0, 0)  # Red
#pixels_RW[1] = (0, 255, 0)  # Green
#pixels_RW[2] = (0, 0, 255)  # Blue
pixels_RW.show()


pixel_pin_LW = board.D11
num_pixels_LW = 75
pixels_LW = neopixel.NeoPixel(pixel_pin_LW, num_pixels_LW, brightness=5, auto_write=False)


for i in range(num_pixels_LW):
   pixels_LW[i] = (255, 0, 0)


#pixels_LW[0] = (255, 0, 0)  # Red
#pixels_LW[1] = (0, 255, 0)  # Green
#pixels_LW[2] = (0, 0, 255)  # Blue
pixels_LW.show()


"""
Setup accelerometers
"""
# Set up I2C connection
i2c = busio.I2C(board.SCL, board.SDA)
accelerometer = adafruit_lis3dh.LIS3DH_I2C(i2c, address=0x19)

# Set range (optional, default is ±2G)
accelerometer.range = adafruit_lis3dh.RANGE_2_G

# Main loop to read accelerometer values
while True:
    x, y, z = accelerometer.acceleration  # Get accelerometer readings
    print("Acceleration (x, y, z): {:.2f}, {:.2f}, {:.2f} m/s^2".format(x, y, z))
    time.sleep(0.1)  # Delay to slow down output

