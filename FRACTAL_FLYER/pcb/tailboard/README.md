# Tailboards

The tailboard is the PCB on each Fractal Flyer. It takes in 50V power, USB, and a reset signal over an RJ45 Cat6 Ethernet cable. It has a FeatherM4 for controlling the Flyer, which is connected to three accelerometers (body, two wings), two H-bridges (one for each wing), and 3 LED strips (body, two wings).
 - tailboard: first tailboard design, bring-up was prevented by onset of COVID pandemic
 - tailboard-v2: changes accelerometers to use 3V instead of 5V
 - tailboard-v3: fixes pinout of Feather M4 board
 - tailboard-v4: changes two wing accelerometers to I2C from SPI, further
 fixes to pinout of Feather M4 board.
 - tailboard-v5: redesign to support larger plugs for LED power
 - tailboard-v6: changes in bringup of v5
 - tailboard-v7: cleanup of v6
 - tailboard-v8: layout cleanup, schematics identical to v6/7
 - tailboard-v9: redesign H-bridges, power converters, and signaling path
 - tailboard-v10: minor changes from bringup of v9

