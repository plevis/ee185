# Fractal Flyer for FLIGHT

This directory stores all of the files, notes, and technical documents
for the Fractal Flyers (the physical art pieces) of the  Packard Interactive 
Light Sculpture Project (FLIGHT). 

The directory structure:
  - body-leds: CAD and design documents for the body lighting
  - body-shell: CAD and design documents for the body shell
  - drawings: mechanical drawings of Flyer
  - manuals: manuals for assembling a Flyer and its parts
  - mounting: CAD and design documents for mounting assemblies
  - pcb: printed circuit board designs
  - signaling: reports and diagrams on cabling and signaling
  - wings: CAD, design documents, and inventory of Flyer wings

The complete [Fractal Flyer CAD](https://cad.onshape.com/documents/1658552cb3cf9b97eb4187b0/w/db1b832a8e6a42671155c1fd/e/7ad09a5b999f18148690839f) is in Onshape, a web-based CAD program.
