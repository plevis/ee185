#include <stdint.h>

#include "py/obj.h"
#include "py/runtime.h"

#include "shared-bindings/ee285/__init__.h"

#include <stdint.h>
#include <string.h>
#include "lib/utils/context_manager_helpers.h"
#include "py/objproperty.h"
#include "py/runtime.h"
#include "py/runtime0.h"
#include "shared-bindings/util.h"



//| .. currentmodule:: ee285

STATIC mp_obj_t ee285_obj_body_leds(mp_obj_t rgb) {
  shared_module_ee285_body_leds(rgb);
  return mp_const_none; //All functions need this?
}
MP_DEFINE_CONST_FUN_OBJ_1(ee285_body_leds_obj, ee285_obj_body_leds); //The "1" is the number of params from python


STATIC mp_obj_t ee285_obj_wing_leds(mp_uint_t n_args, const mp_obj_t *args) {
  int side = mp_obj_get_int(args[0]);
  mp_obj_t rgb1 = args[1];
  mp_obj_t rgb2 = args[2];
  mp_obj_t rgb3 = args[3];
  shared_module_ee285_wing_leds(side, rgb1, rgb2, rgb3);
  return mp_const_none;
}
MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(ee285_wing_leds_obj, 4, 4, ee285_obj_wing_leds); //This is needed for a function with >3 params

STATIC mp_obj_t ee285_obj_wing_angle(mp_obj_t side, mp_obj_t angle) { //This is not implemented yet
  
  int sideval = mp_obj_get_int(side);
  float angleval = mp_obj_get_float(angle);
  shared_module_ee285_wing_angle(sideval, angleval);
  return mp_const_none;
}
MP_DEFINE_CONST_FUN_OBJ_2(ee285_wing_angle_obj, ee285_obj_wing_angle);

STATIC mp_obj_t ee285_obj_get_wing_angle(mp_obj_t side) {
  int sideval = mp_obj_get_int(side);
  float angle = shared_module_ee285_get_wing_angle(sideval);
  return mp_obj_new_float(angle);
}

MP_DEFINE_CONST_FUN_OBJ_1(ee285_get_wing_angle_obj, ee285_obj_get_wing_angle);

STATIC mp_obj_t ee285_obj_get_KP(void) { 
  float KP = shared_module_ee285_get_KP();
  return mp_obj_new_float(KP);
}

MP_DEFINE_CONST_FUN_OBJ_0(ee285_get_KP_obj, ee285_obj_get_KP);

STATIC mp_obj_t ee285_obj_get_KI(void) { //This is not implemented yet
  float KI = shared_module_ee285_get_KI();
  return mp_obj_new_float(KI);
}

MP_DEFINE_CONST_FUN_OBJ_0(ee285_get_KI_obj, ee285_obj_get_KI);

STATIC mp_obj_t ee285_obj_set_KP(mp_obj_t kp) { //This is not implemented yet
  float KP = mp_obj_get_float(kp);
  shared_module_ee285_set_KP(KP);
  return mp_const_none;
}

MP_DEFINE_CONST_FUN_OBJ_1(ee285_set_KP_obj, ee285_obj_set_KP);

STATIC mp_obj_t ee285_obj_set_KI(mp_obj_t ki) { //This is not implemented yet
  float KI = mp_obj_get_float(ki);
  shared_module_ee285_set_KI(KI);
  return mp_const_none;
}

MP_DEFINE_CONST_FUN_OBJ_1(ee285_set_KI_obj, ee285_obj_set_KI);

STATIC mp_obj_t ee285_obj_motor_control_loop_init(void) {
  shared_module_ee285_motor_control_loop_init();
  return mp_const_none;
}


MP_DEFINE_CONST_FUN_OBJ_0(ee285_motor_control_loop_init_obj, ee285_obj_motor_control_loop_init);

STATIC mp_obj_t ee285_obj_test(void) {
  shared_module_ee285_test();
  return mp_const_none;
}

MP_DEFINE_CONST_FUN_OBJ_0(ee285_test_obj, ee285_obj_test);

STATIC const mp_rom_map_elem_t ee285_module_globals_table[] = {
    { MP_ROM_QSTR(MP_QSTR___name__), MP_ROM_QSTR(MP_QSTR_ee285) },
    { MP_ROM_QSTR(MP_QSTR_body_leds), MP_ROM_PTR(&ee285_body_leds_obj) },
    { MP_ROM_QSTR(MP_QSTR_wing_leds), MP_ROM_PTR(&ee285_wing_leds_obj) },
    { MP_ROM_QSTR(MP_QSTR_wing_angle), MP_ROM_PTR(&ee285_wing_angle_obj) },
    { MP_ROM_QSTR(MP_QSTR_get_wing_angle), MP_ROM_PTR(&ee285_get_wing_angle_obj) },
    { MP_ROM_QSTR(MP_QSTR_test), MP_ROM_PTR(&ee285_test_obj) },
    { MP_ROM_QSTR(MP_QSTR_get_KP), MP_ROM_PTR(&ee285_get_KP_obj) },
    { MP_ROM_QSTR(MP_QSTR_get_KI), MP_ROM_PTR(&ee285_get_KI_obj) },
    { MP_ROM_QSTR(MP_QSTR_set_KP), MP_ROM_PTR(&ee285_set_KP_obj) },
    { MP_ROM_QSTR(MP_QSTR_set_KI), MP_ROM_PTR(&ee285_set_KI_obj) },
    { MP_ROM_QSTR(MP_QSTR_motor_control_loop_init), MP_ROM_PTR(&ee285_motor_control_loop_init_obj) },

};

STATIC MP_DEFINE_CONST_DICT(ee285_module_globals, ee285_module_globals_table);

const mp_obj_module_t ee285_module = {
    .base = { &mp_type_module },
    .globals = (mp_obj_dict_t*)&ee285_module_globals,
};





