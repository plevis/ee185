#!/usr/bin/env python3
import sys

# Check for command-line arguments
if len(sys.argv) > 1:
    print("Error: This script does not accept command line arguments.")
    print("Usage: Just run the program as is without any arguments.")
    sys.exit(1)

mapping_file_path = 'mapping.txt'
udev_rules_file_path = 'flyers.rules'

# Start writing the Udev rules file
try:
    with open(mapping_file_path, 'r') as mapping_file, open(udev_rules_file_path, 'w') as udev_rules_file:
        for line in mapping_file:
            # First column is iSerialNum and second is name of flyer
            iSerialNum, flyerNum = line.strip().split()
            
            # Write the Udev rule to the file
            udev_rule = 'SUBSYSTEM=="tty", SUBSYSTEMS=="usb", ATTRS{serial}=="%s", SYMLINK+="flyer%s"\n' % (iSerialNum, flyerNum)
            udev_rules_file.write(udev_rule)
except Exception as e:
    print(f"Error occurred: {e}")
    sys.exit(2)