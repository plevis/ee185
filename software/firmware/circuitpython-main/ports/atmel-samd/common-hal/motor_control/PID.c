#include "ports/atmel-samd/common-hal/motor_control/PID.h"
#include "ports/atmel-samd/common-hal/motor_control/Control.h"
#include "peripherals/samd/pins.h"
// debug 
#include "supervisor/shared/rgb_led_status.h"
#include "supervisor/shared/rgb_led_colors.h"

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <math.h>


// PWM control constants
#define ERR_THRESHOLD_DEGREES 2
#define BEGIN_SLOW_DEGREES 5
#define MAX_DUTY  0.9 //Set to PWM %high that represents max speed we want the wings to move
#define MIN_DUTY  0.2

#define UP true
#define DOWN false
#define LEFT true
#define RIGHT false

// Spin lock var fix 
volatile float raw_angle_left; 
volatile float raw_angle_right; 

// Error case definitions
#define CALC_ERROR_NUM (float) -5000
#define ERROR_HALT (float) 5000.0
#define RESET_NUM_CENTER -1000.0
#define RESET_NUM_WING (float) 1000.0 // this error should pause the wings + reset motors 

#define EPSILON 0.005 // The threshold in which two floats are conisdered equal

// PID controller constants
#define CYCLE_TIME 0.02 // The amount of time between interrupts in s
#define BUFFER_SIZE 50
//#define PID_GRAPHING_DEBUG
//#define PID_ERROR_DEBUG 

//#define EMA_ALPHA 0.05

static float KP = 0.3015;
static float KI = 0.035;
static float prior_integral_left = 0; // TODO: maintain bounds on this integral so it doesn't loop around, and figure out when to reset to 0!!
static float prior_integral_right = 0; // TODO: maintain bounds on this integral so it doesn't loop around, and figure out when to reset to 0!!

static float error_buffer_left[BUFFER_SIZE] = {0.0f}; // should init to zeros
static int oldest_elem_index_left = 0;
//static float ema_left = -100.0;

static float error_buffer_right[BUFFER_SIZE] = {0.0f}; // should init to zeros
static int oldest_elem_index_right = 0;
//static float ema_right = -100.0;

extern void set_pwm(bool left, bool up, float duty);

/*The following is based on the EE285 control loop python code (see the repository for details)*/
void set_KP(float val){
  KP = val;
}

float get_KP(){
  return KP;
}

void set_KI(float val){
  KI = val;
}

float get_KI(){
  return KI;
}

acceleration get_bird_accel(void) {
  return get_raw_accel(CENTER);
}

bool float_equality(float a, float b) {
  return fabs((double)(a - b)) < (double)EPSILON;
}

float abs_float(float a) {
  return a < 0 ? -a : a;
}

float sign_float(float a) {
  return a < 0 ? -1.0 : 1.0;
}

// accepts an acceleration value
// returns true if value is between 9 and 11 m/s^2
// returns false in all other cases
bool valid_magnitude(acceleration accel){
    float mag = pow(accel.x,2)+pow(accel.y,2)+pow(accel.z,2);
    bool valid = (mag > 64 && mag < 121);
    if (!valid) {
	printf("Accelerometer vector out of range: mag^2 is %d (should be >64 and <121)\n", (int)mag);
    }
    return valid;
}

void clear_buffer(bool left){
  if(left){
    for(int i = 0; i < BUFFER_SIZE; i++){
      error_buffer_left[i] = 0.0f;
      prior_integral_left = 0.0f;
    }
  } else {
    for(int i = 0; i < BUFFER_SIZE; i++){
      error_buffer_right[i] = 0.0f;
      prior_integral_right = 0.0f;
    }
  }
}
float pid_update_pwm(bool left, float current_angle, float degrees_C){

    float diff = (degrees_C - current_angle);
    float duty = 0;
    
    /* For calculating the I term in the PID loop. */
    float* error_buffer;
    int* oldest_elem_index;
    float* prior_integral;

    if(left){
    	error_buffer = &error_buffer_left[0];
	    oldest_elem_index = &oldest_elem_index_left;
	    prior_integral = &prior_integral_left;
    }

    else{
    	error_buffer = &error_buffer_right[0];
	    oldest_elem_index = &oldest_elem_index_right;
	    prior_integral = &prior_integral_right;
    }

    float integral = *prior_integral + (diff * CYCLE_TIME) - (error_buffer[*oldest_elem_index] * CYCLE_TIME);

    *prior_integral = integral;
    error_buffer[*oldest_elem_index] = diff;
    *oldest_elem_index = (*oldest_elem_index + 1) % BUFFER_SIZE;
    
    // Integrate P and I components of PID
    float tempduty = (KP * diff) + (KI * integral);

    //clamping duty between MIN_DUTY and MAX_DUTY
    if(abs_float(tempduty) < MIN_DUTY){
        duty = 0;
    }
    else{
        duty = sign_float(tempduty) * max(min(abs_float(tempduty), MAX_DUTY), MIN_DUTY); 
    }
    
    // we want to print wing, current_angle, target angle, Kp, Ki, diff, integral, duty

#ifdef PID_GRAPHING_DEBUG
    printf("PID GRAPHING: wing:%d, current_angle:%.5f, target_angle:%.5f, Kp:%.5f, Ki:%.5f, diff:%.5f, integral:%.5f, duty:%.5f \n", left, (double) current_angle, (double) degrees_C, (double) KP, (double) KI, (double) diff, (double) integral, (double) duty);
#endif
    return duty;
}

float get_wing_angle(bool left){ ///Will take in sensor values from WING accelerometer (LEFT or RIGHT depends on boolean)
    acceleration accel; 
    acceleration bird = get_bird_accel();

    // Check if bird needs reset because all outputs are 0
    if (!valid_magnitude(bird)) {
      return RESET_NUM_CENTER; // this error should pause the wings + reset motors 
    }

    // Check if center of bird is in a reasonable position
    // Varun: this is why the bird is crashing repeatedly!!!
    if (bird.x < -3.0 || bird.x > 3.0 || 
        bird.y < -3.0 || bird.y > 3.0 || 
        bird.z < -11.0 || bird.z > -9.0) {

#ifdef PID_ERROR_DEBUG	    
      printf("******************ERROR**************************\n");
      printf("GET_WING_ANGLE ERROR CENTER RAW: %.5f %.5f %.5f \n", (double)bird.x, (double)bird.y, (double)bird.z);
      printf("*************************************************\n");
#endif
      return ERROR_HALT;
    }

    float accel_x;
    float accel_y;
    float accel_z;
                                 
     // Accelerometer value is read (for the test done in the Python script, the pinout for the body accelerometer was used)
    if (left) {      //for left wing
       accel = get_raw_accel(LEFTWING); 
     } else {          //for right wing
       accel = get_raw_accel(RIGHTWING);
    }
    accel_x = accel.x;                         //X acceleration value from wing sensor
    accel_y = accel.y;                        //Y acceleration value from wing sensor 
    accel_z = accel.z;                        //Z acceleration value from wing sensor
    
    // If all accelerations are 0, return indicator to reset accelerometer

    if (!valid_magnitude(accel)) {
      return RESET_NUM_WING;
    }

    // checking if wings are past -90/90 degrees
    if (accel_z < 0.0){                         
#ifdef PID_ERROR_DEBUG	    
      printf("******************ERROR**************************\n");
      printf("Z ACCEL ERROR: %.5f\n", (double)accel_z);
      printf("*************************************************\n");
#endif
      return ERROR_HALT;
    }
    
    // This is to make sure the tangent calculation does not go to infinity
    if (float_equality(accel_z, 0.0)) {
      accel_z = 0.001;
    }

    if(left) {
      accel_x -= bird.x;
      accel_y -= bird.y;
    } else {
      accel_x += bird.x;
      accel_y += bird.y;
    }

    // bird_accel

    float tan_pitch =  accel_y/accel_z;               //Tangent and angle calculations performed. Similar to the calculations done in get_bird_angle()
    
    float angle_rads = atanf(tan_pitch);
    float raw_angle_degrees = angle_rads * 180 / 3.14;
    return raw_angle_degrees * -1;  // inverted to match positive angle as up

    /*
    float *ema = left ? &ema_left : &ema_right;

    if (float_equality(*ema, -100.0f)) {
      *ema = raw_angle_degrees;
    } else {
      *ema = EMA_ALPHA * raw_angle_degrees + (1.0f - EMA_ALPHA) * (*ema);
    }
    return (*ema) * -1;*/
}

extern void common_hal_time_delay_ms(uint32_t);

//This function takes the degree setpoint from Python and calculates the necessary duty cycle to output. 
//Essentially the heart of the PID control loop.

void set_wing_angle(bool left, int degrees_C){ 
    float current_angle = get_wing_angle(left);

    if (left == LEFT){
      raw_angle_left = current_angle;
    } else {
      raw_angle_right = current_angle;
    }

    // Check for errrors.
    if (float_equality(current_angle, ERROR_HALT)) {
      set_pwm(left, UP, 0);

#ifdef PID_ERROR_DEBUG
       if(left) {
	  printf("*********ERROR***********\n");
          printf("Current_angle: %.5f, left wing error; halting\n", (double)current_angle);
          acceleration accel = get_raw_accel(LEFTWING); 
          printf("LEFT RAW: %.5f %.5f %.5f \n", (double)accel.x, (double)accel.y, (double)accel.z);
	  printf("*************************\n");
       }else {
	  printf("*********ERROR***********\n");
          printf("Current_angle: %.5f, right wing error; halting\n", (double)current_angle);
          acceleration accel = get_raw_accel(RIGHTWING); 
          printf("RIGHT RAW: %.5f %.5f %.5f \n", (double)accel.x, (double)accel.y, (double)accel.z);
	  printf("*************************\n");
       }
#endif
      return;
    } else if (float_equality(current_angle, RESET_NUM_WING)) {
      set_pwm(left, UP, 0);
      
      // Need to delay a little or else i2c crashes (it seems like if we do two 
      // i2c commands in too quick of succession, then i2c starts acting funky)
      // Note: this is very similar to how i2c_accel_init has delays.
      // Note: python doesn't seem to have this issue because python is slow.
      // TODO: experiment with shrinking this delay.
      common_hal_time_delay_ms(1); 

      if(left) {
        accel_reset(LEFTWING);
      }else {
        accel_reset(RIGHTWING);
      }

      if(left) {
        printf("left wing reset\n");
      }else {
        printf("right wing reset\n");
      }
      return;
    }

    else if (float_equality(current_angle, RESET_NUM_CENTER)){
      set_pwm(left, UP, 0);

      common_hal_time_delay_ms(1); 
      accel_reset(CENTER);
      printf("bird accel reset\n");
    }

    // Check for more errors.
    if (current_angle > 50.0 || current_angle < -50.0) {
      set_pwm(left, UP, 0);

#ifdef PID_ERROR_DEBUG
      if(left) {
         printf("LEFT WING OUT OF BOUNDS: %d. HALTING!\n", (int)current_angle);
      }else {
         printf("RIGHT WING OUT OF BOUNDS: %d. HALTING!\n", (int)current_angle);
      }
#endif
      return;
    }

    float duty = pid_update_pwm(left, current_angle, degrees_C);
    // Execute the calculated duty cycle on given wing
    //if (current_angle < degrees_C){
    if (duty > 0.0){
        if(left){ //send the PWM signal to move the left wing up
            set_pwm(LEFT,UP,duty);
        }
        else{ //send the PWM signal moves the right wing up 
            set_pwm(RIGHT,UP,duty);
        }

    } else {
        if(left){  //send a PWM signal to stop the left wing
            set_pwm(LEFT, DOWN, -duty);
        }
        else{      //send a PWM signal to stop the right wing
            set_pwm(RIGHT, DOWN, -duty);
        }
    }

}
