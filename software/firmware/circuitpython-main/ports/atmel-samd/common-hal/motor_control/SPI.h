#ifndef MICROPY_INCLUDED_ATMEL_SAMD_COMMON_HAL_MOTOR_CONTROL_SPI_H
#define MICROPY_INCLUDED_ATMEL_SAMD_COMMON_HAL_MOTOR_CONTROL_SPI_H

#include "ports/atmel-samd/common-hal/motor_control/Control.h"
#include "shared-bindings/adafruit_bus_device/SPIDevice.h"
#include "shared-bindings/busio/SPI.h"
#include "peripherals/samd/pins.h"
#include "shared-bindings/motor_control/__init__.h"

#include "shared-bindings/digitalio/DigitalInOut.h"

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <math.h>

typedef struct {
    adafruit_bus_device_spidevice_obj_t sensor;
    busio_spi_obj_t bus;
    uint8_t gbuf[6];
    digitalio_digitalinout_obj_t cs;
} spi_sensor_t;

void spi_init(spi_sensor_t*);
uint8_t spi_read_register_byte(spi_sensor_t*, uint8_t);
void spi_write_register_byte(spi_sensor_t*, uint8_t, uint8_t);
void spi_reset(spi_sensor_t* device);

/*uint8_t get_spi_range(spi_sensor_t*);
void reset_bird_accel(spi_sensor_t*);
spi_sensor_t get_bird_sensor(void);
*/

#endif