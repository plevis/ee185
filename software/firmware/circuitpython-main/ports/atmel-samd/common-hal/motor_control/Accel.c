#include "ports/atmel-samd/common-hal/motor_control/Accel.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


// new for debug 
#include "supervisor/shared/rgb_led_status.h"
#include "supervisor/shared/rgb_led_colors.h"

// Register addresses:
#define _REG_OUTADC1_L   0x08
#define _REG_WHOAMI      0x0F
#define _REG_TEMPCFG     0x1F
#define _REG_CTRL1       0x20
#define _REG_CTRL3       0x22
#define _REG_CTRL4       0x23
#define _REG_CTRL5       0x24

#define _REG_INT1SRC     0x31
#define _REG_CLICKCFG    0x38
#define _REG_CLICKSRC    0x39
#define _REG_CLICKTHS    0x3A
#define _REG_TIMELIMIT   0x3B
#define _REG_TIMELATENCY 0x3C
#define _REG_TIMEWINDOW  0x3D

#define _REG_OUT_X_L     0x28
#define _REG_OUT_X_H     0x29
#define _REG_OUT_Y_L     0x2A
#define _REG_OUT_Y_H     0x2B
#define _REG_OUT_Z_L     0x2C
#define _REG_OUT_Z_H     0x2D

// Register value constants:
#define RANGE_16_G 0x03   // +/- 16g
#define RANGE_8_G  0x02   // +/- 8g
#define RANGE_4_G  0x01   // +/- 4g
#define RANGE_2_G  0x00   // +/- 2g (default value)
#define DATARATE_1344_HZ 0x09  // 1.344 KHz
#define DATARATE_400_HZ  0x07  // 400Hz
#define DATARATE_200_HZ  0x06  // 200Hz
#define DATARATE_100_HZ  0x05  // 100Hz
#define DATARATE_50_HZ   0x04  // 50Hz
#define DATARATE_25_HZ   0x03  // 25Hz
#define DATARATE_10_HZ   0x02  // 10 Hz
#define DATARATE_1_HZ    0x01  // 1 Hz
#define DATARATE_POWERDOWN 0
#define DATARATE_LOWPOWER_1K6HZ 0x1000
#define DATARATE_LOWPOWER_5KHZ  0x1001

#define STANDARD_GRAVITY 9.806

#define LEFT_WING_I2CADDR 0x19
#define RIGHT_WING_I2CADDR 0x18

extern void common_hal_time_delay_ms(uint32_t);
static uint8_t get_spi_range(spi_sensor_t *device);
static void set_spi_data_rate(spi_sensor_t *device, uint8_t rate);

typedef struct {
  i2c_sensor_t i2c_sensor;
  float divider;
} i2c_accel_sensor_t;

i2c_accel_sensor_t left_wing;
i2c_accel_sensor_t right_wing;
i2c_sensor_t left_i2c_sensor;
i2c_sensor_t right_i2c_sensor;

spi_sensor_t center_sensor;

/*
    Initializes the Feather M4 SPI bus and predetermined body accelerometer output pin.
    Creates a SPI device object for the accelerometer

*/
void spi_accel_init(spi_sensor_t *device)
{    

    // Check device ID
    uint8_t device_id = spi_read_register_byte(device, _REG_WHOAMI);
    //printf("device_id: %d\n", device_id);
    if (device_id != 0x33) {
      printf("Failed to find LIS3DH on chip select");
    }
    // Reboot
    spi_write_register_byte(device, _REG_CTRL5, 0x80);
    common_hal_time_delay_ms(5); 

    // Enable all axes, normal mode.
    spi_write_register_byte(device, _REG_CTRL1, 0x07);
    // Set 400Hz data rate.
    set_spi_data_rate(device, DATARATE_400_HZ);
    // High res & BDU enabled.
    spi_write_register_byte(device, _REG_CTRL4, 0x88);
    // Enable ADCs.
    spi_write_register_byte(device, _REG_TEMPCFG, 0x80);
    // Latch interrupt for INT1
    spi_write_register_byte(device, _REG_CTRL5, 0x08);
}

void i2c_accel_init(i2c_accel_sensor_t* sensor, i2c_sensor_t i2c_sensor) {
  sensor->i2c_sensor = i2c_sensor;
  printf("Initializing %s I2C LIS3DH\n", sensor == &left_wing? "left":"right");
  /*
   * Corresponds to `accel = adafruit_lis3dh.LIS3DH_I2C(i2c)` which goes into 
   * `class LIS3DH_I2C(LIS3DH)`'s `__init__` which will eventually 
   * call `__init__` in `i2c_device.py`
   * 
   * All init does is update object state and call `__probe_for_device()`
   *
   * `__probe_for_device()` just calls `busio_i2c_writeto`, which is a wrapper 
   * for `writeto` in `shared-bindings/busio/I2C.c` which calls 
   * `common_hal_busio_i2c_write` (in common-hal/busio/I2C.c) with an empty 
   * buffer.
   */
  uint8_t trash; 
  common_hal_busio_i2c_write(i2c_sensor.i2c, i2c_sensor.addr, &trash, 0, true);

  /*
   * Next, we enter the constructor for `LIS3DH` in `adafruit_lis3dh.py`
   */
  uint8_t device_id = i2c_read_register_byte(i2c_sensor, _REG_WHOAMI);

  common_hal_time_delay_ms(1); 

  // Check device ID
  if(device_id != 0x33) {
    printf("Failed to find LIS3DH, found 0x%x\n", device_id);
    return;
  }

  // Reboot
  i2c_write_register_byte(i2c_sensor, _REG_CTRL5, 0x80);

  // Take 5 ms
  common_hal_time_delay_ms(5); 

  // Enable all axes, normal mode
  i2c_write_register_byte(i2c_sensor, _REG_CTRL1, 0x07);

  common_hal_time_delay_ms(1); 

  // Set 400Hz data rate
  uint8_t ctl1 = i2c_read_register_byte(i2c_sensor, _REG_CTRL1);
  common_hal_time_delay_ms(1); 
  ctl1 &= ~(0xF0);
  ctl1 |= (DATARATE_400_HZ << 4);
  i2c_write_register_byte(i2c_sensor, _REG_CTRL1, ctl1);
  common_hal_time_delay_ms(1); 

  // High res & BDU enabled.
  i2c_write_register_byte(i2c_sensor, _REG_CTRL4, 0x88);
  common_hal_time_delay_ms(1); 

  // Enable ADCs.
  i2c_write_register_byte(i2c_sensor, _REG_TEMPCFG, 0x80);
  common_hal_time_delay_ms(1); 
  
  // Latch interrupt for INT1.
  i2c_write_register_byte(i2c_sensor, _REG_CTRL5, 0x08);
  common_hal_time_delay_ms(1); 

  // Get range
  uint8_t range = i2c_read_register_byte(i2c_sensor, _REG_CTRL4);
  range = (range >> 4) & 0x03;
  switch(range) {
    case RANGE_16_G:
      sensor->divider = 1365;
      break;
    case RANGE_8_G:
      sensor->divider = 4096;
      break;
    case RANGE_4_G:
      sensor->divider = 8190;
      break;
    case RANGE_2_G:
      sensor->divider = 16380;
      break;
  }
}

extern void common_hal_time_delay_ms(uint32_t);

#include "hal/include/hal_gpio.h"
#include "ports/atmel-samd/asf4/samd51/atmel_start_pins.h"
#include "hal/include/hal_init.h"
#define TEST_PIN PIN_PA14
void gpio_pattern_start(int num_pulses){
  gpio_set_pin_level(TEST_PIN, 1);
  for(int i = 0; i < 2*(num_pulses-1); i++){
    common_hal_time_delay_ms(1);
    gpio_set_pin_level(TEST_PIN, i%2);
  }
}

void gpio_pattern_stop(int num_pulses){
  gpio_set_pin_level(TEST_PIN, 0);
  for(int i = 0; i < 2*(num_pulses-1); i++){
    common_hal_time_delay_ms(1);
    gpio_set_pin_level(TEST_PIN, (i+1)%2);
  }
}

void init_accels(void) {
  gpio_set_pin_direction(TEST_PIN, GPIO_DIRECTION_OUT);
  // TODO: function errors out if try to init I2C when no pins are connected. 
  // Must catch and handle this as a potential danger case if I2C pins come undone while Flyer is hanging
  spi_init(&center_sensor);
  i2c_init();
  left_i2c_sensor = i2c_sensor_create(LEFT_WING_I2CADDR);
  right_i2c_sensor = i2c_sensor_create(RIGHT_WING_I2CADDR);

  gpio_pattern_start(2);

  //printf("init spi\n");
  spi_accel_init(&center_sensor);
  //printf("init i2c left\n");
  i2c_accel_init(&left_wing, left_i2c_sensor);
  //printf("init i2c right\n");
  i2c_accel_init(&right_wing, right_i2c_sensor);

  //printf("] init_accels\n");
  gpio_pattern_stop(2);
}


void reset_bird_accel(spi_sensor_t *device){
    spi_write_register_byte(device, _REG_CTRL5, 0x80);
    common_hal_time_delay_ms(5);
}

/*
Input: spi_sensor_t object
*/
acceleration read_bird_accelerometer(spi_sensor_t *device) {
  uint16_t divider = 1;
  uint8_t accel_range = get_spi_range(device);
  acceleration accel;
  switch (accel_range) {
  case RANGE_16_G:
    divider = 1365;
    break;
  case RANGE_8_G:
    divider = 4096;
    break;
  case RANGE_4_G:
    divider = 8190;
    break;
  case RANGE_2_G:
    divider = 16380;
    break;
  }

    // Values are stored in two registers as left-justified two's complement
    // read into int16_t as these are stored as two's complement
    // read high and low bytes, and shift right 4 (12b high-resolution mode)
    int16_t raw_x = spi_read_register_byte(device, _REG_OUT_X_H | 0x80);
    raw_x <<= 8;
    raw_x |= spi_read_register_byte(device, _REG_OUT_X_L | 0x80);

    int16_t raw_y = spi_read_register_byte(device, _REG_OUT_Y_H | 0x80);
    raw_y <<= 8;
    raw_y |= spi_read_register_byte(device, _REG_OUT_Y_L | 0x80);

    int16_t raw_z = spi_read_register_byte(device, _REG_OUT_Z_H | 0x80);
    raw_z <<= 8;
    raw_z |= spi_read_register_byte(device, _REG_OUT_Z_L | 0x80);

  // convert from Gs to m / s ^ 2 and adjust for the range
  accel.x = ((float)raw_x / divider) * STANDARD_GRAVITY;
  accel.y = ((float)raw_y / divider) * STANDARD_GRAVITY;
  accel.z = ((float)raw_z / divider) * STANDARD_GRAVITY;

  return accel;
}

acceleration read_i2c_accelerometer(i2c_accel_sensor_t device) {
  /*
   * Logic for this comes from `def acceleration(self):` in `adafruit_lis3dh.py`
   */
  acceleration accel;

  float divider = device.divider;
  uint8_t buf[6];
  memset(buf, 0, 6);
  new_status_color(RED);
  i2c_read_register(device.i2c_sensor, _REG_OUT_X_L | 0x80, buf, 6);
  int16_t raw_x = buf[0] | (buf[1] << 8);
  int16_t raw_y = buf[2] | (buf[3] << 8);
  int16_t raw_z = buf[4] | (buf[5] << 8);

  // printf("divider: %f, raw x: %d raw y: %d raw z: %d \n", (double) divider, raw_x, raw_y, raw_z);
  new_status_color(YELLOW);
  // convert from Gs to m / s ^ 2 and adjust for the range
  accel.x = ((float)raw_x / divider) * STANDARD_GRAVITY;
  accel.y = ((float)raw_y / divider) * STANDARD_GRAVITY;
  accel.z = ((float)raw_z / divider) * STANDARD_GRAVITY;
  new_status_color(BLUE);
  return accel;
}

/*
*/
acceleration get_raw_accel(ACCEL_TYPE side){
    acceleration accel;
    if (side == CENTER){
      accel = read_bird_accelerometer(&center_sensor);
    }
    else if(side==RIGHTWING){
      accel = read_i2c_accelerometer(right_wing);
    }
    else if (side==LEFTWING) {
      accel = read_i2c_accelerometer(left_wing);
    }
    else {
        printf("Invalid accelerometer choice. Please select LEFTWING(0), CENTER(1) or RIGHTWING(2)");
    }

    return accel;
}



void accel_reset(ACCEL_TYPE side){
  if (side == CENTER){
    reset_bird_accel(&center_sensor);
  }
  else if(side==RIGHTWING){
    i2c_accel_init(&right_wing, right_i2c_sensor);
  }
  else if (side==LEFTWING) {
    i2c_accel_init(&left_wing, left_i2c_sensor);
  }
}

uint8_t get_spi_data_rate(spi_sensor_t *device) {
/* The data rate of the accelerometer.  Can be DATA_RATE_400_HZ, DATA_RATE_200_HZ,
    DATA_RATE_100_HZ, DATA_RATE_50_HZ, DATA_RATE_25_HZ, DATA_RATE_10_HZ,
    DATA_RATE_1_HZ, DATA_RATE_POWERDOWN, DATA_RATE_LOWPOWER_1K6HZ, or
    DATA_RATE_LOWPOWER_5KHZ. */
    uint8_t ctl1 = spi_read_register_byte(device, _REG_CTRL1);
    return (ctl1 >> 4) & 0x0F;
}

static void set_spi_data_rate(spi_sensor_t *device, uint8_t rate) {
    uint8_t ctl1 = spi_read_register_byte(device, _REG_CTRL1);
    ctl1 &= ~(0xF0);
    ctl1 |= rate << 4;
    spi_write_register_byte(device, _REG_CTRL1, ctl1);
}

static uint8_t get_spi_range(spi_sensor_t *device) {
  /* The range of the accelerometer.  Can be RANGE_2_G, RANGE_4_G, RANGE_8_G, or
      RANGE_16_G. */
    uint8_t ctl4 = spi_read_register_byte(device, _REG_CTRL4);
    return (ctl4 >> 4) & 0x03;
}

__attribute__((unused)) static void set_spi_range(spi_sensor_t *device, uint8_t range_value) {
    uint8_t ctl4 = spi_read_register_byte(device, _REG_CTRL4);
    ctl4 &= ~0x30;
    ctl4 |= range_value << 4;
    spi_write_register_byte(device, _REG_CTRL4, ctl4);
}

//Tests the SPI bus, in the case of the LIS3DH this should print 0x33
//If this prints 0xFF, check the pins/physical connection
void test(void)
{
    uint8_t id = spi_read_register_byte(&center_sensor, 0x0F);
    printf("ID: 0x%X\n", id);

}

void test_spi_reset(void){
  for(uint8_t i = 0; i<1000; i++){
    if(!i/3){
      printf("Resetting device: \n");
      accel_reset(CENTER);
      acceleration postReset = get_raw_accel(CENTER);
      printf("X: %f, Y: %f, Z: %f \n", (double)postReset.x, (double)postReset.y,
             (double)postReset.z);
    }
    acceleration aOut = get_raw_accel(CENTER);
    printf("X: %f, Y: %f, Z: %f \n", (double)aOut.x, (double)aOut.y,
           (double)aOut.z);
  }
}

void test_spi_reading(void) {
  while (1) {
    acceleration aOut = get_raw_accel(CENTER);
    printf("X: %f, Y: %f, Z: %f \n", (double)aOut.x, (double)aOut.y,
           (double)aOut.z);
    common_hal_time_delay_ms(5);
  }
}
