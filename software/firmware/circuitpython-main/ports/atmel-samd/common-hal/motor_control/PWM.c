#include "common-hal/microcontroller/Pin.h"
#include "peripherals/samd/pins.h"
#include "shared-bindings/pwmio/PWMOut.h"
#include <stdbool.h>
#include <stdio.h>

// Frequency to PWM at, issues on SAM32 at 20000
#define MOTOR_PWM_FREQUENCY 20000

/*The pins below are example pins that can be used for outputting PWM.
For winter_2021_PID_demo.py, only pin D43 on the sam32 was used. */

/*Additional pin numbers and registers can be found under:
     ports/atmel-samd/boards/feather_m4_express/pins.c
     ports/atmel-samd/boards/sam32/pins.c */

static bool initted = false;

static pwmio_pwmout_obj_t left_forward_wing;
// Use &pin_PB09 for A3 on feather m4. Use &pin_PA22 for D43 on sam32
static const mcu_pin_obj_t *left_forward_pin = &pin_PA16;

static pwmio_pwmout_obj_t left_backward_wing;
// Use &pin_PA05 for A1 on feather m4. Use &pin_PA05 for DAC1 on sam32
static const mcu_pin_obj_t *left_backward_pin = &pin_PA18;

static pwmio_pwmout_obj_t right_forward_wing;
// Use &pin_PB17 for RX on feather m4. Use &pin_PA07 for D16 on sam32
static const mcu_pin_obj_t *right_forward_pin = &pin_PA19;

static pwmio_pwmout_obj_t right_backward_wing;
// Use &pin_PB16 for TX on feather m4. Use &pin_PA16 for D35 on sam32
static const mcu_pin_obj_t *right_backward_pin = &pin_PA20;

extern pwmout_result_t
common_hal_pwmio_pwmout_construct(pwmio_pwmout_obj_t *self,
                                  const mcu_pin_obj_t *pin, uint16_t duty,
                                  uint32_t frequency, bool variable_frequency);

void set_pwm(bool left, bool up, float fduty) {
  if (fduty > 1 || fduty < 0) {
    printf("Bad duty value\n");
    return;
  }

  //if (!initted) {
    common_hal_pwmio_pwmout_construct(&left_forward_wing, left_forward_pin, 0,
                                      MOTOR_PWM_FREQUENCY, false);
    common_hal_pwmio_pwmout_construct(&left_backward_wing, left_backward_pin, 0,
                                      MOTOR_PWM_FREQUENCY, false);
    common_hal_pwmio_pwmout_construct(&right_forward_wing, right_forward_pin, 0,
                                      MOTOR_PWM_FREQUENCY, false);
    common_hal_pwmio_pwmout_construct(&right_backward_wing, right_backward_pin,
                                      0, MOTOR_PWM_FREQUENCY, false);
    initted = true;
  //}

  // Convert from the float to a fraction of 65536
  float duty_scaled = fduty * 65536; // 2 ** 16
  int duty = (int)duty_scaled;

  if (left && up) {
    // Left and up.

    //set duty to 1 - duty and set the other to 1, because it makes the motor more smoother
    // try to do a digital write rather than an analog write because the cortex processor
    // does not support a 100% duty cycle
    //inside PWM.c:68

    common_hal_pwmio_pwmout_set_duty_cycle(&left_forward_wing, duty);
    common_hal_pwmio_pwmout_set_duty_cycle(&left_backward_wing, 0);
  } else if (left) {
    // Left and down.
    common_hal_pwmio_pwmout_set_duty_cycle(&left_backward_wing, duty);
    common_hal_pwmio_pwmout_set_duty_cycle(&left_forward_wing, 0);
  } else if (up) {
    // Right and up.
    common_hal_pwmio_pwmout_set_duty_cycle(&right_forward_wing, duty);
    common_hal_pwmio_pwmout_set_duty_cycle(&right_backward_wing, 0);
  } else {
    // Right and down.
    common_hal_pwmio_pwmout_set_duty_cycle(&right_backward_wing, duty);
    common_hal_pwmio_pwmout_set_duty_cycle(&right_forward_wing, 0);
  }

  return;
}