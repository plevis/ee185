#include "I2C.h"
#include "shared-bindings/busio/I2C.h"
#include "peripherals/samd/pins.h"

// new for debug 
#include "supervisor/shared/rgb_led_status.h"
#include "supervisor/shared/rgb_led_colors.h"

#define I2CBAUD 100000
#define I2CTIMEOUT 255

extern void common_hal_time_delay_ms(uint32_t);

static const mcu_pin_obj_t *sda_pin = &pin_PA12;
static const mcu_pin_obj_t *scl_pin = &pin_PA13;

static bool i2c_initted = false;
static busio_i2c_obj_t i2c;

void i2c_reset(i2c_sensor_t *device) {
    // Deinitialize the I2C bus
    common_hal_busio_i2c_deinit(device->i2c);

    // Reset the initialized flag
    i2c_initted = false;
}

void i2c_init(void) {
  new_status_color(RED);
  if(i2c_initted) {
    //i2c_reset((i2c_sensor_t *)&i2c);
    //return;
    printf("Not returning from 12c init\n");
    }
  /*
   * Corresponds to `i2c = busio.I2C(board.SCL, board.SDA)`
   * which calls `busio_i2c_make_new` in `I2C.c` which is a wrapper for 
   * `common_hal_busio_i2c_construct`
   */
  common_hal_busio_i2c_construct(&i2c, scl_pin, sda_pin, I2CBAUD, I2CTIMEOUT);
  i2c_initted = true;
}

i2c_sensor_t i2c_sensor_create(uint8_t addr) {
  //i2c_init();
  i2c_sensor_t sensor;
  sensor.addr = addr;
  sensor.i2c = &i2c;
  return sensor;
}

void i2c_read_register(i2c_sensor_t device, uint8_t reg, uint8_t* buf, uint8_t len) {
  /*
   * `_read_register` in `class LIS3DH_I2C(LIS3DH)` of `adafruit_lis3dh.py`
   * first calls `write` of `I2CDevice` in `i2c_device.py` which calls `writeto`
   * which is hooked up to `busio_i2c_writeto` in `shared-bindings/busio/I2C.c`
   * which eventually calls `common_hal_busio_i2c_write`
   */
  common_hal_busio_i2c_write(device.i2c, device.addr, &reg, 1, true);

  /* 
   * After, that in `_read_register`, we go to `readinto` which goes into 
   * `readfrom_into` which goes into `busio_i2c_readfrom_into` which goes into
   * `readfrom` which calls `common_hal_busio_i2c_read` 
   */
  common_hal_busio_i2c_read(device.i2c, device.addr, buf, len);
}

uint8_t i2c_read_register_byte(i2c_sensor_t device, uint8_t reg) {
  uint8_t data;
  i2c_read_register(device, reg, &data, 1);
  return data;
}

void i2c_write_register_byte(i2c_sensor_t device, uint8_t reg, uint8_t val) {
  uint8_t buf[2];
  buf[0] = reg;
  buf[1] = val;
  common_hal_busio_i2c_write(device.i2c, device.addr, buf, 2, true);
}


