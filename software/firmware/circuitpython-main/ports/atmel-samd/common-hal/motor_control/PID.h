/*
 * This file is part of the MicroPython project, http://micropython.org/
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Scott Shawcroft for Adafruit Industries
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// COPIED FROM PULSEIO FOR MOTOR CONTROL (akwasio@stanford.edu 02/25/21)
#ifndef MICROPY_INCLUDED_ATMEL_SAMD_COMMON_HAL_MOTOR_CONTROL_PID_H
#define MICROPY_INCLUDED_ATMEL_SAMD_COMMON_HAL_MOTOR_CONTROL_PID_H

#include <stdio.h>
#include <stdbool.h>
#include "shared-bindings/motor_control/__init__.h"
#include "ports/atmel-samd/common-hal/motor_control/Accel.h"

//Add in global variables for PWM

float get_bird_angle(void);
float get_wing_angle(bool left);
void set_wing_angle(bool left, int degrees_C);
float get_KP(void);
float get_KI(void);
void set_KP(float);
void set_KI(float);
void clear_buffer(bool side);

#endif

