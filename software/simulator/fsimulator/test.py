import simLib
LEFT = 1
RIGHT = 0
simLib.init();
FF = 0

def body_leds(r,g,b):
	simLib.body_leds(FF,r,g,b)
def wing_leds(a,b,c,d):
	simLib.wing_leds(FF,a,b,c,d)
def wing_angle(a,b):
	simLib.wing_angle(FF,a,b)

body_leds(255,255,255)
wing_leds(LEFT,(0,1,2),(3,4,5),(6,7,8))
wing_angle(LEFT,25.5)
simLib.destroy();
