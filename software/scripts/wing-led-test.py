"""
This is the testing framework adapted from the Fall 2024 board team (by software)

It is only testing the pin mappings for LED wings as well as their enable pins
"""

import adafruit_lis3dh
import pwmio
import board
import time
import digitalio
import busio
import neopixel

""" 
Set LED enable
"""
time.sleep(3)
print("STARTING TAILBOARD TEST")

# Reference: FlightDesign.pdf
LEDS_PER_WING = 75
LEDS_IN_BODY = 160

# Set up A4 as a digital output
pin_A4 = digitalio.DigitalInOut(board.A4)
pin_A4.direction = digitalio.Direction.OUTPUT
# Set the pin high (3.3V)
pin_A4.value = True # or False to set it low (0V) --> test the enable input on the LED driver (right LEDs)

# Set up A5 as a digital output
pin_A5 = digitalio.DigitalInOut(board.A5)
pin_A5.direction = digitalio.Direction.OUTPUT
# Set the pin high (3.3V)
pin_A5.value = True # or False to set it low (0V) --> test the enable input on the LED driver (Left LEDs)

# Set up D4 as a digital output
pin_D4 = digitalio.DigitalInOut(board.D4)
pin_D4.direction = digitalio.Direction.OUTPUT
# Set the pin low (0V)
pin_D4.value = False # or True to set it high (3.3V) --> test the enable input on the LED driver (Body LEDs)

"""
Set lights on flyer
"""
pixel_pin_RW = board.D13
pixels_RW = neopixel.NeoPixel(pixel_pin_RW, LEDS_PER_WING, brightness=5, auto_write=False)


for i in range(LEDS_PER_WING):
   pixels_RW[i] = (255, 0, 0)


#pixels_RW[0] = (255, 0, 0)  # Red
#pixels_RW[1] = (0, 255, 0)  # Green
#pixels_RW[2] = (0, 0, 255)  # Blue
pixels_RW.show()


pixel_pin_LW = board.D11
pixels_LW = neopixel.NeoPixel(pixel_pin_LW, LEDS_PER_WING, brightness=5, auto_write=False)


for i in range(LEDS_PER_WING):
   pixels_LW[i] = (0, 0, 255)


#pixels_LW[0] = (255, 0, 0)  # Red
#pixels_LW[1] = (0, 255, 0)  # Green
#pixels_LW[2] = (0, 0, 255)  # Blue
pixels_LW.show()