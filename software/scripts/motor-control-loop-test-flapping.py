# This script tests flapping the wings on a Fractal Flyer.
# It uses the motor control loop implemented in the firmware.

import time
import ee285

# —-------------- Disable the dummy load —--------------
import board
import digitalio

# Set up A3 as a digital output
pin_A3 = digitalio.DigitalInOut(board.A3)
pin_A3.direction = digitalio.Direction.OUTPUT


# Set the pin low (0V)
pin_A3.value = False # power_en tied low - don't run dummy load + burn things

#time.sleep(10)
ee285.motor_control_loop_init()

left_wing_high = 20 #flap left wing this many degrees above level
left_wing_low = -20  #flap left wing this many degrees below level
left_wing_target_angle = left_wing_high
period = 10 #Period (s) before reversing target angle

start = (int)(time.monotonic())
last_passed = -1

while True:
    passed = (int)(time.monotonic()) - start

    #TODO: Pattern LEDS here. For now, simulate with delay
    time.sleep(0.01)
    #If desired, smoother operation could be achieved by only changing lights with wings stationary

    #TODO: Any communication here (change behavior by modifying global variables)

    if passed%period == 0 and passed != last_passed:
        left_wing_target_angle = -left_wing_target_angle
        print("Wings moving to angle", left_wing_target_angle)
        last_passed = passed
    
    # Issue: wings aren't flapping all the way up, as they should be.
    #Testing for hanging issues:
    #print(f'read 1 angle 1: {ee285.get_wing_angle(1)} read angle 0: {ee285.get_wing_angle(0)}')
    #print(f'read 2 angle 1: {ee285.get_wing_angle(1)} read angle 0: {ee285.get_wing_angle(0)}')



    # Set left and right wings to desired angle
    ee285.wing_angle(0, left_wing_target_angle)
    ee285.wing_angle(1, left_wing_target_angle)

