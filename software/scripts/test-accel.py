# This script tests flapping the wings on a Fractal Flyer.
# Its timing and constants are particular to the prototype
# Phil is working on, so don't apply them generally. E.g.,
# with different motor and voltage configurations PWM needs 
# to be calibrated so the two wings move evenly.

import time
import board
import busio
import adafruit_lis3dh
import math
import digitalio
import microcontroller
import pulseio
import neopixel

#SPI setup: used for body (center) accelerometer
from digitalio import DigitalInOut, Direction
#spi = busio.SPI(board.SCK, board.MOSI, board.MISO)
#cs_center = DigitalInOut(board.D6)
#sensor_center = adafruit_lis3dh.LIS3DH_SPI(spi, cs_center)

#I2C setup: used for wing accelerometers
i2c = busio.I2C(board.SCL, board.SDA)
sensor_right = adafruit_lis3dh.LIS3DH_I2C(i2c)
sensor_left = adafruit_lis3dh.LIS3DH_I2C(i2c, address=0x19)

#LED setup

#motor_right_forward = pulseio.PWMOut(board.RX, frequency=MOTOR_PWM_FREQUENCY)
#motor_right_backward = pulseio.PWMOut(board.TX, frequency=MOTOR_PWM_FREQUENCY)
'''Returns the acceeleration of the entire bird (top plate acceleration)'''
def get_bird_accel():
        return (0, 0, 9.8) 

'''Gets the angle of the left wing if left=True, right if left=False, after subtracting out the
acceleration of the entire bird.'''
def get_wing_angle(left):
    global sensor_left, sensor_right
    bird_x, bird_y, bird_z = get_bird_accel()
    
    if (bird_x < -2.0 or bird_x > 2.0 or bird_y < -2.0 or bird_y > 2.0 or bird_z < 9.0 or bird_z > 11.0):
        raise Exception("Bird acceleration is wrong: x: ", bird_x, "y:", bird_y, "z:", bird_z) from None
    
    accel_x = 0.0
    accel_y = 0.0
    accel_z = 0.0
    if left:
        accel_x, accel_y, accel_z = sensor_left.acceleration
    else:
        accel_x, accel_y, accel_z = sensor_right.acceleration

    if accel_x == 0.0 and accel_y == 0.0 and accel_z == 0.0:
        print("left: ", left, "x:", accel_x, 'y:', accel_y, 'z:', accel_z)
        print("Resetting accelerometer!")
        if left:
            sensor_left = adafruit_lis3dh.LIS3DH_I2C(i2c, address=0x19)
        else:
            sensor_right = adafruit_lis3dh.LIS3DH_I2C(i2c)

    if accel_z == 0:
        accel_z = 0.0001
    accel_x -= bird_x
    accel_y -= bird_y
    #print("dx:", accel_x, 'dy:', accel_y, 'dz:', accel_z)

    tan_pitch = accel_y / accel_z
    #print('tan:', tan_pitch)

    angle_rads = math.atan(tan_pitch)
    angle_degrees = angle_rads * 180 / math.pi
    #print('degrees:', angle_degrees)
    return angle_degrees * -1

# Main loop
while True:
    time.sleep(0.01)
    angle = get_wing_angle(True)
    print("Left angle is", angle)
    angle = get_wing_angle(False)
    print("Right angle is", angle)

