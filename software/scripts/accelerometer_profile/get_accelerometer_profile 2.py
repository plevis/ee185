import argparse
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

time_scale = 20

def plot_accelerometer_profile(df, wing):
    fig, ax = plt.subplots()
    angles = df["current_angle"].to_numpy()

    #plot a histogram, fit a normal distribution to angles, and plot it on top of the histogram highlighting the mean and the std
    n, bins, patches = ax.hist(angles, bins=30, density=True, alpha=0.75)
    mu, sigma = np.mean(angles), np.std(angles)
    y = ((1 / (np.sqrt(2 * np.pi) * sigma)) * np.exp(-0.5 * ((bins - mu) / sigma) ** 2))
    ax.plot(bins, y, '--')

    #add text on the graph, for the mean and the variance
    ax.text(0.05, 0.95, f'$\mu={mu:.2f}$\n$\sigma={sigma:.2f}$', verticalalignment='top', horizontalalignment='left', transform=ax.transAxes, fontsize=15)

    ax.set_xlabel("Angle (degrees)")
    ax.set_ylabel("Probability")
    ax.set_title("Histogram of the current angle for {} wing".format("left" if wing == 1 else "right"))

    plt.show()

def plot_graphs_from_df(df):
    fig, ax = plt.subplots()
    t = np.array(list(range(0, len(df)))) * time_scale
    ax.plot(t, df["current_angle"].to_numpy(), label="current_angle")
    ax.plot(t, df["target_angle"].to_numpy(), label="target_angle")

    ax.set_xlim([0, len(df) * time_scale])
    ax.set_xlabel("Time (ms)")
    ax.set_ylabel("Angle (degrees)")

    ax.legend()
    plt.show()

def plot_pid_params(df):
    fig, ax = plt.subplots()
    t = np.array(list(range(0, len(df)))) * time_scale
    ax2 = ax.twinx()
    ax2.plot(t, df["target_angle"].to_numpy(), label="target_angle", c = 'r')
    ax2.plot(t, df["current_angle"].to_numpy(), label="current_angle" , c = 'k')
    ax.plot(t, df["duty"].to_numpy(), label="duty_cycle")
    ax.plot(t, df["diff"].to_numpy() * df["Kp"].to_numpy(), label="P_comp")
    ax.plot(t, df["integral"].to_numpy() * df["Ki"].to_numpy(), label="I_comp")

    ax.set_xlim([0, len(df) * time_scale])
    ax.set_xlabel("Time (ms)")
    ax.set_ylabel("value ")

    ax.legend()
    plt.show()

parser = argparse.ArgumentParser()
parser.add_argument("--input_file", "-i", type=str, help="The text file to extract data from")

args = parser.parse_args()
lines = []
pairs = []

grepstring = "PID GRAPHING:"
with open(args.input_file, "r") as f:
    lines = [line.split("\t")[0].strip() for line in f.readlines() if line.startswith(grepstring)]


for line in lines[:-1]:
    print(line)
    pairs += [{inst.split(":")[0].strip() : float(inst.split(":")[1].strip()) for inst in line[len(grepstring):].split(",")}]

df = pd.DataFrame(pairs)
plot_accelerometer_profile(df[df.wing == 1], 1) # left
plot_accelerometer_profile(df[df.wing == 0], 0)

