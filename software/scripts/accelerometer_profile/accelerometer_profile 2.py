# This script tests flapping the wings on a Fractal Flyer.
# It uses the motor control loop implemented in the firmware.

import time
import ee285
import math

ee285.motor_control_loop_init()

DELAY = 0.05  # control loop delay (s)
PERIOD = 12  # Period (s) before changing target angle

time.sleep(1.0)
last_increment_time = time.time()

ee285.wing_angle(0, 0)   # right
ee285.wing_angle(1, 0)   # left

while True:
    continue;
