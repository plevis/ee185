# EE 285 test file for motor control PID. This script calls the function control.tasks() which runs the PID module Control in the circuitpython firmware. For the test done in Winter 2021, the flyer's body accelerometer values were hard coded while the wing accelerometer 
# was rotated in order to change the duty cycle value. 

## Note: This demo script was run on a SAM32 microcontroller during Winter 2021. 
## For the SAM32 microcontroller, the PWM output should appear on D43
## If using a Feather M4, for example, please see the comments in the firmware code for pinouts under PWM.c and Control.C
## To-do (if needed): This script can sometimes hang at times and not output PWM values unless you reset the board. 
##digitalio, pulseio

import ee285
import control

control.init_spi()    ##initialize spi for accelrometer sampling 
while(True):
    control.tasks(0)  #run PID loop to control PWM duty cycle 

