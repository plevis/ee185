# Full demo of working flyer

from ee285 import *
import time

'''Cycles through colors (used for LED patterns)'''
def wheel(pos):
    if pos > 255:
        pos = pos - 256
    # Input a value 0 to 255 to get a color value.
    # The colours are a transition r - g - b - back to r.
    if pos < 0 or pos > 255:
        r = g = b = 0
    elif pos < 85:
        r = int(pos * 3)
        g = int(255 - pos*3)
        b = 0
    elif pos < 170:
        pos -= 85
        r = int(255 - pos*3)
        g = 0
        b = int(pos*3)
    else:
        pos -= 170
        r = 0
        g = int(pos*3)
        b = int(255 - pos*3)
    return (r, g, b)


motor_control_loop_init()

counter = 0

while 1:
    wing_angle(0, 30)
    wing_angle(1, 30)
    wing_leds(0, (255, 255, 255), (255, 255, 255), (255, 255, 255))
    wing_leds(1, (255, 255, 255), (255, 255, 255), (255, 255, 255))
    body_leds(wheel(counter % 256))
    counter = counter + 37 
    time.sleep(15)

    wing_angle(0, -30)
    wing_angle(1, -30)
    wing_leds(0, (255, 255, 255), (255, 255, 255), (255, 255, 255))
    wing_leds(1, (255, 255, 255), (255, 255, 255), (255, 255, 255))
    body_leds(wheel(counter % 256))
    counter = counter + 37
    time.sleep(15)

print("repl.py: finished :)")



