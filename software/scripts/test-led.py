# This script is used to the LED strips connected to a Feather
# M4.  It also lights up both wings and the body cross with a 
# rotating segment of the hue wheel of HSV color.
#
# Author: Philip Levis <pal@cs.stanford.edu>

import time
import board
import busio
import math
import digitalio
import microcontroller
import pulseio
import neopixel

LEDS_PER_WING = 180
LEDS_IN_BODY = 200

#LED setup
left_wing_lights = neopixel.NeoPixel(board.D10, LEDS_PER_WING, brightness=1, auto_write=False)
body_lights = neopixel.NeoPixel(board.D11, LEDS_IN_BODY, brightness=1, auto_write=False)
right_wing_lights = neopixel.NeoPixel(board.D12, LEDS_PER_WING, brightness=1, auto_write=False)

'''Cycles through colors (used for LED patterns)'''
def wheel(pos):
    # Input a value 0 to 255 to get a color value.
    # The colours are a transition r - g - b - back to r.
    if pos < 0 or pos > 255:
        r = g = b = 0
    elif pos < 85:
        r = int(pos * 3)
        g = int(255 - pos*3)
        b = 0
    elif pos < 170:
        pos -= 85
        r = int(255 - pos*3)
        g = 0
        b = int(pos*3)
    else:
        pos -= 170
        r = 0
        g = int(pos*3)
        b = int(255 - pos*3)
    return (r, g, b)

def set_body_led_pattern(pattern):
    for i in range(0, LEDS_IN_BODY):
        body_lights[i] = pattern[i]
    body_lights.show()

def set_wing_led_pattern(left, pattern):
    if left:
        for i in range(0, LEDS_PER_WING):
            left_wing_lights[i] = pattern[i]
        left_wing_lights.show()
    else:
        for i in range(0, LEDS_PER_WING):
            right_wing_lights[i] = pattern[i]
        right_wing_lights.show()


pattern = []
for i in range(0, 512):
  pattern.append(wheel(int(i/2) % 256))


counter = 0

while True:
    #TODO: Pattern LEDS here. For now, simulate with delay
    #If desired, smoother operation could be achieved by only changing lights with wings stationary

    set_wing_led_pattern(False, pattern[counter:])
    set_wing_led_pattern(True, pattern[counter:])
    set_body_led_pattern(pattern[counter:])
    counter = (counter + 1) % 256



