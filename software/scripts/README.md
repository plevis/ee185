# FLIGHT: Packard Interactive Light Sculpture Project

This directory stores Python scripts and libraries for Fractal Flyers.
It has the following example scripts. To install a script, copy it
to CIRCUITPY and name it `code.py`.
  - `flapping-i2c.py`: A script that flaps the left and right wings 
  together, from -45 to 45 degrees. It assumes the left and right
  wing sensors are I2C LIS3DH and the body sensor is an LISD3DH over SPI. 
  The left sensor has I2C address 0x19 and the right sensor has 0x17.
  The center sensor has a chip select line of D6. The left motor is on
  pins A1/A3 and the right motor is on pins RX/TX.
  - `testboard-v2`: A script that runs on version 2 of the wing testboard.
  This is a [red printed circuit board](testboard-v2.png) that you can
  plug two wing LEDs into: one can be a known working wing as a control
  (that the script is working) and the second can be a wing to test. Version
   1 of the board is purple.
  The script checks whether any of the pins on the wing connector are
  shorted with one another, and if not, tries displaying a changing 
  color pattern. You install this script on a Feather M4 Express board
  running CircuitPython. You must connect a 5V power supply to the board
  to power the LEDs (expected current is ~3A). The CircuitPython board
  must be powered separately, through its USB connector. After you plug in
  a wing you should hit the reset button on the board to run the wing
  connection diagnostics.
It also has these library modules:
  - `adafruit_lis3dh.py` is the Adafruit library for the LIS3DH sensor.
  Put it in the `lib` directory of CIRCUITPY.
  - `neopixel.mpy` is the Adafruit library for controlling neopixel
  LEDs. Put it in the `lib` directory of CIRCUITPY.
  - `adafruit_bus_device` is the Adafruit library for using the SPI
  and I2C buses. Put this direcotry in the `lib` directory of
  CIRCUITPY.
`PID_testing/` contains various python scripts which can be helpful when
fitting various components of the PID loop, along with various scripts 
which plot various observed components.

`accelerometer_profile/` contains python scripts to aid noise 
characterisation of the accelerometers.
