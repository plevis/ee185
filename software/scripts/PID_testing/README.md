This folder contains various pythons scripts to aid the tuning of
various parameters in a PID loop.

`pid_debug_script.py` is a python script that can be copied onto the
Feather M4, to set KP and KI to the desired values. The `screen` command
with the `-L` flag (with an additional `-Logfile` flag for a custom file 
name), can be used to log the `printf`s in the feather M4 firmware and 
the python script to a file (which is named `screenlog.0` by default).

`plot_graphs.py` will plot the relevant components of the PID loop, given
the output of the firmware, via the `-i` flag. 

`interpolated_angles.py` is just an experiment to move both wings in sync 
at all times.

`testing_outputs/` contains some outputs of the firmware, and can be used
as a reference.