import argparse
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

time_scale = 20
def plot_graphs_from_df(df):
    fig, ax = plt.subplots()
    t = np.array(list(range(0, len(df)))) * time_scale
    ax.plot(t, df["current_angle"].to_numpy(), label="current_angle")
    ax.plot(t, df["target_angle"].to_numpy(), label="target_angle")

    ax.set_xlim([0, len(df) * time_scale])
    ax.set_xlabel("Time (ms)")
    ax.set_ylabel("Angle (degrees)")

    ax.legend()
    plt.show()

def plot_pid_params(df):
    fig, ax = plt.subplots()
    t = np.array(list(range(0, len(df)))) * time_scale
    ax2 = ax.twinx()
    current_angles = df["current_angle"].to_numpy()
    # window_size = 10
    # moving_avg = np.convolve(current_angles, np.ones(window_size) * (1 / window_size))[window_size-1:]
    # ax2.plot(t, moving_avg, label="angle_avg")
    l1 = ax2.plot(t, current_angles, label="Wing Angle", c = 'k')
    l2 = ax2.plot(t, df["target_angle"].to_numpy(), label="Target Angle", c = 'r')
    l3 = ax.plot(t, df["duty"].to_numpy(), label="Duty Cycle")
    l4 = ax.plot(t, df["diff"].to_numpy() * df["Kp"].to_numpy(), label="Proportional Component")
    l5 = ax.plot(t, df["integral"].to_numpy() * df["Ki"].to_numpy(), label="Integral Component")

    lns = l1 + l2 + l3 + l4 + l5
    labs = [l.get_label() for l in lns]
    ax.legend(lns, labs, loc=0)
    ax2.set_ylabel("Angle (degrees)")
    ax.set_xlim([0, len(df) * time_scale])
    ax.set_xlabel("Time (ms)")
    ax.set_ylabel("Controller Values")
    plt.show()

parser = argparse.ArgumentParser()
parser.add_argument("--input_file", "-i", type=str, help="The text file to extract data from")

args = parser.parse_args()
lines = []
pairs = []

grepstring = "PID GRAPHING:"
with open(args.input_file, "r") as f:
    lines = [line.split("\t")[0].strip() for line in f.readlines() if line.startswith(grepstring)]


for line in lines[:-1]:
    print(line)
    pairs += [{inst.split(":")[0].strip() : float(inst.split(":")[1].strip()) for inst in line[len(grepstring):].split(",")}]

df = pd.DataFrame(pairs)
plot_pid_params(df[df.wing == 1]) # left
plot_pid_params(df[df.wing == 0])

# plot_graphs_from_df(df[df.wing == 1])
# plot_graphs_from_df(df[df.wing == 0])

