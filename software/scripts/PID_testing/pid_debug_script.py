# This script tests flapping the wings on a Fractal Flyer.
# It uses the motor control loop implemented in the firmware.

import time
import ee285

ee285.motor_control_loop_init()

angle_sweep = []

for i in range(-40, 40+1, 5):
    angle_sweep.extend([i, -i])

period = 15  # Period (s) before changing target angle


step = 0
target_angle = angle_sweep[0]

ee285.set_KP(0.3015)
ee285.set_KI(0.035)

last_increment_time = time.time()

while True:
    current_time = time.time()

    #TODO: Pattern LEDS here. For now, simulate with delay
    time.sleep(0.01)
    #If desired, smoother operation could be achieved by only changing lights with wings stationary

    #TODO: Any communication here (change behavior by modifying global variables)

    if current_time - last_increment_time >= period:
        target_angle = angle_sweep[step]
        print("Wings moving to angle", target_angle)
        # Set left and right wings to desired angle
        ee285.wing_angle(0, target_angle)  # right
        ee285.wing_angle(1, target_angle)  # left

        step += 1
        if step == len(angle_sweep):
            step = 0
        last_increment_time = current_time


