import argparse
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

time_scale = 20
def plot_graphs_from_df(df):
    fig, ax = plt.subplots()
    t = np.array(list(range(0, len(df)))) * time_scale
    ax.plot(t, df["current_angle"].to_numpy(), label="current_angle")
    ax.plot(t, df["target_angle"].to_numpy(), label="target_angle")

    ax.set_xlim([0, len(df) * time_scale])
    ax.set_xlabel("Time (ms)")
    ax.set_ylabel("Angle (degrees)")

    ax.legend()
    plt.show()

def plot_pid_params(df):
    fig, ax = plt.subplots()
    t = np.array(list(range(0, len(df)))) * time_scale
    ax2 = ax.twinx()
    ax2.plot(t, df["target_angle"].to_numpy(), label="target_angle", c = 'r')
    ax2.plot(t, df["current_angle"].to_numpy(), label="current_angle" , c = 'k')
    ax.plot(t, df["duty"].to_numpy(), label="duty_cycle")
    ax.plot(t, df["diff"].to_numpy() * df["Kp"].to_numpy(), label="P_comp")
    ax.plot(t, df["integral"].to_numpy() * df["Ki"].to_numpy(), label="I_comp")

    ax.set_xlim([0, len(df) * time_scale])
    ax.set_xlabel("Time (ms)")
    ax.set_ylabel("value ")

    ax.legend()
    plt.show()

parser = argparse.ArgumentParser()
parser.add_argument("--input_file", "-i", type=str, help="The text file to extract data from")

args = parser.parse_args()
lines = []
pairs = []

grepstring = "PID GRAPHING:"
with open(args.input_file, "r") as f:
    lines = [line.split("\t")[0].strip() for line in f.readlines() if line.startswith(grepstring)]


for line in lines[:-1]:
    print(line)
    pairs += [{inst.split(":")[0].strip() : float(inst.split(":")[1].strip()) for inst in line[len(grepstring):].split(",")}]

df = pd.DataFrame(pairs)
plot_pid_params(df[df.wing == 1]) # left
plot_pid_params(df[df.wing == 0])

# plot_graphs_from_df(df[df.wing == 1])
# plot_graphs_from_df(df[df.wing == 0])

