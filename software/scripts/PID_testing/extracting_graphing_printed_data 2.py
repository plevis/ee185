from matplotlib import pyplot as plt
import numpy as np

# Takes in a text file and string that labels the floating points that you would like to find
# Returns a list of all the floating point data coming after the label string
def extract_data(text_file, string_label) :

    # Set up
    f = open(text_file, "r")
    data_list = []
    label_size = len(string_label)

    # Read each line from the file and extract data if present
    while True:
        # Get next line from file
        line = f.readline()

        # If line is empty
        # End of file is reached
        if not line:
            break

        # Find data cycle data
        data_index = line.find(string_label)
        
        
    # If found, extract data
    # Our floats are printed with 5 decimal places
        if data_index != -1 :
            # Go to actual index of the data rather than the label
            float_start_index = data_index + label_size

            # Find decimal point index and add 5 decimal points to find size of floating point number
            float_size = line[float_start_index:].find(".") + 5

            # Add floating point data into list
            data_point = float(line[float_start_index: float_start_index + float_size + 1])
            if data_point > 0.8 :
                print(line)
                print(data_point)
                print(data_index)
            data_list.append(float(line[float_start_index: float_start_index + float_size + 1]))

    
    f.close()
    
    return data_list

# Plots line graph given y data
def plot_data(y_data, x_label, y_label, title) :
    plt.plot(y_data)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.title(title)
    plt.show()  
    
# Main :
# find and print data
text_file = "/Users/francesraphael/school/cs241/ee185/software/scripts/PID_testing/testing_outputs/manual_tune.txt"
data_label = "duty_right: "
data_list = extract_data(text_file, data_label)
title = "Calculated PID Right Wing Duty Cycles: KI = 0.005 & KP = 0.005"
y_title = "Calculated Right Wing Duty Cycle"
plot_data(data_list[0:10000], "Time Interval (20 ms)", y_title, title)

