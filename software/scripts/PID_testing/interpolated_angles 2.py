# This script tests flapping the wings on a Fractal Flyer.
# It uses the motor control loop implemented in the firmware.

import time
import ee285
import math


def interpolate_angles(current_angle, target_angle, total_time, delay):
    angle_sweep = []
    # Calculate the angle increment per step
    angle_increment = (target_angle - current_angle) / (total_time / delay)

    next_angle = current_angle

    while (current_angle - target_angle) * (next_angle - target_angle) >= 0:
        next_angle = next_angle + angle_increment
        angle_sweep.append(next_angle)
    return angle_sweep


ee285.motor_control_loop_init()

DELAY = 0.05  # control loop delay (s)
PERIOD = 12  # Period (s) before changing target angle

target_angles = []

for i in range(-45, 45+1, 5):
    target_angles.extend([i, -i])

target_angle_index = 0
step = 0

time.sleep(1.0)
last_increment_time = time.time()

print(ee285.get_wing_angle(1))
print(target_angles[0])

current_sweep_left = interpolate_angles(ee285.get_wing_angle(1), target_angles[0], PERIOD, DELAY)
current_sweep_right = interpolate_angles(ee285.get_wing_angle(0), target_angles[0], PERIOD, DELAY)

while True:
    current_time = time.time()

    time.sleep(DELAY)

    if current_time - last_increment_time >= PERIOD:
        target_angle_index += 1
        if target_angle_index == len(target_angles):
            target_angle_index = 0
        target_angle = target_angles[target_angle_index]
        print("Wings moving to angle", target_angle)

        current_sweep_left = interpolate_angles(ee285.get_wing_angle(1), target_angle, PERIOD, DELAY)
        current_sweep_right = interpolate_angles(ee285.get_wing_angle(0), target_angle, PERIOD, DELAY)
        step = 0
        last_increment_time = current_time

    # Set left and right wings to desired angle
    ee285.wing_angle(0, current_sweep_right[step])   # right
    ee285.wing_angle(1, current_sweep_left[step])   # left

    step += 1
