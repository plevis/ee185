from matplotlib import pyplot as plt
import numpy as np

# Function to extract the current_angle and target_angle from the text file
def extract_angles(text_file):
    current_angles = []
    target_angles = []
    
    # Open the file
    with open(text_file, "r") as f:
        # Read each line and extract relevant data
        for line in f:
            if "PID GRAPHING:" in line:
                # Find the index of 'current_angle' and 'target_angle'
                current_index = line.find("current_angle:")
                target_index = line.find("target_angle:")
                
                if current_index != -1 and target_index != -1:
                    # Extract current_angle and target_angle values
                    current_angle = float(line[current_index+14:line.find(",", current_index)])
                    target_angle = float(line[target_index+13:line.find(",", target_index)])
                    
                    # Append to respective lists
                    current_angles.append(current_angle)
                    target_angles.append(target_angle)

    return current_angles, target_angles

# Function to plot the angles
def plot_angles(current_angles, target_angles):
    plt.plot(current_angles, label="Current Angle", color='b')
    plt.plot(target_angles, label="Target Angle", color='r', linestyle='--')
    
    # Adding labels and title
    plt.xlabel("Sample Number")
    plt.ylabel("Angle (Degrees)")
    plt.title("Current vs Target Angle")
    plt.legend()
    plt.show()

# Main:
text_file = "/Users/francesraphael/school/cs241/ee185/software/scripts/PID_testing/debug 2.txt"

# Extract the angles
current_angles, target_angles = extract_angles(text_file)

# Plot the data
plot_angles(current_angles, target_angles)

