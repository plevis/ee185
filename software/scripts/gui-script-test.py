import ee285
import time
#TODO: make generator add time.sleep or no?
ee285.motor_control_loop_init()

# —-------------- Disable the dummy load —--------------
import board
import digitalio

# Set up A3 as a digital output
pin_A3 = digitalio.DigitalInOut(board.A3)
pin_A3.direction = digitalio.Direction.OUTPUT

# Set the pin low (0V)
pin_A3.value = False # power_en tied low - don't run dummy load + burn things
time.sleep(3)
print("gui-script-test.py: Starting generated python commands")

# —-------------- Test GUI script generation —--------------
LEFT = 0
RIGHT = 1

for i in range(2):
    time.sleep(0.01)
    ee285.wing_angle(LEFT, -5)
    ee285.wing_angle(RIGHT, 22)
    ee285.body_leds((0, 0, 0))
    ee285.wing_leds(LEFT, (0, 0, 0), (0, 0, 0), (0, 0, 0))
    ee285.wing_leds(LEFT, (0, 0, 0), (0, 0, 0), (0, 0, 0))
    time.sleep(0.01)


    ee285.wing_angle(LEFT, -26)
    ee285.wing_angle(RIGHT, 44)
    time.sleep(0.01)


    ee285.wing_angle(LEFT, -43)
    ee285.wing_angle(RIGHT, 62)


print("gui-script-test.py: finished :)")