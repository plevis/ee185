# FLIGHT: Packard Interactive Light Sculpture Project

This directory contains the animation application for FLIGHT, based upon a custom build of the Chromatik / LX software framework.

## Dependencies

Install Maven and a 64-bit distribution of Java 17. Recommend using a Temurin build from [Adoptium](https://adoptium.net/). You can confirm these are correctly configured via `java -version` and `mvn -v`

## Building and Running

- The first time you check out the project, run `mvn validate` one time to import the LX / Chromatik dependencies into your system's local Maven repository
- `% mvn compile` builds the application
- `% mvn exec:exec` will run the application

You can do both at once with `mvn compile exec:exec`. Note that the `-XstartOnFirstThread` VM argument is required on macOS. This should be automatically detected and included by `mvn exec:exec`.

## Chromatik / LX documentation

- [Javadocs](https://chromatik.co/api/)
- [Chromatik User Guide](https://chromatik.co/guide/)
