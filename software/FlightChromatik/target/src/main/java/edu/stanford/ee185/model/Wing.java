package edu.stanford.ee185.model;

public interface Wing extends FlightGeometry {

  int flyerIndex = 0;

  boolean isRight = false;

  int getFlyerIndex();
  boolean getIsRight();

  int getIndex();


  int getSkew();
  void setSkew(int skew);

}
