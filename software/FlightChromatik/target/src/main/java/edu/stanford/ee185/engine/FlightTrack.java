package edu.stanford.ee185.engine;

import java.io.File;

/**
 * FlightTrack is the interface that defines all the functionality needed by the
 * UI in order to record, play, stop, load and save a FLIGHT track
 * 
 * @author Moses Swai
 */
public interface FlightTrack {

    /** Sets up and starts recording a track */
    void startRecording();

    /** Stops recording a track. */
    void stopRecording();

    /** Starts playback of a loaded track */
    void playbackTrack(int trackIdx);

    /**
     * Specifies whether a track should be played once or looped.
     */
    void setTrackLooping(boolean looping);

    /** Stops recording or playback of a track */
    void stopTrack();

    /**
     * Loads a track from a file
     * 
     * @param filename The relative path of the track file to be loaded
     */
    void loadTrack(String filename);

    /**
     * Loads a track from a file
     * 
     * @param file The file object of the track file to be loaded
     */
    void loadTrack(File file);

    void removeTrack(int index);

    /**
     * Saves a loaded or recorded track onto a file
     * 
     * @param filename The relative path of the track file to be saved
     */
    void saveRecordingToTrack(String filename);

    /**
     * Saves a loaded or recorded track onto a file
     * 
     * @param file The file object of the track file to be saved
     */
    void saveRecordingToTrack(File file);

}
