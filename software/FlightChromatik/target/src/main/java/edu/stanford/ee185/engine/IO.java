package edu.stanford.ee185.engine;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.lang.reflect.Type;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import edu.stanford.ee185.model.FlyerConfig;

/**
 * IO class contains all the functions that are used for reading and writing
 * files by the UI. This involves loading and saving of flyer configurations and
 * tracks. All files are saved in .json format. The path for all files should be
 * relative to the project path passed when constructing the object
 * 
 * @author Moses Swai
 */
public class IO {

  final String projectPath;

  /**
   * CONSTRUCTOR
   * 
   * @param projectPath The top level project path from which the UI is run
   */
  public IO(String projectPath) {
    this.projectPath = projectPath;
  }

  /***** PUBLIC FUNCTIONS *****/

  /**
   * Loads a flyer configuration from a JSON file
   * 
   * @param filename The name of the configuration file relative to the project
   *                 path
   * @return A FlyerConfig array
   */
  public FlyerConfig[] loadConfigFile(String filename) {
    return loadJSONFile(createPath(filename), new TypeToken<FlyerConfig[]>() {
    }.getType());
  }

  /**
   * Saves the current flyer configuration to a JSON file
   * 
   * @param config   A FlyerConfig array
   * @param filename The name of the configuration file to be saved relative to
   *                 the project path
   */
  public void saveConfigFile(FlyerConfig[] config, String filename) {
    saveJSONToFile(config, createPath(filename));
  }

  /**
   * Loads a saved FLIGHT track from a JSON file
   * 
   * @param filename The name of the track file relative to the project path
   * @return A JsonArray object with the track
   */
  public JsonElement loadTrackFile(String filename) {
    return loadJSONFile(createPath(filename), JsonElement.class);
  }

  /**
   * Loads a saved FLIGHT track from a JSON file
   * 
   * @param file A File object specifying the track file within the project path
   * @return A JsonArray object with the track
   */
  public JsonElement loadTrackFile(File file) {
    return loadJSONFile(file.getPath(), JsonElement.class);
  }

  /**
   * Saves a FLIGHT track set to a JSON file
   * 
   * @param array    A JsonArray with the track
   * @param filename The name of the track file to be saved relative to the
   *                 project path
   */
  public void saveTrackFile(JsonArray array, String filename) {
    System.out.println(filename);
    saveJSONToFile(array, createPath(filename));
  }

  /**
   * Saves a FLIGHT track to a JSON file
   * 
   * @param array A JsonArray with the track
   * @param file  A file object specifying the track file to be saved within the
   *              project path
   */
  public void saveTrackFile(JsonArray array, File file) {
    saveJSONToFile(array, file.getPath());
  }

  /**
   * Saves a FLIGHT playlist set to a JSON file
   * 
   * @param playlist    A JsonObject with the info and tracks
   * @param filename        The name of the playlist file to be saved relative to the
   *                        project path
   */
  public void savePlaylistFile(JsonObject playlist, String filename) {
    System.out.println(filename);
    saveJSONToFile(playlist, createPath(filename));
  }

  /**
   * Saves a FLIGHT playlist set to a JSON file
   * 
   * @param playlist    A JsonObject with the info and tracks
   * @param file            A file object specifying the track file to be saved within the
   *                        project path
   */
  public void savePlaylistFile(JsonObject playlist, File file) {
    saveJSONToFile(playlist, file.getPath());
  }
  
  /**
   * Loads a saved output file that contains all the file names for generator
   * outputs from a JSON fiel
   * 
   * @param filename The name of the output file relative to the project path
   * @return A String array
   */
  public String[] loadOutputFile(String filename) {
    return loadJSONFile(createPath(filename), String[].class);
  }

  /**
   * Saves the generator output names to a JSON file
   * 
   * @param array    A String array with the generator output names
   * @param filename The name of th output file to be saved relative to the
   *                 project path
   */
  public void saveOutputFile(String[] array, String filename) {
    saveJSONToFile(array, createPath(filename));
  }

  /** 
   * Concatenates the file name to the project path.
   * @param filename The name of the file relative to the project path.
   * @return A String of the absolute path to the file.
   *  */
  public String createPath(String filename) {
    return projectPath + File.separator + filename;
  }

  /*
   * Loads a JSON File from the specified 'path' and into the given type
   * 'typeToken'
   */
  private static <T> T loadJSONFile(String path, Type typeToken) {
    try (Reader reader = new BufferedReader(new FileReader(path))) {
      return new Gson().fromJson(reader, typeToken);
    } catch (IOException ioe) {
      System.out.print("Error reading json file: ");
      System.out.println(ioe.getMessage());
    }
    return null;
  }

  /* Serialize and save flyerConfig list 'config' to Json */
  private static void saveJSONToFile(FlyerConfig[] config, String path) {
    try (PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(path)))) {
      writer.write(new GsonBuilder().setPrettyPrinting().create().toJson(config));
    } catch (IOException ioe) {
      System.err.println("Error writing List to json file.");
    }
  }

  /* Serialize and save a track 'array' to Json */
  private static void saveJSONToFile(JsonArray array, String path) {
    try (PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(path)))) {
      writer.write(new GsonBuilder().setPrettyPrinting().create().toJson(array));
    } catch (IOException ioe) {
      System.err.println("Error writing JsonArray to json file.");
    }
  }

  /* Serialize and save an output names 'array' to Json */
  private static void saveJSONToFile(String[] array, String path) {
    try (PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(path)))) {
      writer.write(new GsonBuilder().setPrettyPrinting().create().toJson(array));
    } catch (IOException ioe) {
      System.err.println("Error writing JsonArray to json file.");
    }
  }
  
  /* Serialize and save a playlist object to Json */
  private static void saveJSONToFile(JsonObject playlist, String path) {
    try (PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(path)))) {
      writer.write(new GsonBuilder().setPrettyPrinting().create().toJson(playlist));
    } catch (IOException ioe) {
      System.err.println("Error writing JsonArray to json file.");
    }
  }
}