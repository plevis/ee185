package edu.stanford.ee185.model;

public class Model implements FlightGeometry {
  static FlightModel flight;

  public Model(FlyerConfig[] flyerConfig) {
    flight = new FlightModel(flyerConfig);
  }


  public FlightModel getFlightModel() {
    return flight;
  }

}

