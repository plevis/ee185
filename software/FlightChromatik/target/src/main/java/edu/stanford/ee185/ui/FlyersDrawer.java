package edu.stanford.ee185.ui;
import edu.stanford.ee185.engine.FlyerHighlighter;
import edu.stanford.ee185.model.*;


import heronarts.glx.GLX;
import heronarts.glx.VertexBuffer;
import heronarts.glx.VertexDeclaration;
import heronarts.glx.View;
import heronarts.glx.ui.UI;
import heronarts.lx.LX;
import heronarts.lx.LXEngine;
import heronarts.lx.studio.FlightApp;
import heronarts.lx.utils.LXUtils;
import heronarts.lx.color.LXColor;
import static org.lwjgl.bgfx.BGFX.bgfx_set_transform;

// To work with Vertex Buffers
import org.lwjgl.system.MemoryUtil;

import java.nio.ByteBuffer;

/**
 * The FLIGHT GUI window that draws the stairwell of the Packard
 * building at Stanford with FLIGHT's Fractal Flyers. The Fractal
 * Flyers visualize light colors and wing position.
 *
 * Drawing relies heavily on matrices to draw things in the right
 * places with simple logic. You can think of a matrix as a stored
 * represention of what (0, 0, 0) and the direction of the axes are
 * when we draw something. For example, the code to draw a Flyer draws
 * it with its center of mass at (0, 0, 0), its tail at (-x1, 0, 0)
 * and its head at (x2, 0, 0). To draw a flyer at (50, 50, 50) and
 * pointing in the Z direction we create a new matrix, translate to
 * (50, 50, 50), rotate to Z, draw the flyer, then discard ("pop") the
 * matrix.
 *
 * @author Gordon Martinez-Piedra <martigp@stanford.edu>
 */
public class FlyersDrawer extends FlightDrawer implements FlightGeometry {
    FlightApp app;
    FlightModel model;
    FlyerHighlighter flyerHighlighter;

    private FlyerHangingWire wireBuffers[];
    // private final VertexBuffer.UnitCube cubeBuffer;
    private final FlyerLeftWing leftWingBuffer;
    private final FlyerRightWing rigthWingBuffer;
    private final FlyerTopPlate topPlateBuffer;
    private final FlyerBodyShell bodyShellBuffer;

    /* This mimics the PGraphics high level library of push and pop Matrix.
     * Allows a simple way to save previous model matrix used.
     */

    private class FlyerHangingWire extends VertexBuffer {
        public static final int NUM_VERTICES = 2;
        public float wireHeight;

        private FlyerHangingWire(GLX glx, float wireHeight) {
            super(glx, NUM_VERTICES, VertexDeclaration.ATTRIB_POSITION);
            this.wireHeight = wireHeight;
        }

        @Override
        protected void bufferData(ByteBuffer buffer) {
            putVertex(0f, 0f, 0f);
            putVertex(0f, this.wireHeight, 0f);
            // putVertex(0.25f, this.wireHeight, 0.25f);
            // putVertex(-0.25f, this.wireHeight, 0.25f);
            // putVertex(0.25f, 0f, 0.25f);
            // putVertex(-0.25f, 0f, 0.25f);
            // putVertex(-0.25f, 0f, -0.25f);
            // putVertex(-0.25f, this.wireHeight, 0.25f);
            // putVertex(-0.25f, this.wireHeight, -0.25f);
            // putVertex(0.25f, this.wireHeight, 0.25f);
            // putVertex(0.25f, this.wireHeight, -0.25f);
            // putVertex(0.25f, 0f, 0.25f);
            // putVertex(0.25f, 0f, -0.25f);
            // putVertex(-0.25f, 0f, -0.25f);
            // putVertex(0.25f, this.wireHeight, -0.25f);
            // putVertex(-0.25f, this.wireHeight, -0.25f);
        }
    }

    private class FlyerLeftWing extends VertexBuffer {

        public static final int NUM_VERTICES = 4;

        private FlyerLeftWing(GLX glx) {
            super(glx, NUM_VERTICES, VertexDeclaration.ATTRIB_POSITION);
        }

        @Override
        protected void bufferData(ByteBuffer buffer) {
            putVertex(0, 0, 0);
            putVertex(LXUtils.cosf(LX.PIf / 3f) * 7.87f, 0,
                LXUtils.sinf(LX.PIf / 3f) * 7.87f);
            putVertex(23.036f, 0, 0);
            putVertex(0, 0, 0);
        }

    }
    
    private class FlyerRightWing extends VertexBuffer {

        public static final int NUM_VERTICES = 4;

        private FlyerRightWing(GLX glx) {
            super(glx, NUM_VERTICES, VertexDeclaration.ATTRIB_POSITION);
        }

        @Override
        protected void bufferData(ByteBuffer buffer) {
            putVertex(0, 0, 0);
            putVertex(LXUtils.cosf(LX.PIf / 3f) * 7.87f, 0,
                -LXUtils.cosf(LX.PIf / 3f) * 7.87f);
            putVertex(23.036f, 0, 0);
            putVertex(0, 0, 0);
        }

    }
    
    private class FlyerTopPlate extends VertexBuffer {

        public static final int NUM_VERTICES = 5;
        public final int COLOR = LXColor.rgb(0xa0,0xa0, 0xa0);

        private FlyerTopPlate(GLX glx) {
            super(glx, NUM_VERTICES, VertexDeclaration.ATTRIB_POSITION);
            
        }

        @Override
        protected void bufferData(ByteBuffer buffer) {
            // Top Plate
            putVertex(0, 0, 0);
            putVertex(3.33f, 0, 4.04f);
            putVertex(26.26f, 0, 0);
            putVertex(3.33f, 0, -4.04f);
            putVertex(0, 0, 0);
        }
    }

    private class FlyerBodyShell extends VertexBuffer {

        public static final int NUM_VERTICES = 12;

        private FlyerBodyShell(GLX glx) {
            super(glx, NUM_VERTICES, VertexDeclaration.ATTRIB_POSITION);
        }

        @Override
        protected void bufferData(ByteBuffer buffer) {
            // Top Plate
            putVertex(0, 0, 0);
            putVertex(3.33f, 0, 4.04f);
            putVertex(5.29f, -4.39f, 0);
        
            putVertex(0, 0, 0);
            putVertex(3.33f, 0, -4.04f);
            putVertex(5.29f, -4.39f, 0);
        
            putVertex(26.26f, 0, 0);
            putVertex(3.33f, 0, 4.04f);
            putVertex(5.29f, -4.39f, 0);
        
            putVertex(26.26f, 0, 0);
            putVertex(3.33f, 0, -4.04f);
            putVertex(5.29f, -4.39f, 0);
        }

    }

    public FlyersDrawer(FlightApp app, FlightModel model, FlyerHighlighter flyerHighlighter) {
        super();
        this.app = app;
        this.model = model;
        this.flyerHighlighter = flyerHighlighter;

        this.wireBuffers = new FlyerHangingWire[model.getFlyers().size()];
        for (FlyerModel flyer : model.getFlyers()) {
            this.wireBuffers[flyer.getIndex()] = new FlyerHangingWire(app,
                flyer.getHangDistance());
        }
        this.leftWingBuffer = new FlyerLeftWing(app);
        this.rigthWingBuffer = new FlyerRightWing(app);
        this.topPlateBuffer = new FlyerTopPlate(app);
        this.bodyShellBuffer = new FlyerBodyShell(app);
    }

    @Override
    protected void onDraw(UI ui, View view) {
        try {
            drawFlyers(ui, view);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * Draw all of the Fractal Flyers.
     *
     */
    private void drawFlyers(UI ui, View view) {
        computeFlyerLightsAndWings();

        // We draw each flyer by first computing a geometric
        // transformation (matrix) for its position and orientation,
        // drawing it, then popping the tranformation.  This allows the
        // draw code to just operate in a normalized coordinate space. -pal
        for (FlyerModel flyer : model.getFlyers()) {
            float x = flyer.getX();
            float y = flyer.getY();
            float z = flyer.getZ();
        
            pushMatrix();
            this.modelMatrix.identity().translate(x,y,z);
            // TODO: this is not doing anything because line is 1D, ask
            // Marc Slee how to do this.
            // this.wireBuffers[flyer.getIndex()] = new FlyerHangingWire(app, flyer.getHangDistance());
            // int wireColor = LXColor.rgb(128, 128, 128);

            // pushMatrix();
            // drawState |= BGFX_STATE_PT_LINES;
            // bgfx_set_transform(this.modelMatrix.get(this.modelMatrixBuf));
            // ui.lx.program.uniformFill.submit(view, drawState,  0x80808000,
            //     this.wireBuffers[flyer.getIndex()]);
            // popMatrix();
            this.modelMatrix.rotateY(-flyer.getRotation() * LX.PIf / 180f);
            this.modelMatrix.rotateZ(-flyer.getTilt() * LX.PIf / 180f);
            drawFlyer(ui, view, flyer);
            popMatrix();
        }
  }

    /**
     * Draw one Fractal Flyer at (0,0,0). its long dimension
     * along the X axis, its narrow dimension along the Z axis,
     * and its body hanging down in the Y axis.
     * All of these values are in inches, taken from
    // FRACTAL_FLYER/drawings/body-dimensions-for-ui-graphics.pdf
     */
    private void drawFlyer(UI ui, View view, FlyerModel flyer) {
        LXEngine.Frame frame = app.uiFrame;
        int colors[] = frame.getColors();

        // (0, 0, 0) is be at the hanging point, so translate backwards
        // so (0, 0, 0) is at the back corner, leaving the hanging offset
        // configurable.
        int curFillColor;


        pushMatrix();
        this.modelMatrix.translate(-FLYER_HANGING_OFFSET, 0, 0);

        // Top Plate
        bgfx_set_transform(this.modelMatrix.get(this.modelMatrixBuf));
        ui.lx.program.uniformFill.submit(view, drawStateTriStrip,
                                         this.topPlateBuffer.COLOR,
                                         this.topPlateBuffer);

        // Left wing
        Wing leftWing = flyer.getLeftWing();
        pushMatrix();
        this.modelMatrix.translate(4.01f, 0f, 4.96f).rotateY(LX.PIf / 18);
        curFillColor = colors[wingLightIndex(flyer.getIndex(), false, 0)];
        float langle = (float)leftWing.getSkew() / 90f * LX.PIf / 2f;
        this.modelMatrix.rotateX(langle);
        bgfx_set_transform(this.modelMatrix.get(this.modelMatrixBuf));
        ui.lx.program.uniformFill.submit(view, drawStateTriStrip, curFillColor,
            this.leftWingBuffer);
        popMatrix();
        

        // Right wing
        Wing rightWing = flyer.getRightWing();
        curFillColor = colors[wingLightIndex(flyer.getIndex(), true, 0)];
        pushMatrix();
        this.modelMatrix.translate(4.01f, 0f, -4.96f).rotateY(- LX.PIf/18);
        float rangle = (float)rightWing.getSkew() / 90f * LX.PIf / 2f;
        this.modelMatrix.rotateX(-rangle);
        bgfx_set_transform(this.modelMatrix.get(this.modelMatrixBuf));
        ui.lx.program.uniformFill.submit(view, drawStateTriStrip, curFillColor,
            this.rigthWingBuffer);
        popMatrix();

        // Body shell
        curFillColor = colors[bodyLightIndex(flyer.getIndex())];
        bgfx_set_transform(this.modelMatrix.get(this.modelMatrixBuf));
        ui.lx.program.uniformFill.submit(view, drawStateTriStrip, curFillColor,
            this.bodyShellBuffer);
        
        popMatrix();
    }

    /**
     * A hook for UI-specific additions to the visualization of the Fractal
     * Flyers. Used, for example, to allow the UI to highlight a Fractal
     * Flyer that has been selected in the physical layout tool.
     */

    @Override
    public void dispose() {
        this.topPlateBuffer.dispose();
        this.leftWingBuffer.dispose();
        this.rigthWingBuffer.dispose();
        this.bodyShellBuffer.dispose();
        for (FlyerHangingWire wireBuff : wireBuffers) {
            wireBuff.dispose();
        }

        MemoryUtil.memFree(this.modelMatrixBuf);
        super.dispose();
    }
    private void computeFlyerLightsAndWings() {
        // flyerHighlighter.highlight();
    }
}