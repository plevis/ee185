package edu.stanford.ee185.ui;

import static org.lwjgl.bgfx.BGFX.BGFX_STATE_BLEND_ALPHA;
import static org.lwjgl.bgfx.BGFX.BGFX_STATE_DEPTH_TEST_LESS;
import static org.lwjgl.bgfx.BGFX.BGFX_STATE_WRITE_RGB;
import static org.lwjgl.bgfx.BGFX.BGFX_STATE_WRITE_Z;
import static org.lwjgl.bgfx.BGFX.BGFX_STATE_PT_TRISTRIP;

import java.nio.FloatBuffer;
import java.util.Stack;

import org.joml.Matrix4f;
import org.lwjgl.system.MemoryUtil;

import heronarts.glx.ui.UI3dComponent;

public abstract class FlightDrawer extends UI3dComponent {

  protected Matrix4f modelMatrix = new Matrix4f();
  protected final FloatBuffer modelMatrixBuf;

  // The draw state specifies that we're drawing the RGB color buffer, the Z-depth buffer,
  // with alpha blending and depth testing for Z-collisions. The flags passed
  // into the pg.beginShape and pg.endShape APIs are passed into this.
  
  protected long drawStateTriangle = 
    BGFX_STATE_WRITE_RGB |
    BGFX_STATE_WRITE_Z |
    BGFX_STATE_BLEND_ALPHA |
    BGFX_STATE_DEPTH_TEST_LESS;
  
    protected long drawStateTriStrip =
      drawStateTriangle | BGFX_STATE_PT_TRISTRIP;
  
  
    protected final Stack<Matrix4f> modelMatrixStack;

  protected void pushMatrix() {
    this.modelMatrixStack.push(new Matrix4f(this.modelMatrix));
  }

  protected void popMatrix() {
    this.modelMatrix = new Matrix4f(this.modelMatrixStack.pop());
  }

  protected FlightDrawer() {
    // 4x4 matrix - just part of Java
    this.modelMatrixBuf = MemoryUtil.memAllocFloat(16);
    this.modelMatrix.get(this.modelMatrixBuf);
    this.modelMatrixStack = new Stack<Matrix4f>();
  }

  @Override
  public void dispose() {
    MemoryUtil.memFree(this.modelMatrixBuf);
    super.dispose();
  }

}
