package edu.stanford.ee185.ui;

import edu.stanford.ee185.model.FlightGeometry;
import heronarts.lx.model.LXModel.Geometry;
import heronarts.lx.studio.FlightApp;
import heronarts.glx.ui.UI3dComponent;
import heronarts.glx.View;
import heronarts.glx.GLX;
import heronarts.glx.VertexBuffer;
import heronarts.glx.VertexDeclaration;
import heronarts.glx.ui.UI;
import heronarts.lx.utils.LXUtils;
import heronarts.lx.color.LXColor;

import static org.lwjgl.bgfx.BGFX.bgfx_set_transform;

import java.nio.ByteBuffer;

public class PackardDrawer extends FlightDrawer implements FlightGeometry {
    // These variables are all derived from constants in FlightGeometry -pal

    // The X offset of the vertical bars on the front window. These are
    // used as reference points for drawing the overhead pipes as well
    // as the front window bars.
    private final float[] VBAR_OFFSETS;

    // The X and Z offset of reference points on the back wall. These
    // correspond to a subset of the VBAR_OFFSETS, but rotated -P/4 along
    // the Y axis.
    private final float[] BACK_WALL_OFFSETS;

    // The angle of the wall in radians between the front window and back
    // wall (45 degrees)
    protected final double WALL_ANGLE = Math.toRadians(45);

    protected final float BACK_CORNER_X = FRONT_WINDOW_WIDTH
        * LXUtils.cosf(WALL_ANGLE);

    protected final float BACK_CORNER_Z = FRONT_WINDOW_WIDTH
        * LXUtils.sinf(WALL_ANGLE);

    FlightApp app;

    // Walls
    FrontWindow frontWindowBuffer;
    BackWall backWallBuffer;
    Floor floorBuffer;
    WindowRoofBar roofBarBuffer;
    WindowVBARs windowVBARsBuffer;
    WindowHBARs windowHBARsBuffer;

    private class Cylinder extends VertexBuffer {

        public static final int NUM_VERTICES = 42;
        public static final float r = FlightGeometry.CEILING_PIPE_RADIUS;
        public float h;

        Cylinder(GLX glx, float h) {
            super(glx, NUM_VERTICES, VertexDeclaration.ATTRIB_POSITION);
            this.h = h;
        }

        /**
         * Draws a cylinder of radius r and height h, with one end
         * centered at (0, 0, 0) and the other at (0, 0, h). If you want
         * to move the cylinder, call this method having set up a matrix
         * so these coordinates are at your desired position. Used to draw
         * pipes in the ceiling of the stairwell. Uses whatever fill color
         * was specified before calling.
         */
        @Override
        protected void bufferData(ByteBuffer buffer) {
            int sides = 36;
            float angle = 360 / sides;
            // draw top shape
            for (int i = 0; i < sides; i++) {
                float x = LXUtils.cosf(Math.toRadians(i * angle)) * r;
                float y = LXUtils.sinf(Math.toRadians(i * angle)) * r;
                putVertex(x, y, 0);
            }
            putVertex(r, 0, 0);

            //TODO might need to separate these?
            // draw bottom shape
            for (int i = 0; i < sides; i++) {
                float x = LXUtils.cosf(Math.toRadians(i * angle)) * r;
                float y = LXUtils.sinf(Math.toRadians(i * angle)) * r;
                putVertex(x, y, h);
            }
        }
    }
    
    private class FrontWindow extends VertexBuffer {
        public static final int NUM_VERTICES = 5;
        public final int COLOR = LXColor.rgb(0xc0,0xc0, 0xc0);

        FrontWindow(GLX glx) {
            super(glx, NUM_VERTICES, VertexDeclaration.ATTRIB_POSITION);
        }

        @Override
        protected void bufferData(ByteBuffer buffer) {
            putVertex(0f, 0f, 0f);
            putVertex(FRONT_WINDOW_WIDTH, 0f, 0f);
            putVertex(FRONT_WINDOW_WIDTH, -180f, 0f);
            putVertex(0f, -180f, 0f);
            putVertex(0f, 0f, 0f);
        }
    }
    
    /**
     * Back wall of packard buildilng
     */
    private class BackWall extends VertexBuffer {
        public static final int NUM_VERTICES = 5;
        public final int COLOR = LXColor.rgb(0xf0, 0xf0, 0xf0);

        BackWall(GLX glx) {
            super(glx, NUM_VERTICES, VertexDeclaration.ATTRIB_POSITION);
        }

        @Override
        protected void bufferData(ByteBuffer buffer) {
            putVertex(0, -180f, 0);
            putVertex(0, FRONT_WINDOW_CORNER_HEIGHT, 0);
            putVertex(BACK_CORNER_X, FRONT_WINDOW_CORNER_HEIGHT, BACK_CORNER_Z);
            putVertex(BACK_CORNER_X, -180f, BACK_CORNER_Z);
            putVertex(0, -180f, 0);
        }
    }
    
    /**
     * The floor of the packard building (basement floor)
     */
    private class Floor extends VertexBuffer {
        public static final int NUM_VERTICES = 3;
        public final int COLOR = LXColor.rgb(0x80, 0x80, 0x80);

        Floor(GLX glx) {
            super(glx, NUM_VERTICES, VertexDeclaration.ATTRIB_POSITION);
        }

        @Override
        protected void bufferData(ByteBuffer buffer) {
            putVertex(0, -180f, 0);
            putVertex(FRONT_WINDOW_WIDTH, -180f, 0);
            putVertex(BACK_CORNER_X, -180f, BACK_CORNER_Z);
        }
    }

    /**
     * All Vertical Bars on the Packard Front Window.
     */
    private class WindowVBARs extends VertexBuffer {
        public static final int NUM_VERTICES_PER_BAR = 6;

        WindowVBARs(GLX glx) {
            super(glx,
                NUM_VERTICES_PER_BAR * FlightGeometry.FRONT_WINDOW_VBAR_SPACINGS.length,
                VertexDeclaration.ATTRIB_POSITION);
        }

        // Chromatik requires we draw all bars in a single buffer. Each 
        // Iteration of the for loop as two triangles.
        @Override
        protected void bufferData(ByteBuffer buffer) {
            int i = 0;
            for (float xPos : VBAR_OFFSETS) {
                float height = FRONT_WINDOW_VBAR_HEIGHTS[i];

                putVertex(xPos, 0, 0);
                putVertex(xPos + FRONT_WINDOW_BAR_WIDTH, 0, 0);
                putVertex(xPos, height, 0);

                putVertex(xPos + FRONT_WINDOW_BAR_WIDTH, 0, 0);
                putVertex(xPos, height, 0);
                putVertex(xPos + FRONT_WINDOW_BAR_WIDTH, height, 0);
                i++;
            }
        }
    }

    /**
     * All Horizontal bars on the front window of the Packard Building.
     */
    private class WindowHBARs extends VertexBuffer {
        public static final int NUM_VERTICES_PER_BAR = 6;

        WindowHBARs(GLX glx) {
            super(glx,
                NUM_VERTICES_PER_BAR * FRONT_WINDOW_HBAR_SPACINGS.length,
                VertexDeclaration.ATTRIB_POSITION);
        }

        // See comments for WindowVBAR, same concept.
        @Override
        protected void bufferData(ByteBuffer buffer) {
            float yPos = 0F;
            for (float spacing : FRONT_WINDOW_HBAR_SPACINGS) {
                yPos += spacing;
                putVertex(0f, yPos, 0f);
                putVertex(0f, yPos + FRONT_WINDOW_BAR_WIDTH, 0f);
                putVertex(FRONT_WINDOW_WIDTH, yPos + FRONT_WINDOW_BAR_WIDTH,
                    0f);

                putVertex(0f, yPos, 0f);
                putVertex(FRONT_WINDOW_WIDTH, yPos + FRONT_WINDOW_BAR_WIDTH,
                    0f);
                putVertex(FRONT_WINDOW_WIDTH, yPos, 0f);
            }
        }
    }

    /**
     * The Bar on the top of the Packard Building
     */
    private class WindowRoofBar extends VertexBuffer {
        // Quadilaterals are drawns with two triangles, needing 6 verticies
        public static final int NUM_VERTICES_PER_QUADRILATERAL = 6;
        // The roof bar is drawn with two quadilaterals as described in the
        // buffer data method. 
        public static final int NUM_QUADS = 2;

        WindowRoofBar(GLX glx) {
            super(glx, NUM_QUADS * NUM_VERTICES_PER_QUADRILATERAL,
                 VertexDeclaration.ATTRIB_POSITION);
        }

        /**
         * Window roof bar is drawn by drawing two quadilaterals that meet
         * at the second to last VBAR (with the lowest height). The roof bar
         * descends linearly to this point and ascends linearly after this point
         * going in +x direction. The quadilateral is from 0 -> the minimum 
         * point. The second is from the mimunum point -> end of front window.
         */
        @Override
        protected void bufferData(ByteBuffer buffer) {
            // Lowest VBAR Index is the index of the VBAR that marks the 
            // lowest point of the window roof bar.
            int lowestVBARIndex = VBAR_OFFSETS.length - 2;
            int lastVBARIndex = VBAR_OFFSETS.length - 1;

            // 1st quadilateral: Made of two triangles
            putVertex(0, FRONT_WINDOW_CORNER_HEIGHT, 0);
            putVertex(0, FRONT_WINDOW_CORNER_HEIGHT + FRONT_WINDOW_TOP_WIDTH,
                0);
            putVertex(VBAR_OFFSETS[lowestVBARIndex] + FRONT_WINDOW_BAR_WIDTH,
                FRONT_WINDOW_VBAR_HEIGHTS[lowestVBARIndex], 0);
            
            
            putVertex(VBAR_OFFSETS[lowestVBARIndex] + FRONT_WINDOW_BAR_WIDTH,
                FRONT_WINDOW_VBAR_HEIGHTS[lowestVBARIndex] + FRONT_WINDOW_TOP_WIDTH,
                0);
            putVertex(0, FRONT_WINDOW_CORNER_HEIGHT, 0);
            putVertex(VBAR_OFFSETS[lowestVBARIndex] + FRONT_WINDOW_BAR_WIDTH,
                FRONT_WINDOW_VBAR_HEIGHTS[lowestVBARIndex], 0);

            // 2nd Quadilateral: Made of two triangles
            putVertex(VBAR_OFFSETS[lowestVBARIndex] + FRONT_WINDOW_BAR_WIDTH,
                FRONT_WINDOW_VBAR_HEIGHTS[lowestVBARIndex], 0);
            putVertex(VBAR_OFFSETS[lowestVBARIndex] + FRONT_WINDOW_BAR_WIDTH,
                FRONT_WINDOW_VBAR_HEIGHTS[lowestVBARIndex] + FRONT_WINDOW_TOP_WIDTH,
                0);
            putVertex(VBAR_OFFSETS[lastVBARIndex] + FRONT_WINDOW_BAR_WIDTH,
                FRONT_WINDOW_VBAR_HEIGHTS[lastVBARIndex] + FRONT_WINDOW_TOP_WIDTH,
                0);
            
            
            putVertex(VBAR_OFFSETS[lastVBARIndex] + FRONT_WINDOW_BAR_WIDTH,
                FRONT_WINDOW_VBAR_HEIGHTS[lastVBARIndex] + FRONT_WINDOW_TOP_WIDTH,
                0);
            putVertex(VBAR_OFFSETS[lowestVBARIndex] + FRONT_WINDOW_BAR_WIDTH,
                      FRONT_WINDOW_VBAR_HEIGHTS[lowestVBARIndex], 0);
            putVertex(VBAR_OFFSETS[lastVBARIndex] + FRONT_WINDOW_BAR_WIDTH,
                FRONT_WINDOW_VBAR_HEIGHTS[lastVBARIndex], 0);
        }

    }

    /**
     * 
     * @param app Our LX instance
     * @param model The Flight Model used to 
     */
    public PackardDrawer(FlightApp app) {
        this.app = app;

        this.frontWindowBuffer = new FrontWindow(app);
        this.backWallBuffer = new BackWall(app);
        this.floorBuffer = new Floor(app);
        
        int numVbars = FRONT_WINDOW_VBAR_SPACINGS.length;
        VBAR_OFFSETS = new float[numVbars];
        float xPos = 0.0f;
        for (int i = 0; i < numVbars; i++) {
            xPos += FRONT_WINDOW_VBAR_SPACINGS[i];
            VBAR_OFFSETS[i] = xPos;
        }

        this.windowVBARsBuffer = new WindowVBARs(app);

        BACK_WALL_OFFSETS = new float[BACK_WALL_SPACINGS.length];
        float yPos = 0f;
        for (int i = 0; i < BACK_WALL_SPACINGS.length; i++) {
            yPos += BACK_WALL_SPACINGS[i];
            BACK_WALL_OFFSETS[i] = yPos;
        }

        this.windowHBARsBuffer = new WindowHBARs(app);
        this.roofBarBuffer = new WindowRoofBar(app);
    }

    @Override
    protected void onDraw(UI ui, View view) {
        try {

            // drawCeiling(ui, view);
            drawWalls(ui, view);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

//      /**
//      * Draws a cylinder of radius r and height h, with one end
//      * centered at (0, 0, 0) and the other at (0, 0, h). If you want
//      * to move the cylinder, call this method having set up a matrix
//      * so these coordinates are at your desired position. Used to draw
//      * pipes in the ceiling of the stairwell. Uses whatever fill color
//      * was specified before calling.
//      */

//     /** Draw a ceiling pipe from (x0, z0) to (x1, z1). Y is constant,
//      * at FlightGeometry.FRONT_WINDOW_NADIR_HEIGHT (the height of
//      * the pipe superstructure).
//      */
//     private void drawCeilingPipe(UI ui, View view,
//                                  float x0, float z0, float x1, float z1) {
//         int currFillColor = 0xc0c0c000;
//         pushMatrix();
//         float y0 = FRONT_WINDOW_NADIR_HEIGHT + FlightGeometry.FRONT_WINDOW_BAR_WIDTH / 2.0f;
//         float y1 = FRONT_WINDOW_NADIR_HEIGHT + FlightGeometry.FRONT_WINDOW_BAR_WIDTH / 2.0f;

//         float xAngle = LXUtils.atan2pf((x1 - x0),(z1 - z0));

//         float length = LXUtils.distf(x0,z0,x1,z1);

//         this.modelMatrix.translate(x0, y0, z0).rotateY(xAngle);



//         drawCylinder(ui, view, FlightGeometry.CEILING_PIPE_RADIUS, length);
//         popMatrix();
//     }

//     /**
//      * Draw all of the ceiling pipes. Their placement is based on the
//      * prow architectural drawing in the project repository. Four pipes
//      * stretch from the front window to the back wall, while four other
//      * pipes join with these to form two internal triangles.
//      */
//     private void drawCeiling(UI ui, View view) {
//     // Because the back wall is at a 45 degree angle to the front window,
//     // the X and Z displacement is the same. -pal

//         // Draw straight pipes
//         for (int i = 0; i < BACK_WALL_OFFSETS.length; i++) {
//             float backWallOffset = BACK_WALL_OFFSETS[i] * 0.707f;
//             // Straight pipe across
//             drawCeilingPipe(ui, view
//                             VBAR_OFFSETS[i * 2], 0.0,
//                             backWallOffset, backWallOffset);
//             if (i == 1 || i == 2) {
//                 float nextFX = VBAR_OFFSETS[2 * (i + 1)];
//                 float nextFZ = 0.0f;
//                 float nextBX = BACK_WALL_OFFSETS[i + 1] * 0.707f;
//                 float nextBZ = BACK_WALL_OFFSETS[i + 1] * 0.707f;
//                 float midpointX = nextFX + ((nextBX - nextFX) / 2.0f);
//                 float midpointZ = nextFZ + ((nextBZ - nextFZ)/ 2.0f);
//                 // Two pipes of the V that meet at next one
//                 drawCeilingPipe(ui, view,
//                                 VBAR_OFFSETS[i * 2], 0.0,
//                                 midpointX, midpointZ);
//                 drawCeilingPipe(ui, view,
//                                 midpointX, midpointZ,
//                                 backWallOffset, backWallOffset);
//             }

//         }
//     }

// //     /** Draw a flight of up or down stairs at a given yOffset (floor
// //      * height). Facing the stairs from the back wall, up stairs are
// //      * on the left (-x) while down stairs are on the right (+x). It
// //      * also draws half of the corner platform.
// //      */
// //     private void drawStairFlight(UI ui, View view, float yOffset, boolean up) {
// //         pg.fill(#606060);
// //         pg.stroke(#000000);
// //         pushMatrix();
// //         pg.translate(0, yOffset, 0);
// //         if (up) {
// //             pushMatrix();
// //             pg.translate(-168.0f + 126.0f * sin(PI/8f),
// //                         0,
// //                         32.0f + 126f * cos(PI/8f));
// //             pg.rotateY(PI/8f);
// //             pg.beginShape();
// //             pg.vertex(0, 0, 0);
// //             pg.vertex(0, 90f, 186f);
// //             // This 128 is a hacked constant. I got sick of trying
// //             // to do the triginometry. -pal
// //             pg.vertex(0, 90f, 186f + 128f);
// //             pg.vertex(69f, 90f, 186f);
// //             pg.vertex(69f, 0, 0);
// //             pg.endShape(CLOSE);
// //             popMatrix();
// //         } else {
// //             pushMatrix();
// //             pg.translate(168.0f - 126.0f * sin(PI/8f),
// //                         0,
// //                         32.0f + 126f * cos(PI/8f));
// //             pg.rotateY(-PI/8f);
// //             pg.beginShape();
// //             pg.vertex(0, 0, 0);
// //             pg.vertex(0, -90f, 186f);
// //             // This 128 is a hacked constant. I got sick of trying
// //             // to do the triginometry. -pal
// //             pg.vertex(0, -90f, 186f + 128f);
// //             pg.vertex(-69f, -90f, 186f);
// //             pg.vertex(-69f, 0, 0);
// //             pg.endShape(CLOSE);
// //             popMatrix();
// //         }

// //         popMatrix();
// //     }

// //     /** Draw a platform against the interior wall at a given yOffset;
// //      * this is the platform that the doors from the interior of
// //      * the building open on to, so you can take stairs up and down.
// //      * Constants taken from the DWG model of the stairwell in the
// //      * repository.
// //      */
// //     private void drawPlatform(UI ui, View view, float yOffset) {
// //         pg.fill(#606060);
// //         pg.stroke(#000000);
// //         pushMatrix();
// //         pg.translate(0, yOffset, 0);
// //         pg.beginShape();
// //         pg.vertex(126.0f, 0, 0.0f);
// //         pg.vertex(126.0f, 0, 32.0f);
// //         pg.vertex(168.0f, 0, 32.0f);
// //         pg.vertex(168.0f - 126.0f * sin(PI/8f),
// //                     0,
// //                     32.0f + 126f * cos(PI/8f));
// //         pg.vertex(168.0f - 126.0f * sin(PI/8f) - 69f * sin(5f / 8f * PI),
// //                     0,
// //                     32f + 126f * cos(PI/8f) + 69f * cos(5f / 8f * PI));
// //         pg.vertex(168.0f - 126.0f * sin(PI/8f) - 69f * sin(5f / 8f * PI) + 21f * sin(PI/8f),
// //                     0,
// //                     32f + 126f * cos(PI/8f) + 69f * cos(5f / 8f * PI) - 21f * cos(PI/8f));
// //         pg.vertex(-168.0f + 126.0f * sin(PI/8f) + 69f * sin(5f / 8f * PI) - 21f * sin(PI/8f),
// //                     0,
// //                     32f + 126f * cos(PI/8f) + 69f * cos(5f / 8f * PI) - 21f * cos(PI/8f));
// //         pg.vertex(-168.0f + 126.0f * sin(PI/8f) + 69f * sin(5f / 8f * PI),
// //                     0,
// //                     32f + 126f * cos(PI/8f) + 69f * cos(5f / 8f * PI));
// //         pg.vertex(-168.0f + 126.0f * sin(PI/8),
// //                     0,
// //                     32.0f + 126f * cos(PI/8));
// //         pg.vertex(-168.0f, 0, 32.0f);
// //         pg.vertex(-126.0f, 0, 32.0f);
// //         pg.vertex(-126.0f, 0, 0.0f);

// //         pg.endShape(CLOSE);
// //         popMatrix();
// //     }

// //     /**
// //      * Draw all of the stairs: the platforms on the ground floor, floor 2,
// //      * and floor 3 as well as the stairs that connec them.
// //      */
// //     private void drawStairs(UI ui, View view) {
// //         pushMatrix();
// //         // Translate to the midpoint of the center wall that
// //         // the stair platforms are on; this is halfway between
// //         // the end of the front wall (WIDTH) and the end of the back wall
// //         // (0.707 * WIDTH) -pal
// //         pg.translate(FRONT_WINDOW_WIDTH * (1f + 0.707) / 2f,
// //                     0f,
// //                     FRONT_WINDOW_WIDTH * (0.707) / 2f);
// //         // The stairwell is 45 degrees; this means the mid angle from the
// //         // center of the stair wall -22.5 degrees from along the X axis,
// //         // so -(PI/4 + P/16)
// //         pg.rotateY(- 5.0f/8.0f * PI);
// //         drawPlatform(ui, view, 0f);
// //         drawPlatform(ui, view, 180f);
// //         drawPlatform(ui, view, 360f);
// //         drawStairFlight(ui, view, -180f, true);
// //         drawStairFlight(ui, view, 0f, false);
// //         drawStairFlight(ui, view, 0, true);
// //         drawStairFlight(ui, view, 180f, false);
// //         drawStairFlight(ui, view, 180f, true);
// //         drawStairFlight(ui, view, 360f, false);
// //         popMatrix();
// //     }

// //     /**
// //      * Draw the back wall in white and the latticework on the front
// //      * window.
// //      */
private void drawWalls(UI ui, View view) {
        
        int barColor = LXColor.rgb(0xc0, 0xc0, 0xc0);

        bgfx_set_transform(this.modelMatrix.get(this.modelMatrixBuf));
        ui.lx.program.uniformFill.submit(view, drawStateTriStrip, barColor,
                                         this.frontWindowBuffer);

        // Vertical bars: need a polygon not a rectangle
        // because their top has an angle with the roof.
        // TODO: Give each shape lines pg.stroke(#202020);
        ui.lx.program.uniformFill.submit(view, drawStateTriangle, barColor,
                                         this.windowVBARsBuffer);
        
        
        ui.lx.program.uniformFill.submit(view, drawStateTriangle, barColor,
                                         this.windowHBARsBuffer);
        
        ui.lx.program.uniformFill.submit(view, drawStateTriangle, barColor,
            roofBarBuffer);

        // Bottom floor
        ui.lx.program.uniformFill.submit(view, drawStateTriStrip,
            this.floorBuffer.COLOR, this.floorBuffer);
        
        // Back Wall
        ui.lx.program.uniformFill.submit(view, drawStateTriStrip,
            this.backWallBuffer.COLOR, this.backWallBuffer);
    }
}
