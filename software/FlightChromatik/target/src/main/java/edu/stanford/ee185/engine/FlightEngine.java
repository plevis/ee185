package edu.stanford.ee185.engine;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.*;

import edu.stanford.ee185.model.FlightModel;
import edu.stanford.ee185.model.FlyerConfig;

import java.io.File;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import java.util.Date;
import java.time.LocalDateTime;

import heronarts.lx.LX;
import heronarts.lx.mixer.*;
import heronarts.lx.studio.LXStudio;
import heronarts.lx.pattern.LXPattern;
import heronarts.lx.parameter.LXParameter;
import heronarts.lx.parameter.LXParameterListener;
import heronarts.lx.parameter.DiscreteParameter;
import heronarts.lx.LXComponent;
import heronarts.lx.clip.LXClip;

/**
 * Central controller for a FLIGHT installation, responsible for loading and
 * running tracks, loading the available Patterns, and configuring output to
 * generate Python code.
 * 
 * This class is able to load multiple tracks, have each loop for some number of
 * times, and transition to a new track. This collection of tracks is called a
 * playlist. The playlist can also be set to start at a specific start time
 * which is tied to the host computers system clock
 *
 * @author Philip Levis <pal@cs.stanford.edu>
 * @author Moses Swai
 * @author Ashley Chen
 */

public class FlightEngine {

  static final int NUM_CHANNELS = 8;

  // Directory root from which to read/write files
  final private String projectPath;

  // Helper class for reading/write data
  final private IO io;

  // The ground-truth copy of the data model: Flyer locations, etc.
  final private FlightModel model;

  // The LXStudio used to drive the whole application.
  final private LXStudio lx;

  // Because we need to be able to access channels quite often, just
  // store references to them here. These are also passed into the
  // LXStudio LXMixer so it mixes and displays them properly.
  final private List<LXChannel> channels;

  // The set of Patterns that can be selected for each channel.
  // We have this here so we can query this (e.g., size) without
  // having to poke at a channel.
  final private ArrayList<LXPattern> patterns;

  // Helper class that generates Python code to control FLIGHT based
  // on the state of the FlightModel.
  private Generator generator;
  
  public FlightPlaylist playlist;

  public FlightEngine(String projectPath, FlightModel model, LXStudio lx) {
    // super(lx);
    this.projectPath = projectPath;
    this.io = new IO(projectPath);
    this.model = model;
    this.lx = lx;
    this.patterns = new ArrayList<LXPattern>();
    this.channels = new ArrayList<LXChannel>();

    /* All engine configuration */
    configureGeneratorOutput();
    configureChannels();
    
    this.playlist = new FlightPlaylist(this.io, lx);
  }

  /***** PUBLIC FUNCTIONS *****/

  /**
   * Returns the FlightModel used to create the LXModel
   *
   * @return The FlightModel
   */
  public FlightModel getModel() {
    return model;
  }

  /**
   * Returns an array with all the flyer configurations This array can be used to
   * save the current flyer states to a .json file using the IO class
   *
   * @return A FlyerConfig array
   */
  public FlyerConfig[] getFlyerConfigs() {
    return model.getFlyerConfigs();
  }

  /**
   * Returns the number of patterns in each channel. This assumes that every
   * channel has the same patterns
   *
   * @return The number of patterns
   */
  public int numPatterns() {
    return this.patterns.size();
  }

  /**
   * Returns the number of channels in the LXMixerEngine NOTE: This does not count
   * the master channel
   *
   * @return The number of channels
   */
  public int numChannels() {
    return this.lx.engine.mixer.channels.size();
  }

  /**
   * Returns the IO class object for saving and loading flyer configurations and
   * track
   *
   * @return The IO class object
   */
  public IO getIO() {
    return io;
  }

  /**
   * Sets the script generation rate.
   * 
   * @param frequency The frequency, in Hz, to generate scripts.
   */
  public void setGeneratorRate(Double frequency) {
    generator.setRate(frequency);
  }

  /**
   * Turns script generation on or off.
   * 
   * @param enabled True to turn generator on, false to turn off.
   */
  public void setGeneratorEnabled(boolean enabled) {
    generator.setEnabled(enabled);
  }

  // PRIVATE FUNCTIONS

  /*
   * Adds patterns to the pattern list passed as parameter. This pattern is useful
   * because each channel has its own list of patterns, since each instance of a
   * pattern might have different settings.
   * 
   * If you change the order with which these are added, you should also re-map the APC40.
   */
  private void addPatterns(ArrayList<LXPattern> patterns) {
    patterns.add(new ColorSweep(lx));
    patterns.add(new ColorWall(lx));
    patterns.add(new ColorWave(lx));
    patterns.add(new ColorWheel(lx));
    patterns.add(new ColorGradient(lx));
    patterns.add(new WheelOfcolor(lx));
    patterns.add(new Surge(lx));
    patterns.add(new Fractal(lx));
    patterns.add(new Spotlight(lx));
    patterns.add(new RainFall(lx));
    patterns.add(new WingColor(lx));
    patterns.add(new BodyOffset(lx));
    patterns.add(new Drawer(lx));
    patterns.add(new RaspberryTrip(lx));

    patterns.add(new ColorSnake(lx));
    patterns.add(new Twinkle(lx));
    patterns.add(new LightHouseColor(lx));
    patterns.add(new SlowFlap(lx));
    patterns.add(new ClimbFlap(lx));
    patterns.add(new SplitWings(lx));
    patterns.add(new VogueWings(lx));
    patterns.add(new WingSnake(lx));
    patterns.add(new HeartBeat(lx));
    patterns.add(new FireWorks(lx));
    patterns.add(new POVPattern(lx));
    patterns.add(new BreathingSphere(lx));
    patterns.add(new BreathingSphereWings(lx));
    patterns.add(new BreathingUp(lx));
    patterns.add(new Fountain(lx));
  }

  /* Create an array with a new instance of each available Pattern. */
  private LXPattern[] createPatternArray() {
    ArrayList<LXPattern> list = new ArrayList<LXPattern>();
    addPatterns(list);
    return list.toArray(new LXPattern[patterns.size()]);
  }

  /*
   * Creates all of the channels for the mixer, giving each its own copy of each
   * available pattern and giving it a clip for later recording or playback.
   */
  private void configureChannels() {
    addPatterns(this.patterns);
    LXMixerEngine mixer = lx.engine.mixer;
    // mixer.removeChannel(mixer.getLastChannel());
    mixer.clear();
    for (int c = 0; c < NUM_CHANNELS; ++c) {
      LXChannel channel = mixer.addChannel(c, createPatternArray());
      channels.add(channel);
      // By default channel 1 is on and others are off.
      if (c == 0) {
        channel.fader.setValue(1);
      } else {
        channel.fader.setValue(0);
      }
      channel.addClip();
    }
    mixer.setFocusedChannel(mixer.getChannel(0));
    mixer.selectChannel(mixer.getChannel(0));
    mixer.masterBus.addClip();
  }

  /* Configures the GeneratorOutput */
  private void configureGeneratorOutput() {
    PrintStream[] outputSockets = new PrintStream[76];
    String[] outputFileNames = io.loadOutputFile("data/outputs.json");
    for (int i = 0; i < 76; i++) {
      try {
        outputSockets[i] = new PrintStream(outputFileNames[i]);
      } catch (Exception ex) {
        System.out.println(ex);
      }
    }

    try {
      generator = new Generator(lx, model, null, outputSockets);
      lx.addOutput(generator);
    } catch (Exception ex) {
      System.out.println("IN generator");
      System.out.println(ex);
    }
  }

}
