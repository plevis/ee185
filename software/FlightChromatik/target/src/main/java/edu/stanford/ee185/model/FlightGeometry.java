package edu.stanford.ee185.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.IntPredicate;

import heronarts.lx.transform.LXVector;

import heronarts.lx.color.LXColor;

public interface FlightGeometry {
	
  final static float INCHES = 1f;
  final static float FEET = 12f * INCHES;

  /**
   * room dimensions
   */
  final static float X_ROOM = 40 * FEET;
  final static float Y_ROOM = 40 * FEET;
  final static float Z_ROOM = 40 * FEET;

  final static float CEILING_PIPE_RADIUS = 6f * INCHES;
    
  final static float FRONT_WINDOW_BAR_WIDTH = 0.25f * FEET;
  final static float FRONT_WINDOW_TOP_WIDTH = 1f * FEET;
  final static float FRONT_CORNER_WIDTH = 4.0f * FEET + 8.75f;
  final static float FRONT_WINDOW_WIDTH = 55f * FEET;
  final static float FRONT_WINDOW_CORNER_HEIGHT = 54f * FEET + 9.0f;
  final static float FRONT_WINDOW_NADIR_HEIGHT = (42f * FEET) + 3.5f;
  final static float FRONT_WINDOW_NADIR_DROP = 12f * FEET + 5.5f;
  final static float FRONT_WINDOW_BASE_HEIGHT = (45f * FEET) + 3.5f;
  final static float[] FRONT_WINDOW_VBAR_SPACINGS = {
        6f * FEET + 11.0f + 3.0f/16.0f,
        6f * FEET + 11.0f + 5.0f/32.0f,
        6f * FEET + 11.0f + 5.0f/32.0f,
        6f * FEET + 11.0f + 5.0f/32.0f,
        6f * FEET + 11.0f + 5.0f/32.0f,
        6f * FEET + 11.0f + 5.0f/32.0f,
        6f * FEET + 11.0f + 5.0f/32.0f,
        6f * FEET + 5.0f  + 1.0f/8.0f
  };
  final static float[] FRONT_WINDOW_VBAR_HEIGHTS = {
        FRONT_WINDOW_CORNER_HEIGHT - (FRONT_WINDOW_NADIR_DROP * 1.0f / 7.0f),
        FRONT_WINDOW_CORNER_HEIGHT - (FRONT_WINDOW_NADIR_DROP * 2.0f / 7.0f),
        FRONT_WINDOW_CORNER_HEIGHT - (FRONT_WINDOW_NADIR_DROP * 3.0f / 7.0f),
        FRONT_WINDOW_CORNER_HEIGHT - (FRONT_WINDOW_NADIR_DROP * 4.0f / 7.0f),
        FRONT_WINDOW_CORNER_HEIGHT - (FRONT_WINDOW_NADIR_DROP * 5.0f / 7.0f),
        FRONT_WINDOW_CORNER_HEIGHT - (FRONT_WINDOW_NADIR_DROP * 6.0f / 7.0f),
        FRONT_WINDOW_NADIR_HEIGHT,
        FRONT_WINDOW_BASE_HEIGHT
  };
  final static float[] FRONT_WINDOW_HBAR_SPACINGS = {
      (float)(10 * FEET + 4.0 + 1.0/2.0),
      (float)(8  * FEET + 3.0 + 1.0/2.0),
      (float)(7  * FEET + 10.0 + 1.0/2.0),
      (float)(7  * FEET + 10.0 + 1.0/2.0),
      (float)(7  * FEET + 10.0 + 1.0/2.0)
  };

  final static float[] BACK_WALL_SPACINGS = {
      (float)(6 * FEET + 11.0 + 3.0/16.0),
      (float)(13 * FEET + 10.0 + 5.0/16.0),
      (float)(13 * FEET + 10.0 + 5.0/16.0),
      (float)(13 * FEET + 10.0 + 5.0/16.0)
  };

  /**
   * num_x_per_y constants
   */
  final static int NUM_WINGS_PER_FLYER = 2;
  final static int NUM_LIGHT_POINTS_PER_WING = 3;
  final static int NUM_LIGHT_POINTS_PER_BODY = 1;
  final static int NUM_LIGHT_POINTS_PER_FLYER = NUM_WINGS_PER_FLYER * NUM_LIGHT_POINTS_PER_WING + NUM_LIGHT_POINTS_PER_BODY;

  default int bodyLightIndex(int flyer) {
      return flyer * NUM_LIGHT_POINTS_PER_FLYER + LightSamplePoint.LightCorners.BODY.position;
  }

  default int flyerLightStartIndex(int flyer) {
      return flyer * NUM_LIGHT_POINTS_PER_FLYER;
  }

  default int wingLightIndex(int flyer, boolean right, int which) {
      return (flyer * NUM_LIGHT_POINTS_PER_FLYER) + (right? NUM_LIGHT_POINTS_PER_WING: 0) + which;
  }

  default int wingIndex(int flyer, boolean right) {
    return (flyer * NUM_WINGS_PER_FLYER) + (right? 1: 0);
  }

  IntPredicate isWingLight = lightIndex -> lightIndex % NUM_LIGHT_POINTS_PER_FLYER != LightSamplePoint.LightCorners.BODY.position;

  IntPredicate isLeftWingLight = lightIndex -> lightIndex % NUM_LIGHT_POINTS_PER_FLYER < NUM_LIGHT_POINTS_PER_WING;

  IntPredicate isRightWingLight = lightIndex -> (lightIndex % NUM_LIGHT_POINTS_PER_FLYER >= NUM_LIGHT_POINTS_PER_WING &&
                                                 lightIndex % NUM_LIGHT_POINTS_PER_FLYER != LightSamplePoint.LightCorners.BODY.position);


  /**
   * indexing scheme constants
   */
  final static int NUM_FLYERS = 76;
  final static int NUM_WINGS = NUM_FLYERS * NUM_WINGS_PER_FLYER;
  final static int NUM_WING_LIGHT_POINTS = NUM_WINGS * NUM_LIGHT_POINTS_PER_WING;
  final static int NUM_BODY_LIGHT_POINTS = NUM_FLYERS;
  final static int NUM_TOTAL_LIGHT_POINTS = NUM_WING_LIGHT_POINTS + NUM_BODY_LIGHT_POINTS;


  /**
   * Pattern constants
   */
  final static int COLOR =  LXColor.GREEN;
  final static int BRIGHTNESS = 100;
  final static String LIGHT_PATTERN = "lightning";
  final static String MOVEMENT_PATTERN = "gentle_flap";

  /**
   * Physical constants of a Fractal Flyer.
   */
  // How far from the wide tip the center of mass/hanging point is
  public final static float FLYER_HANGING_OFFSET = 10 * INCHES;


  /**
   * Body LED dimensions
   */
  public final static float LENGTH_BODY_CROSS = 22 * INCHES;
  public final static float WIDTH_BODY_CROSS = (float) (3.8125 * INCHES);
  public final static float DEPTH_BODY_CROSS = (float) (.394 * INCHES);

  /**
   * Wing LED dimensions
   */
  final static float CORNER_DISTANCE = (float) (7.06 * INCHES);
  final static float TOP_EDGE_DISTANCE = (float) (1.18 * INCHES);
  final static float SIDE_EDGE_DISTANCE = (float) (18.04 * INCHES);
  final static float INNER_EDGE_DISTANCE = (float) (20.69 * INCHES);
  final static float WING_THICKNESS_MM = 8;


  List<LXVector> lightGeometryPoints = new ArrayList<LXVector>(Arrays.asList(
    new LXVector(4.011f, -4.967f, 0f), // left head
    new LXVector(9.116f, -11.173f, 0f), // left side
    new LXVector(26.741f, -0.987f, 0f), // left tail
    new LXVector(4.011f, 4.967f, 0f), // right head
    new LXVector(9.116f, 11.173f, 0f), // right side
    new LXVector(26.741f, 0.987f, 0f), // right tail
    new LXVector(5.288f, 0f, 0f) // body
  ));


  List<LXVector> bodyGeometryPoints = new ArrayList<LXVector>(Arrays.asList(
    new LXVector(0f, 0f, 0f), // head
    new LXVector(3.333f, 4.040f, 0f), // right side
    new LXVector(3.333f, -4.040f, 0f), // left side
    new LXVector(26.259f, 0f, 0f), // tail
    new LXVector(5.288f, 0f, -4.394f) // body tip

  ));

  public default List<LXVector> buildLights() {
    return new ArrayList<LXVector>(lightGeometryPoints);
  }


}
