package heronarts.lx.studio.ui.model;

import edu.stanford.ee185.engine.Config;
import edu.stanford.ee185.engine.FlyerHighlighter;
import edu.stanford.ee185.model.*;
import heronarts.glx.event.MouseEvent;
import heronarts.lx.studio.FlightApp;
import heronarts.lx.LXComponent;
import heronarts.lx.studio.LXStudio.UI;
import heronarts.glx.ui.UI2dComponent;
import heronarts.lx.parameter.BooleanParameter;
import heronarts.lx.parameter.BoundedParameter;
import heronarts.lx.parameter.DiscreteParameter;
import heronarts.lx.parameter.LXListenableParameter;
import heronarts.lx.parameter.LXParameter;
import heronarts.lx.parameter.LXParameterListener;
import heronarts.glx.ui.component.*;


public class FlyerLayoutControlWindow extends UICollapsibleSection {

   /**
     * Does all the heavy lifting of creating parameters and setting up listeners
     * that will communicate any changes made by in the UI to the flyer layout
     * to the drawer.
     */
  private class FlyerLayoutControl extends LXComponent {

    private final String[] faceOptions = getFaceOptions();


    private final String[] pipeOptions = { "0".intern(), "1".intern(),
      "2".intern(), "3".intern(), "4".intern(), "5".intern(), "6".intern(),
      "7".intern() };
    
    private final String[] yOptions = { "1".intern(), "2".intern(),
      "3".intern(), "4".intern(), "5".intern() };

    public DiscreteParameter flyerIndex;
    public final BooleanParameter flashFlyer;
    public final DiscreteParameter face;
    public final BoundedParameter hang;
    public final BoundedParameter tilt;
    public final BoundedParameter rotation;
    public final BoundedParameter x;
    public final DiscreteParameter y;
    public final BoundedParameter stairPosition;
    public final DiscreteParameter pipe;
    public final BoundedParameter mount;

    private String[] getFaceOptions() {
      int length = Config.FACE_NAMES.length;
      String options[] = new String[length];
      for (int i = 0; i < length; i++) {
        options[i] = Config.FACE_NAMES[i].intern();
      }
      return options;
    }

    FlyerLayoutControl() {
      super(app);

      this.flashFlyer = new BooleanParameter("flash", true);
      this.face = new DiscreteParameter("face", faceOptions);
      this.hang = new BoundedParameter("hang", 0, 8 * FlightGeometry.FEET);
      this.tilt = new BoundedParameter("tilt", -10, 10);
      this.rotation = new BoundedParameter("rotation", 0, 360);
      this.x = new BoundedParameter("x", 0, FlightGeometry.FRONT_WINDOW_WIDTH);
      this.y = new DiscreteParameter("y", yOptions);
      this.stairPosition = new BoundedParameter("Position", 0, 0, 1);
      this.pipe = new DiscreteParameter("pipe", pipeOptions);
      this.mount = new BoundedParameter("mount", 0, 0, 1);

      LXParameterListener flashListener = new LXParameterListener() {
        public void onParameterChanged(LXParameter parameter) {
          BooleanParameter enabled = (BooleanParameter) parameter;
          if (enabled.getValueb()) {
            flyerSelect.focus(new MouseEvent(0, 0, 0f, 0f, 0));
            // When you enable, flash the selected flyer;
            // otherwise, it only flashes when the value changes.
            // If it only flashes when the value changes, then the
            // first time you enable, the selected Flyer flashes,
            // but it doesn't on subsequent enables.
            flyerHighlighter.trigger();
            updateEditableParameters(); 
          }
        }
      };

      this.addGeometryParameter(this.flashFlyer, flashListener);
      this.addGeometryParameter(this.face, null);
      this.addGeometryParameter(this.hang, null);
      this.addGeometryParameter(this.tilt, null);
      this.addGeometryParameter(this.rotation, null);
      this.addGeometryParameter(this.x, null);
      this.addGeometryParameter(this.y, null);
      this.addGeometryParameter(this.stairPosition, null);
      this.addGeometryParameter(this.pipe, null);

      this.respondToParameterChange(null);
    };

    private void addGeometryParameter(LXListenableParameter p, LXParameterListener l) {
      this.addParameter(p.getLabel(), p);
      if (l == null) {
        p.addListener(this::respondToParameterChange);
      }
      else {
        p.addListener(l);
      }
    }

    /**
     * The generic listener that will update all the flyer layout parameters
     * to their most recent value for use by the Drawer.
     * @param p Unused parameter that changed. This function will just update
     * all the parameters at once since there aren't that many.
     */
    public void respondToParameterChange(LXParameter p) {
      // Get Config where parameters are stored for the drawer
      FlyerConfig flyerConfig = flyerHighlighter.getConfig();
      FlyerConfig.FaceConfig faceConfig = flyerConfig.getFaceConfig();

      // Update all parameters
      faceConfig.face = face.getOption();
      updateEditableParameters();
      flyerConfig.rotation = rotation.getValuef();
      faceConfig.hangDistance = hang.getValuef();
      flyerConfig.tilt = tilt.getValuef();
      faceConfig.x = x.getValuef();
      faceConfig.y = Integer.parseInt(y.getOption());
      faceConfig.position = stairPosition.getValuef();
      faceConfig.pipe = Integer.parseInt(pipe.getOption());
      faceConfig.mountPoint = mount.getValuef();

      // Reload the model for Flight App with new values.
      flyerHighlighter.reloadModel();
    }
  }

  // The width of each UI component in the window
  final static float COMPONENT_W = 60.0F;
  // The height of each UI component in the window
  final static float COMPONENT_H = 20.0F;
  // The padding between each UI component in the window
  final static float COMPONENT_PADDING = 15.0F;
  // The number of UI componenets we have
  final static float NUM_UI_COMPONENTS = 11.0F;
  // Has to be defined for a UICollapsibleSection - size of each component * 
  // number of components
  final static float HEIGHT = (COMPONENT_H + COMPONENT_PADDING) * NUM_UI_COMPONENTS;


  
  private UIButton flyerFlashButton; // Makes the flyer strobe
  private UIIntegerBox flyerSelect; //The flyer we are changing
  private UIToggleSet faceToggle; // Determines the type of layout 
  private UISlider  hangDistanceSlider; // All faces
  private UISlider  tiltSlider; // All faces
  private UISlider  rotationSlider; // All faces
  private UISlider xSlider; // Front window: x along row
  private UIToggleSet yToggle; // Front window: which row
  private UISlider  stairPositionSlider; // Spirals: point along spiral
  private UIToggleSet  pipeToggle; // Ceiling: which pipe
  private UISlider  mountPointSlider; // Ceiling: were on pipe

  private FlightApp app;
  private FlyerHighlighter flyerHighlighter;
  private FlyerLayoutControl flyerLayoutControl;

  void updateEditableParameters() {
      try {
          String face = faceToggle.getSelectedOption();
          // Every mounting point has a hang distance
          hangDistanceSlider.setVisible(true);
          tiltSlider.setVisible(true);
          rotationSlider.setVisible(true);
          
          // Other measurements are face-dependent; only make relevant
          // ones visible.
          xSlider.setVisible(false);
          yToggle.setVisible(false);
          pipeToggle.setVisible(false);
          stairPositionSlider.setVisible(false);
          mountPointSlider.setVisible(false);
          
          if (face.equals("C")) {
              pipeToggle.setVisible(true);
              mountPointSlider.setVisible(true);
          } else if (face.equals("F")) {
              xSlider.setVisible(true);
              yToggle.setVisible(true);
          } else if (face.equals("I")) {
            stairPositionSlider.setVisible(true);
          } else if (face.equals("E")) {
            stairPositionSlider.setVisible(true);
          } else {
              System.err.println("Unrecognized face: " + face + "; disable all controls.");
          }
      } catch (NullPointerException ex) {
        // There's something wrong with the initialization sequence.
        // Sometimes this is called before all of the different UI
        // elements have been initialized, so a NPI is thrown.
        // Just catch it here - it only happens at startup.
          System.out.println("Caught NPE at startup -- this is normal and recoverable, but would be good to eventually make this bug go away. -pal");
      }
  }

  /**
   * Constructor for the FlyerLayoutControlWindow
   * @param ui The UI on which this window will be a child.
   * @param app The FlightApp, used to access IO features
   * @param flyerHighlighter The flyerHighlighter to access and modify model values
   */
  public FlyerLayoutControlWindow(UI ui, FlightApp app,
    FlyerHighlighter flyerHighlighter) {
    super(ui, 0.0f, 0.0f, ui.leftPane.model.getContentWidth(), HEIGHT);

    this.app = app;
    this.flyerHighlighter = flyerHighlighter;
    this.flyerLayoutControl = new FlyerLayoutControl();
    
    // This parameter is owned by the flyerHighlighter hence it is not part of
    // our FlyerLayoutControl.
    this.flyerSelect = new UIIntegerBox(COMPONENT_W, COMPONENT_H,
      flyerHighlighter.flyerIndex);

    LXParameterListener selectListener = new LXParameterListener() {
      public void onParameterChanged(LXParameter parameter) {
          setFlyer();
      }
    };
    flyerHighlighter.flyerIndex.addListener(selectListener);

    this.flyerFlashButton = new UIButton(COMPONENT_W, COMPONENT_H,
      flyerLayoutControl.flashFlyer).setActiveLabel("Enabled")
      .setInactiveLabel("Disabled");

    this.faceToggle = new UIToggleSet(COMPONENT_W, COMPONENT_H,
      flyerLayoutControl.face);
    this.rotationSlider = new UISlider(COMPONENT_W, COMPONENT_H,
      flyerLayoutControl.rotation);
    this.hangDistanceSlider = new UISlider(COMPONENT_W, COMPONENT_H,
      flyerLayoutControl.hang);
    this.tiltSlider = new UISlider(COMPONENT_W, COMPONENT_H,
      flyerLayoutControl.tilt);
    this.xSlider = new UISlider(COMPONENT_W, COMPONENT_H, flyerLayoutControl.x);
    this.yToggle = new UIToggleSet(COMPONENT_W, COMPONENT_H,
      flyerLayoutControl.y);
    this.stairPositionSlider = new UISlider(COMPONENT_W, COMPONENT_H,
      flyerLayoutControl.stairPosition);
    this.pipeToggle = new UIToggleSet(COMPONENT_W, COMPONENT_H,
      flyerLayoutControl.pipe);

    this.mountPointSlider = new UISlider(COMPONENT_W, COMPONENT_H,
      flyerLayoutControl.mount);

    this.setTitle("Flyer Layout Tool");
    this.setLayout(Layout.VERTICAL, COMPONENT_PADDING);
    this.addChildren(
      new UI2dComponent[] { this.controlRow(ui, "Flyer #", flyerSelect),
        this.controlRow(ui, "Flash Individual Flyer",
          flyerFlashButton),
        this.controlRow(ui, "Position Type", faceToggle),
        this.controlRow(ui, "Hang Distance", hangDistanceSlider),
        this.controlRow(ui, "Z-Tilt", tiltSlider),
        this.controlRow(ui, "Rotation", rotationSlider),
        this.controlRow(ui, "X Position", xSlider),
        this.controlRow(ui, "Horizontal Bar Index", yToggle),
        this.controlRow(ui, "Stair Position", stairPositionSlider),
        this.controlRow(ui, "Ceiling Pipe Index", pipeToggle),
        this.controlRow(ui, "MOUNT", mountPointSlider)
        // this.controlRow(ui, "Save", saveButton)
      });

    setFlyer();
    setDescription("Tool for physically laying out Fractal Flyers in"
      + " Packard stairwell");
  }

  // private void makeSaveButton(float yPos) {
  //   UIButton saveButton = new UIButton(4, yPos, this.width-8, 20) {
  //     public void onToggle(boolean active) {
  //       if (active) {
  //         String backupFileName = Config.FLYER_CONFIG_FILE + ".backup." + month() + "." + day() + "." + hour() + "." + minute() + "." + second();
  //         saveStream(backupFileName, Config.FLYER_CONFIG_FILE);
  //         FlightModel model = (FlightModel) app.getModel();
  //         FlyerConfig[] configs = model.getFlyerConfigs();
  //         new IO(app.flags.mediaPath).saveConfigFile(configs, Config.FLYER_CONFIG_FILE);
  //         setLabel("Saved. Restart needed.");
  //       }
  //     }
  //   };
  //   saveButton.setMomentary(true).setLabel("Save Changes").addToContainer(this);
  //   saveButton.setDescription("Save current physical layout to " + Config.FLYER_CONFIG_FILE + " and make a backup of the previous layout");
  // }
    
  private void setFlyer() {
    FlightModel model = (FlightModel) app.getModel();
    FlyerConfig config = model.getFlyerConfigs()[flyerHighlighter.flyerIndex
        .getValuei()];
    
    hangDistanceSlider.getParameter().setValue(config.getFaceConfig().hangDistance);
    tiltSlider.getParameter().setValue(config.tilt);
    rotationSlider.getParameter().setValue(config.rotation);
    xSlider.getParameter().setValue(config.getFaceConfig().x);
    String yVal = Integer.toString(config.getFaceConfig().y).intern();
    yToggle.setSelectedOption(yVal);
    faceToggle.setSelectedOption(config.getFaceConfig().face.intern());
    stairPositionSlider.getParameter().setValue(config.getFaceConfig().position);
    pipeToggle.setSelectedOption(Integer.toString((int)config.getFaceConfig().pipe));
    mountPointSlider.getParameter().setValue(config.getFaceConfig().mountPoint);
    updateEditableParameters();
  }
}
