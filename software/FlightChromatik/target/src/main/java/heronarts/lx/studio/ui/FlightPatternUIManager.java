// package heronarts.lx.studio.ui;

// import java.util.List;
// import java.util.ArrayList;

// import edu.stanford.ee185.model.*;
// import heronarts.glx.ui.UI2dComponent;
// import heronarts.glx.ui.UI2dContainer;
// import heronarts.glx.ui.UI2dContext;
// import heronarts.glx.ui.component.*;


// import heronarts.lx.studio.FlightApp;
// import heronarts.lx.studio.ui.preview.UIPreviewWindow;
// import heronarts.lx.studio.LXStudio.UI;
// import heronarts.lx.mixer.*;
// import heronarts.lx.pattern.*;
// import heronarts.lx.parameter.*;
// import heronarts.glx.ui.component.UIItemList.*;
// import heronarts.glx.ui.UI2dContainer.*;

// /**
//  * Window that lets you choose the pattern for the currently
//  * selected channel in the bottom mixer as well as set the parameters
//  * of that Pattern. It queries the FlightEngine for the set of available
//  * patterns and provides those as options. A pattern's parameters are
//  * displayed as sliders. Heavily copied from SQUARED's UIMultiDeck.
//  *
//  * A significant portion of the logic in this class is to properly
//  * handle automation (playing a track). For example, if a track
//  * changes the pattern on a channel, we want to reflect that in the GUI.
//  * Put another way, these GUI elements can be modified both by user
//  * interaction as well as scripts, so we need to observe when things change
//  * using listeners.
//  *
//  * @author Philip Levis <pal@cs.stanford.edu>
//  */
// public class FlightPatternUIManager {
//   final static int NUM_PATTERNS_VISIBLE = 6;
//   final static int PATTERN_ROW_HEIGHT = 40;
//   final static int PATTERN_LIST_HEIGHT = NUM_PATTERNS_VISIBLE
//     * PATTERN_ROW_HEIGHT;
  
//   public final UIItemList.ScrollList[] patternLists;
//   final LXChannel.Listener[] channelListeners;
//   FlightApp app;
//   UI ui;
//   FlightPatternSliders sliders;

//   private class FlightPatternSliders extends UI2dContainer {
//     final static int NUM_SLIDERS = 4;
//     final static float SLIDER_HEIGHT = 24;
//     final static float SLIDER_SPACING = 16;
//     public final UISlider[] sliders;

//     FlightPatternSliders(UI ui) {
//       super(0.0f, 0.0f, ui.contentPicker.getContentWidth(),
//         (1 + NUM_SLIDERS) * (SLIDER_SPACING + SLIDER_HEIGHT));
//       this.setLayout(Layout.VERTICAL, SLIDER_SPACING);
//       sliders = new UISlider[NUM_SLIDERS];
//       for (int si = 0; si < sliders.length; ++si) {
//         UISlider slider = new UISlider(ui.contentPicker.getContentWidth(),
//           SLIDER_HEIGHT, null);
//         slider.setEditable(true);
//         sliders[si] = slider;
//       }
//     }
//   }

//     /**
//      * The elements in the list of patterns. This subclass
//      * is mostly about giving them the right label based
//      * on the underlying class (LXPattern) they wrap around.
//      * Heavily copied from SQUARED.
//      *
//      * @author Philip Levis <pal@cs.stanford.edu>
//      */
//   private class PatternScrollItem extends UIItemList.Item {

//     private final LXChannel channel;
//     private final LXPattern pattern;

//     private final String label;

//     PatternScrollItem(LXChannel channel, LXPattern pattern) {
//       this.channel = channel;
//       this.pattern = pattern;
//       this.label = pattern.getClass().getSimpleName();
//     }

//     public String getLabel() {
//       return this.label;
//     }

//     public boolean isSelected() {
//       return this.channel.getActivePattern() == this.pattern;
//     }

//     public boolean isPending() {
//       return this.channel.getNextPattern() == this.pattern;
//     }

//     public void onActivate() {
//       this.channel.goPattern(this.pattern);
//     }

//     public void onFocus() {
//       this.channel.goPattern(this.pattern);
//     }
//   }

//   FlightPatternUIManager(FlightApp app, UI ui) {
//     this.app = app;
//     this.ui = ui;

//     List<LXAbstractChannel> channels = app.engine.mixer.getChannels();
//     int numChannels = channels.size();
//     patternLists = new UIItemList.ScrollList[numChannels];
//     channelListeners = new LXChannel.Listener[numChannels];
//     // Set up the pattern selection list
//     for (int i = 0; i < numChannels; i++) {
//       LXChannel channel = (LXChannel) channels.get(i);
//       int index = channel.getIndex();
//       List<UIItemList.Item> items = new ArrayList<UIItemList.Item>();
//       for (LXPattern p : channel.getPatterns()) {
//         items.add(new PatternScrollItem(channel, p));
//       }
//       UIItemList.ScrollList list = new UIItemList.ScrollList(ui, 0, 0,
//         Math.round(ui.contentPicker.getContentWidth()), PATTERN_LIST_HEIGHT);
//       list.setItems(items);
//       list.setVisible(
//         channel.getIndex() == app.engine.mixer.focusedChannel.getValuei());
//       patternLists[index] = list;
//     }
    
//     // Set up the pattern selection. We need to listen to channels in case automation
//     // (e.g., a track) changes the pattern on a channel.
//     for (int i = 0; i < numChannels; i++) {
//       LXChannel channel = (LXChannel)app.engine.mixer.getChannel(i); 
//       channelListeners[channel.getIndex()] = new LXChannel.AbstractListener() {
//           @Override
//           public void patternWillChange(LXChannel channel,
//                                         LXPattern pattern,
//                                         LXPattern nextPattern) {
//               patternLists[channel.getIndex()].redraw();
//           }
              
//           @Override
//           /* When the pattern for a channel changes (e.g. due a track), then
//           *  show this change in the UI. Change the channel name to
//           *  the pattern and make that pattern focused in the list. */
//           public void patternDidChange(LXChannel channel, LXPattern pattern) {
//                 System.out.println("changing pattern");
//               channel.goPattern(pattern);
//               channel.label.setValue(pattern.getClass().getSimpleName());
//               List<LXPattern> patterns = channel.getPatterns();
//               for (int i = 0; i < patterns.size(); ++i) {
//                   if (patterns.get(i) == pattern) {
//                   System.out.println("pattern index: " + i);
//                       patternLists[channel.getIndex()].setFocusIndex(i);
//                       break;
//                   }
//               }  
//               patternLists[channel.getIndex()].redraw();

//               /* Change the LXParameters associated with the sliders to the parameters of
//               * the newly selected channel. */
//               if (channel.getIndex() == thislx.engine.mixer.focusedChannel.getValuei()) {
//                   int pi = 0;
//                   for (LXParameter parameter : pattern.getParameters()) {
//                       if (pi >= sliders) {
//                           break;
//                       }
//                       if (parameter instanceof LXListenableNormalizedParameter) {
//                           sliders[pi].setParameter((LXListenableNormalizedParameter)parameter);
//                           sliders[pi].setEditable(true);
//                           pi++;
//                       }
//                   }
//                   while (pi < sliders.length) {
//                       sliders[pi++].setParameter(null);
//                   }
//               }
//           }
//       };
//       channel.addListener(channelListeners[channel.getIndex()]);
//       channelListeners[channel.getIndex()].patternDidChange(channel, channel.getActivePattern());
//   }
//   }

//   public void addPatternUIElements() {
//     ui.contentPicker.patternList.addChildren(patternLists);
//     ui.contentPicker.addChildren(sliders);

//   }
// }

//   public class FlightPatternController extends UI2dComponent {
//     // A bunch of configuration constants.
//     final static int WIDTH = 150;
//     final static int HEIGHT = 500;
//     final static int BORDER = 4;
//     final static int SLIDERS = 4;
//     final static int SLIDER_HEIGHT = 24;
//     final static int SLIDER_SPACING = 40;
//     final static int NUM_PATTERNS_VISIBLE = 6;
//     final static int PATTERN_ROW_HEIGHT = 40;
//     final static int PATTERN_LIST_HEIGHT = NUM_PATTERNS_VISIBLE
//       * PATTERN_ROW_HEIGHT;
//     final static int TITLE_LABEL_HEIGHT = 25;

//     // Sliders below the pattern list, for modifying the parameters of
//     // the selected pattern.
//     final UISlider[] sliders;
//     final LX thislx;

//     FlightPatternController(UI ui, FlightApp app) {
//           // Set up the pattern selection. We need to listen to channels in case automation
//           // (e.g., a track) changes the pattern on a channel.
//           for (int i = 0; i < numChannels; i++) {
//               LXChannel channel = (LXChannel)thislx.engine.mixer.getChannel(i); 
//               channelListeners[channel.getIndex()] = new LXChannel.AbstractListener() {
//                   @Override
//                   public void patternWillChange(LXChannel channel,
//                                                 LXPattern pattern,
//                                                 LXPattern nextPattern) {
//                       patternLists[channel.getIndex()].redraw();
//                   }
                      
//                   @Override
//                   /* When the pattern for a channel changes (e.g. due a track), then
//                   *  show this change in the UI. Change the channel name to
//                   *  the pattern and make that pattern focused in the list. */
//                   public void patternDidChange(LXChannel channel, LXPattern pattern) {
//                         System.out.println("changing pattern");
//                       channel.goPattern(pattern);
//                       channel.label.setValue(pattern.getClass().getSimpleName());
//                       List<LXPattern> patterns = channel.getPatterns();
//                       for (int i = 0; i < patterns.size(); ++i) {
//                           if (patterns.get(i) == pattern) {
//                           System.out.println("pattern index: " + i);
//                               patternLists[channel.getIndex()].setFocusIndex(i);
//                               break;
//                           }
//                       }  
//                       patternLists[channel.getIndex()].redraw();
    
//                       /* Change the LXParameters associated with the sliders to the parameters of
//                       * the newly selected channel. */
//                       if (channel.getIndex() == thislx.engine.mixer.focusedChannel.getValuei()) {
//                           int pi = 0;
//                           for (LXParameter parameter : pattern.getParameters()) {
//                               if (pi >= sliders.length) {
//                                   break;
//                               }
//                               if (parameter instanceof LXListenableNormalizedParameter) {
//                                   sliders[pi].setParameter((LXListenableNormalizedParameter)parameter);
//                                   sliders[pi].setEditable(true);
//                                   pi++;
//                               }
//                           }
//                           while (pi < sliders.length) {
//                               sliders[pi++].setParameter(null);
//                           }
//                       }
//                   }
//               };
//               channel.addListener(channelListeners[channel.getIndex()]);
//               channelListeners[channel.getIndex()].patternDidChange(channel, channel.getActivePattern());
//           }
    
//           // Registers when the focused channel changes and updates the displayed pattern list to be
//           // of that channel.
//           thislx.engine.mixer.focusedChannel.addListener(new LXParameterListener() {
//                   public void onParameterChanged(LXParameter parameter) {
//                       try {
//                         LXChannel channel = (LXChannel)thislx.engine.mixer.getFocusedChannel();
//                         redraw();
                      
//                         channelListeners[channel.getIndex()].patternDidChange(channel, channel.getActivePattern());
                      
//                         int pi = 0;
//                         for (UIItemList.ScrollList patternList : patternLists) {
//                             patternList.setVisible(pi == thislx.engine.mixer.focusedChannel.getValuei());
//                             pi++;
//                         }
//                       } catch (ClassCastException ex) {
//                         // Some channels in the mixer panel are special ones,
//                         // like the master volume; ignore them.
//                       }
//                   }
//               });
      
//           this.setDescription("Select and configure patterns for the currently selected channel (" + thislx.engine.mixer.getFocusedChannel().getIndex() + ")");
//       }
// }
