# FLIGHT: Packard Interactive Light Sculpture Project

This directory contains the animation application for FLIGHT, based upon a custom build of the Chromatik / LX software framework.

## Dependencies

Install Maven and a 64-bit distribution of Java 17. Recommend using a Temurin build from [Adoptium](https://adoptium.net/). You can confirm these are correctly configured via `java -version` and `mvn -v`

## Building and Running

- The first time you check out the project, run `mvn validate` one time to import the LX / Chromatik dependencies into your system's local Maven repository
- `mvn compile` builds the application
- `mvn exec:exec` will run the application

You can do both at once with `mvn compile exec:exec`. Note that the `-XstartOnFirstThread` VM argument is required on macOS. This should be automatically detected and included by `mvn exec:exec`.

## Chromatik / LX documentation

- [Javadocs](https://chromatik.co/api/)
- [Chromatik User Guide](https://chromatik.co/guide/)
- [LXStudioJavadocs] (https://lx.studio/api/)

## Object Hierarchy
To draw custom UI components for FLIGHT's 3D scene, there are two abstract
classes: Flight Drawer and FlightVertexBuffer. A FlightDrawer allows for easy
rendering of collections of UI objects. For example the FLIGHT 3D scene consists
of two FlightDrawers. The first, FlyersDrawer, renders all the flyers and the 
second, Packard Drawer, renders the Packard building and stairwell.

A FlightDrawer contains a modelMatrix member, which is a transformation matrix
that can be applied on any UI components being rendered. For example, for each 
flyer, the transformation matrix is set based on each flyer's configuration 
parameters that specifiy its position in the Packard Stairwell. The drawFlyers 
function in FlyersDrawer demonstrates how to change the transformation matrix
and then apply it when rendering an object. To actually render a solid shape, 
Chromatik uses the abstraction of a VertexBuffer (an array of vertices). 
To render a shape, you create a subclass of a VertexBuffer (e.g. FlyerTopPlate) 
and specify the shapes' vertices in an overridden method called bufferData. 
There are a number of different ways to render a shape; the most straightforward
method is describing it as a collection of triangles.

FlightVertexBuffer extends the VertexBuffer abstract class to provide a method
called putTriangle. This method renders a triangle by taking in 3 vertices as
parameters, where each vertex is a floating point array of size 3 corresponding
to an x, y and z coordinate. The method putTriangle should be called in the 
overridden bufferData method instead of calling putVertex three times with
unique hard coded coordinates. See FlyerRightWing in FlyersDrawer as 
an example.

In an instance of a FlightDrawer class, the shapes it renders should be defined
as subclasses of (Flight)VertexBuffers and make them member variables of the 
FlightDrawer class. Then these shapes should be defined in the overridden 
onDraw method. Read through the drawFlyer method in FlyersDrawer for an example 
of the steps necessary.

![alt text](ObjectClassHierarchy.png)

You should try to scope any hard coded constant as needed. For example the
TOP_PLATE_LONG_DIAGONAL is a static member of the FlyersDrawer because it is
used by all the classes. However BOTTOM_POINT_X is a static member of the
FlyerBottomShell because it is only relevant information to render the bottom
shell.

Note that since each of the flyers are identical in shape and size, only a
single buffer is used for each component. From there matrix transformations are
leveraged to position them spatially. As a cautionary note, the verticies of a 
VertexBuffer must use static values and you cannot dynamically change them e.g. 
by passing values into the constructor. This is because of a bug in Chromatik 
that means the bufferData method is called in the superclass constructor meaning
parameters in a constructor cannot be used in the bufferData method. For this
reasons, the Chromatik GUI does not render the hanging wires since their length
can be dynamically changed by the GUI.