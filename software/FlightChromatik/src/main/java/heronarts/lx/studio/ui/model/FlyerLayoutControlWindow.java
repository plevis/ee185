package heronarts.lx.studio.ui.model;

import edu.stanford.ee185.engine.Config;
import edu.stanford.ee185.engine.FlyerHighlighter;
import edu.stanford.ee185.engine.IO;
import edu.stanford.ee185.model.*;
import heronarts.glx.event.MouseEvent;
import heronarts.lx.parameter.*;
import heronarts.lx.studio.FlightApp;
import heronarts.lx.LXComponent;
import heronarts.lx.studio.LXStudio.UI;
import heronarts.glx.ui.UI2dComponent;
import heronarts.glx.ui.component.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class FlyerLayoutControlWindow extends UICollapsibleSection {

   /**
     * Does all the heavy lifting of creating parameters and setting up listeners
     * that will communicate any changes made by in the UI to the flyer layout
     * to the drawer.
     */
  private class FlyerLayoutControl extends LXComponent {

    public static final String[] faceOptions = getFaceOptions();


    public static final String[] pipeOptions = { "0".intern(), "1".intern(),
      "2".intern(), "3".intern(), "4".intern(), "5".intern(), "6".intern(),
      "7".intern() };
    
    public static final String[] yOptions = { "1".intern(), "2".intern(),
      "3".intern(), "4".intern(), "5".intern() };

    public DiscreteParameter flyerIndex;
    public final BooleanParameter flashFlyer;
    public final DiscreteParameter face;
    public final BoundedParameter hang;
    public final BoundedParameter tilt;
    public final BoundedParameter rotation;
    public final BoundedParameter x;
    public final DiscreteParameter y;
    public final BoundedParameter stairPosition;
    public final DiscreteParameter pipe;
    public final BoundedParameter mount;

    private FlyerLayoutControlWindow window;

    private static String[] getFaceOptions() {
      int length = Config.FACE_NAMES.length;
      String options[] = new String[length];
      for (int i = 0; i < length; i++) {
        options[i] = Config.FACE_NAMES[i].intern();
      }
      return options;
    }

    FlyerLayoutControl() {
      super(app);

      this.flashFlyer = new BooleanParameter("flash", true);
      this.face = new DiscreteParameter("face", faceOptions);
      this.hang = new BoundedParameter("hang", 0, 8 * FlightGeometry.FEET);
      this.tilt = new BoundedParameter("tilt", -10, 10);
      this.rotation = new BoundedParameter("rotation", 0, 360);
      this.x = new BoundedParameter("x", 0, FlightGeometry.FRONT_WINDOW_WIDTH);
      this.y = new DiscreteParameter("y", yOptions);
      this.stairPosition = new BoundedParameter("Position", 0, 0, 1);
      this.pipe = new DiscreteParameter("pipe", pipeOptions);
      this.mount = new BoundedParameter("mount", 0, 0, 1);

      LXParameterListener flashListener = new LXParameterListener() {
        public void onParameterChanged(LXParameter parameter) {
          BooleanParameter enabled = (BooleanParameter) parameter;
          if (enabled.getValueb()) {
            flyerSelect.focus(new MouseEvent(0, 0, 0f, 0f, 0));
            // When you enable, flash the selected flyer;
            // otherwise, it only flashes when the value changes.
            // If it only flashes when the value changes, then the
            // first time you enable, the selected Flyer flashes,
            // but it doesn't on subsequent enables.
            flyerHighlighter.trigger();
            updateEditableParameters(); 
          }
        }
      };

      this.addGeometryParameter(this.flashFlyer, flashListener);
      this.addGeometryParameter(this.face, null);
      this.addGeometryParameter(this.hang, null);
      this.addGeometryParameter(this.tilt, null);
      this.addGeometryParameter(this.rotation, null);
      this.addGeometryParameter(this.x, null);
      this.addGeometryParameter(this.y, null);
      this.addGeometryParameter(this.stairPosition, null);
      this.addGeometryParameter(this.pipe, null);

      this.respondToParameterChange(null);
    };

    private void addGeometryParameter(LXListenableParameter p, LXParameterListener l) {
      this.addParameter(p.getLabel(), p);
      if (l == null) {
        p.addListener(this::respondToParameterChange);
      }
      else {
        p.addListener(l);
      }
    }

    /**
     * The generic listener that will update all the flyer layout parameters
     * to their most recent value for use by the Drawer.
     * @param p Unused parameter that changed. This function will just update
     * all the parameters at once since there aren't that many.
     */
    public void respondToParameterChange(LXParameter p) {
      // Get Config where parameters are stored for the drawer
      FlyerConfig flyerConfig = flyerHighlighter.getConfig();
      FlyerConfig.FaceConfig faceConfig = flyerConfig.getFaceConfig();

      // Update all parameters
      if(this.window != null) {
        faceConfig.face = face.getOption();
        window.updateEditableParameters();
        flyerConfig.rotation = rotation.getValuef();
        faceConfig.hangDistance = hang.getValuef();
        flyerConfig.tilt = tilt.getValuef();
        faceConfig.x = x.getValuef();
        faceConfig.y = Integer.parseInt(y.getOption());
        faceConfig.position = stairPosition.getValuef();
        faceConfig.pipe = Integer.parseInt(pipe.getOption());
        faceConfig.mountPoint = mount.getValuef();

        // Reload the model for Flight App with new values.
        flyerHighlighter.reloadModel();
      }
    }
  }

  // The width of each UI component in the window
  final static float COMPONENT_W = 60.0F;
  // The height of each UI component in the window
  final static float COMPONENT_H = 20.0F;
  // The padding between each UI component in the window
  final static float COMPONENT_PADDING = 15.0F;
  // The number of UI components we have
  final static int NUM_UI_COMPONENTS = 12;
  // Has to be defined for a UICollapsibleSection - size of each component * 
  // number of components
  final static float HEIGHT = (COMPONENT_H + COMPONENT_PADDING) * (float)NUM_UI_COMPONENTS;
  
  private UIButton flyerFlashButton; // Makes the flyer strobe
  private UIIntegerBox flyerSelect; //The flyer we are changing
  private UIToggleSet faceToggle; // Determines the type of layout 
  private UISlider  hangDistanceSlider; // All faces
  private UISlider  tiltSlider; // All faces
  private UISlider  rotationSlider; // All faces
  private UISlider xSlider; // Front window: x along row
  private UIToggleSet yToggle; // Front window: which row
  private UISlider  stairPositionSlider; // Spirals: point along spiral
  private UIToggleSet  pipeToggle; // Ceiling: which pipe
  private UISlider  mountPointSlider; // Ceiling: were on pipe
  private UIButton saveButton;

  private FlightApp app;
  private FlyerHighlighter flyerHighlighter;
  private FlyerLayoutControl flyerLayoutControl;

  void updateEditableParameters() {
    String face = faceToggle.getSelectedOption();
    // Every mounting point has a hang distance
    hangDistanceSlider.setVisible(true);
    tiltSlider.setVisible(true);
    rotationSlider.setVisible(true);

    // Other measurements are face-dependent; only make relevant
    // ones visible.
    xSlider.setVisible(false);
    yToggle.setVisible(false);
    pipeToggle.setVisible(false);
    stairPositionSlider.setVisible(false);
    mountPointSlider.setVisible(false);

    if (face.equals("C")) {
      pipeToggle.setVisible(true);
      mountPointSlider.setVisible(true);
    } else if (face.equals("F")) {
      xSlider.setVisible(true);
      yToggle.setVisible(true);
    } else if (face.equals("I")) {
      stairPositionSlider.setVisible(true);
    } else if (face.equals("E")) {
      stairPositionSlider.setVisible(true);
    } else {
      System.err.println("Unrecognized face: " + face + "; disable all controls.");
    }
  }

  /**
   * Constructor for the FlyerLayoutControlWindow
   * @param ui The UI on which this window will be a child.
   * @param app The FlightApp, used to access IO features
   * @param flyerHighlighter The flyerHighlighter to access and modify model values
   */
  public FlyerLayoutControlWindow(UI ui, FlightApp app,
    FlyerHighlighter flyerHighlighter) {
    super(ui, 0.0f, 0.0f, ui.leftPane.model.getContentWidth(), HEIGHT);

    this.app = app;
    this.flyerHighlighter = flyerHighlighter;
    this.flyerLayoutControl = new FlyerLayoutControl();
    
    // This parameter is owned by the flyerHighlighter hence it is not part of
    // our FlyerLayoutControl.
    this.flyerSelect = new UIIntegerBox(COMPONENT_W, COMPONENT_H,
      flyerHighlighter.flyerIndex);

    LXParameterListener selectListener = new LXParameterListener() {
      public void onParameterChanged(LXParameter parameter) {
          setFlyer();
      }
    };
    flyerHighlighter.flyerIndex.addListener(selectListener);

    this.flyerFlashButton = new UIButton(COMPONENT_W, COMPONENT_H,
      flyerLayoutControl.flashFlyer).setActiveLabel("Enabled")
      .setInactiveLabel("Disabled");

    this.faceToggle = new UIToggleSet(COMPONENT_W, COMPONENT_H,
      flyerLayoutControl.face);
    this.rotationSlider = new UISlider(COMPONENT_W, COMPONENT_H,
      flyerLayoutControl.rotation);
    this.hangDistanceSlider = new UISlider(COMPONENT_W, COMPONENT_H,
      flyerLayoutControl.hang);
    this.tiltSlider = new UISlider(COMPONENT_W, COMPONENT_H,
      flyerLayoutControl.tilt);
    this.xSlider = new UISlider(COMPONENT_W, COMPONENT_H, flyerLayoutControl.x);
    this.yToggle = new UIToggleSet(COMPONENT_W, COMPONENT_H,
      flyerLayoutControl.y);
    this.stairPositionSlider = new UISlider(COMPONENT_W, COMPONENT_H,
      flyerLayoutControl.stairPosition);
    this.pipeToggle = new UIToggleSet(COMPONENT_W, COMPONENT_H,
      flyerLayoutControl.pipe);

    this.mountPointSlider = new UISlider(COMPONENT_W, COMPONENT_H,
      flyerLayoutControl.mount);

    this.saveButton = (UIButton)(new UIButton(this.width - 8, COMPONENT_H) {
      @Override
      protected void onToggle(boolean active) {
        if (active) {
          FlyerLayoutControlWindow.this.saveLayout();
        }
      }
    }
      .setMomentary(true)
      .setLabel("Save Layout")
      .setDescription("Save current physical layout to " + Config.FLYER_CONFIG_FILE + " and make a backup of the previous layout")
    );

    this.setTitle("Flyer Layout Tool");
    this.setLayout(Layout.VERTICAL, COMPONENT_PADDING);
    this.addChildren(
      new UI2dComponent[] { this.controlRow(ui, "Flyer #", flyerSelect),
        this.controlRow(ui, "Flash Individual Flyer",
          flyerFlashButton),
        this.controlRow(ui, "Position Type", faceToggle),
        this.controlRow(ui, "Hang Distance", hangDistanceSlider),
        this.controlRow(ui, "Z-Tilt", tiltSlider),
        this.controlRow(ui, "Rotation", rotationSlider),
        this.controlRow(ui, "X Position", xSlider),
        this.controlRow(ui, "Horizontal Bar Index", yToggle),
        this.controlRow(ui, "Stair Position", stairPositionSlider),
        this.controlRow(ui, "Ceiling Pipe Index", pipeToggle),
        this.controlRow(ui, "MOUNT", mountPointSlider),
        saveButton
      });

    this.flyerLayoutControl.window = this;

    // setFlyer will call updateEditableParameters()
    setFlyer();
    setDescription("Tool for physically laying out Fractal Flyers in"
      + " Packard stairwell");
  }

  private void saveLayout() {
    String timeString = LocalDateTime.now().format(DateTimeFormatter.ofPattern("uuuu.MM.dd.HH.mm.ss"));
    String backupFileName = Config.FLYER_CONFIG_FILE + ".backup." + timeString;
    try {
      Files.copy(
              Path.of(Config.FLYER_CONFIG_FILE),
              Path.of(backupFileName),
              StandardCopyOption.COPY_ATTRIBUTES,
              StandardCopyOption.REPLACE_EXISTING);
    } catch(IOException ex) {
      System.out.println("Failed to save layout!");
      throw new RuntimeException("Failed to save layout: " + ex.getMessage());
    }
    FlightModel model = (FlightModel) app.getModel();
    FlyerConfig[] configs = model.getFlyerConfigs();
    new IO(app.flags.mediaPath).saveConfigFile(configs, Config.FLYER_CONFIG_FILE);
    this.saveButton.setLabel("Saved. Restart needed.");
  }

  /**
   * Code from the original UIComponents.pde file to produce the save button.
   * Because of different scoping in Chromatik version of this window we do not
   * have access to some of the objects here.
   * TODO: bring relevant objects into scope to allow this to perform the desired
   * functionality.
   */
  // private void makeSaveButton(float yPos) {
  //   UIButton saveButton = new UIButton(4, yPos, this.width-8, 20) {
  //     public void onToggle(boolean active) {
  //       if (active) {
  //         String backupFileName = Config.FLYER_CONFIG_FILE + ".backup." + month() + "." + day() + "." + hour() + "." + minute() + "." + second();
  //         saveStream(backupFileName, Config.FLYER_CONFIG_FILE);
  //         FlightModel model = (FlightModel) app.getModel();
  //         FlyerConfig[] configs = model.getFlyerConfigs();
  //         new IO(app.flags.mediaPath).saveConfigFile(configs, Config.FLYER_CONFIG_FILE);
  //         setLabel("Saved. Restart needed.");
  //       }
  //     }
  //   };
  //   saveButton.setMomentary(true).setLabel("Save Changes").addToContainer(this);
  //   saveButton.setDescription("Save current physical layout to " + Config.FLYER_CONFIG_FILE + " and make a backup of the previous layout");
  // }

  /**
   * Sets the values for the currently selected flyer.
   */
  private void setFlyer() {
    // Flyer position values are loaded from configuration stored in the Flight
    // Model
    int flyerIndex = flyerHighlighter.flyerIndex.getValuei();
    FlightModel model = (FlightModel) app.getModel();
    FlyerConfig config = model.getFlyerConfigs()[flyerIndex];

    // Need to pass (..., notifyListeners=false), otherwise respondToParameterChange triggers, and we start losing data.
    // This is defined for LXListenableParameter; however the UI components getParameter() method returns an
    // LXNormalizedParameter which does not have the overload. Therefore, we need to cast up. Use
    // LXListenableNormalizedParameter instead of the bare LXListenableParameter so as not to bypass any further
    // overloading

    // However, the UIToggleSets work differently; there's no way to disable event propagation with setSelectedOption;
    // HOWEVER we can switch setSelectedOption -> setParameter, which should have the same effect.

    ((LXListenableNormalizedParameter)hangDistanceSlider.getParameter()).setValue(config.getFaceConfig().hangDistance, false);
    ((LXListenableNormalizedParameter)tiltSlider.getParameter()).setValue(config.tilt, false);
    ((LXListenableNormalizedParameter)rotationSlider.getParameter()).setValue(config.rotation, false);
    ((LXListenableNormalizedParameter)xSlider.getParameter()).setValue(config.getFaceConfig().x, false);

//    yToggle.setSelectedOption(yVal);
    String yOption = Integer.toString(config.getFaceConfig().y).intern();
    yToggle.getParameter().setValue(yToggle.getParameter().getMinValue() + FlyerLayoutControlWindow.findStringIndexInArray(yOption, FlyerLayoutControl.yOptions), false);
    yToggle.onParameterChanged(yToggle.getParameter());
//    faceToggle.setSelectedOption(config.getFaceConfig().face.intern());
    String faceOption = config.getFaceConfig().face.intern();
    faceToggle.getParameter().setValue(FlyerLayoutControlWindow.findStringIndexInArray(faceOption, FlyerLayoutControl.faceOptions), false);
    faceToggle.onParameterChanged(faceToggle.getParameter());

    ((LXListenableNormalizedParameter)stairPositionSlider.getParameter()).setValue(config.getFaceConfig().position, false);

//    pipeToggle.setSelectedOption(Integer.toString((int)config.getFaceConfig().pipe));
    String pipeOption = Integer.toString((int)config.getFaceConfig().pipe);
    pipeToggle.getParameter().setValue(FlyerLayoutControlWindow.findStringIndexInArray(pipeOption, FlyerLayoutControl.pipeOptions), false);
    pipeToggle.onParameterChanged(pipeToggle.getParameter());

    ((LXListenableNormalizedParameter)mountPointSlider.getParameter()).setValue(config.getFaceConfig().mountPoint, false);

    // Updates the FlyerLayoutControl Window to present position options based
    // on the Flyer's configuration.
    updateEditableParameters();
  }

  private static int findStringIndexInArray(String searchFor, String[] array) throws java.lang.IndexOutOfBoundsException {
    for(int i = 0;i < array.length;++i) {
      if(array[i].equals(searchFor)) {
        return i;
      }
    }
    throw new java.lang.IndexOutOfBoundsException();
  }
}
