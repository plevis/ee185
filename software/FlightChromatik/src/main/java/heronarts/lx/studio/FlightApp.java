/**
 * Copyright 2022- Mark C. Slee, Heron Arts LLC
 *
 * This file is part of the LX Studio software library. By using
 * LX, you agree to the terms of the LX Studio Software License
 * and Distribution Agreement, available at: http://lx.studio/license
 *
 * Please note that the LX license is not open-source. The license
 * allows for free, non-commercial use.
 *
 * HERON ARTS MAKES NO WARRANTY, EXPRESS, IMPLIED, STATUTORY, OR
 * OTHERWISE, AND SPECIFICALLY DISCLAIMS ANY WARRANTY OF
 * MERCHANTABILITY, NON-INFRINGEMENT, OR FITNESS FOR A PARTICULAR
 * PURPOSE, WITH RESPECT TO THE SOFTWARE.
 *
 * @author Mark C. Slee <mark@heronarts.com>
 *
 * Contributors:
 * Dave Deriso <deriso@gmail.com>
 */

package heronarts.lx.studio;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import edu.stanford.ee185.engine.Config;
import edu.stanford.ee185.engine.FlyerHighlighter;
import edu.stanford.ee185.engine.IO;
import edu.stanford.ee185.engine.FlightEngine;
import edu.stanford.ee185.ui.FlyersDrawer;
import edu.stanford.ee185.ui.PackardDrawer;
import edu.stanford.ee185.model.FlightGeometry;
import edu.stanford.ee185.model.FlightModel;
// import edu.stanford.ee185.engine.ChannelMixerWindow;
// import heronarts.lx.studio.ui.FlightPatternControlWindow;
import edu.stanford.ee185.model.FlyerConfig;
import edu.stanford.ee185.model.Model;
// import edu.stanford.ee185.ui.UIShapes;
// No UI3dComponent defintion?
import heronarts.lx.LX;
import heronarts.lx.LXPlugin;
import heronarts.lx.studio.ui.model.FlyerLayoutControlWindow;
import org.lwjgl.bgfx.BGFX;
import heronarts.lx.studio.ui.global.MasterControl;

public class FlightApp extends LXStudio {

  public static final String CATEGORY = "Flight"; // general category for all patterns/effects
                                                                  // (everything should really go under here)

  private static final DateFormat LOG_FILENAME_FORMAT = new SimpleDateFormat(
    "'Flight-'yyyy.MM.dd-HH.mm.ss'.log'");
  

  @LXPlugin.Name("Flight")
  public static class Plugin implements LXStudio.Plugin, LX.Listener {

    FlyersDrawer flightDrawer;
    PackardDrawer packardDrawer;
    // ScriptControlWindow scriptControl;
    MasterControl masterControl;
    FlyerLayoutControlWindow layoutControl;
    FlyerHighlighter flyerHighlighter;
    // ChannelMixerWindow mixer;
    // PatternControlWindow patternControl;

    public Plugin(LX lx) {
      log("Plugin(LX)");
      lx.addListener(this);
    }

    public void initialize(LX lx) {
      log("Plugin.initialize()");
      
      // Register flight patterns + effects here
      // lx.registry.addPattern(edu.stanford.ee185.pattern.PatternClassName.class);
      // lx.registry.addEffect(edu.stanford.ee185.effect.EffectClassName.class);
    }

    public void initializeUI(LXStudio lx, LXStudio.UI ui) {
      log("Plugin.initializeUI()");
    }

    public void onUIReady(LXStudio lx, LXStudio.UI ui) {
      log("Plugin.onUIReady()");

      FlightApp app = (FlightApp) lx;
      FlightModel model = (FlightModel) app.getModel();

      flyerHighlighter = new FlyerHighlighter(lx, model);
      lx.addEffect(flyerHighlighter);

      // Configure the camera to rotate around a center point that's
      // in the middle of the front window and 15 feet inside.
      // Also set some camera angle constants so things look OK. Something
      // more principled would be great. -pal
      ui.preview.setTheta((float)(30.0 * Math.PI / 180.0))
                .setPhi((float) (10.0 * Math.PI / 180.0))
                .setCameraVelocity(10000F)
                .setCameraAcceleration(10000F)
                .setCenter(FlightGeometry.FRONT_WINDOW_WIDTH / 2,
                           FlightGeometry.FRONT_WINDOW_CORNER_HEIGHT / 4f,
                           20 * FlightGeometry.FEET)
                .setRadius(95 * FlightGeometry.FEET);
      
      masterControl = new MasterControl(ui, app);
      ui.leftPane.global.removeAllChildren();
      ui.leftPane.global.addChildren(masterControl);

      flightDrawer = new FlyersDrawer(app, model, flyerHighlighter, masterControl);
      ui.preview.addComponent(flightDrawer);

      packardDrawer = new PackardDrawer(app);
      ui.preview.addComponent(packardDrawer);

      // TODO: this does not seem to be doing what we expect it to do
      ui.contentPicker.patternList.removeAllChildren();
      ui.contentPicker.effectLabel.removeFromContainer();
      ui.contentPicker.effectList.removeFromContainer();

      // scriptControl = new ScriptRateWindow(ui);
      // ui.preview.addComponent(scriptControl);

      layoutControl = new FlyerLayoutControlWindow(ui, app, flyerHighlighter);
      ui.leftPane.model.removeAllChildren();
      ui.leftPane.model.addChildren(layoutControl);

      FlightEngine engine = new FlightEngine(app.flags.mediaPath, model, app);
    }

    public void dispose() {

    }
  }

  private FlightApp(Flags flags, FlyerConfig[] flyerConfigs) throws IOException {
    super(flags, new Model(flyerConfigs).getFlightModel());
  }

  @Override
  protected final Permissions getPermissions() {
    return Permissions.UNRESTRICTED;
  }

  private static int WINDOW_WIDTH = 1400;
  private static int WINDOW_HEIGHT = 800;

  public static void main(String[] args) {

    log("LXStudio.VERSION: " + LXStudio.VERSION);
    log("Running java " +
        System.getProperty("java.version") + " " +
        System.getProperty("java.vendor") + " " +
        System.getProperty("os.name") + " " +
        System.getProperty("os.version") + " " +
        System.getProperty("os.arch"));

    final Flags flags = new Flags();

    flags.windowTitle = "Flight";
    flags.windowWidth = WINDOW_WIDTH;
    flags.windowHeight = WINDOW_HEIGHT;
    flags.classpathPlugins.add("heronarts.lx.studio.FlightApp$Plugin");

    String logFileName = LOG_FILENAME_FORMAT.format(Calendar.getInstance().getTime());
    File logs = new File(LX.Media.LOGS.getDirName());
    if (!logs.exists()) {
      logs.mkdir();
    }
    setLogFile(new File(LX.Media.LOGS.getDirName(), logFileName));

    try {

      System.out
        .println("Starting LXStudio; initialized Fractal Flyer data model from "
          + Config.FLYER_CONFIG_FILE);
      
      // default mediaPath is current working directory
      IO io = new IO(flags.mediaPath);
      final FlyerConfig[] flyerConfigs = io
        .loadConfigFile(Config.FLYER_CONFIG_FILE);

      // On the Linux ThinkPad, the default renderer that BGFX chooses is Vulkan. However, GLX does not support Vulkan
      // shaders, so we use OpenGL instead.
      // Unfortunately, on some platforms (e.g. macOS), the OpenGL backend doesn't really work, so  we:
      //  - dynamically detect the set of BGFX supported renderers
      //  - cull any that GLX doesn't have shaders for
      //  - if OpenGL is the only thing left AND there were other options that BGFX supports, set the useOpenGL flag to
      //    force OpenGL usage.
      {
        int[] renderers = new int[BGFX.BGFX_RENDERER_TYPE_COUNT];
        BGFX.bgfx_get_supported_renderers(renderers);
        List<Integer> platformSupportedRenderers = new ArrayList<>();
        System.out.println("Detecting BGFX supported renderers...");
        for(int r : renderers) {
          if(r != BGFX.BGFX_RENDERER_TYPE_NOOP) {
            System.out.println("\tRenderer: " + r + " (" + BGFX.bgfx_get_renderer_name(r) + ")");
            platformSupportedRenderers.add(r);
          }
        }
        final List<Integer> glxSupportedRenderers = Arrays.asList(
                BGFX.BGFX_RENDERER_TYPE_DIRECT3D9,
                BGFX.BGFX_RENDERER_TYPE_DIRECT3D11,
                BGFX.BGFX_RENDERER_TYPE_DIRECT3D12,
                BGFX.BGFX_RENDERER_TYPE_OPENGL,
                BGFX.BGFX_RENDERER_TYPE_METAL);
        List<Integer> permittedRenderers = new ArrayList<>();
        for(int i = 0;i < platformSupportedRenderers.size();i++) {
          int rendererType = platformSupportedRenderers.get(i);
          if(glxSupportedRenderers.contains(rendererType)) {
            permittedRenderers.add(rendererType);
          } else {
            System.out.println("Warning: renderer type " + rendererType + " (" + BGFX.bgfx_get_renderer_name(rendererType) + ") is not supported by GLX");
          }
        }
        if(platformSupportedRenderers.size() > 1 && permittedRenderers.size() == 1 && permittedRenderers.get(0) == BGFX.BGFX_RENDERER_TYPE_OPENGL) {
          System.out.println("Forcing OpenGL usage as fallback.");
          flags.useOpenGL = true;
        }
      }

      final FlightApp lx = new FlightApp(flags, flyerConfigs);

      // TODO(2024): Decide on bootstrapping logic here, maybe automatically open
      // a project file?
      // lx.openProject(autoPlayFile);

      lx.run();
    } catch (Exception x) {
      throw new RuntimeException(x);
    }
  }

}
