package heronarts.lx.studio.ui.global;

import heronarts.lx.studio.FlightApp;
import heronarts.lx.studio.LXStudio.UI;
import heronarts.glx.ui.UI2dComponent;
import heronarts.glx.ui.UI2dContainer;
import heronarts.lx.parameter.BooleanParameter;
import heronarts.lx.parameter.BoundedParameter;
import heronarts.glx.ui.component.*;


public class MasterControl extends UICollapsibleSection {

  public class MasterMeter extends UI2dContainer {
   public MasterMeter(UI ui, float x, float y, float w, float h) {
      super(x, y, w, h);
      float y_position = h - 42.0F;
      float delta = 42.0F;
      float margin = 4.0F;

      enableButton = new UIButton(this.getWidth(), 20.0F, enableStatus)
                                 .setActiveLabel("Enable").setInactiveLabel("Disable");
      hueKnob = new UIKnob(margin, y_position, hueParameter);
      satKnob = new UIKnob(margin+delta, y_position, saturationParameter);
      briKnob = new UIKnob(margin+2*delta, y_position, brightnessParameter);
      speedKnob = new UIKnob(margin+3*delta, y_position, speedParameter);

      enableButton.addToContainer(this);
      hueKnob.addToContainer(this);
      satKnob.addToContainer(this);
      briKnob.addToContainer(this);
      speedKnob.addToContainer(this);
   }
  }

  // The width of each UI component in the window
  final static float COMPONENT_W = 40.0F;
  // The height of each UI component in the window
  final static float COMPONENT_H = 20.0F;
  // The padding between each UI component in the window
  final static float COMPONENT_PADDING = 30.0F;
  // The number of UI componenets we have
  final static float NUM_UI_COMPONENTS = 12.0F;
  // Has to be defined for a UICollapsibleSection - size of each component * 
  // number of components
  final static float HEIGHT = (COMPONENT_H + COMPONENT_PADDING) * NUM_UI_COMPONENTS;

  private FlightApp app;
  public final BoundedParameter speedParameter;
  public final BoundedParameter saturationParameter;
  public final BoundedParameter brightnessParameter;
  public final BoundedParameter hueParameter;
  public final BooleanParameter enableStatus;
  public UIButton enableButton;
  public UIKnob hueKnob;
  public UIKnob satKnob;
  public UIKnob briKnob;
  public UIKnob speedKnob;

  /**
   * Constructor for the FlyerLayoutControlWindow
   * @param ui The UI on which this window will be a child.
   * @param app The FlightApp, used to access IO features
   * @param flyerHighlighter The flyerHighlighter to access and modify model values
   */
  public MasterControl(UI ui, FlightApp app) {
    super(ui, 0.0f, 0.0f, ui.leftPane.model.getContentWidth(), HEIGHT);

    this.speedParameter = new BoundedParameter("Speed",100, 0, 100);
    this.saturationParameter = new BoundedParameter("Sat",100, 0, 100);
    this.brightnessParameter = new BoundedParameter("Bri",100, 0, 100);
    this.hueParameter = new BoundedParameter("Hue",0, 0, 360);
    this.enableStatus = new BooleanParameter("enableButton", true);

    this.app = app;
    this.setTitle("Master Control");
    this.setLayout(Layout.VERTICAL, COMPONENT_PADDING);
    this.addChildren(
      new UI2dComponent[] {new MasterMeter(ui, 0.0F, 0.0F, this.getContentWidth(), 68.0F)}
    );
    setDescription("Tool for physically laying out Fractal Flyers in"
      + " Packard stairwell");
  }
}
