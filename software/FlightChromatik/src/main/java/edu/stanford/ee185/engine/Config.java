package edu.stanford.ee185.engine;

public final class Config {
  public static String[] AUTOMATION_FILES = { "data/track.json" };
  public static final String FLYER_CONFIG_FILE = "data/flyers.json";
  public static final String OUTPUT_NAMES_FILE = "data/outputs.json";
  public static final String[] FACE_NAMES = { "C", "F", "I", "E" };
}
