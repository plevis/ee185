package edu.stanford.ee185.model;

import heronarts.lx.model.LXPoint;

public class LightSamplePointModel extends LXPoint implements LightSamplePoint {
  protected LightSamplePoint.LightCorners location;
  protected Integer wingIndex;
  protected int bodyIndex;

  public LightSamplePointModel(LightSamplePoint.LightCorners location, Integer wingIndex, int bodyIndex) {
    this(new LightSamplePointModelBuilder(location, wingIndex, bodyIndex));
  }

  public LightSamplePointModel(LightSamplePointModelBuilder lightSamplePointModelBuilder) {
    super(lightSamplePointModelBuilder.point);
    this.wingIndex = lightSamplePointModelBuilder.wingIndex;
    this.bodyIndex = lightSamplePointModelBuilder.bodyIndex;
    this.location = lightSamplePointModelBuilder.location;
  }

  private static class LightSamplePointModelBuilder {
    private final LightSamplePoint.LightCorners location;
    private final Integer wingIndex;
    private final int bodyIndex;
    private final LXPoint point;

    private LightSamplePointModelBuilder(LightSamplePoint.LightCorners location, Integer wingIndex, int bodyIndex) {
      this.location = location;
      this.wingIndex = wingIndex;
      this.bodyIndex = bodyIndex;

      if (wingIndex != null) {
        this.point = new LXPoint(lightGeometryPoints.get((wingIndex % NUM_WINGS_PER_FLYER) * NUM_LIGHT_POINTS_PER_WING + location.position));
      } else {
        this.point = new LXPoint(lightGeometryPoints.get(location.position));
      }
    }
  }

  @Override
  public int getIndex() {
    return this.index;
  }

  @Override
  public int getBodyIndex() {
    return this.bodyIndex;
  }

  @Override
  public LightCorners getLocation() {
    return this.location;
  }

  @Override
  public Integer getWingIndex() {
    return this.wingIndex;
  }
}