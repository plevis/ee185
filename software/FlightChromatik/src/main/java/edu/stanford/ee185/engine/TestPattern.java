package edu.stanford.ee185.engine; 

import java.util.List;
import java.util.PrimitiveIterator.OfInt;
import java.util.ArrayList;
import java.util.BitSet;

import edu.stanford.ee185.model.FlyerModel;
/* May be bad style to include FlightModel here. Used in the FlyerShape API. */
import edu.stanford.ee185.model.FlightModel;
import edu.stanford.ee185.model.FlightGeometry;
import edu.stanford.ee185.model.LightSamplePointModel;
import edu.stanford.ee185.model.WingModel;
import java.util.Random;

import heronarts.lx.LX;
import heronarts.lx.modulator.SawLFO;
import heronarts.lx.modulator.SinLFO;
import heronarts.lx.modulator.TriangleLFO;
import heronarts.lx.parameter.BoundedParameter;


/**
 * Rotates all the Flyers through colors on the HSB color wheel, controlled by a
 * sinusoid. The wing position is based on the hue, with 0 being fully down and
 * 360 being fully up. There is one control, the speed they move through the
 * wheel.
 *
 * @author Philip Levis <pal@cs.stanford.edu>
 */
class ColorWheel extends FlightPattern {
  final BoundedParameter period = new BoundedParameter("PERIOD", 15000, 1000, 60000);
  final SinLFO color = new SinLFO(0, 360, period);

  ColorWheel(LX lx) {
    super(lx);
    addParameter("ColorWheel_PERIOD", period);
    addModulator(color).start();
  }

  @Override
  public void run(double deltaMs) {
    float hue = color.getValuef();
    int skew = (int) ((hue - 180f) / 2);

    for (WingModel wing : model.getAllWings()) {
      setSkew(wing, skew);
    }
    int bodycount = 0;
    for (LightSamplePointModel bodyPoint : model.getAllBodyLights()) {
      setColor(bodyPoint, LX.hsb(hue, 100, 100));
    }
    int wingcount = 0;
    for (LightSamplePointModel wingPoint : model.getAllWingsLights()) {
      setColor(wingPoint, LX.hsb(hue, 100, 100));
    }
  }
}

/**
 * Defines a color gradient based on distance from a user defined center. This
 * is controllable with x,y,z and the color hue.
 * 
 * @author Kyung-Tae Kim <kkim801@stanford.edu>
 */
class ColorGradient extends FlightPattern {
  final BoundedParameter period = new BoundedParameter("PERIOD", 15000, 1000, 60000);
  final BoundedParameter x_var = new BoundedParameter("X", 335, 25, 644);
  final BoundedParameter y_var = new BoundedParameter("Y", 250, 0, 507);
  final BoundedParameter z_var = new BoundedParameter("Z", 209, 24, 393);
  final SinLFO color = new SinLFO(0, 360, period);

  ColorGradient(LX lx) {
    super(lx);
    addParameter("ColorGradient_PERIOD", period);
    addParameter("ColorGradient_X", x_var);
    addParameter("ColorGradient_Y", y_var);
    addParameter("ColorGradient_Z", z_var);
    addModulator(color).start();
  }

  /**
   * Returns the base color set by the color SinLFO, along with the distance from
   * the user designated center point. This gives it the effect of a gradient.
   */
  int retHue(FlyerModel fl, float x, float y, float z) {
    double x_pow = Math.pow((fl.getX() - x), 2);
    double y_pow = Math.pow((fl.getY() - y), 2);
    double z_pow = Math.pow((fl.getZ() - z), 2);
    return (int) (color.getValuef() + Math.sqrt(x_pow + y_pow + z_pow)) % 360;
  }

  /**
   * Standard run function, responsible for running the loop.
   */
  @Override
  public void run(double deltaMs) {
    for (int i = 0; i < FlightGeometry.NUM_FLYERS; i++) {
      for (LightSamplePointModel point : model.getFlyerLights(i)) {
        // Sets color of a given point to the value returned by retHue
        setColor(point, LX
            .hsb(retHue(model.getFlyers().get(i), x_var.getValuef(), y_var.getValuef(), z_var.getValuef()), 100, 100));
      }
    }
  }
}

/**
 * This function creates a rotating ray of color around a center point.
 * 
 * @author Kyung-Tae Kim <kkim801@stanford.edu>
 */
class WheelOfcolor extends FlightPattern {
  final BoundedParameter period = new BoundedParameter("PERIOD", 15000, 1000, 60000);
  final SinLFO color = new SinLFO(0, 360, period);

  WheelOfcolor(LX lx) {
    super(lx);
    addParameter("WheelOfColor_PERIOD", period);
    addModulator(color).start();
  }

  /**
   * Returns the color for any given FlyerModel
   * 
   */
  int retHue(FlyerModel fl) {
    float x = fl.getX() - 310; // Approximate x offset from center
    float y = fl.getY() - 250; // Approximate y offset from center
    double angle = 57.3 * Math.atan(x / y);// Calculate angle of flyer
    if (y < 250)// decide if it is below the horizontal line
      return (int) (angle + color.getValuef() + 180) % 360;
    else
      return (int) (angle + color.getValuef()) % 360;
  }

  @Override
  public void run(double deltaMs) {
    for (int i = 0; i < FlightGeometry.NUM_FLYERS; i++) {
      for (LightSamplePointModel point : model.getFlyerLights(i)) {
        setColor(point, LX.hsb(retHue(model.getFlyers().get(i)), 100, 100));
      }
    }
  }
}

/**
 * Flyer wing only function, where the flyers traverse in an ordering, flpping
 * its wings in succession
 * 
 * @author Kyung-Tae Kim <kim1@stanford.edu>
 */
class ClimbFlap extends FlightPattern {
  int NUM_FLYERS = FlightGeometry.NUM_FLYERS;
  int FLYER_MOD = NUM_FLYERS - 1;
  final BoundedParameter rate = new BoundedParameter("RATE", 5000, 5000, 100000);
  final SawLFO flyerIndex = new SawLFO(0, FLYER_MOD, rate);

  final BoundedParameter speed = new BoundedParameter("PERIOD", 13500, 12500, 25000);
  final SinLFO colorFlap = new SinLFO(0, 360, speed);

  ClimbFlap(LX lx) {
    super(lx);
    addParameter("climbFlap_RATE", rate);
    // Starting modulator that takes in those speeds
    addModulator(flyerIndex).start();
    // MODULATOR "the part that calculates the values"
    addParameter("climbFlap_SPEED", speed);
    addModulator(colorFlap).start();
  }

  @Override
  public void run(double deltaMs) {
    for (int i = 0; i < FlightGeometry.NUM_FLYERS; i++) {
      for (WingModel wm : model.getFlyerWings(i)) {
        int skew = (int) ((colorFlap.getValuef() - i * (180f / FlightGeometry.NUM_FLYERS)) / 2);
        setSkew(wm, skew);
      }
    }
  }
}

/**
 * Flaps the wings in a slow fashion where the right and left wing are skewed
 * 
 * @author Kyung-Tae Kim <kim1@stanford.edu>
 */
class SlowFlap extends FlightPattern {
  int NUM_FLYERS = FlightGeometry.NUM_FLYERS;
  int FLYER_MOD = NUM_FLYERS - 1;

  // Speed by which the wings are rotating "GUESS"
  final BoundedParameter speed = new BoundedParameter("PERIOD", 13500, 12500, 25000);
  final SinLFO colorFlap = new SinLFO(0, 360, speed);

  SlowFlap(LX lx) {
    super(lx);
    addParameter("slowFlap_SPEED", speed);
    addModulator(colorFlap).start();
  }

  @Override
  public void run(double deltaMs) {
    int skew = (int) ((colorFlap.getValuef() - 180f) / 2);
    int rightskew = -(int) ((colorFlap.getValuef() - 180f) / 2);
    for (int i = 0; i < FlightGeometry.NUM_FLYERS; i++) {
      WingModel leftmodel = model.getFlyerLeftWing(i);
      setSkew(leftmodel, skew);
      WingModel rightmodel = model.getFlyerRightWing(i);
      setSkew(rightmodel, rightskew);
    }
  }
}

/**
 * Function to create a gradient across all wingpoints. In the standard GUI,
 * this does not have an effect as the body points do not have discrete points
 * yet.
 * 
 * @author Kyung-Tae Kim <kim1@stanford.edu>
 */
class WingColor extends FlightPattern {
  WingColor(LX lx) {
    super(lx);
  }

  @Override
  public void run(double deltaMs) {
    int wingcount = 0;
    for (LightSamplePointModel wingPoint : model.getAllWingsLights()) {
      setColor(wingPoint, LX.hsb(wingcount, 100, 100));
      wingcount = (wingcount + 1) % 255;
    }
  }
}

/**
 * Simple function to createa an offset to seperate the body color from the wing
 * color. Should be used as a filter function
 * 
 * @author Kyung-Tae Kim <kkim801@stanford.edu>
 */
class BodyOffset extends FlightPattern {
  final BoundedParameter color = new BoundedParameter("COLOR", 0, 0, 360);
  final BoundedParameter saturation = new BoundedParameter("SATURATION", 50, 0, 100);
  final BoundedParameter brightness = new BoundedParameter("BRIGHTNESS", 50, 0, 100);

  BodyOffset(LX lx) {
    super(lx);
    addParameter("BodyOffset_COLOR", color);
    addParameter("BodyOffset_SAT", saturation);
    addParameter("BodyOffset_BRI", brightness);
  }

  @Override
  public void run(double deltaMs) {
    for (int i = 0; i < FlightGeometry.NUM_FLYERS; i++) {
      setColor(model.getFlyerBodyLight(i),
          LX.hsb((int) color.getValuef(), (int) saturation.getValuef(), (int) brightness.getValuef()));
    }
  }
}

/**
 * Function to flap the wings in a cascading fashion, where the origin is the
 * center y plane
 *
 * @author Kyung-Tae Kim <kkim801@stanford.edu>
 */
class SplitWings extends FlightPattern {
  final BoundedParameter speed = new BoundedParameter("PERIOD", 13500, 12500, 25000);
  final SinLFO flapSpeed = new SinLFO(0, 360, speed);

  SplitWings(LX lx) {
    super(lx);
    addParameter("SplitWings_SPEED", speed);
    addModulator(flapSpeed).start();
  }

  @Override
  public void run(double deltaMs) {
    for (int i = 0; i < FlightGeometry.NUM_FLYERS; i++) {
      for (WingModel wm : model.getFlyerWings(i)) {
        int skew = (int) ((flapSpeed.getValuef() - (model.getFlyers().get(i).getZ() - 185f)) / 2);
        setSkew(wm, skew);
      }
    }
  }
}

/**
 * Creates a pattern like the audio level indicators, where the colors are
 * organzed in a rainbow hue. The function works by either setting a "goal" of a
 * random height or 0. If the bar is ascending, we ascend until we hit the
 * "goal." Otherwise, when the bar is descending, we descend until we hit 0.
 * 
 * @author Kyung-Tae Kim <kkim801@stanford.edu>
 */
class Surge extends FlightPattern {
  final BoundedParameter period = new BoundedParameter("PERIOD", 1000, 100, 4000);
  final TriangleLFO alternator = new TriangleLFO(0, 2, period);
  Random rand = new Random();
  // height track tracks the current "bar" height
  int height_track[] = new int[6];
  // height goal sets the height that the bar should reach
  int height_goal[] = new int[6];
  // hue values for the rainbows
  final int colors[] = { 0, 30, 60, 120, 210, 270 };

  Surge(LX lx) {
    super(lx);
    addParameter("Surge_PERIOD", period);
    addModulator(alternator).start();
  }

  // Returns a new height for a "bar" to reach
  public int getHeight() {
    return rand.nextInt(250) + 250;
  }

  // Returns whether a flyer should be on
  public boolean getOn(FlyerModel fl) {
    return height_track[(int) (fl.getX() / 110)] > fl.getY();
  }

  int prev = (int) alternator.getValuef();
  int current;

  @Override
  public void run(double deltaMs) {
    int current = (int) alternator.getValuef();
    if (current != prev) {
      prev = current;
      for (int i = 0; i < 6; i++) {
        // If the bar hits the ground again, a new goal height is set
        if (height_track[i] < 1) {
          height_goal[i] = getHeight();
          height_track[i] = 2;// cheeky code
        } else {
          // If the bar is in the descending stage, we decrement height
          if (height_goal[i] == 0) {
            height_track[i] -= 5;
          }
          // The bar has exceeded the goal, start descending
          else if (height_track[i] > height_goal[i]) {
            height_goal[i] = 0;
          }
          // We strictly ascend.
          else {
            height_track[i] += 5;
          }
        }
      }
      for (FlyerModel fl : model.getFlyers()) {
        for (LightSamplePointModel point : model.getFlyerLights(fl.getIndex())) {
          if (getOn(fl))
            setColor(point, LX.hsb(colors[(int) fl.getX() / 110], 100, 100));
          else
            setColor(point, LX.hsb(colors[(int) fl.getX() / 110], 0, 0));
        }
      }
    }
  }
}

/**
 * This pattern runs as a loop. At the start of each iteration, a new random
 * flyer is selected. This may propogate to a nearby neightbor, depending on a
 * probability set in the function. The wing colors are colored as a gradient
 * that gets brighter over time.
 * 
 * @author Kyung-Tae Kim <kim1@stanford.edu>
 */
class Fractal extends FlightPattern {
  final BoundedParameter color = new BoundedParameter("COLOR", 1000, 100, 4000);
  final BoundedParameter period = new BoundedParameter("PERIOD", 1000, 100, 4000);
  final TriangleLFO alternator = new TriangleLFO(0, 2, period);
  final SinLFO color_sel = new SinLFO(0, 360, period);

  Fractal(LX lx) {
    super(lx);
    addParameter("Fractal_COLOR", color);
    addModulator(color_sel).start();
    addParameter("Fractal_PERIOD", period);
    addModulator(alternator).start();
  }

  ArrayList<ArrayList<Integer>> distances = new ArrayList<ArrayList<Integer>>(model.getFlyers().size());
  Random rand = new Random();

  public FlyerModel chooseRand() {
    int size = model.getFlyers().size();
    int sel = rand.nextInt(size);
    return model.getFlyers().get(sel);
  }

  /**
   * Helper function determine if fl1 is in sphere defined by distanceMargin. I'm
   * not quite sure why i have this and getRadiusOn as seperate functions.
   */
  public boolean closeness(FlyerModel fl0, FlyerModel fl1, int distanceMargin) {
    double distance = Math.sqrt(Math.pow((fl0.getX() - fl1.getX()), 2) + Math.pow((fl0.getY() - fl1.getY()), 2)
        + Math.pow((fl0.getZ() - fl1.getZ()), 2));
    return (distance < distanceMargin);
  }

  /**
   * This function returns whether a given flyer falls wtthin a radius defined by
   * a entral flyer and the distVariable
   * 
   * @param distVariable Defines the radius within which the flyer must fall
   * @param f0           the center flyer
   * @param f1           the flyer we are deciding if we should illuminate
   */
  public boolean getRadiusOn(FlyerModel fl0, FlyerModel fl1, int distVariable) {
    if (Math.abs(fl0.getX() - fl1.getX()) < distVariable && Math.abs(fl0.getY() - fl1.getY()) < distVariable
        && Math.abs(fl0.getZ() - fl1.getZ()) < distVariable)
      return true;
    return false;
  }

  /**
   * precomputes nearby flyers at beginning of execution. It stores nearby flyers
   * in an ArrayList, if the given Flyer falls within the radius defined by
   * distaneMargin
   */
  public void precompute(int distanceMargin) {
    for (int i = 0; i < model.getFlyers().size(); i++) {
      ArrayList<Integer> temp = new ArrayList<Integer>();
      distances.add(temp);
    }
    for (int i = 0; i < model.getFlyers().size(); i++) {
      FlyerModel fl0 = model.getFlyers().get(i);
      for (int j = 0; j < model.getFlyers().size(); j++) {
        if (i != j) {
          if (!distances.get(i).contains(j) && !distances.get(j).contains(i)) {
            if (closeness(fl0, model.getFlyers().get(j), 150)) {
              distances.get(i).add(j);
              distances.get(j).add(i);
            }
          }
        }
      }
    }
  }

  /**
   * Selects a nearby flyer given by a flyer in the neighbor arraylist
   */
  public int chooseNearRandom(int flyerModelIndex) {
    int next_flyer = rand.nextInt(distances.get(flyerModelIndex).size());
    System.out.println(next_flyer);
    return distances.get(flyerModelIndex).get(next_flyer);
  }

  int current = rand.nextInt(model.getFlyers().size());
  int[] active_nodes = new int[model.getFlyers().size()];
  boolean[] active_edges = new boolean[model.getFlyers().size()];

  /**
   * Randomly designates neighbors of currently active edges to be newly active
   * nodes. active_edges are the "newly" activated nodes active_nodes are
   * currently active nodes
   */
  private void new_active_edges() {
    Random rand = new Random();
    for (int i = 0; i < model.getFlyers().size(); i++) {
      if (active_edges[i])
        active_nodes[i] = 50;
    }
    for (int i = 0; i < model.getFlyers().size(); i++) {
      if (active_edges[i]) {
        active_edges[i] = false;
        for (int j = 0; j < distances.get(i).size(); j++) {
          int temp = distances.get(i).get(j);
          // the number 25 designates the probability that a edge
          // is selected to be activated or not
          if (active_nodes[temp] == 0 && rand.nextInt(100) < 25) {
            active_edges[temp] = true;
          }
        }
      }
    }
  }

  /**
   * Deactivates and resets all nodes, so that we can start a new fractal pattern
   *
   */
  private void restart() {
    for (int i = 0; i < FlightGeometry.NUM_FLYERS; i++) {
      active_nodes[i] = 0;
      active_edges[i] = false;
    }
    Random rand = new Random();
    active_edges[rand.nextInt(76)] = true;
    ;
  }

  boolean precompute = true;
  int alternatorval = (int) alternator.getValuef();
  int previous = alternatorval;
  int iterations = 0;
  int current_color = (int) color_sel.getValuef();

  @Override
  public void run(double deltaMs) {
    if (precompute) {
      precompute(100);
      precompute = false;
      active_edges[current] = true;
    }
    int newval = (int) alternator.getValuef();
    if (iterations > 10) {
      restart();
      current_color = (int) color_sel.getValuef();
      iterations = 0;
    }
    if (previous != newval) {
      iterations++;
      new_active_edges();
      previous = newval;
      for (int i = 0; i < model.getFlyers().size(); i++) {
        for (LightSamplePointModel point : model.getFlyerLights(i)) {
          // set non-active nodes to 0
          if (active_nodes[i] == 0)
            setColor(point, LX.hsb(0, 0, 0));
          else {
            // Decreemnt the integers in active_nodes
            active_nodes[i]--;
            setColor(point, LX.hsb(current_color, active_nodes[i] * 2, active_nodes[i] * 2));
          }
        }
      }
    }
  }
}

/**
 * A spotlight pattern that randomly traverses across various flyers This method
 * simulates a spotlight by creating a white sphere of light that travels from a
 * flyer to its neighbors
 * 
 * @author Kyung-Tae Kim <kkim801@stanford.edu>
 */
class Spotlight extends FlightPattern {
  final BoundedParameter period = new BoundedParameter("PERIOD", 300, 100, 4000);
  final TriangleLFO alternator = new TriangleLFO(0, 2, period);

  Spotlight(LX lx) {
    super(lx);
    addParameter("Spotlight_PERIOD", period);
    addModulator(alternator).start();
  }

  // This distance is used to store the distance from each of the flyer
  // to other flyers
  ArrayList<ArrayList<Integer>> distances = new ArrayList<ArrayList<Integer>>(model.getFlyers().size());
  Random rand = new Random();

  /**
   * Choose a random flyer
   */
  public FlyerModel chooseRand() {
    int size = model.getFlyers().size();
    int sel = rand.nextInt(size);
    return model.getFlyers().get(sel);
  }

  /**
   * Helper function determine if fl1 is in sphere defined by distanceMargin I'm
   * not quite sure why i have this and getRadiusOn as seperate functions.
   */
  public boolean closeness(FlyerModel fl0, FlyerModel fl1, int distanceMargin) {
    double distance = Math.sqrt(Math.pow((fl0.getX() - fl1.getX()), 2) + Math.pow((fl0.getY() - fl1.getY()), 2)
        + Math.pow((fl0.getZ() - fl1.getZ()), 2));
    return (distance < distanceMargin);
  }

  /**
   * This function returns whether a given flyer falls wtthin a radius defined by
   * a entral flyer and the distVariable
   * 
   * @param distVariable Defines the radius within which the flyer must fall
   * @param f0           the center flyer
   * @param f1           the flyer we are deciding if we should illuminate
   */
  public boolean getRadiusOn(FlyerModel fl0, FlyerModel fl1, int distVariable) {
    if (Math.abs(fl0.getX() - fl1.getX()) < distVariable && Math.abs(fl0.getY() - fl1.getY()) < distVariable
        && Math.abs(fl0.getZ() - fl1.getZ()) < distVariable)
      return true;
    return false;
  }

  /**
   * precomputes nearby flyers at beginning of execution. It stores nearby flyers
   * in an ArrayList, if the given Flyer falls within the radius defined by
   * distaneMargin
   */
  public void precompute(int distanceMargin) {
    for (int i = 0; i < model.getFlyers().size(); i++) {
      ArrayList<Integer> temp = new ArrayList<Integer>();
      distances.add(temp);
    }

    for (int i = 0; i < model.getFlyers().size(); i++) {
      FlyerModel fl0 = model.getFlyers().get(i);
      for (int j = 0; j < model.getFlyers().size(); j++) {
        if (i != j) {
          if (!distances.get(i).contains(j) && !distances.get(j).contains(i)) {
            if (closeness(fl0, model.getFlyers().get(j), 150)) {
              distances.get(i).add(j);
              distances.get(j).add(i);
            }
          }
        }
      }
    }
  }

  /**
   * Chooses a random flyer that is nearby
   *
   */
  public int chooseNearRandom(int flyerModelIndex) {
    int next_flyer = rand.nextInt(distances.get(flyerModelIndex).size());
    System.out.println(next_flyer);
    return distances.get(flyerModelIndex).get(next_flyer);
  }

  int current = 25;
  boolean precompute = true;
  int swap = (int) alternator.getValue();
  int prev = swap;

  @Override
  public void run(double deltaMs) {
    if (precompute) {
      precompute(100);
      precompute = false;
    }
    swap = (int) alternator.getValuef();
    if (prev != swap) {
      // chooses a nearby flyer to travel to
      current = chooseNearRandom(current);
      for (FlyerModel fl : model.getFlyers()) {
        for (LightSamplePointModel point : model.getFlyerLights(fl.getIndex())) {
          if (getRadiusOn(model.getFlyers().get(current), fl, 100))
            setColor(point, LX.hsb(0, 0, 100));
          else
            setColor(point, LX.hsb(240, 32, 27));
        }
      }
      prev = swap;
    }
  }

}

/**
 * Creates a rainfall pattern with a controllable number of falling patterns
 * 
 * @author Kyung-Tae Kim <kkim801@stanford.edu>
 */
class RainFall extends FlightPattern {
  final BoundedParameter lag = new BoundedParameter("LAG", 100, 100, 200);
  final BoundedParameter color = new BoundedParameter("COLOR", 0, 0, 360);
  final BoundedParameter semaphore = new BoundedParameter("SEMAPHORE", 2, 1, 10);
  Random rand = new Random();

  RainFall(LX lx) {
    super(lx);
    addParameter("RainFall_LAG", lag);
    addParameter("RainFall_COLOR", color);
    addParameter("RainFall_SEMAPHORE", semaphore);
  }

  /**
   * This function returns whether a given flyer falls wtthin a radius defined by
   * a entral flyer and the distVariable
   * 
   * @param distVariable Defines the radius within which the flyer must fall
   * @param flyer        the center flyer
   * @param x            x variable of flyr to compare
   * @param y            y variable of flyr to compare
   * @param z            z variable of flyr to compare
   */
  public boolean getRadiusOn(FlyerModel flyer, int distVariable, int x, int y, int z) {
    if (Math.abs(flyer.getZ() - z) < 100 && Math.abs(flyer.getX() - x) < 100) {
      if (Math.abs(flyer.getY() - y) < distVariable) {
        return true;
      }
    }
    return false;
  }

  /**
   * Choose a random flyer
   */
  public FlyerModel chooseRand() {
    int size = model.getFlyers().size();
    int sel = rand.nextInt(size);
    return model.getFlyers().get(sel);
  }

  /**
   * Defines a Cylinder class This is important for the rainfall class, as each
   * failling "rainfall" is defined as a cylinder shape, where flyers illuminated
   * in a cylinder pattern is illuminated
   */
  class Cylinder {
    public FlyerModel fl;
    public int currdist;
    public float hueval;

    Cylinder(FlyerModel fl, int currdist, float hueval) {
      this.fl = fl;
      this.currdist = currdist;
      this.hueval = hueval;
    }
  }

  int x_cent, y_cent, z_cent;
  boolean newGen = true;
  int distVariable = 0;
  // ArrayList of current active cylinders
  ArrayList<Cylinder> all_Cylinder = new ArrayList<Cylinder>();

  @Override
  public void run(double deltaMs) {
    float hue = color.getValuef();
    int currActive = (int) semaphore.getValuef();
    for (int i = 0; i < all_Cylinder.size(); i++) {
      // The cylinder expires after travelling 700
      if (all_Cylinder.get(i).currdist > 700)
        all_Cylinder.remove(i);
    }
    int randval = rand.nextInt(100);
    // Initializing a new Cylinder
    // If there are no cylinders, instantiate immediately
    // If there are current cylinders, generate one with probability 3%
    if (all_Cylinder.size() == 0 || (all_Cylinder.size() < currActive && randval > 97)) {
      float set_color;
      if (hue == 0f)
        set_color = rand.nextFloat() * 360;
      else
        set_color = hue;
      FlyerModel fl = chooseRand();
      all_Cylinder.add(new Cylinder(fl, 0, set_color));
    }
    float s = 100;
    int heightVar = 560;
    for (Cylinder Cylinders : all_Cylinder) {
      for (FlyerModel flyer : model.getFlyers()) {
        for (LightSamplePointModel point : model.getFlyerLights(flyer.getIndex())) {
          if (getRadiusOn(flyer, Cylinders.currdist, (int) Cylinders.fl.getX(), heightVar, (int) Cylinders.fl.getZ())) {
            setColor(point, LX.hsb(Cylinders.hueval, 100, 100));
          }
          if (getRadiusOn(flyer, Cylinders.currdist - (int) lag.getValuef(), (int) Cylinders.fl.getX(), heightVar,
              (int) Cylinders.fl.getZ())) {
            setColor(point, LX.hsb(0, 0, 0));
          }
        }
      }
      Cylinders.currdist += 3;
    }
  }
}

/**
 * Drawer class that lets an overlay be defined manually through an array of 1's
 * and 0's Usability to... various levels of effectiveness The function takes
 * the patterns set by the arrays and cycles through them over time
 * 
 * @author Kyung-Tae Kim <kkim801@stanford.edu>
 */
class Drawer extends FlightPattern {
  final BoundedParameter speed = new BoundedParameter("PERIOD", 20000, 1000, 30000);
  final SawLFO position = new SawLFO(0, 100, speed);

  // Arrays to define the patterns
  char[][] toset0 = {
      { '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1',
          '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', },
      { '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1',
          '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', },
      { '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1',
          '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', },
      { '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1',
          '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', },
      { '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1',
          '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', },
      { '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1',
          '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', },
      { '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1',
          '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', },
      { '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1',
          '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', },
      { '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1',
          '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', },
      { '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1',
          '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', },
      { '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1',
          '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', },
      { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
          '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', },
      { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
          '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', },
      { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
          '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', },
      { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
          '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', },
      { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
          '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', },
      { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
          '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', },
      { '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1',
          '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', },
      { '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1',
          '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', },
      { '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1',
          '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', },
      { '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1',
          '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', },
      { '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1',
          '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', },
      { '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1',
          '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', },
      { '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1',
          '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', },
      { '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1',
          '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', },
      { '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1',
          '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', },
      { '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1',
          '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', }, };
  char[][] toset1 = {
      { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0',
          '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', },
      { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0',
          '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', },
      { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0',
          '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', },
      { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0',
          '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', },
      { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0',
          '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', },
      { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0',
          '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', },
      { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0',
          '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', },
      { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0',
          '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', },
      { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0',
          '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', },
      { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0',
          '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', },
      { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0',
          '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', },
      { '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1',
          '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', },
      { '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1',
          '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', },
      { '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1',
          '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', },
      { '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1',
          '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', },
      { '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1',
          '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', },
      { '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1',
          '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', },
      { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0',
          '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', },
      { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0',
          '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', },
      { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0',
          '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', },
      { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0',
          '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', },
      { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0',
          '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', },
      { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0',
          '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', },
      { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0',
          '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', },
      { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0',
          '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', },
      { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0',
          '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', },
      { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0',
          '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', }, };

  Random rand = new Random();
  // only arrays that are included here will be included in the cylce of patterns
  char[][][] total_set = { toset0, toset1 };
  boolean[][] bool_set = new boolean[total_set.length][76];

  Drawer(LX lx) {
    super(lx);
    addParameter("Drawer_speed", speed);
    addModulator(position).start();
  }

  // This precomputes whether a given flyer should be illuminted
  public void precompute() {
    for (int l = 0; l < bool_set.length; l++) {
      for (int k = 0; k < model.getFlyers().size(); k++) {
        FlyerModel flyer = model.getFlyers().get(k);
        int x = (int) flyer.getX();
        int y = (int) flyer.getY();
        int maxi = (((int) ((x + 45) / 20) + 1) > total_set[l][0].length - 1) ? total_set[l][0].length - 1
            : (int) ((x + 45) / 20) + 1;
        int maxj = (((int) (y / 20) + 1) > total_set[l].length - 1) ? total_set[l].length - 1 : (int) (y / 20) + 1;
        for (int i = (((int) ((x + 45) / 20) - 1) < 0) ? 0 : (int) ((x + 45) / 20) - 1; i < maxi; i++) {
          for (int j = (((int) (y / 20) - 1) < 0) ? 0 : (int) (y / 20) - 1; j < maxj; j++) {
            if (total_set[l][j][i] == '1') {
              bool_set[l][k] = true;
            }
          }
        }
      }
    }
  }

  boolean computed = false;
  int k = 0;
  int prev = (int) position.getValuef();
  int current = prev;

  @Override
  public void run(double deltaMs) {
    if (!computed) {
      computed = true;
      precompute();
    }
    current = (int) position.getValuef();
    System.out.println(current);
    if (prev != current) {
      prev = current;
      for (int i = 0; i < model.getFlyers().size(); i++) {
        for (LightSamplePointModel point : model.getFlyerLights(model.getFlyers().get(i).getIndex())) {
          if (bool_set[k][i]) {
            setColor(point, LX.hsb(100, 0, 0));
          } else {
            setColor(point, LX.hsb(0, 100, 100));
          }
        }
      }
      k = (k + 1) % total_set.length;
    }
  }
}

/**
 * Generates a firework pattern where the firework pattern is 2 spheres
 * expanding throughout the 3d space. The first sphere is a colored sphere, and
 * the second sphere is a black sphere that coors over the first. This creates
 * an effect of an expanding firework Maybe a gradient fading pattern can be
 * applied here like in fractals
 */
class FireWorks extends FlightPattern {
  final BoundedParameter lag = new BoundedParameter("LAG", 100, 100, 200);
  final BoundedParameter color = new BoundedParameter("COLOR", 0, 0, 360);
  final BoundedParameter semaphore = new BoundedParameter("SEMAPHORE", 2, 1, 5);
  Random rand = new Random();

  FireWorks(LX lx) {
    super(lx);
    addParameter("FireWorks_LAG", lag);
    addParameter("FireWorks_COLOR", color);
    addParameter("FireWorks_SEMAPHORE", semaphore);
  }

  /**
   * This function returns whether a given flyer falls wtthin a radius defined by
   * a entral flyer and the distVariable
   * 
   * @param distVariable Defines the radius within which the flyer must fall
   * @param flyer        the center flyer
   * @param x            x variable of flyr to compare
   * @param y            y variable of flyr to compare
   * @param z            z variable of flyr to compare
   */
  public boolean getRadiusOn(FlyerModel flyer, int distVariable, int x, int y, int z) {
    if (Math.abs(flyer.getX() - x) < distVariable) {
      if (Math.abs(flyer.getY() - y) < distVariable) {
        if (Math.abs(flyer.getZ() - z) < distVariable) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Choose a random flyer
   */
  public FlyerModel chooseRand() {
    int size = model.getFlyers().size();
    int sel = rand.nextInt(size);
    return model.getFlyers().get(sel);
  }

  /**
   * A sphere class defining various values for a sphere expanding through space
   */
  class Sphere {
    public FlyerModel fl;
    // currdist measures how long it has been expanding throughout space
    public int currdist;
    public float hueval;

    Sphere(FlyerModel fl, int currdist, float hueval) {
      this.fl = fl;
      this.currdist = currdist;
      this.hueval = hueval;
    }
  }

  int x_cent, y_cent, z_cent;
  boolean newGen = true;
  int distVariable = 0;
  ArrayList<Sphere> all_Sphere = new ArrayList<Sphere>();

  @Override
  public void run(double deltaMs) {
    float hue = color.getValuef();
    int currActive = (int) semaphore.getValuef();
    for (int i = 0; i < all_Sphere.size(); i++) {
      // After it has been traelling through space for 700 units, halt
      if (all_Sphere.get(i).currdist > 700)
        all_Sphere.remove(i);
    }
    // generate a new firework
    if (all_Sphere.size() < currActive) {
      float set_color;
      if (hue == 0f)
        set_color = rand.nextFloat() * 360;
      else
        set_color = hue;
      FlyerModel fl = chooseRand();
      all_Sphere.add(new Sphere(fl, 0, set_color));
    }
    float s = 100;
    for (Sphere spheres : all_Sphere) {
      for (FlyerModel flyer : model.getFlyers()) {
        for (LightSamplePointModel point : model.getFlyerLights(flyer.getIndex())) {
          if (getRadiusOn(flyer, spheres.currdist, (int) spheres.fl.getX(), (int) spheres.fl.getY(),
              (int) spheres.fl.getZ())) {
            setColor(point, LX.hsb(spheres.hueval, 100, 100));
          }
          if (getRadiusOn(flyer, spheres.currdist - (int) lag.getValuef(), (int) spheres.fl.getX(),
              (int) spheres.fl.getY(), (int) spheres.fl.getZ())) {
            setColor(point, LX.hsb(0, 0, 0));
          }
        }
      }
      spheres.currdist += 3;
    }
  }
}

/**
 * This pattern flaps all the wings in unison, while a pink streak goes from the
 * first flyer to the last, leaving a graduated pink trail. Flyers not included
 * in the streak are cycling through pastel colors
 * 
 * @author achen
 *
 */
class RaspberryTrip extends FlightPattern {
  int NUM_FLYERS = FlightGeometry.NUM_FLYERS;
  int FLYER_MOD = NUM_FLYERS - 1;
  final BoundedParameter rate = new BoundedParameter("RATE", 5000, 5000, 100000);
  // Go between 0 and FLyer_MOD at RATE set by boundedparameter
  final SawLFO flyerIndex = new SawLFO(0, FLYER_MOD, rate);

  // Speed by which the wings are rotating "GUESS"
  final BoundedParameter speed = new BoundedParameter("SPEED", 6000, 5000, 25000);
  final SinLFO colorFlap = new SinLFO(0, 360, speed);

  // ADding rate and speed sliders to UI
  RaspberryTrip(LX lx) {
    super(lx);
    addParameter("RaspberryTrip_RATE", rate);
    // Starting modulator that takes in those speeds
    addModulator(flyerIndex).start();
    // MODULATOR "the part that calculates the values"
    addParameter("RaspberryTrip_SPEED", speed);
    addModulator(colorFlap).start();
  }

  private int calculateHSB(int highlightedFlyerIndex, int flyerIndex, float hue) {
    int pinkHue = 330;
    int pinkSat = 100;
    int pinkBri = 100;
    int diff = highlightedFlyerIndex - flyerIndex;
    if (diff < 0) {
      diff += NUM_FLYERS;
    }

    if (diff == 0) {
      return LX.hsb(pinkHue, pinkSat - 5, pinkBri - 5);
    } else if (diff <= 3) {
      return LX.hsb(pinkHue, pinkSat - (10 * diff), pinkBri - (10 * diff));
    } else if (diff <= 11) {
      return LX.hsb(pinkHue, pinkSat - 30 - diff, pinkBri - 30);
    } else if (diff <= 33) {
      return LX.hsb(pinkHue, pinkSat - 31 - (2 * diff), pinkBri - 30 - diff / 5);
    } else if (diff == 34) {
      return LX.hsb(0, 1, 60);
    } else {
      return LX.hsb(hue, 25, 60);
    }
  }

  @Override
  public void run(double deltaMs) {
    List<LightSamplePointModel> allLights = model.getAllLights();
    int highlightedFlyerIndex = (int) flyerIndex.getValue();

    float hue = colorFlap.getValuef();
    for (LightSamplePointModel lightPoint : allLights) {
      int flyerIndex = lightPoint.getBodyIndex();
      setColor(lightPoint, calculateHSB(highlightedFlyerIndex, flyerIndex, hue));
    }

    int skew = (int) ((hue - 180f) / 2);
    for (WingModel wing : model.getAllWings()) {
      setSkew(wing, skew);
    }

  }
}

/**
 * Makes a color wave across FLIGHT, such that full range of colors (hue in HSB)
 * is shown across plane of the front window. There are four controls: the speed
 * of the wave, the polar direction of the wave (0 degrees is bottom to top),
 * the saturation and the brightness. The wing position is set to the color hue,
 * with red (0) being fully down and blue (360 being up).
 *
 * @author Philip Levis <pal@cs.stanford.edu>
 */
class ColorWave extends FlightPattern {

  final BoundedParameter period = new BoundedParameter("PERIOD", 15000, 1000, 60000);
  final TriangleLFO position = new TriangleLFO(0, 1, period);
  final BoundedParameter angle = new BoundedParameter("ANGLE", 0, 0, 360);
  final BoundedParameter saturation = new BoundedParameter("SATURATION", 100, 0, 100);
  final BoundedParameter brightness = new BoundedParameter("BRIGHTNESS", 100, 0, 100);

  ColorWave(LX lx) {
    super(lx);
    addParameter("ColorWave_PERIOD", period);
    addParameter("ColorWave_ANGLE", angle);
    addParameter("ColorWave_SATURATION", saturation);
    addParameter("ColorWave_BRIGHTNESS", brightness);
    addModulator(position).start();
  }

  @Override
  public void run(double deltaMs) {
    float s = saturation.getValuef();
    float b = brightness.getValuef();

    float opposite = (float) Math.tan(angle.getValuef() * Math.PI / 180f) * FlightGeometry.FRONT_WINDOW_NADIR_HEIGHT;
    float adjacent = FlightGeometry.FRONT_WINDOW_NADIR_HEIGHT;
    float hypotenuse = (float) Math.sqrt(opposite * opposite + adjacent * adjacent);
    float height = hypotenuse;
    int center = (int) (position.getValuef() * height);
    System.out.println((int) position.getValuef());

    // Make the height of the window a color gradient from 0-360,
    // with the position of the LFO being the zero point: the
    // gradient goes up the window and wraps around.
    for (FlyerModel flyer : model.getFlyers()) {
      float y = flyer.getY() * (float) Math.cos(angle.getValuef());
      float x = flyer.getX() * (float) Math.sin(angle.getValuef());
      float distance = (float) Math.sqrt((x * x) + (y * y));
      distance += height;
      float offset = (distance - center) % height;
      float hue = 360f * offset / height;
      for (LightSamplePointModel point : model.getFlyerLights(flyer.getIndex())) {
        setColor(point, LX.hsb(hue, s, b));
      }
      int skew = (int) (-90f + 180f * position.getValuef());
      setSkew(flyer.getLeftWing(), skew);
      setSkew(flyer.getRightWing(), skew);
    }
  }
}

/**
 * Makes a color sweep across FLIGHT, starting from front facing apex of the
 * building going all the way to the back. There are three controls: the speed
 * of the sweep, the range of flyers alight up by the sweep and the hue of the
 * sweep. The sweep is tries to resemble a wave done by fans in sports stadiums.
 * The wing are set to open up as the sweep goes outwards and close as it
 * contracts back to the apex.
 * 
 * @author Moses Swai
 */
class ColorSweep extends FlightPattern {

  float max_x = FlightGeometry.FRONT_WINDOW_WIDTH;
  float max_z = FlightGeometry.Z_ROOM;
  float max_distance = (float) Math.sqrt((max_x * max_x) + (max_z * max_z));

  final BoundedParameter period = new BoundedParameter("PERIOD", 10000, 1000, 50000);
  final BoundedParameter range = new BoundedParameter("RANGE", 5, 0, 100);
  final BoundedParameter hue = new BoundedParameter("HUE", 100, 0, 360);
  final TriangleLFO position = new TriangleLFO(0, max_distance, period);

  float prev_position = position.getValuef();

  ColorSweep(LX lx) {
    super(lx);
    addParameter("ColorSweep_PERIOD", period);
    addParameter("ColorSweep_RANGE", range);
    addParameter("ColorSweep_HUE", hue);
    addModulator(position).start();
  }

  @Override
  public void run(double deltaMs) {

    for (FlyerModel flyer : model.getFlyers()) {
      float x = flyer.getX();
      float z = flyer.getZ();
      float distance = (float) Math.sqrt((x * x) + (z * z));
      if (position.getValuef() < distance + range.getValuef() && position.getValuef() > distance - range.getValuef()) {
        for (LightSamplePointModel point : model.getFlyerLights(flyer.getIndex())) {
          setColor(point, LX.hsb(hue.getValuef(), 100, 100));
        }
        int skew;
        if (prev_position < position.getValuef()) {
          skew = -60;
        } else {
          skew = 60;
        }
        setSkew(flyer.getLeftWing(), skew);
        setSkew(flyer.getRightWing(), skew);
      } else {
        for (LightSamplePointModel point : model.getFlyerLights(flyer.getIndex())) {
          setColor(point, LX.hsb(0, 0, 0));
        }
      }
    }
    prev_position = position.getValuef();
  }
}

/**
 * Similar to ColorWave except it propogates a single color. There are four
 * controls: the speed of the wave, the polar direction of the wave (0 degrees
 * is bottom to top), the color and the length of the wave. The wing saturation
 * is set to 100 and the brightness is dictated by the a combination of the
 * position and length of the wave.
 *
 * @author Andrew Malty <maltyam@stanford.edu>
 */
class ColorWall extends FlightPattern {

  final BoundedParameter speed = new BoundedParameter("PERIOD", 10000, 1000, 20000);
  final SawLFO position = new SawLFO(0, 1, speed);
  final BoundedParameter angle = new BoundedParameter("ANGLE", 0, 0, 360);
  final BoundedParameter color = new BoundedParameter("COLOR", 50, 0, 360);
  final BoundedParameter length = new BoundedParameter("LENGTH", 2, 1, 50);

  ColorWall(LX lx) {
    super(lx);
    addParameter("ColorWall_SPEED", speed);
    addParameter("ColorWall_ANGLE", angle);
    addParameter("ColorWall_COLOR", color);
    addParameter("ColorWall_LENGTH", length);
    addModulator(position).start();
  }

  @Override
  public void run(double deltaMs) {
    float s = 100;
    float hue = color.getValuef();
    float l = length.getValuef();

    float opposite = (float) Math.tan(angle.getValuef() * Math.PI / 180f) * FlightGeometry.FRONT_WINDOW_NADIR_HEIGHT;
    float adjacent = FlightGeometry.FRONT_WINDOW_NADIR_HEIGHT;
    float hypotenuse = (float) Math.sqrt(opposite * opposite + adjacent * adjacent);
    float height = hypotenuse;
    int center = (int) (position.getValuef() * height);

    for (FlyerModel flyer : model.getFlyers()) {
      float y = flyer.getY() * (float) Math.cos(angle.getValuef());
      float x = flyer.getX() * (float) Math.sin(angle.getValuef());
      float distance = (float) Math.sqrt((x * x) + (y * y));
      distance += height;
      float offset = (distance - center) % height;
      float b = 100f * (float) Math.pow(offset / height, l);
      for (LightSamplePointModel point : model.getFlyerLights(flyer.getIndex())) {
        setColor(point, LX.hsb(hue, s, b));
      }
    }
  }
}

/**
 * The FlyerShape superclass provides an API for easily updating flyers
 * that are located (or not located) in specified 3D shapes. This class 
 * is subclassed by FlyerLine and FlyerSphere. When writing a pattern,
 * you can loop over the ArrayLists given by getFlyers() and getNonFlyers()
 * to change the color/wings of those flyers accordingly.
 *
 * @author Jonathan Flat <jflat@stanford.edu>
 */
class FlyerShape {
  // static final int NUM_FLYERS = 76;
  private ArrayList<FlyerModel> flyers;
  private ArrayList<FlyerModel> nonFlyers;
  
  FlyerShape() {
    this.flyers = new ArrayList<FlyerModel>();
    this.nonFlyers = new ArrayList<FlyerModel>();
  }

  public ArrayList<FlyerModel> getFlyers() {
    return flyers;
  }

  public ArrayList<FlyerModel> getNonFlyers() {
    return nonFlyers;
  }

  public void updateFlyers(ArrayList<FlyerModel> flyers, ArrayList<FlyerModel> nonFlyers) {
    this.flyers = flyers;
    this.nonFlyers = nonFlyers;
  }
}

/**
 * The FlyerSphere class represents a sphere in 3D space, and keeps track
 * of the flyers inside and outside of the sphere. You can modify the location,
 * radius, and inner radius (for a spherical shell). Call updateFlyers() after
 * any modification, then use the superclass's getFlyers() and getNonFlyers()
 * to get ArrayLists of flyers inside and outside the sphere.
 *
 * @author Jonathan Flat <jflat@stanford.edu>
 */
class FlyerSphere extends FlyerShape {
  LX lx;
  FlightModel model;

  private float x;
  private float y;
  private float z;
  private float radius;
  private float innerRadius;

  FlyerSphere(LX lx) {
    super();
    this.lx = lx;
    this.model = (FlightModel)lx.getModel();
    this.innerRadius = 0;
  }

  public void setSphere(float x, float y, float z, float radius) {
    this.setCenter(x, y, z);
    this.radius = radius;
  }

  public void setCenter(float x, float y, float z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }

  public void setX(float x) { this.x = x; }
  public void setY(float y) { this.y = y; }
  public void setZ(float z) { this.z = z; }
  public void setRadius(float radius) { this.radius = radius; }
  public void setInnerRadius(float iradius) {this.innerRadius = iradius; }

  public float getX() { return this.x; }
  public float getY() { return this.y; }
  public float getZ() { return this.z; }
  public float getRadius() { return this.radius; }
  public float getInnerRadius() { return this.innerRadius; }

  public void updateFlyers() {
    ArrayList<FlyerModel> flyers = new ArrayList<FlyerModel>();
    ArrayList<FlyerModel> nonFlyers = new ArrayList<FlyerModel>();
    for (FlyerModel flyer : model.getFlyers()) {
      if (isInSphere(flyer)) flyers.add(flyer);
      else nonFlyers.add(flyer);
    }
    super.updateFlyers(flyers, nonFlyers);
  }

  private boolean isInSphere(FlyerModel flyer) {
    float fx = flyer.getX();
    float fy = flyer.getY();
    float fz = flyer.getZ();
    float d = (float)Math.sqrt(Math.pow(x-fx, 2) + Math.pow(y-fy, 2) + Math.pow(z-fz, 2));
    return d < radius && d >= innerRadius;
  }
}

/**
 * The FlyerLine class represents a line (or cylinder) in 3D space, and keeps track
 * of the flyers inside and outside of this volume. You can modify the location of
 * both endpoints, radius, inner radius (for a cylindrical shell), whether the line
 * mimics a single point-of-view (cone), and whether each endpoint of the line is 
 * strict (not an infinite line). Call updateFlyers() after any modification, then 
 * use the superclass's getFlyers() and getNonFlyers() to get ArrayLists of flyers 
 * inside and outside the line.
 *
 * @author Jonathan Flat <jflat@stanford.edu>
 */
class FlyerLine extends FlyerShape {
  LX lx;
  FlightModel model;

  private float x1;
  private float y1;
  private float z1;
  private float x2;
  private float y2;
  private float z2;
  private float radius;
  private float innerRadius;
  private boolean pov;
  private boolean p1Strict;
  private boolean p2Strict;

  FlyerLine(LX lx) {
    super();
    this.lx = lx;
    this.model = (FlightModel)lx.getModel();
    this.pov = false;
    this.innerRadius = 0;
    this.p1Strict = false;
    this.p2Strict = false;
  }

  public void setLine(float x1, float y1, float z1,
                      float x2, float y2, float z2, float radius) {
    this.setP1(x1, y1, z1);
    this.setP2(x2, y2, z2);
    this.radius = radius;
  }

  public void setP1(float x1, float y1, float z1) {
    this.x1 = x1;
    this.y1 = y1;
    this.z1 = z1;
  }

  public void setP2(float x2, float y2, float z2) {
    this.x2 = x2;
    this.y2 = y2;
    this.z2 = z2;
  }

  public void setX1(float x1) { this.x1 = x1; }
  public void setY1(float y1) { this.y1 = y1; }
  public void setZ1(float z1) { this.z1 = z1; }
  public void setX2(float x2) { this.x2 = x2; }
  public void setY2(float y2) { this.y2 = y2; }
  public void setZ2(float z2) { this.z2 = z2; }
  public void setRadius(float radius) { this.radius = radius; }
  public void setInnerRadius(float iradius) { this.innerRadius = iradius; }
  public void setPOV(boolean pov) { this.pov = pov; }
  public void setP1Strict(boolean strict) { this.p1Strict = strict; }
  public void setP2Strict(boolean strict) { this.p2Strict = strict; }

  public float getX1() { return this.x1; }
  public float getY1() { return this.y1; }
  public float getZ1() { return this.z1; }
  public float getX2() { return this.x2; }
  public float getY2() { return this.y2; }
  public float getZ2() { return this.z2; }
  public float getRadius() { return this.radius; }
  public float getInnerRadius() { return this.innerRadius; }
  public boolean isPOV() { return this.pov; }
  public boolean isP1Strict() { return this.p1Strict; }
  public boolean isP2Strict() { return this.p2Strict; }

  public void updateFlyers() {
    ArrayList<FlyerModel> flyers = new ArrayList<FlyerModel>();
    ArrayList<FlyerModel> nonFlyers = new ArrayList<FlyerModel>();
    for (FlyerModel flyer : model.getFlyers()) {
      if (isInLine(flyer)) flyers.add(flyer);
      else nonFlyers.add(flyer);
    }
    super.updateFlyers(flyers, nonFlyers);
  }

  public boolean isInLine(FlyerModel flyer) {
    float x = flyer.getX();
    float y = flyer.getY();
    float z = flyer.getZ();
    float d1 = (float)Math.sqrt(Math.pow(x-x1, 2) + Math.pow(y-y1, 2) + Math.pow(z-z1, 2));
    float d2 = (float)Math.sqrt(Math.pow(x-x2, 2) + Math.pow(y-y2, 2) + Math.pow(z-z2, 2));
    float base = (float)Math.sqrt(Math.pow(x2-x1, 2) + Math.pow(y2-y1, 2) + Math.pow(z2-z1, 2));
    float s = (d1 + d2 + base) / 2.0f; /* Semi-perimeter of the triangle. */
    /* Heron's Formula */
    float area = (float)Math.sqrt(s * (s - d1) * (s - d2) * (s - base));
    float height = 2.0f * area / base;
    if ((p1Strict && (d1*d1 + base*base < d2*d2)) ||
        (p2Strict && (d2*d2 + base*base < d1*d1)))
          return false;
    if (!pov) {
      return height < radius && height >= innerRadius;
    } else {
      float proj = (float)Math.sqrt(Math.pow(d1, 2) - Math.pow(height, 2));
      return height < (proj / base) * radius && height >= (proj / base) * innerRadius;
    }
  }
}

/**
 * A very simple pattern that lights up flyers on the front window
 * in a wave going left to right across the window.
 *
 * @author Jonathan Flat <jflat@stanford.edu>
 */
class FrontWall extends FlightPattern {
  
  private FlyerLine line;
  final BoundedParameter color = new BoundedParameter("COLOR", 10, 0, 360);
  final SawLFO saw = new SawLFO(25, 25, 5000);

	FrontWall(LX lx) {
    super(lx);
    addModulator(saw).start();
    addParameter("FrontWall_COLOR", color);
    this.line = new FlyerLine(lx);
    this.line.setLine(25.0f, 0.0f, 25.0f, 25.0f, 50.0f, 25.0f, 100.0f);
  }
  
  @Override
	public void run(double deltaMs) {
    float xPos = (float)saw.getValue();
    line.setX1(xPos);
    line.setX2(xPos);
    line.updateFlyers();
		for (FlyerModel flyer: line.getFlyers()) {
			for (LightSamplePointModel point : model.getFlyerLights(flyer.getIndex())) {
				setColor(point, LX.hsb(color.getValuef(), 100, 100));
			}
    }
    for (FlyerModel flyer: line.getNonFlyers()) {
			for (LightSamplePointModel point : model.getFlyerLights(flyer.getIndex())) {
				setColor(point, LX.hsb(0, 0, 0));
			}
    }
  }
}

/**
 * One color snakes its way across all of the flyers
 * It leaves a tail of length stored in the bounded parameter
 * At every period, it moves to a nearby flyer, the specifc flyer has a random factor to it
 * 
 * @author David Van Dyje <dhvd.stanford.edu>
 */
class ColorSnake extends FlightPattern {
  final BoundedParameter color = new BoundedParameter("COLOR", 0, 0, 360);
  final BoundedParameter length = new BoundedParameter("LENGTH", 5, 2, 12);
  final BoundedParameter speed = new BoundedParameter("PERIOD", 1000, 10, 5000);
  final List<Integer> flyerPath = new ArrayList<Integer>();
  double counter = 0;
  ColorSnake(LX lx) {
      super(lx);
      addParameter("ColorSnake_COLOR", color);
      addParameter("ColorSnake_LENGTH", length);
      addParameter("ColorSnake_PERIOD", speed);

      flyerPath.add(0, 0);
      flyerPath.add(1, 1);
  }
  
  @Override
  public void run(double deltaMs) {
    //Only run if the time has passed the period length
    counter = counter + deltaMs;
    if(counter < speed.getValuef()) return;

    counter = 0;
    float s = 100;
    float hue = color.getValuef();

    //Interperet a straight line from the previous two flyers
    float deltaX = model.getFlyers().get(flyerPath.get(0)).getX() - model.getFlyers().get(flyerPath.get(1)).getX();
    float deltaY = model.getFlyers().get(flyerPath.get(0)).getY() - model.getFlyers().get(flyerPath.get(1)).getY();
    float deltaZ = model.getFlyers().get(flyerPath.get(0)).getZ() - model.getFlyers().get(flyerPath.get(1)).getZ();

    //Find the closest flyer if you continue this change
    //Ignore flyers currently in the path
    float newX = model.getFlyers().get(flyerPath.get(0)).getX();// + deltaX/2;
    float newY = model.getFlyers().get(flyerPath.get(0)).getY();// + deltaY/2;
    float newZ = model.getFlyers().get(flyerPath.get(0)).getZ();// + deltaZ/2;
    float minDist = 100000;
    int closestFlyer = flyerPath.get(0);
    for (FlyerModel flyer: model.getFlyers()) {
      if (flyerPath.contains(flyer.getIndex())) continue;
      if(new Random().nextDouble() < 0.5) continue;
      float x = flyer.getX();
      float y = flyer.getX();
      float z = flyer.getZ();
      float dist = (float)Math.sqrt(Math.pow((newX - x),2) + Math.pow((newY - y),2) + Math.pow((newZ - z),2));
      if((dist < minDist)){
        minDist = dist;
        closestFlyer = flyer.getIndex();
      }
    }

    //Add the current flyer to the front of the list
    flyerPath.add(0, closestFlyer);
    while(flyerPath.size() > (int)length.getValuef()) flyerPath.remove(flyerPath.size() - 1);

    //Update the colors on the flyers
    //The flyers near the tail have a lower brightness
    int[] brightness = new int[flyerPath.size()];
    for(int i = 0; i < flyerPath.size(); ++i){
      brightness[i] = (int)(100 * (float)(flyerPath.size() - i)/((float)(flyerPath.size())));
    }
    for(int i=0;i < flyerPath.size(); ++i){
      for (LightSamplePointModel point : model.getFlyerLights(flyerPath.get(i))) {
        setColor(point, LX.hsb(hue, s, brightness[i]));
      }
    }

    // for (WingModel wing: model.getAllWings()) {
    //     setSkew(wing, skew);
    // }
  }
}

/**
 * A wing pattern snakes across different flyers, in the same way to ColorSnake
 * It leaves a tail of length stored in the bounded parameter
 * At every period, it moves to a nearby flyer, the specifc flyer has a random factor to it
 * 
 * @author David Van Dyje <dhvd.stanford.edu>
 */
class WingSnake extends FlightPattern {
  final BoundedParameter length = new BoundedParameter("LENGTH", 5, 2, 12);
  final BoundedParameter speed = new BoundedParameter("PERIOD", 3000, 100, 10000);
  final List<Integer> flyerPath = new ArrayList<Integer>();
  double counter = 0;
  WingSnake(LX lx) {
      super(lx);
      addParameter("WingSnake_LENGTH", length);
      addParameter("WingSnake_PERIOD", speed);

      flyerPath.add(0, 0);
      flyerPath.add(1, 1);
  }
  
  @Override
  public void run(double deltaMs) {
    //Only run if the time has passed the period length
    counter = counter + deltaMs;
    if(counter < speed.getValuef()) return;

    counter = 0;

    //Interperet a straight line from the previous two flyers
    float deltaX = model.getFlyers().get(flyerPath.get(0)).getX() - model.getFlyers().get(flyerPath.get(1)).getX();
    float deltaY = model.getFlyers().get(flyerPath.get(0)).getY() - model.getFlyers().get(flyerPath.get(1)).getY();
    float deltaZ = model.getFlyers().get(flyerPath.get(0)).getZ() - model.getFlyers().get(flyerPath.get(1)).getZ();

    //Find the closest flyer if you continue this change
    //Ignore flyers currently in the path
    float newX = model.getFlyers().get(flyerPath.get(0)).getX();// + deltaX/2;
    float newY = model.getFlyers().get(flyerPath.get(0)).getY();// + deltaY/2;
    float newZ = model.getFlyers().get(flyerPath.get(0)).getZ();// + deltaZ/2;
    float minDist = 100000;
    int closestFlyer = flyerPath.get(0);
    for (FlyerModel flyer: model.getFlyers()) {
      if (flyerPath.contains(flyer.getIndex())) continue;
      if(new Random().nextDouble() < 0.5) continue;
      float x = flyer.getX();
      float y = flyer.getX();
      float z = flyer.getZ();
      float dist = (float)Math.sqrt(Math.pow((newX - x),2) + Math.pow((newY - y),2) + Math.pow((newZ - z),2));
      if((dist < minDist)){
        minDist = dist;
        closestFlyer = flyer.getIndex();
      }
    }

    //Add the current flyer to the front of the list
    flyerPath.add(0, closestFlyer);
    while(flyerPath.size() > (int)length.getValuef()) flyerPath.remove(flyerPath.size() - 1);

    //Update the colors on the flyers
    //The flyers near the tail have a lower brightness
    int[] skew = new int[flyerPath.size()];
    for(int i = 0; i < flyerPath.size(); ++i){
      skew[i] = (int)(60 * (float)(flyerPath.size()-1 - i)/((float)(flyerPath.size()-1)));
    }
    for(int i=0;i < flyerPath.size(); ++i){
      for(WingModel wm : model.getFlyerWings(flyerPath.get(i))){
        setSkew(wm, skew[i]);
      }
    }
  }
}

/*
 * Twinkles by adjusting the brightness of individual light points, without regard to how the light points are defined in the space.
 * Swap out the commented code to see the effect on only the body lights. You might want to do this because the GUI only shows each wing as 
 * one light point, when in reality it is 3.
 * Adapted from the Twinkle pattern from Entwined
 *
 * @author Ashley Chen <ashleyic@cs.stanford.edu>
 */
class Twinkle extends FlightPattern {

  private SinLFO[] bright;
  final BoundedParameter brightnessParam = new BoundedParameter("BRIGHTNESS", 1, 0.5, 1);
  final int numBrights = 30;
  final int density = 20;
  int[] sparkleTimeOuts;
  int[] lightPointToModulatorMapping;
  Random rand = new Random();
  static private final long millisOffset = System.currentTimeMillis();

  Twinkle(LX lx) {
    super(lx);
    addParameter("Twinkle_BRIGHTNESS", brightnessParam);

    // sparkleTimeOuts = new int[model.getAllBodyLights().size()];
    // lightPointToModulatorMapping = new int[model.getAllBodyLights().size()];

    sparkleTimeOuts = new int[model.getAllLights().size()];
    lightPointToModulatorMapping = new int[model.getAllLights().size()];

    for (int i = 0; i < lightPointToModulatorMapping.length; i++) {
      lightPointToModulatorMapping[i] = rand.nextInt(numBrights);
    }

    bright = new SinLFO[numBrights];
    int numLight = density / 100 * bright.length; // number of brights array that are most bright
    int numDarkReverse = (bright.length - numLight) / 2; // number of brights array that go from light to dark

    for (int i = 0; i < bright.length; i++) {
      if (i <= numLight) {
        if (rand.nextFloat() < 0.5f) {
          bright[i] = new SinLFO(rand.nextInt(100 - 80) + 80, 0, rand.nextInt(7700 - 2300) + 2300);
        } else {
          bright[i] = new SinLFO(0, rand.nextInt(90 - 70) + 70, rand.nextInt(9200 - 5300) + 5300);
        }
      } else if (i < numDarkReverse) {
        bright[i] = new SinLFO(rand.nextInt(70 - 50) + 50, 0, rand.nextInt(11300 - 3300) + 3300);
      } else {
        bright[i] = new SinLFO(0, rand.nextInt(80 - 30) + 30, rand.nextInt(9300 - 3100) + 3100);
      }
      addModulator(bright[i]).start();
    }
  }

  @Override
  public void run(double deltaMs) {
    // for (LightSamplePointModel point : model.getAllBodyLights()) {
    // if (sparkleTimeOuts[point.getBodyIndex()] < (int)(System.currentTimeMillis()
    // - millisOffset)) {
    // // randomly change modulators
    // if (rand.nextInt(10) <= 3) {
    // lightPointToModulatorMapping[point.getBodyIndex()] =
    // rand.nextInt(numBrights);
    // }
    // sparkleTimeOuts[point.getBodyIndex()] = (int)(System.currentTimeMillis() -
    // millisOffset) + rand.nextInt(23300 - 11100) + 11100;
    // }
    // setColor(point, LX.hsb(
    // 0,
    // 0,
    // bright[lightPointToModulatorMapping[point.getBodyIndex()]].getValuef() *
    // brightnessParam.getValuef()
    // ));
    // }

    for (LightSamplePointModel point : model.getAllLights()) {
      if (sparkleTimeOuts[point.index] < (int) (System.currentTimeMillis() - millisOffset)) {
        // randomly change modulators
        if (rand.nextInt(10) <= 3) {
          lightPointToModulatorMapping[point.index] = rand.nextInt(numBrights);
        }
        sparkleTimeOuts[point.index] = (int) (System.currentTimeMillis() - millisOffset) + rand.nextInt(23300 - 11100)
            + 11100;
      }
      setColor(point,
          LX.hsb(0, 0, bright[lightPointToModulatorMapping[point.index]].getValuef() * brightnessParam.getValuef()));
    }
  }
}

/**
 * This creates a rotating ray of colored light around a center point.
 * 
 * @author Ashley Chen <ashleyic@cs.stanford.edu>
 */
class LightHouseColor extends FlightPattern {
  private float speedMult = 1000;
  final BoundedParameter hue = new BoundedParameter("HUE", 50, 0, 360);
  final BoundedParameter width = new BoundedParameter("WIDTH", 45, 0, 100);
  final BoundedParameter colorSpeed = new BoundedParameter("COLOR PERIOD", 160, 0, 200);
  final BoundedParameter speedParam = new BoundedParameter("PERIOD", 5, 0.01, 20);
  final SawLFO wave = new SawLFO(0, 360, 1000);
  float total_ms = 0;
  int shrub_offset[];

  LightHouseColor(LX lx) {
    super(lx);
    addModulator(wave).start();
    addParameter("LightHouse_hue", hue);
    addParameter("LightHouse_speedParam", speedParam);
    addParameter("LightHouse_colorSpeed", colorSpeed);
    addParameter("LightHouse_width", width);

  }

  @Override
  public void run(double deltaMs) {
    wave.setPeriod(speedParam.getValuef() * speedMult);
    total_ms += deltaMs * speedParam.getValuef();
    float offset = (wave.getValuef() + 360.0f) % 360.0f;
    for (LightSamplePointModel point : model.getAllLights()) {
      FlyerModel fl = model.getFlyers().get(point.getBodyIndex());
      float x = fl.getX() - 310; // Approximate x offset from center
      float y = fl.getY() - 250; // Approximate y offset from center
      // float x = point.x - 310; //Approximate x offset from center
      // float y = point.y-250; //Approximate y offset from center
      double angle = 57.3 * Math.atan(x / y);// Calculate angle of flyer
      float diff = (360.0f + (wave.getValuef() - (float) angle) % 360.0f) % 360.f; // smallest postive representation
                                                                                   // modulo 360
      if ((360 - diff) < diff) {
        diff = 360 - diff;
      }
      float b = diff < width.getValuef() ? 100.0f : 0.0f;
      float h = (360 + (hue.getValuef() + total_ms * colorSpeed.getValuef() / 10000) % 360) % 360;
      setColor(point, LX.hsb(h, 100, b));
    }
  }
}

/**
 * This should allow the user to manually adjust the XYZ parameters to sweep a
 * beam of green across the specified coordinate plane. It does not work at the
 * moment, because the model has not been set up to support coordinate
 * calculations down to the LightSamplePoint level :'(
 * 
 * @author Ashley Chen <ashleyic@cs.stanford.edu>
 */
class TestSweep extends FlightPattern {

  final BoundedParameter x;
  final BoundedParameter y;
  final BoundedParameter z;
  final BoundedParameter beam;

  TestSweep(LX lx) {
    super(lx);
    addParameter("test_x", x = new BoundedParameter("X", 0, model.xMin, model.xMax));
    addParameter("test_y", y = new BoundedParameter("Y", 0, model.yMin, model.yMax));
    addParameter("test_z", z = new BoundedParameter("Z", 0, model.zMin, model.zMax));
    addParameter("test_beam", beam = new BoundedParameter("beam", 5, 1, 15));
  }

  static public final float abs(float n) {
    return (n < 0) ? -n : n;
  }

  public void run(double deltaMs) {

    for (LightSamplePointModel cube : model.getAllLights()) {
      if (abs(cube.x - x.getValuef()) < beam.getValuef() || abs(cube.y - y.getValuef()) < beam.getValuef()
          || abs(cube.z - z.getValuef()) < beam.getValuef()) {
        colors[cube.index] = lx.hsb(135, 100, 100);
      } else {
        colors[cube.index] = lx.hsb(135, 100, 0);
      }
    }
  }
}

/**
 * Pattern to make the wings appear as if they're dancing. Move into a position
 * then hold the pose. Pretty glitchy, but it might work okay with how commands
 * are issued to the actual wing motors themselves.
 *
 * @author Ashley Chen <ashleyic@cs.stanford.edu>
 */
class VogueWings extends FlightPattern {
  final BoundedParameter speed = new BoundedParameter("PERIOD", 6000, 5000, 25000);
  Random rand = new Random();
  OfInt randomSkew;
  long millisOffset = System.currentTimeMillis();
  private SinLFO[] leftPoseTransitions = new SinLFO[20];
  private SinLFO[] rightPoseTransitions = new SinLFO[20];

  int firstLeftPose;
  int firstRightPose;

  int leftCursor = 0;
  int rightCursor = 1;
  boolean isPosed = false;

  VogueWings(LX lx) {
    super(lx);
    addParameter("VogueWings_SPEED", speed);
    randomSkew = rand.ints(-90, 90).iterator();

    int leftSkew = 0;
    int rightSkew = 0;
    firstLeftPose = leftSkew;
    firstRightPose = rightSkew;

    for (int i = 0; i < leftPoseTransitions.length; i++) {
      if (i != leftPoseTransitions.length - 1) {
        int temp = randomSkew.nextInt();
        leftPoseTransitions[i] = new SinLFO(leftSkew, temp, speed);
        leftSkew = temp;
        temp = randomSkew.nextInt();
        rightPoseTransitions[i] = new SinLFO(rightSkew, temp, speed);
        rightSkew = temp;
      } else {
        leftPoseTransitions[i] = new SinLFO(leftSkew, firstLeftPose, speed);
        rightPoseTransitions[i] = new SinLFO(rightSkew, firstRightPose, speed);
      }
      addModulator(leftPoseTransitions[i]).start();
      addModulator(rightPoseTransitions[i]).start();
    }
  }

  @Override
  public void run(double deltaMs) {
    if ((int) speed.getValuef() / 2 < (int) (System.currentTimeMillis() - millisOffset)) {
      millisOffset = System.currentTimeMillis();
      if (isPosed) {
        if (leftCursor != leftPoseTransitions.length - 1) {
          leftCursor++;
        } else {
          leftCursor = 0;
        }
        if (rightCursor != rightPoseTransitions.length - 1) {
          rightCursor++;
        } else {
          rightCursor = 0;
        }
        isPosed = false;
      } else {
        isPosed = true;
      }
    }

    if (!isPosed) {
      for (int i = 0; i < FlightGeometry.NUM_FLYERS; i++) {
        setSkew(model.getFlyerLeftWing(i), (int) leftPoseTransitions[leftCursor].getValuef());
        setSkew(model.getFlyerRightWing(i), (int) rightPoseTransitions[rightCursor].getValuef());
      }
    }
  }
}

/**
 * Makes a heartbeat pattern that expands and contracts across FLIGHT. There are
 * four controls: the x and y position of the center of the heart, the size of
 * the heart, and the color (hue) of the heart.
 *
 * @author Abdu Mohamdy <amohamdy@stanford.edu>
 */
class HeartBeat extends FlightPattern {
  // final float centerX = 300.0f;
  // final float centerY = 250f;
  final SawLFO sawlfo1 = new SawLFO(0, 100, 1000);
  // final SawLFO sawlfo2 = new SawLFO(0, 25, 1000);
  // final SinLFO sinlfo1 = new SinLFO(0, 50, 2000);
  // final SinLFO sinlfo2 = new SinLFO(0, 10, 1000);

  final BoundedParameter centerX = new BoundedParameter("X POSITION", 300, 0, 600);
  final BoundedParameter centerY = new BoundedParameter("Y POSITION", 250, 0, 500);
  final BoundedParameter size = new BoundedParameter("SIZE", 150, 50, 500);
  final BoundedParameter hue = new BoundedParameter("HUE", 360, 0, 360);

  HeartBeat(LX lx) {
    super(lx);
    addModulator(sawlfo1).start();
    addParameter("HeartBeat_X", centerX);
    addParameter("HeartBeat_Y", centerY);
    addParameter("HeartBeat_Size", size);
    addParameter("HeartBeat_Hue", hue);

    // addModulator(sawlfo2).start();
    // addModulator(sinlfo1).start();
    // addModulator(sinlfo2).start();
  }

  @Override
  public void run(double deltaMs) {
    for (FlyerModel flyer : model.getFlyers()) {
      // System.out.println("-------");
      // System.out.println("Index: " + flyer.getIndex());
      // System.out.println("X: " + flyer.getX() + "Y: " + flyer.getY() + "Z: " +
      // flyer.getZ());
      float distanceX = flyer.getX() - centerX.getValuef();
      float distanceY = flyer.getY() - centerY.getValuef();
      if (Math.sqrt(distanceX * distanceX + distanceY * distanceY) <= size.getValuef()
          - sawlfo1.getValuef() /* - sawlfo2.getValuef() + sinlfo1.getValuef() + sinlfo2.getValuef() */) {
        // System.out.println("*******IN THERE!!!");
        for (LightSamplePointModel point : model.getFlyerLights(flyer.getIndex())) {
          setColor(point, LX.hsb(hue.getValuef(), 100, 100));
        }
      } else {
        for (LightSamplePointModel point : model.getFlyerLights(flyer.getIndex())) {
          setColor(point, LX.hsb(0, 0, 0));
        }
      }
    }
  }
}

// class Chaos extends FlightPattern {
// Random rand = new Random();

// Chaos(LX lx) {
// super(lx);
// }

// @Override
// public void run(double deltaMs) {
// for (int i = 0; i < rand.nextInt(model.getFlyers().size()); i++) {
// FlyerModel flyer =
// model.getFlyers().get(rand.nextInt(model.getFlyers().size()));
// System.out.println("Chaos #" + i + ": Flyer with index " + flyer.getIndex());
// for (WingModel wing: flyer.getWings()) {
// setSkew(wing, 360 - wing.getSkew());
// }
// }
// }
// }

/*
 * A pattern that demonstrates the point-of-view capabilities of the
 * FlyerLine class. Allows the user to change the POV location with GUI
 * sliders. From that location, the view of the Flyers will show a circular
 * rainbow wave collapsing inward. From other viewpoints, the pattern looks
 * pseudorandom.
 *
 * @author Jonathan Flat <jflat@stanford.edu>
 */
class POVPattern extends FlightPattern {
  private FlyerLine line;
  final BoundedParameter period = new BoundedParameter("PERIOD", 15000, 1000, 60000);
  final SawLFO wave = new SawLFO(0, 1, period);
  final BoundedParameter xPov = new BoundedParameter("X POV", 300, -200, 800);
  final BoundedParameter yPov = new BoundedParameter("Y POV", 50, -200, 800);
  final BoundedParameter zPov = new BoundedParameter("Z POV", -400, -600, 600);

  POVPattern(LX lx) {
    super(lx);
    addParameter("POV_PERIOD", period);
    addModulator(wave).start();
    addParameter("POV_X", xPov);
    addParameter("POV_Y", yPov);
    addParameter("POV_Z", zPov);
    this.line = new FlyerLine(lx);
    this.line.setP2(300, 250, 100);
    this.line.setRadius(75);
    this.line.setPOV(true);
  }

  @Override
  public void run(double deltaMs) {
    line.setP1(xPov.getValuef(), yPov.getValuef(), zPov.getValuef());
    for (int i = 400; i >= 25; i -= 25) {
      line.setRadius(i);
      line.setInnerRadius(i-25);
      line.updateFlyers();
      for (FlyerModel flyer: line.getFlyers()) {
        for (LightSamplePointModel point : model.getFlyerLights(flyer.getIndex())) {
          setColor(point, LX.hsb((360f * wave.getValuef() + i) % 360, 100, 100));
        }
      }
    }
  }
}

/**
 * A pattern that breathes in and out in a spherical color formation.
 * Color lights up on the way in then darkens as it flows out.
 *
 * @author Jonathan Flat <jflat@stanford.edu>
 */
class BreathingSphere extends FlightPattern {
  private FlyerSphere sphere;
  final BoundedParameter period = new BoundedParameter("PERIOD", 15000, 1000, 60000);
  final TriangleLFO wave = new TriangleLFO(0, 1, period);
  final BoundedParameter color = new BoundedParameter("COLOR", 240, 0, 360);

  BreathingSphere(LX lx) {
    super(lx);
    addParameter("BreathingSphere_PERIOD", period);
    addModulator(wave).start();
    addParameter("BreathingSphere_COLOR", color);
    sphere = new FlyerSphere(lx);
    sphere.setCenter(300, 250, 100);
  }

  @Override
  public void run(double deltaMs) {
    sphere.setRadius(380f * (1f - wave.getValuef()));
    sphere.updateFlyers();
    for (FlyerModel flyer: sphere.getFlyers()) {
      for (LightSamplePointModel point : model.getFlyerLights(flyer.getIndex())) {
        setColor(point, LX.hsb(color.getValuef(), 100f * (1f - wave.getValuef()), 100));
      }
    }
  }
}

/**
 * The same breathing sphere pattern as above, but this time with wings.
 * Wing angle increases as the pattern goes inward, decreases together
 * on the way out.
 *
 * @author Jonathan Flat <jflat@stanford.edu>
 */
class BreathingSphereWings extends FlightPattern {
  private FlyerSphere sphere;
  final BoundedParameter period = new BoundedParameter("PERIOD", 15000, 1000, 60000);
  final TriangleLFO wave = new TriangleLFO(0, 1, period);
  final BoundedParameter color = new BoundedParameter("COLOR", 240, 0, 360);

  BreathingSphereWings(LX lx) {
    super(lx);
    addParameter("BreathingSphere_PERIOD", period);
    addModulator(wave).start();
    addParameter("BreathingSphere_COLOR", color);
    sphere = new FlyerSphere(lx);
    sphere.setCenter(300, 250, 100);
  }

  @Override
  public void run(double deltaMs) {
    sphere.setRadius(380f * (1f - wave.getValuef()));
    sphere.updateFlyers();
    for (FlyerModel flyer: sphere.getFlyers()) {
      for (WingModel wm : model.getFlyerWings(flyer.getIndex())) {
        int skew = (int)(180f * (1f - wave.getValuef())) - 90;
        setSkew(wm, skew);
      }
    }
  }
}

/**
 * A pattern that breathes up and down. Color lightens when breathing
 * upwards and darkens on the way down.
 *
 * @author Jonathan Flat <jflat@stanford.edu>
 */
class BreathingUp extends FlightPattern {
  final BoundedParameter period = new BoundedParameter("PERIOD", 15000, 1000, 60000);
  final TriangleLFO wave = new TriangleLFO(0, 1, period);
  final BoundedParameter color = new BoundedParameter("COLOR", 240, 0, 360);
  private boolean up;

  BreathingUp(LX lx) { 
    super(lx);
    addParameter("BreathingUp_PERIOD", period);
    addModulator(wave).start();
    addParameter("BreathingUp_COLOR", color);
  }

  @Override
  public void run(double deltaMs) {
    up = wave.getValue() - wave.getValue() < 0;
    float currHeight = wave.getValuef() * FlightGeometry.FRONT_WINDOW_NADIR_HEIGHT - 20;
    for (FlyerModel flyer: model.getFlyers()) {
      float y = flyer.getY();
      if (up) {
        if (y > currHeight - 10f && y < currHeight + 10) {
          for (LightSamplePointModel point : model.getFlyerLights(flyer.getIndex())) {
            setColor(point, LX.hsb(color.getValuef(), 100f * (1f - wave.getValuef()), 100));
          }
        }
      } else {
        if (y > currHeight) {
          for (LightSamplePointModel point : model.getFlyerLights(flyer.getIndex())) {
            setColor(point, LX.hsb(color.getValuef(), 100f * (1f - wave.getValuef()), 100));
          }
        }
      }
		}
  }
}

/**
 * A fountain pattern that mimics a fountain coming up from the
 * inside of the display then falling along the outer edges.
 *
 * @author Jonathan Flat <jflat@stanford.edu>
 */
class Fountain extends FlightPattern {
  final BoundedParameter period = new BoundedParameter("PERIOD", 15000, 1000, 60000);
  final TriangleLFO wave = new TriangleLFO(0, 1, period);
  final BoundedParameter color = new BoundedParameter("COLOR", 240, 0, 360);
  final BoundedParameter length = new BoundedParameter("LENGTH", 200, 10, 1000);
  private boolean up;
  private FlyerLine innerLine;
  private FlyerLine outerLine;
  private float innerTop;
  private float innerBottom;
  private float outerTop;
  private float outerBottom;
  private final float height = FlightGeometry.FRONT_WINDOW_NADIR_HEIGHT;

  Fountain(LX lx) { 
    super(lx);
    addParameter("Fountain_PERIOD", period);
    addModulator(wave).start();
    addParameter("Fountain_COLOR", color);
    addParameter("Fountain_LENGTH", length);
    innerLine = new FlyerLine(lx);
    innerLine.setP1Strict(true);
    innerLine.setP2Strict(true);
    innerLine.setP1(300, -20, 100);
    innerLine.setP2(300, 200, 100);
    innerLine.setRadius(150);
    outerLine = new FlyerLine(lx);
    outerLine.setP1Strict(true);
    outerLine.setP2Strict(true);
    outerLine.setP1(300, 300, 100);
    outerLine.setP2(300, 500, 100);
    outerLine.setRadius(500);
    outerLine.setInnerRadius(150);
  }

  @Override
  public void run(double deltaMs) {
    up = wave.getValue() - wave.getValue() < 0;
    float len = length.getValuef();
    boolean outsideOn = true;
    boolean insideOn = true;
    if (up) {
      innerTop = wave.getValuef() * height;
      outerBottom = 0;
      if (innerTop > len) {
        innerBottom = innerTop - len;
        outerTop = 0;
        outsideOn = false;
      } else {
        innerBottom = 0;
        outerTop = len - innerTop;
      }
    } else {
      innerTop = height;
      outerBottom = wave.getValuef() * height;
      if (height - outerBottom < len) {
        innerBottom = 2*height - outerBottom - len;
        outerTop = height;
      } else {
        innerBottom = height;
        outerTop = outerBottom + len;
        insideOn = false;
      }
    }
    outerLine.setY2(outerTop);
    outerLine.setY1(outerBottom);
    outerLine.updateFlyers();
    innerLine.setY2(innerTop);
    innerLine.setY1(innerBottom);
    innerLine.updateFlyers();
    for (FlyerModel flyer: model.getFlyers()) {
      if ((outsideOn && outerLine.isInLine(flyer)) || (insideOn && innerLine.isInLine(flyer))) {
        for (LightSamplePointModel point : model.getFlyerLights(flyer.getIndex())) {
          setColor(point, LX.hsb(color.getValuef(), 100, 100));
        }
      } else {
        for (LightSamplePointModel point : model.getFlyerLights(flyer.getIndex())) {
          setColor(point, LX.hsb(color.getValuef(), 100, 10));
        }
      }
    }
  }
}
