package edu.stanford.ee185.model;

/** The physical configuration of a Fractal Flyer: where it is hung,
 *  its rotation, and what aspect of the EE department history is
 *  written on its wings. Used by Patterns that depend on the
 *  physical position or identity of the Fractal Flyer, as well
 *  as the UI for drawing.
 *
 *  Author: Ashley Chen 
 */
public class FlyerConfig {
  /** Index of the flyer: its unique ID, starting from 0, that is used to
    * identify it and access array-based data structures. */
  public int index;

  /** Specifies how and where the flyer is hung. */
  public FaceConfig faceConfig;

  /** Rotation of the flyer along the Y axis, in degrees. 0 represents
   *  pointing to the right in the front window. */
  public float rotation;
    
  /** Tilt of the flyer in the Z axis, in degrees. 0 is parallel
   *  to the ground and positive values point then nose upwards. */
  public float tilt;

  /** Information such as who/what the Flyer represents in the story
   * of the EE department. */
  public FlyerMetadata metadata;

  // A Fractal Flyer's 
  public static class FlyerMetadata {
    public String name; // Name of person, event, or organization
    public String text; // Descriptive text
    public String category; // What kind of Flyer: student, event, etc.
    public String department; // For people, which department (if any)
    public int year; // A distinctive year for this particular fact

      @Override
      public String toString() {
          String s = "Name: " + name + "\n";
          s += "Text: " + text + "\n";
          s += "Category: " + category + "\n";
          s += "Department: " + department + "\n";
          s += "Year: " + year + "\n";
          return s;
      }
  }

  public static class FaceConfig{
    // Specifies where the flyer is hung: C is a ceiling pipe, F is
    // the front window, E is the exterior edge of the stairs, I is
    // the interior edge of the stairs.
    public String face;
      
    public float x; // 0-window width for Flyers on the front window
    public int y;   // [0-4] for Flyers on the front window (which bar)
    public float position; // 0-1, location on stairway for stairway flyers
    public float pipe;     // Which pipe a ceiling Flyer hangs from
    public float mountPoint; // Where on the pipe a ceiling Flyer is attached
    public float hangDistance; // How far a Flyer is hung from its point

      @Override
      public String toString() {
          String s = "Face: \"" + face + "\"\n";
          s += "x: " + x + "\n";
          s += "y: " + y + "\n";
          s += "position: " + position + "\n";
          s += "pipe: " + pipe+ "\n";
          s += "mountPoint: " + mountPoint + "\n";
          s += "hangDistance: " + hangDistance + "\n";
          return s;
      }

  }
    
  public FaceConfig getFaceConfig() {
    return faceConfig;
  }
  
  public FaceConfig getFaceConfigCopy() {
    FaceConfig newFaceConfig = new FaceConfig();
    newFaceConfig.face = this.faceConfig.face;
    newFaceConfig.x = this.faceConfig.x;
    newFaceConfig.y = this.faceConfig.y;
    newFaceConfig.pipe = this.faceConfig.pipe;
    newFaceConfig.hangDistance = this.faceConfig.hangDistance;
    newFaceConfig.position = this.faceConfig.position;
    newFaceConfig.mountPoint = this.faceConfig.mountPoint;
    return newFaceConfig;
  }

  public FlyerMetadata getMetadata() {
    return metadata;
  }

  public FlyerMetadata getMetadataCopy() {
    FlyerMetadata newFlyerMetadata = new FlyerMetadata();
    newFlyerMetadata.category = this.metadata.category;
    newFlyerMetadata.department = this.metadata.department;
    newFlyerMetadata.name = this.metadata.name;
    newFlyerMetadata.text = this.metadata.text;
    newFlyerMetadata.year = this.metadata.year;
    return newFlyerMetadata;
  }

  @Override
  public String toString() {
      String s = "Index: " + index + "\n";
      s += metadata.toString();
      s += faceConfig.toString();
      s += "Rotation: " + rotation + "\n";
      s += "Tilt: " + tilt + "\n";
      return s;
  }
}
