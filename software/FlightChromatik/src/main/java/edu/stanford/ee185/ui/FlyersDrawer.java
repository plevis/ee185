package edu.stanford.ee185.ui;
import edu.stanford.ee185.engine.FlyerHighlighter;
import edu.stanford.ee185.model.*;


import heronarts.glx.GLX;
import heronarts.glx.VertexBuffer;
import heronarts.glx.VertexDeclaration;
import heronarts.glx.View;
import heronarts.glx.ui.UI;
import heronarts.lx.LX;
import heronarts.lx.LXEngine;
import heronarts.lx.studio.FlightApp;
import heronarts.lx.studio.ui.global.MasterControl;
import heronarts.lx.utils.LXUtils;
import heronarts.lx.color.LXColor;
import static org.lwjgl.bgfx.BGFX.bgfx_set_transform;
import java.awt.Color;

// To work with Vertex Buffers
import org.lwjgl.system.MemoryUtil;

import java.nio.ByteBuffer;

/**
 * The FLIGHT GUI window that draws the stairwell of the Packard
 * building at Stanford with FLIGHT's Fractal Flyers. The Fractal
 * Flyers visualize light colors and wing position.
 *
 * Drawing relies heavily on matrices to draw things in the right
 * places with simple logic. You can think of a matrix as a stored
 * represention of what (0, 0, 0) and the direction of the axes are
 * when we draw something. For example, the code to draw a Flyer draws
 * it with its center of mass at (0, 0, 0), its tail at (-x1, 0, 0)
 * and its head at (x2, 0, 0). To draw a flyer at (50, 50, 50) and
 * pointing in the Z direction we create a new matrix, translate to
 * (50, 50, 50), rotate to Z, draw the flyer, then discard ("pop") the
 * matrix.
 *
 * @author Gordon Martinez-Piedra <martigp@stanford.edu>
 */
public class FlyersDrawer extends FlightDrawer implements FlightGeometry {
    private FlightApp app;
    private FlightModel model;
    private FlyerHighlighter flyerHighlighter;

    private final FlyerLeftWing leftWingBuffer;
    private final FlyerRightWing rightWingBuffer;
    private final FlyerTopPlate topPlateBuffer;
    private final FlyerBodyShell bodyShellBuffer;
    private final FlyerCable cableBuffer;
    private static final float WING_TRIANGLE_ANGLE = LX.PIf / 3f;
    private static final float FLYER_WIDE_ANGLE = toRadiansF(101);
    private static final float TOP_PLATE_LONG_DIAGONAL = 26.26f;
    private static final float TOP_PLATE_SHORT_DIAGONAL = 8.08f;
    private static final float WING_LONG_EDGE = 23.036f;
    private static final float WING_SHORT_EDGE = 7.87f;
    /* Boolean constant for right wing !RIGHT_WING indicates left wing. */
    private static final boolean RIGHT_WING = true;

    private MasterControl masterControl;

    private class FlyerCable extends VertexBuffer {
        private static final float CABLE_RADIUS = 0.5f;

        private static float[][] vertices = null;
        private static boolean didInitVertices = false;
        private static void initVertices() {
            if(didInitVertices)
                return;
            didInitVertices = true;

            final float sqrt3 = (float)Math.sqrt(3.0);
            final float a = CABLE_RADIUS*sqrt3;
            final float dz = a / 2f;
            final float nx =  CABLE_RADIUS / 2f;
            final float px = CABLE_RADIUS;

            vertices = new float[][] {
                    {-nx, 1, dz}, // A
                    {-nx, 0, dz}, // B
                    {-nx, 1, -dz}, // C
                    {-nx, 0, -dz}, // D
                    {px, 1, 0}, // E
                    {px, 0, 0}, // F
                    {-nx, 1, dz}, // A
                    {-nx, 0, dz}, // B
            };
        }

        private FlyerCable(GLX glx) {
            super(glx, 8, VertexDeclaration.ATTRIB_POSITION);
        }

        @Override
        protected void bufferData(ByteBuffer byteBuffer) {
            initVertices();

            for (float[] vertex : vertices) {
                putVertex(vertex[0], vertex[1], vertex[2]);
            }
        }
    }

    private class FlyerLeftWing extends FlightVertexBuffer {
        public static final int NUM_VERTICES = 3;
        public static final float[] A = new float[] {0, 0, 0};
        public static final float[] B = new float[] {
            LXUtils.cosf(WING_TRIANGLE_ANGLE) * WING_SHORT_EDGE,
            0,
            LXUtils.sinf(WING_TRIANGLE_ANGLE) * WING_SHORT_EDGE };
        public static final float[] C = new float[] {WING_LONG_EDGE, 0, 0};

        private FlyerLeftWing(GLX glx) {
            super(glx, NUM_VERTICES, VertexDeclaration.ATTRIB_POSITION);
        }

        @Override
        protected void bufferData(ByteBuffer buffer) {
            putTriangle(A, B, C);
        }

    }
    
    private class FlyerRightWing extends FlightVertexBuffer {
        public static final int NUM_TRIANGLES = 1;
        public static final float[] A = new float[] { 0, 0, 0 };
        public static final float[] B = new float[] {
            LXUtils.cosf(WING_TRIANGLE_ANGLE) * WING_SHORT_EDGE,
            0,
            -LXUtils.cosf(WING_TRIANGLE_ANGLE) * WING_SHORT_EDGE };
        public static final float[] C = new float[] { WING_LONG_EDGE, 0, 0 };

        private FlyerRightWing(GLX glx) {
            super(glx, NUM_TRIANGLES * NUM_VERTICIES_PER_TRIANGLE, VertexDeclaration.ATTRIB_POSITION);
        }

        @Override
        protected void bufferData(ByteBuffer buffer) {
            putTriangle(A, B, C);
        }

    }
    
    /* Vertex Buffer that represents a top plate of a Flyer */
    private class FlyerTopPlate extends FlightVertexBuffer {
        /* Number of triangles to draw a quadilateral */
        public static final int NUM_TRIANGLES = 2;
        /* The top plate color */
        public final int COLOR = LXColor.rgb(0xa0, 0xa0, 0xa0);
        /* The X position of top plate's verticies on the short diagonal */
        public static final float X_POS = (TOP_PLATE_SHORT_DIAGONAL / 2) / LXUtils.tanf(FLYER_WIDE_ANGLE / 2);
        
        /* The verticies of the top flyer plate */
        public static final float[] A = new float[]{0, 0, 0};
        public static final float[] B = new float[]{X_POS, 0, TOP_PLATE_SHORT_DIAGONAL/2};
        public static final float[] C = new float[]{TOP_PLATE_LONG_DIAGONAL, 0, 0};
        public static final float[] D = new float[] {X_POS, 0, -TOP_PLATE_SHORT_DIAGONAL/2};

        private FlyerTopPlate(GLX glx) {
            super(glx, NUM_TRIANGLES * NUM_VERTICIES_PER_TRIANGLE, VertexDeclaration.ATTRIB_POSITION);
            System.out.println(FLYER_WIDE_ANGLE);
            System.out.println(X_POS);
        }

        @Override
        protected void bufferData(ByteBuffer buffer) {
            // Top Plate
            putTriangle(A, C, D);
            putTriangle(A, C, B);
        }
    }

    private class FlyerBodyShell extends FlightVertexBuffer {

        public static final int NUM_VERTICES = 12;
        private static final float SHELL_HEIGHT = 4.39f;
        /* The X position of the bottom point of the body shell. */
        private static final float BOTTOM_POINT_X = 5.29f;
        /* The X position of body shells verticies on the short diagonal */
        public static final float X_POS = (TOP_PLATE_SHORT_DIAGONAL / 2) / LXUtils.tanf(FLYER_WIDE_ANGLE / 2);

        private FlyerBodyShell(GLX glx) {
            super(glx, NUM_VERTICES, VertexDeclaration.ATTRIB_POSITION);
        }

        @Override
        protected void bufferData(ByteBuffer buffer) {
            putVertex(0, 0, 0);
            putVertex(X_POS, 0, TOP_PLATE_SHORT_DIAGONAL/2);
            putVertex(BOTTOM_POINT_X, -SHELL_HEIGHT, 0);
        
            putVertex(0, 0, 0);
            putVertex(X_POS, 0, -TOP_PLATE_SHORT_DIAGONAL/2);
            putVertex(BOTTOM_POINT_X, -SHELL_HEIGHT, 0);
        
            putVertex(TOP_PLATE_LONG_DIAGONAL, 0, 0);
            putVertex(X_POS, 0, TOP_PLATE_SHORT_DIAGONAL/2);
            putVertex(BOTTOM_POINT_X, -SHELL_HEIGHT, 0);
        
            putVertex(TOP_PLATE_LONG_DIAGONAL, 0, 0);
            putVertex(X_POS, 0, -TOP_PLATE_SHORT_DIAGONAL/2);
            putVertex(BOTTOM_POINT_X, -SHELL_HEIGHT, 0);
        }

    }

    public FlyersDrawer(FlightApp app, FlightModel model, FlyerHighlighter flyerHighlighter, MasterControl masterControl) {
        super();
        this.app = app;
        this.model = model;
        this.flyerHighlighter = flyerHighlighter;
        this.masterControl = masterControl;

        this.leftWingBuffer = new FlyerLeftWing(app);
        this.rightWingBuffer = new FlyerRightWing(app);
        this.topPlateBuffer = new FlyerTopPlate(app);
        this.bodyShellBuffer = new FlyerBodyShell(app);
        this.cableBuffer = new FlyerCable(app);
    }

    @Override
    protected void onDraw(UI ui, View view) {
        try {
            drawFlyers(ui, view);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * Draw all of the Fractal Flyers.
     */
    private void drawFlyers(UI ui, View view) {
        computeFlyerLightsAndWings();
        double masterSpeed = masterControl.speedParameter.getValuef();
        ui.lx.setSpeed(masterSpeed/100);
        
        // We draw each flyer by first computing a geometric
        // transformation (matrix) for its position and orientation,
        // drawing it, then popping the tranformation.  This allows the
        // draw code to just operate in a normalized coordinate space. -pal
        for (FlyerModel flyer : model.getFlyers()) {
            float x = flyer.getX();
            float y = flyer.getY();
            float z = flyer.getZ();

            pushMatrix();
            this.modelMatrix.identity().translate(x,y,z);
            this.modelMatrix.rotateY(toRadiansF(-flyer.getRotation()));
            this.modelMatrix.rotateZ(toRadiansF(-flyer.getTilt()));
            drawFlyer(ui, view, flyer);
            popMatrix();
        }
    }

    /**
     * Draw one Fractal Flyer at (0,0,0). its long dimension
     * along the X axis, its narrow dimension along the Z axis,
     * and its body hanging down in the Y axis.
     * All of these values are in inches, taken from
    // FRACTAL_FLYER/drawings/body-dimensions-for-ui-graphics.pdf
     */
    private void drawFlyer(UI ui, View view, FlyerModel flyer) {
        LXEngine.Frame frame = app.uiFrame;
        int colors[] = frame.getColors();

        int curFillColor;

        pushMatrix();

        pushMatrix();
        modelMatrix.scale(1f, (float)flyer.getHangDistance(), 1f);
        bgfx_set_transform(this.modelMatrix.get(this.modelMatrixBuf));
        ui.lx.program.uniformFill.submit(view, drawStateTriStrip, 0, this.cableBuffer);
        popMatrix();

        this.modelMatrix.translate(-FLYER_HANGING_OFFSET, 0, 0);

        // Top Plate
        bgfx_set_transform(this.modelMatrix.get(this.modelMatrixBuf));
        ui.lx.program.uniformFill.submit(view, drawStateTriangle,
                                         this.topPlateBuffer.COLOR,
                                         this.topPlateBuffer);

        // Left wing
        Wing leftWing = flyer.getLeftWing();
        pushMatrix();
        this.modelMatrix.translate(4.01f, 0f, 4.96f).rotateY(LX.PIf / 18);
        curFillColor = colorMaster(colors[wingLightIndex(flyer.getIndex(), !RIGHT_WING, 0)]);
        float langle = toRadiansF((float)leftWing.getSkew());
        this.modelMatrix.rotateX(langle);
        bgfx_set_transform(this.modelMatrix.get(this.modelMatrixBuf));
        ui.lx.program.uniformFill.submit(view, drawStateTriangle, curFillColor,
            this.leftWingBuffer);
        popMatrix();
        

        // Right wing
        Wing rightWing = flyer.getRightWing();
        curFillColor = colorMaster(colors[wingLightIndex(flyer.getIndex(), RIGHT_WING, 0)]);
        pushMatrix();
        this.modelMatrix.translate(4.01f, 0f, -4.96f).rotateY(- LX.PIf/18);
        float rangle = toRadiansF((float)rightWing.getSkew());
        this.modelMatrix.rotateX(-rangle);
        bgfx_set_transform(this.modelMatrix.get(this.modelMatrixBuf));
        ui.lx.program.uniformFill.submit(view, drawStateTriangle, curFillColor,
            this.rightWingBuffer);
        popMatrix();

        // Body shell
        curFillColor = colorMaster(colors[bodyLightIndex(flyer.getIndex())]);
        bgfx_set_transform(this.modelMatrix.get(this.modelMatrixBuf));
        ui.lx.program.uniformFill.submit(view, drawStateTriStrip, curFillColor,
            this.bodyShellBuffer);
        
        popMatrix();
    }

    // Using the knobs on the master panel to change colors.
    private int colorMaster(int color) {
        if (masterControl.enableStatus.getValueb()){
            float masterHue = masterControl.hueParameter.getValuef();
            float masterSat = masterControl.saturationParameter.getValuef();
            float masterBri = masterControl.brightnessParameter.getValuef();
            String hex = Integer.toHexString(color);
            int r = Integer.parseInt(hex.substring(2,4), 16);
            int g = Integer.parseInt(hex.substring(4,6), 16);
            int b = Integer.parseInt(hex.substring(6,8), 16);
            float[] hsbvals = Color.RGBtoHSB(r,g,b,null);
            return LX.hsb((hsbvals[0]*360 + masterHue)%360, (hsbvals[1] * masterSat), (hsbvals[2] * masterBri));
        } else {
            return color;
        }
    }

    @Override
    public void dispose() {
        this.topPlateBuffer.dispose();
        this.leftWingBuffer.dispose();
        this.rightWingBuffer.dispose();
        this.bodyShellBuffer.dispose();

        MemoryUtil.memFree(this.modelMatrixBuf);
        super.dispose();
    }
    private void computeFlyerLightsAndWings() {
        // flyerHighlighter.highlight();
    }
}