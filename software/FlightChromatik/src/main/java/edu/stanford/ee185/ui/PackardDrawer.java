package edu.stanford.ee185.ui;

import edu.stanford.ee185.model.FlightGeometry;
import heronarts.lx.model.LXModel.Geometry;
import heronarts.lx.studio.FlightApp;
import heronarts.glx.ui.UI3dComponent;
import heronarts.glx.View;
import heronarts.glx.GLX;
import heronarts.glx.VertexBuffer;
import heronarts.glx.VertexDeclaration;
import heronarts.glx.ui.UI;
import heronarts.lx.utils.LXUtils;
import heronarts.lx.color.LXColor;
import heronarts.lx.LX;
import edu.stanford.ee185.ui.FlightDrawer.*;

import static org.lwjgl.bgfx.BGFX.bgfx_set_transform;

import java.nio.ByteBuffer;

public class PackardDrawer extends FlightDrawer implements FlightGeometry {
    // These variables are all derived from constants in FlightGeometry -pal

    // The X offset of the vertical bars on the front window. These are
    // used as reference points for drawing the overhead pipes as well
    // as the front window bars.
    private final float[] VBAR_OFFSETS;

    // The X and Z offset of reference points on the back wall. These
    // correspond to a subset of the VBAR_OFFSETS, but rotated -P/4 along
    // the Y axis.
    private final float[] BACK_WALL_OFFSETS;

    // The angle of the wall in radians between the front window and back
    // wall (45 degrees)
    private final double WALL_ANGLE = Math.toRadians(45);

    private final float BACK_CORNER_X = FRONT_WINDOW_WIDTH
        * LXUtils.cosf(WALL_ANGLE);

    private final float BACK_CORNER_Z = FRONT_WINDOW_WIDTH
            * LXUtils.sinf(WALL_ANGLE);

    private final int NUM_PLATFORMS = 3;
    private final int NUM_FLIGHTS = 6;

    private static final float BASEMENT_Y = -1 * FLOOR_HEIGHT;
    private static final float GROUND_FLOOR_Y = 0 * FLOOR_HEIGHT;
    private static final float FIRST_FLOOR_Y = 1 * FLOOR_HEIGHT;
    private static final float FIRST_SECOND_Y = 2 * FLOOR_HEIGHT;
    
    enum STAIR_DIRECTION {
        UP,
        DOWN
    }

    FlightApp app;

    // Walls
    FrontWindow frontWindowBuffer;
    BackWall backWallBuffer;
    Floor floorBuffer;
    WindowRoofBar roofBarBuffer;
    WindowVBARs windowVBARsBuffer;
    WindowHBARs windowHBARsBuffer;
    StairPlatform stairPlatformBuffer;
    UpStairFlight upFlightBuffer;
    DownStairFlight downFlightbuffer;
    
    /**
     * Fr
     */
    private class FrontWindow extends FlightVertexBuffer {
        public static final int NUM_VERTICES = 5;
        public final int COLOR = LXColor.rgb(0xc0,0xc0, 0xc0);

        FrontWindow(GLX glx) {
            super(glx, NUM_VERTICES, VertexDeclaration.ATTRIB_POSITION);
        }

        @Override
        protected void bufferData(ByteBuffer buffer) {
            putVertex(0f, GROUND_FLOOR_Y, 0f);
            putVertex(FRONT_WINDOW_WIDTH, GROUND_FLOOR_Y, 0f);
            putVertex(FRONT_WINDOW_WIDTH, BASEMENT_Y, 0f);
            putVertex(0f, BASEMENT_Y, 0f);
            putVertex(0f, GROUND_FLOOR_Y, 0f);
        }
    }
    
    /**
     * Back wall of packard buildilng
     */
    private class BackWall extends FlightVertexBuffer {
        public static final int NUM_VERTICES = 5;
        public final int COLOR = LXColor.rgb(0xf0, 0xf0, 0xf0);

        BackWall(GLX glx) {
            super(glx, NUM_VERTICES, VertexDeclaration.ATTRIB_POSITION);
        }

        @Override
        protected void bufferData(ByteBuffer buffer) {
            putVertex(0, BASEMENT_Y, 0);
            putVertex(0, FRONT_WINDOW_CORNER_HEIGHT, 0);
            putVertex(BACK_CORNER_X, FRONT_WINDOW_CORNER_HEIGHT, BACK_CORNER_Z);
            putVertex(BACK_CORNER_X, BASEMENT_Y, BACK_CORNER_Z);
            putVertex(0, BASEMENT_Y, 0);
        }
    }
    
    /**
     * The floor of the packard building (basement floor)
     */
    private class Floor extends FlightVertexBuffer {
        public static final int NUM_VERTICES = 3;
        public final int COLOR = LXColor.rgb(0x80, 0x80, 0x80);

        Floor(GLX glx) {
            super(glx, NUM_VERTICES, VertexDeclaration.ATTRIB_POSITION);
        }

        @Override
        protected void bufferData(ByteBuffer buffer) {
            putVertex(0, BASEMENT_Y, 0);
            putVertex(FRONT_WINDOW_WIDTH, BASEMENT_Y, 0);
            putVertex(BACK_CORNER_X, BASEMENT_Y, BACK_CORNER_Z);
        }
    }

    /**
     * All Vertical Bars on the Packard Front Window.
     */
    private class WindowVBARs extends FlightVertexBuffer {
        public static final int NUM_VERTICES_PER_BAR = 6;

        WindowVBARs(GLX glx) {
            super(glx,
                NUM_VERTICES_PER_BAR * FlightGeometry.FRONT_WINDOW_VBAR_SPACINGS.length,
                VertexDeclaration.ATTRIB_POSITION);
        }

        // Chromatik requires we draw all bars in a single buffer. Each 
        // Iteration of the for loop as two triangles.
        @Override
        protected void bufferData(ByteBuffer buffer) {
            int i = 0;
            for (float xPos : VBAR_OFFSETS) {
                float height = FRONT_WINDOW_VBAR_HEIGHTS[i];

                putVertex(xPos, 0, 0);
                putVertex(xPos + FRONT_WINDOW_BAR_WIDTH, 0, 0);
                putVertex(xPos, height, 0);

                putVertex(xPos + FRONT_WINDOW_BAR_WIDTH, 0, 0);
                putVertex(xPos, height, 0);
                putVertex(xPos + FRONT_WINDOW_BAR_WIDTH, height, 0);
                i++;
            }
        }
    }

    /**
     * All Horizontal bars on the front window of the Packard Building.
     */
    private class WindowHBARs extends FlightVertexBuffer {
        public static final int NUM_VERTICES_PER_BAR = 6;

        WindowHBARs(GLX glx) {
            super(glx,
                NUM_VERTICES_PER_BAR * FRONT_WINDOW_HBAR_SPACINGS.length,
                VertexDeclaration.ATTRIB_POSITION);
        }

        // See comments for WindowVBAR, same concept.
        @Override
        protected void bufferData(ByteBuffer buffer) {
            float yPos = 0F;
            for (float spacing : FRONT_WINDOW_HBAR_SPACINGS) {
                yPos += spacing;
                putVertex(0f, yPos, 0f);
                putVertex(0f, yPos + FRONT_WINDOW_BAR_WIDTH, 0f);
                putVertex(FRONT_WINDOW_WIDTH, yPos + FRONT_WINDOW_BAR_WIDTH,
                    0f);

                putVertex(0f, yPos, 0f);
                putVertex(FRONT_WINDOW_WIDTH, yPos + FRONT_WINDOW_BAR_WIDTH,
                    0f);
                putVertex(FRONT_WINDOW_WIDTH, yPos, 0f);
            }
        }
    }

    /**
     * The Bar on the top of the Packard Building
     */
    private class WindowRoofBar extends FlightVertexBuffer {

        // Quadilaterals are drawns with two triangles, needing 6 verticies
        public static final int NUM_VERTICES_PER_QUADRILATERAL = 4;
        // The roof bar is drawn with two quadilaterals as described in the
        // buffer data method. 
        public static final int NUM_QUADS = 3;


        WindowRoofBar(GLX glx) {
            super(glx, NUM_QUADS * NUM_VERTICES_PER_QUADRILATERAL,
                    VertexDeclaration.ATTRIB_POSITION);
        }

        /**
         * Window roof bar is drawn by drawing two quadilaterals that meet
         * at the second to last VBAR (with the lowest height). The roof bar
         * descends linearly to this point and ascends linearly after this point
         * going in +x direction. The quadilateral is from 0 -> the minimum 
         * point. The second is from the mimunum point -> end of front window.
         */
        @Override
        protected void bufferData(ByteBuffer buffer) {
            // Lowest VBAR Index is the index of the VBAR that marks the 
            // lowest point of the window roof bar.
            int lowestVBARIndex = VBAR_OFFSETS.length - 2;
            int lastVBARIndex = VBAR_OFFSETS.length - 1;

            // 1st quadilateral: Made of two triangles
            putVertex(0, FRONT_WINDOW_CORNER_HEIGHT, 0);
            putVertex(0, FRONT_WINDOW_CORNER_HEIGHT + FRONT_WINDOW_TOP_WIDTH,
                    0);
            putVertex(VBAR_OFFSETS[lowestVBARIndex] + FRONT_WINDOW_BAR_WIDTH,
                    FRONT_WINDOW_VBAR_HEIGHTS[lowestVBARIndex], 0);

            putVertex(VBAR_OFFSETS[lowestVBARIndex] + FRONT_WINDOW_BAR_WIDTH,
                    FRONT_WINDOW_VBAR_HEIGHTS[lowestVBARIndex] + FRONT_WINDOW_TOP_WIDTH,
                    0);
            putVertex(0, FRONT_WINDOW_CORNER_HEIGHT, 0);
            putVertex(VBAR_OFFSETS[lowestVBARIndex] + FRONT_WINDOW_BAR_WIDTH,
                    FRONT_WINDOW_VBAR_HEIGHTS[lowestVBARIndex], 0);

            // 2nd Quadilateral: Made of two triangles
            putVertex(VBAR_OFFSETS[lowestVBARIndex] + FRONT_WINDOW_BAR_WIDTH,
                    FRONT_WINDOW_VBAR_HEIGHTS[lowestVBARIndex], 0);
            putVertex(VBAR_OFFSETS[lowestVBARIndex] + FRONT_WINDOW_BAR_WIDTH,
                    FRONT_WINDOW_VBAR_HEIGHTS[lowestVBARIndex] + FRONT_WINDOW_TOP_WIDTH,
                    0);
            putVertex(VBAR_OFFSETS[lastVBARIndex] + FRONT_WINDOW_BAR_WIDTH,
                    FRONT_WINDOW_VBAR_HEIGHTS[lastVBARIndex] + FRONT_WINDOW_TOP_WIDTH,
                    0);

            putVertex(VBAR_OFFSETS[lastVBARIndex] + FRONT_WINDOW_BAR_WIDTH,
                    FRONT_WINDOW_VBAR_HEIGHTS[lastVBARIndex] + FRONT_WINDOW_TOP_WIDTH,
                    0);
            putVertex(VBAR_OFFSETS[lowestVBARIndex] + FRONT_WINDOW_BAR_WIDTH,
                    FRONT_WINDOW_VBAR_HEIGHTS[lowestVBARIndex], 0);
            putVertex(VBAR_OFFSETS[lastVBARIndex] + FRONT_WINDOW_BAR_WIDTH,
                    FRONT_WINDOW_VBAR_HEIGHTS[lastVBARIndex], 0);
        }

    }
    
    /**
     * The landing platform for the packard staircase. The vertices A-L
     * correspond to the 12 vertecies of the stair platform with A being the
     * vertex on the longest edge of the platform closest to the back wall.
     * Rotating clockwise the L then corresponds to the vertex on the longest
     * edge of the platform closest to the front wall.
     */
    private class StairPlatform extends FlightVertexBuffer {
        // Object is rendered using 10 triangles.
        private static final int NUM_TRIANGLES = 10;

        private static final float[] A = new float[] { 126.0f, 0.0f, 0.0f };
        private static final float[] B = new float[] { 126.0f, 0.0f, 32.0f };
        private static final float[] C = new float[] { 168.0f, 0.0f, 32.0f };
        private static final float[] D = new float[] { 168.0f - 126.0f * LXUtils.sinf(LX.PIf / 8f),
                0.0f,
                32.0f + 126f * LXUtils.cosf(LX.PIf / 8f) };
        private static final float[] E = new float[] {
                168.0f - 126.0f * LXUtils.sinf(LX.PIf / 8f) - 69f * LXUtils.sinf(5f / 8f * LX.PIf),
                0.0f,
                32f + 126f * LXUtils.cosf(LX.PIf / 8f) + 69f * LXUtils.cosf(5f / 8f * LX.PIf) };
        private static final float[] F = new float[] {
                168.0f - 126.0f * LXUtils.sinf(LX.PIf / 8f) - 69f * LXUtils.sinf(5f / 8f * LX.PIf)
                        + 21f * LXUtils.sinf(LX.PIf / 8f),
                0f,
                32f + 126f * LXUtils.cosf(LX.PIf / 8f) + 69f * LXUtils.cosf(5f / 8f * LX.PIf)
                        - 21f * LXUtils.cosf(LX.PIf / 8f) };
        private static final float[] G = new float[] {
                -168.0f + 126.0f * LXUtils.sinf(LX.PIf / 8f) + 69f * LXUtils.sinf(5f / 8f * LX.PIf)
                        - 21f * LXUtils.sinf(LX.PIf / 8f),
                0f,
                32f + 126f * LXUtils.cosf(LX.PIf / 8f) + 69f * LXUtils.cosf(5f / 8f * LX.PIf)
                        - 21f * LXUtils.cosf(LX.PIf / 8f) };
        private static final float[] H = new float[] {
                -168.0f + 126.0f * LXUtils.sinf(LX.PIf / 8f) + 69f * LXUtils.sinf(5f / 8f * LX.PIf),
                0f,
                32f + 126f * LXUtils.cosf(LX.PIf / 8f) + 69f * LXUtils.cosf(5f / 8f * LX.PIf) };
        private static final float[] I = new float[] {
                -168.0f + 126.0f * LXUtils.sinf(LX.PIf / 8),
                0f,
                32.0f + 126f * LXUtils.cosf(LX.PIf / 8) };
        private static final float[] J = new float[] { -168.0f, 0, 32.0f };
        private static final float[] K = new float[] { -126.0f, 0, 32.0f };
        private static final float[] L = new float[] { -126.0f, 0, 0.0f };

        StairPlatform(GLX glx) {
            super(glx, NUM_TRIANGLES * NUM_VERTICIES_PER_TRIANGLE,
                    VertexDeclaration.ATTRIB_POSITION);
        }

        @Override
        protected void bufferData(ByteBuffer buffer) {

            putTriangle(B, D, C);
            putTriangle(B, D, F);
            putTriangle(D, F, E);
            putTriangle(B, F, A);
            putTriangle(F, A, L);
            putTriangle(J, I, K);
            putTriangle(I, K, G);
            putTriangle(I, G, H);
            putTriangle(G, K, L);
            putTriangle(L, F, G); // LFG!
        }
    }
    
    private class UpStairFlight extends FlightVertexBuffer {
        private static final int NUM_TRIANGLES = 3;
        private static final float[] A = new float[] { 0, 0, 0 };
        private static final float[] B = new float[] { 0, FLOOR_HEIGHT / 2, 186f };
        private static final float[] C = new float[] { 0, FLOOR_HEIGHT / 2, 186f + 128f };
        private static final float[] D = new float[] { 69f, FLOOR_HEIGHT / 2, 186f };
        private static final float[] E = new float[] { 69f, 0, 0 };

        UpStairFlight(GLX glx) {
            super(glx, NUM_TRIANGLES * NUM_VERTICIES_PER_TRIANGLE,
                    VertexDeclaration.ATTRIB_POSITION);
        }

        @Override
        protected void bufferData(ByteBuffer buffer) {
            putTriangle(A,B,E);
            putTriangle(B,D,E);
            putTriangle(B,C,D);
        }
    }
    
    private class DownStairFlight extends FlightVertexBuffer {
        private static final int NUM_TRIANGLES = 3;
        private static final int NUM_VERTICIES_PER_TRIANGLE = 3;
        private static final float[] A = new float[] { 0, 0, 0 };
        private static final float[] B = new float[] { 0, -FLOOR_HEIGHT / 2, 186f };
        private static final float[] C = new float[] { 0, -FLOOR_HEIGHT / 2, 186f + 128f };
        private static final float[] D = new float[] { -69f, -FLOOR_HEIGHT / 2, 186f };
        private static final float[] E = new float[] { -69f, 0, 0 };


        DownStairFlight(GLX glx) {
            super(glx, NUM_TRIANGLES * NUM_VERTICIES_PER_TRIANGLE,
                    VertexDeclaration.ATTRIB_POSITION);
        }

        @Override
        protected void bufferData(ByteBuffer buffer) {
            putTriangle(A,B,E);
            putTriangle(B,D,E);
            putTriangle(B,C,D);
        }
    }

    /**
     * This class renders everything except for the flyers in Packard including
     * the walls, the window, the floor and the stairwell.
     * 
     * @param app The Flight App instance
     */
    public PackardDrawer(FlightApp app) {
        this.app = app;

        this.frontWindowBuffer = new FrontWindow(app);
        this.backWallBuffer = new BackWall(app);
        this.floorBuffer = new Floor(app);
        
        int numVbars = FRONT_WINDOW_VBAR_SPACINGS.length;
        VBAR_OFFSETS = new float[numVbars];
        float xPos = 0.0f;
        for (int i = 0; i < numVbars; i++) {
            xPos += FRONT_WINDOW_VBAR_SPACINGS[i];
            VBAR_OFFSETS[i] = xPos;
        }

        this.windowVBARsBuffer = new WindowVBARs(app);

        BACK_WALL_OFFSETS = new float[BACK_WALL_SPACINGS.length];
        float yPos = 0f;
        for (int i = 0; i < BACK_WALL_SPACINGS.length; i++) {
            yPos += BACK_WALL_SPACINGS[i];
            BACK_WALL_OFFSETS[i] = yPos;
        }

        this.windowHBARsBuffer = new WindowHBARs(app);
        this.roofBarBuffer = new WindowRoofBar(app);
        this.stairPlatformBuffer = new StairPlatform(app);
        this.upFlightBuffer = new UpStairFlight(app);
        this.downFlightbuffer = new DownStairFlight(app);
    }

    @Override
    protected void onDraw(UI ui, View view) {
        try {

            // drawCeiling(ui, view);
            drawWalls(ui, view);
            drawStairs(ui, view);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * Draw all of the stairs: the platforms on the ground floor, floor 2,
     * and floor 3 as well as the stairs that connec them.
     */
    private void drawStairs(UI ui, View view) {
        int stairColor = LXColor.rgb(0x60, 0x60, 0x60);
        pushMatrix();
        // Translate to the midpoint of the center wall that
        // the stair platforms are on; this is halfway between
        // the end of the front wall (WIDTH) and the end of the back wall
        // (0.707 * WIDTH) -pal
        this.modelMatrix.translate(FRONT_WINDOW_WIDTH * (1f + 0.707f) / 2f,
                    0f,
                    FRONT_WINDOW_WIDTH * (0.707f) / 2f);
        // The stairwell is 45 degrees; this means the mid angle from the
        // center of the stair wall -22.5 degrees from along the X axis,
        // so -(PI/4 + P/16)
        this.modelMatrix.rotateY(-5.0f / 8.0f * LX.PIf);

        // Drawing stair platform
        pushMatrix();
        for (int i = 0; i < NUM_PLATFORMS; i++) {
            bgfx_set_transform(this.modelMatrix.get(this.modelMatrixBuf));
            ui.lx.program.uniformFill.submit(view, drawStateTriangle, stairColor,
                    this.stairPlatformBuffer);
            this.modelMatrix.translate(0, FLOOR_HEIGHT, 0);
        }
        popMatrix();

        // Drawing stair flights
        pushMatrix();
        this.modelMatrix.translate(0f, -FLOOR_HEIGHT, 0f);
        for (int i = 0; i < NUM_FLIGHTS; i++) {
            boolean isUpStair = (i % 2 == 0);
            float stairPolarity = isUpStair ? 1f : -1f;
            VertexBuffer stairBuffer = isUpStair ? this.upFlightBuffer : this.downFlightbuffer;

            pushMatrix();
            this.modelMatrix.translate(
                stairPolarity * (-168.0f + 126.0f * LXUtils.sinf(LX.PIf/8f)),
                0,
                32.0f + 126f * LXUtils.cosf(LX.PIf / 8f));
            this.modelMatrix.rotateY(stairPolarity * LX.PIf/8f);
            bgfx_set_transform(this.modelMatrix.get(this.modelMatrixBuf));

            ui.lx.program.uniformFill.submit(view, drawStateTriangle, stairColor,
                stairBuffer);
            popMatrix();

            if (isUpStair) {
                this.modelMatrix.translate(0f, FLOOR_HEIGHT, 0f);
            }
        }
        popMatrix();
        popMatrix();
    }

    private void drawWalls(UI ui, View view) {   
        int barColor = LXColor.rgb(0xc0, 0xc0, 0xc0);

        bgfx_set_transform(this.modelMatrix.get(this.modelMatrixBuf));
        ui.lx.program.uniformFill.submit(view, drawStateTriStrip, barColor,
                                        this.frontWindowBuffer);

        // VBARS
        ui.lx.program.uniformFill.submit(view, drawStateTriangle, barColor,
                                        this.windowVBARsBuffer);
        
        
        // HBARs
        ui.lx.program.uniformFill.submit(view, drawStateTriangle, barColor,
                                        this.windowHBARsBuffer);
        
        // Roof Bar
        ui.lx.program.uniformFill.submit(view, drawStateTriangle, barColor,
            roofBarBuffer);

        // Bottom floor
        ui.lx.program.uniformFill.submit(view, drawStateTriStrip,
            this.floorBuffer.COLOR, this.floorBuffer);
        
        // Back Wall
        ui.lx.program.uniformFill.submit(view, drawStateTriStrip,
            this.backWallBuffer.COLOR, this.backWallBuffer);
    }
}
