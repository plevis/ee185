package edu.stanford.ee185.ui;

import static org.lwjgl.bgfx.BGFX.BGFX_STATE_BLEND_ALPHA;
import static org.lwjgl.bgfx.BGFX.BGFX_STATE_DEPTH_TEST_LESS;
import static org.lwjgl.bgfx.BGFX.BGFX_STATE_WRITE_RGB;
import static org.lwjgl.bgfx.BGFX.BGFX_STATE_WRITE_Z;
import static org.lwjgl.bgfx.BGFX.BGFX_STATE_PT_TRISTRIP;

import java.nio.FloatBuffer;
import java.util.Stack;

import org.joml.Matrix4f;
import org.lwjgl.system.MemoryUtil;

import heronarts.glx.ui.UI3dComponent;
import heronarts.glx.VertexBuffer;
import heronarts.glx.GLX;
import heronarts.lx.LX;

/**
 * This interface is used to create a custom renderer. It contains several
 * constants that are frequently used when rendering as well as some helper
 * functionality such as a model matrix stack to make rendering simpler.
 */
public abstract class FlightDrawer extends UI3dComponent {

  protected Matrix4f modelMatrix = new Matrix4f();
  protected final FloatBuffer modelMatrixBuf;

 
  /**
   * The draw state is passed into a parameter when you make a call
   * to the rendering API (ui.lx.program.uniformFill.submit). The draw state
   * specifies how to interpret the vertices provided. Draw state triangle
   * interprets vertices as a list of triangles and fills in each triangle
   * with the specified color.
   */
  protected final long drawStateTriangle = 
    BGFX_STATE_WRITE_RGB |
    BGFX_STATE_WRITE_Z |
    BGFX_STATE_BLEND_ALPHA |
    BGFX_STATE_DEPTH_TEST_LESS;
  
  /**
   * For the more advanced graphics user this essentially allows you to draw
   * triangles without having to re-specify vertices. An example can be found
   * for the FrontWindowBuffer in PackardDrawer.java.
   */
  protected final long drawStateTriStrip =
      drawStateTriangle | BGFX_STATE_PT_TRISTRIP;
  
  // 
  protected static final float FLOOR_HEIGHT = 180f;
    
  /* The model Matrix stack used to store previous transformation matrices
   * for re-use.*/
  protected final Stack<Matrix4f> modelMatrixStack;

  /**
   * A thin wrapper on top of Vertex Buffers to allow simple rendering using
   * triangles.
   */
  public abstract class FlightVertexBuffer extends VertexBuffer {

    public static int NUM_VERTICIES_PER_TRIANGLE = 3;

    FlightVertexBuffer(GLX glx, int numVerticies, int attributes) {
      super(glx, numVerticies, attributes);
    }

    /**
     * Method to render a triangle. This is the simplest way to draw shapes
     * using Chromatik. For example a quadilateral is 2 triangles.
     * 
     * @param a,b,c The three verticies of the triangle you are trying
     * to render. Each is a float array of size three, corresponding to
     * the x, y, and z coordinates of the triangle.
     */
    public void putTriangle(float[] a, float[] b, float[] c) {
      putVertex(a[0],a[1],a[2]);
      putVertex(b[0],b[1],b[2]);
      putVertex(c[0],c[1],c[2]);
    }
  }

  /**
   * Saves the current modelMatrix so that it can be reused later (via popMatrix)
   * You should call this when you want to perform a translation or rotation
   * of the model matrix to later render something but want to be able to axess
   * 
   */
  protected void pushMatrix() {
    this.modelMatrixStack.push(new Matrix4f(this.modelMatrix));
  }

  /**
   * This method restores the current model matrix to the previous value.
   * Helpful when you need to apply a specific transform e.g. for a flyer
   * but want to return to the previous transform state afterwards.
   */
  protected void popMatrix() {
    this.modelMatrix = new Matrix4f(this.modelMatrixStack.pop());
  }

  /**
   * Helper function to convert an angle in degrees its floating point
   * representation in radians.
   * @param angdeg the angle in degrees
   * @return The angle in radians.
   */
  protected static float toRadiansF(float angdeg) {
    return angdeg * LX.PIf / 180f;
  }

  protected FlightDrawer() {
    // 4x4 matrix - just part of Java
    this.modelMatrixBuf = MemoryUtil.memAllocFloat(16);
    /**
     * The tranformation that is applied to all verticies when rendering. This
     * is an essential part of rendering for graphics.
     */
    this.modelMatrix.get(this.modelMatrixBuf);
    /**
     * The stack that allows you to build up from where you are.s
     */
    this.modelMatrixStack = new Stack<Matrix4f>();
  }

  @Override
  public void dispose() {
    MemoryUtil.memFree(this.modelMatrixBuf);
    super.dispose();
  }

}
