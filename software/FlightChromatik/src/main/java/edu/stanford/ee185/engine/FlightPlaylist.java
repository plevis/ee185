package edu.stanford.ee185.engine;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.*;

import java.io.File;
import java.util.Collections;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import heronarts.lx.mixer.*;
import heronarts.lx.studio.LXStudio;
import heronarts.lx.parameter.LXParameter;
import heronarts.lx.parameter.LXParameterListener;
import heronarts.lx.parameter.DiscreteParameter;
import heronarts.lx.clip.LXClip;

/**
 * Central controller for a FLIGHT installation, responsible for loading and
 * running tracks, loading the available Patterns, and configuring output to
 * generate Python code.
 *
 * This class is able to load multiple tracks, have each loop for some number of
 * times, and transition to a new track. This collection of tracks is called a
 * playlist. The playlist can also be set to start at a specific start time
 * which is tied to the host computers system clock
 *
 * @author Ashley Chen <ashleyic@cs.stanford.edu>
 * @author David Van Dyje <dhvd.stanford.edu>
 * @author Abdu Mohamdy <amohamdy@stanford.edu>
 */

public class FlightPlaylist implements FlightTrack {

  // Helper class for reading/write data
  final private IO io;

  // The LXStudio used to drive the whole application.
  final private LXStudio lx;

  // The queue of tracks to be played
  final private ArrayList<Track> playQueue;

  // Indices indicating which track is playing and which is selected.
  final public DiscreteParameter playIndex;

  private boolean playIndexUpdate;

  // Initialize a timer to see if the track should transition
  private Timer timer;

  // Store the listener and the clip for moving across tracks
  private LXParameterListener trackListener = null;
  private LXClip clip;

  // Flag that shows us if we are in a looping state
  private boolean isLoop;

  // Flag for if start time is currently enabled
  private boolean isStartTimeEnabled;

  // Stores the current start time
  private long playlistStartTime;

  // List of loops
  private ArrayList<Integer> trackLoops;

  // List of counter to keep track of the loops
  private ArrayList<Integer> trackLoopsCounter;

  // List of names of the tracks
  private ArrayList<String> trackNames;

  // Start time where index [0] is the the hour and [1] is the minute
  private int[] startTime = new int[] { 0, 0 };

  public FlightPlaylist(IO io, LXStudio lx) {
    this.io = io;
    this.lx = lx;
    this.playQueue = new ArrayList<Track>();
    this.playIndex = new DiscreteParameter("playIndex", 0, 0, 256);
    this.playIndexUpdate = false;
    this.isLoop = false;
    this.isStartTimeEnabled = false;
    this.trackLoops = new ArrayList<Integer>();
    this.trackLoopsCounter = new ArrayList<Integer>();
    this.trackNames = new ArrayList<String>();

    // Set the transition timer to check the time every minute
    timer = new Timer();
    timer.schedule(new CheckTime(), 0, 1000);
  }

  /***** PUBLIC FUNCTIONS *****/

  /**
   * Checks if the current time exceeds a dedicated transition time Causes the
   * track to increment if that is the case
   *
   * @return none
   */
  class CheckTime extends TimerTask {
    @Override
    public void run() {
      if (playIndexUpdate) {
        System.out.println(trackLoopsCounter.get(playIndex.getValuei()));
        System.out.println(trackLoops.get(playIndex.getValuei()));

        if (trackLoopsCounter.get(playIndex.getValuei()) == trackLoops.get(playIndex.getValuei())) {
          playIndex.increment();
        }
        clip.running.removeListener(trackListener);
        playIndexUpdate = false;
        if (playIndex.getValuei() == playQueue.size()) {
          System.out.println("tracks have ended");
          if (isLoop) {
            System.out.println("loop from first track");
            trackLoopsCounter = new ArrayList<Integer>(Collections.nCopies(trackLoops.size(), 0));
            playbackTrack(0);
          }
        } else {
          System.out.println("advance the track! " + playIndex.getValuei());
          playbackTrack(playIndex.getValuei());
        }
      }
    }
  }

  class playbackTask extends TimerTask {
    @Override
    public void run() {
      System.out.println("Target time occured");
      playIndex.increment();
      playIndex.decrement();
      playbackTrack(0);
      isStartTimeEnabled = false;
    }
  }

  /**
   * Returns the LXMixerEngine object in the LXEngine
   *
   * @return A list of LXAbstractChannel
   */
  public LXMixerEngine getMixerEngine() {
    return lx.engine.mixer;
  }

  /**
   * Returns the loaded channels in the LX engine. NOTE: This does not include the
   * master channel
   *
   * @return A list of LXAbstractChannel
   */
  List<LXAbstractChannel> getChannels() {
    return lx.engine.mixer.channels;
  }

  /**
   * Sets up and starts recording a track: Arms all the channels in the mixer to a
   * recording state and triggers all the clips at index 0 (each channel is
   * configured to have one clip only
   */
  public void startRecording() {
    lx.engine.mixer.masterBus.arm.setValue(true);
    for (LXAbstractChannel channel : getChannels()) {
      channel.arm.setValue(true);
    }
    lx.engine.clips.launchScene(0);
  }

  /**
   * Stops recording a track.
   */
  public void stopRecording() {
    lx.engine.mixer.masterBus.arm.setValue(false);
    for (LXAbstractChannel channel : getChannels()) {
      channel.arm.setValue(false);
    }
  }

  /**
   * Starts playback of a loaded track: Disarms all the channels in the mixer to a
   * playback state and triggers all the clips at index 0 (each channel is
   * configured to have one clip only
   */
  public void playbackTrack(int trackIdx) {
    lx.engine.clips.stopClips();
    lx.engine.mixer.masterBus.arm.setValue(false);
    for (LXAbstractChannel channel : getChannels()) {
      channel.arm.setValue(false);
    }

    if (trackIdx == 0) {
      playIndex.decrement(256, false);
    }
    playQueue.get(trackIdx).loadTrack(lx);

    System.out.println("playbackTrack(" + trackIdx + ") called");

    clip = getMixerEngine().masterBus.getClip(0);

    trackListener = new LXParameterListener() {
      public void onParameterChanged(LXParameter p) {
        if (clip.running.isOn()) {
          // System.out.println("The clip has started " + trackIdx);
        } else {
          // System.out.println("The clip has stopped " + trackIdx);
          trackLoopsCounter.set(trackIdx, trackLoopsCounter.get(trackIdx) + 1);
          playIndexUpdate = true;
        }
      }
    };

    clip.running.addListener(trackListener);

    lx.engine.clips.launchScene(0);
  }

  /*
   * Configures the software to start playback once a certain time is reached
   */
  public void startAtTime(int hour, int minute) {
    System.out.println("SCHEDULED TIME");
    Calendar today = Calendar.getInstance();
    today.set(Calendar.HOUR_OF_DAY, hour);
    today.set(Calendar.MINUTE, minute);
    today.set(Calendar.SECOND, 0);

    timer.schedule(new playbackTask(), today.getTime(), TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS));
    // isStartTimeEnabled = true;
    // startTime[0] = hour;
    // startTime[1] = minute;
  }

  /**
   * Stops recording or playback of a track: Stops all clips in the mixer. This
   * will stop recording if the channels were in a recording state or stop
   * playback if the channels were in a playback state
   */
  public void stopTrack() {
	lx.engine.clips.stopClips();
    // timer.cancel();
    playIndex.decrement(256, false);
    playIndexUpdate = false;
    trackLoopsCounter = new ArrayList<Integer>(Collections.nCopies(trackLoops.size(), 0));
    if (trackListener != null) {
        clip.running.removeListener(trackListener);
    }

    isStartTimeEnabled = false;
  }

  /**
   * Loads a track from a file: Creates a Track object from a JsonArray that is
   * loaded by the IO class. It then calls the Track class function that loads the
   * track array onto the channels
   *
   * @param filename The relative path of the track file to be loaded
   */
  public void loadTrack(String filename) {
    try {
      JsonElement trackOrPlaylist = io.loadTrackFile(filename);
      if (trackOrPlaylist.isJsonArray()) {
        Track track = new Track(trackOrPlaylist.getAsJsonArray(), filename);
        playQueue.add(track);
        trackLoops.add(1);
        trackLoopsCounter.add(0);
        trackNames.add(filename);
      } else if (trackOrPlaylist.isJsonObject()) {
        JsonObject playlistJson = trackOrPlaylist.getAsJsonObject();
        // parse info
        JsonObject infoJson = playlistJson.get("info").getAsJsonObject();
        this.isStartTimeEnabled = infoJson.get("isStartTimeEnabled").getAsBoolean();
        this.playlistStartTime = infoJson.get("playlistStartTime").getAsLong();
        for (JsonElement trackLoop : infoJson.get("trackLoops").getAsJsonArray()) {
          this.trackLoops.add(trackLoop.getAsInt());
          this.trackLoopsCounter.add(0);
        }
        int trackStartAddIndex = this.trackNames.size();
        for (JsonElement trackName : infoJson.get("trackNames").getAsJsonArray()) {
          this.trackNames.add(trackName.getAsString());
        }

        // parse tracks
        for (JsonElement trackJson : playlistJson.get("tracks").getAsJsonArray()) {
          Track track = new Track(trackJson.getAsJsonArray(), this.trackNames.get(trackStartAddIndex));
          playQueue.add(track);
          trackStartAddIndex++;
        }
      }
    } catch (Exception e) {
      System.err.println("Error loading track from " + filename);
      e.printStackTrace();
    }
  }

  /**
   * Loads a track from a file: Creates a Track object from a JsonArray that is
   * loaded by the IO class. It then calls the Track class function that loads the
   * track array onto the channels
   *
   * @param file The file object of the track file to be loaded
   */
  public void loadTrack(File file) {
    JsonElement trackOrPlaylist = io.loadTrackFile(file);

    if (trackOrPlaylist.isJsonArray()) {
      Track track = new Track(trackOrPlaylist.getAsJsonArray(), file.getName());
      playQueue.add(track);
      trackLoops.add(1);
      trackLoopsCounter.add(0);
      trackNames.add(file.getName());
    } else if (trackOrPlaylist.isJsonObject()) {
      JsonObject playlistJson = trackOrPlaylist.getAsJsonObject();
      // parse info
      JsonObject infoJson = playlistJson.get("info").getAsJsonObject();
      this.isStartTimeEnabled = infoJson.get("isStartTimeEnabled").getAsBoolean();
      this.playlistStartTime = infoJson.get("playlistStartTime").getAsLong();
      for (JsonElement trackLoop : infoJson.get("trackLoops").getAsJsonArray()) {
        this.trackLoops.add(trackLoop.getAsInt());
        this.trackLoopsCounter.add(0);
      }
      int trackStartAddIndex = this.trackNames.size();
      for (JsonElement trackName : infoJson.get("trackNames").getAsJsonArray()) {
        this.trackNames.add(trackName.getAsString());
      }

      // parse tracks
      for (JsonElement trackJson : playlistJson.get("tracks").getAsJsonArray()) {
        Track track = new Track(trackJson.getAsJsonArray(), this.trackNames.get(trackStartAddIndex));
        playQueue.add(track);
        trackStartAddIndex++;
      }
    }
    System.out.println("playQueue: " + playQueue);

  }

  public void removeTrack(int index) {
    playQueue.remove(index);
    trackLoops.remove(index);
    trackLoopsCounter.remove(index);
    trackNames.remove(index);
  }

  /**
   * Saves a recorded track onto a file: Creates a Track object from a the
   * channels in the mixer and then save the resulting JsonArray through the IO
   * class
   *
   * @param filename The relative path of the track file to be saved
   */
  public void saveRecordingToTrack(String filename) {
    Track track = new Track(lx);
    io.saveTrackFile(track.getTrackArray(), filename);
  }

  /**
   * Saves a recorded track onto a file: Creates a Track object from a the
   * channels in the mixer and then save the resulting JsonArray through the IO
   * class
   *
   * @param file The file object of the track file to be saved
   */
  public void saveRecordingToTrack(File file) {
    Track track = new Track(lx);
    io.saveTrackFile(track.getTrackArray(), file);
  }

  /**
   * Helper function for saving playlist as a track Returns the loaded track
   * objects as a Json Array
   */
  private JsonObject getPlaylistJson() {
    JsonObject playlistJson = new JsonObject();
    // parse info
    JsonObject infoJson = new JsonObject();
    infoJson.addProperty("isStartTimeEnabled", isStartTimeEnabled);
    infoJson.addProperty("playlistStartTime", playlistStartTime);

    JsonArray trackLoopsJson = new JsonArray();
    for (Integer loop : trackLoops) {
      trackLoopsJson.add(loop);
    }
    infoJson.add("trackLoops", trackLoopsJson);

    JsonArray trackNamesJson = new JsonArray();
    JsonArray tracksArray = new JsonArray();
    for (Track track : playQueue) {
      trackNamesJson.add(track.getFilename());
      tracksArray.add(track.getTrackArray());
    }

    infoJson.add("trackNames", trackNamesJson);
    playlistJson.add("info", infoJson);
    playlistJson.add("tracks", tracksArray);

    return playlistJson;
  }

  /**
   * Saves a loaded track onto a file: Takes the loaded Track objects as
   * JsonArrays, adds them as JsonElements to another JsonArray, then saves the
   * resulting JsonArray through the IO class
   *
   * @param filename The relative path of the track file to be saved
   */
  public void savePlaylistAsTrack(String filename) {
    io.savePlaylistFile(getPlaylistJson(), filename);
  }

  /**
   * Saves a loaded track onto a file: Takes the loaded Track objects as
   * JsonArrays, adds them as JsonElements to another JsonArray, then saves the
   * resulting JsonArray through the IO class
   *
   * @param file The file object of the track file to be saved
   */
  public void savePlaylistAsTrack(File file) {
    io.savePlaylistFile(getPlaylistJson(), file);
  }

  /**
   * Sets whether the current track should run once or loop.
   */
  public void setTrackLooping(boolean looping) {
    isLoop = looping;
  }

  public List<Track> getTracks() {
    return playQueue;
  }

  public int getTrackLoop(int trackIndex) {
    return this.trackLoops.get(trackIndex);
  }

  /**
   * Moves tracks up and down @param playQueue
   *
   * @param index : Index of track to move
   * @param up    : True: move up. False: move down
   */
  public void moveInQueue(int index, boolean up) {
    if ((index == 0 && up) || (index == this.playQueue.size() - 1 && !up))
      return;

    Collections.swap(this.playQueue, index, index + (up ? -1 : 1));
    Collections.swap(this.trackNames, index, index + (up ? -1 : 1));
    Collections.swap(this.trackLoops, index, index + (up ? -1 : 1));
    Collections.swap(this.trackLoopsCounter, index, index + (up ? -1 : 1));

    if (index == playIndex.getValuei()) // moving the current track, adjust index
      if (up)
        this.playIndex.decrement();
      else
        this.playIndex.increment();
    // this.playIndex.updateValue(this.playIndex.getValuei() + (up ? -1 : 1));
  }

  /**
   * Increase/decrease the number of times a track is played @param playQueue
   *
   * @param index    : Index of track to move
   * @param increase : True: increase. False: decrease
   */
  public void updateLoopCount(int index, boolean increase) {
    if (trackLoops.get(index) == 1 && !increase) {
      return;
    }

    if (increase) {
      trackLoops.set(index, trackLoops.get(index) + 1);
    } else {
      trackLoops.set(index, trackLoops.get(index) - 1);
    }
  }


  // PRIVATE FUNCTIONS


}
