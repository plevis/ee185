package edu.stanford.ee185.model;

import heronarts.lx.model.LXModel;
import heronarts.lx.model.LXPoint;

public interface Flyer extends FlightGeometry {
  public final static int PIXELS_PER_BODY =
    LightSamplePoint.LightCorners.BODY.numPixels;

  int getIndex();

  float getX();

  float getY();

  float getZ();

  float getRotation();

  float getTilt();

  WingModel[] getWings();

  WingModel getLeftWing();

  WingModel getRightWing();

  LXPoint getBodyLightPoint();

//  List<? extends LXPoint> getLightPoints();

  FlyerConfig getConfig();

}
