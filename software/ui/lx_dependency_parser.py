import sys
import json

# Uses the grep output in the form
# <relative_path>:<line with string "lx">
def findlayer1LXUse(inputStr) -> dict:
    layer1Deps = inputStr.split("\n")

    # Maps path of file to the lx libraries imported
    pathToLxImportLibs = dict()

    # bug for some reason starts with end of inputs?
    for lineno, line in enumerate(layer1Deps):
        endOfFileName = line.find(":")

        if endOfFileName == -1:
            print(f"No path found in line: {lineno}")
            continue
        path = line[:endOfFileName]
        lineWithLxUsed = line[endOfFileName + 1:]

        importIndex = lineWithLxUsed.find("import")

        # Recording all imported libraries for each file/path
        if importIndex != -1:
            if path not in pathToLxImportLibs:
                pathToLxImportLibs[path] = []
            pathToLxImportLibs[path].append(lineWithLxUsed[len("import") + 1:])

    return pathToLxImportLibs

# Strip Parent Modules from LX
def stripParentModules(pathToLxImports : 
                       dict[str:list[str]]) -> dict[str:list[str]]:
    
    pathToLxModules = dict()
    for path in pathToLxImports:

        importLibs = pathToLxImports[path]

        pathToLxModules[path] = []

        for importLib in importLibs:
            # Everything after final super module\
            splitLxModule = importLib.split(".")
            lxModule = ""

            if "*" in importLib:
                # In case where wildcard operator used take first supermodule
                # as well as wildcard
                lxModule = f"{splitLxModule[-2]}.{splitLxModule[-1]}"
            else:
                lxModule = "".join(splitLxModule[-1:])
            
            # Remove everything after trailing semicolon
            semiColonIndex = lxModule.find(";")
            lxModule = lxModule[:semiColonIndex]

            pathToLxModules[path].append(lxModule)
    
    return pathToLxModules


"""
TODO: implement this recursive function that extracts the name of
a definition that includes this or anything that calls it.

Currently this simply looks at layer2 of dependencies i.e. finds all
lines that directly use an imported LX module
"""
def recurseLayerDeps(currentLevelVars : dict[str:list[str]]):
    layer2Deps = dict()
    for path in currentLevelVars:
        
        fileLayer2Deps = dict()
        vars = currentLevelVars[path]

        with open(path, 'r') as f:
            multiLineComment = False
            for lineno, line in enumerate(f):

                """
                Following section skips any areas invalidated by comments
                """
                if multiLineComment:
                    if line.find("*/") != -1:
                        multiLineComment = False
                    continue

                singleLineCommentIndex = line.find("//")
                if singleLineCommentIndex != -1:
                    line = line[:singleLineCommentIndex]
                
                multiLineCommentIndex = line.find("/*")
                if multiLineCommentIndex != -1:
                    line = line[:multiLineCommentIndex]
                    multiLineComment = True

                # skip import lines
                if "import" in line:
                    continue
                
                for var in vars:
                    if var in line:
                        fileLayer2Deps[lineno] = line.strip()
                        break
        
        layer2Deps[path] = fileLayer2Deps
    return layer2Deps



def main():
    inputStr = sys.stdin.read()

    pathToLxImportLibs = findlayer1LXUse(inputStr)
    
    pathToLxModules = stripParentModules(pathToLxImportLibs)

    pathToDirectLxModuleUse = recurseLayerDeps(pathToLxModules)

    with open("layer2deps.json", 'x') as f:
        json.dump(pathToDirectLxModuleUse, f)




if __name__ == '__main__':
    main()