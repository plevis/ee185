#!/bin/sh
DEPSFILE=layer2deps.json
if [ -f "$DEPSFILE" ]; then
    rm $DEPSFILE;
fi

grep -r "LXStudio" ./src/* | grep "\.java" | python3 ./lx_dependency_parser.py

