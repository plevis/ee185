EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLedger 17000 11000
encoding utf-8
Sheet 1 1
Title "Tailboard_Signal_PCB"
Date ""
Rev ""
Comp "blame- EE 285 Aut 2021 Signaling Group"
Comment1 "Stanford, CA 94305-9505"
Comment2 "350 Jane Stanford Way"
Comment3 "Electrical Engineering Department"
Comment4 "Light Sculpture Project / Stanford University"
$EndDescr
$Comp
L power:GND #PWR013
U 1 1 618211A8
P 7500 5000
F 0 "#PWR013" H 7500 4750 50  0001 C CNN
F 1 "GND" V 7505 4872 50  0000 R CNN
F 2 "" H 7500 5000 50  0001 C CNN
F 3 "" H 7500 5000 50  0001 C CNN
	1    7500 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8000 6550 8100 6550
NoConn ~ 8100 6550
Text Label 10900 6400 0    50   ~ 0
ethernet_D+
Wire Wire Line
	7400 7900 7100 7900
Wire Wire Line
	7100 8200 7400 8200
$Comp
L power:GND #PWR0102
U 1 1 617E707E
P 7700 8500
F 0 "#PWR0102" H 7700 8250 50  0001 C CNN
F 1 "GND" V 7705 8372 50  0000 R CNN
F 2 "" H 7700 8500 50  0001 C CNN
F 3 "" H 7700 8500 50  0001 C CNN
	1    7700 8500
	1    0    0    -1  
$EndComp
$Comp
L Interface_UART:SN75176AD U1
U 1 1 617D92C8
P 7700 8100
F 0 "U1" H 7500 8500 50  0000 C CNN
F 1 "SN75176AD" H 8000 7700 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 7700 7600 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn75176a.pdf" H 9300 7900 50  0001 C CNN
	1    7700 8100
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C3
U 1 1 617FBA6D
P 7500 4850
F 0 "C3" H 7382 4804 50  0000 R CNN
F 1 "10uF" H 7382 4895 50  0000 R CNN
F 2 "tailboard_signalling_pcb:CAPC2012X135N" H 7538 4700 50  0001 C CNN
F 3 "https://www.digikey.com/en/products/detail/samsung-electro-mechanics/CL21A106KPFNNNG/3894419" H 7500 4850 50  0001 C CNN
	1    7500 4850
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8450 5100 8450 5250
Wire Wire Line
	7100 6050 7100 5950
Wire Wire Line
	7200 6050 7100 6050
Wire Wire Line
	7200 6350 7100 6350
Wire Wire Line
	8000 6450 8150 6450
Wire Wire Line
	9700 7200 9700 7100
Wire Wire Line
	9200 7100 9200 7200
Wire Wire Line
	9200 6250 9200 6400
Wire Wire Line
	8000 6250 9200 6250
Wire Wire Line
	8000 6150 9700 6150
Connection ~ 7100 5950
Wire Wire Line
	7200 5950 7100 5950
$Comp
L power:GND #PWR010
U 1 1 617C8A3A
P 9700 7200
F 0 "#PWR010" H 9700 6950 50  0001 C CNN
F 1 "GND" V 9705 7072 50  0000 R CNN
F 2 "" H 9700 7200 50  0001 C CNN
F 3 "" H 9700 7200 50  0001 C CNN
	1    9700 7200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR09
U 1 1 617C856E
P 9200 7200
F 0 "#PWR09" H 9200 6950 50  0001 C CNN
F 1 "GND" V 9205 7072 50  0000 R CNN
F 2 "" H 9200 7200 50  0001 C CNN
F 3 "" H 9200 7200 50  0001 C CNN
	1    9200 7200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 617C7A10
P 9200 6950
F 0 "R7" V 9407 6950 50  0000 C CNN
F 1 "51" V 9316 6950 50  0000 C CNN
F 2 "tailboard_signalling_pcb:RESC2012X65N" V 9130 6950 50  0001 C CNN
F 3 "https://www.digikey.com/en/products/detail/te-connectivity-passive-product/CRG0805F51R/2380916" H 9200 6950 50  0001 C CNN
	1    9200 6950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 617C71FA
P 9700 6950
F 0 "R5" V 9907 6950 50  0000 C CNN
F 1 "51" V 9816 6950 50  0000 C CNN
F 2 "tailboard_signalling_pcb:RESC2012X65N" V 9630 6950 50  0001 C CNN
F 3 "https://www.digikey.com/en/products/detail/te-connectivity-passive-product/CRG0805F51R/2380916" H 9700 6950 50  0001 C CNN
	1    9700 6950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 617C40A5
P 4900 6550
F 0 "#PWR05" H 4900 6300 50  0001 C CNN
F 1 "GND" V 4905 6422 50  0000 R CNN
F 2 "" H 4900 6550 50  0001 C CNN
F 3 "" H 4900 6550 50  0001 C CNN
	1    4900 6550
	-1   0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 617DEF4B
P 8450 4950
F 0 "C2" H 8550 4850 50  0000 C CNN
F 1 "1000pF" H 8300 5050 50  0000 C CNN
F 2 "tailboard_signalling_pcb:CAPC2012X135N" H 8488 4800 50  0001 C CNN
F 3 "https://www.digikey.com/en/products/detail/samsung-electro-mechanics/CL21B102KBANNNC/3886678" H 8450 4950 50  0001 C CNN
	1    8450 4950
	-1   0    0    1   
$EndComp
$Comp
L tailboard_signalling_pcb-rescue:XRCGB24M000F0L00R0-tailboard_signalling_pcb Y1
U 1 1 617D7C42
P 6300 6750
F 0 "Y1" H 6250 6950 50  0000 L CNN
F 1 "XRCGB24M000F0L00R0" H 5800 6600 50  0000 L CNN
F 2 "tailboard_signalling_pcb:OSC_XRCGB24M000F0L00R0" H 6300 6750 50  0001 L BNN
F 3 "https://www.digikey.com/en/products/detail/murata-electronics/xrcgb24m000f0g00r0/4423081" H 6300 6750 50  0001 L BNN
F 4 "N/A" H 6300 6750 50  0001 L BNN "PARTREV"
F 5 "0.7 mm" H 6300 6750 50  0001 L BNN "MAXIMUM_PACKAGE_HEIGHT"
F 6 "Murata Electronics" H 6300 6750 50  0001 L BNN "MANUFACTURER"
F 7 "Manufacturer Recommendations" H 6300 6750 50  0001 L BNN "STANDARD"
	1    6300 6750
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR07
U 1 1 617D35B3
P 8500 6600
F 0 "#PWR07" H 8500 6350 50  0001 C CNN
F 1 "GND" V 8505 6472 50  0000 R CNN
F 2 "" H 8500 6600 50  0001 C CNN
F 3 "" H 8500 6600 50  0001 C CNN
	1    8500 6600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR08
U 1 1 617D3038
P 8450 5250
F 0 "#PWR08" H 8450 5000 50  0001 C CNN
F 1 "GND" V 8455 5122 50  0000 R CNN
F 2 "" H 8450 5250 50  0001 C CNN
F 3 "" H 8450 5250 50  0001 C CNN
	1    8450 5250
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 617D0DDF
P 7900 4850
F 0 "C4" H 8015 4896 50  0000 L CNN
F 1 "1uF" H 8015 4805 50  0000 L CNN
F 2 "tailboard_signalling_pcb:CAPC2012X135N" H 7938 4700 50  0001 C CNN
F 3 "https://www.digikey.com/en/products/detail/samsung-electro-mechanics/CL21B105KOFNNNE/3886684" H 7900 4850 50  0001 C CNN
	1    7900 4850
	1    0    0    1   
$EndComp
$Comp
L Device:C C6
U 1 1 617D061A
P 11050 4650
F 0 "C6" H 10950 4550 50  0000 C CNN
F 1 "1uF" H 10950 4750 50  0000 C CNN
F 2 "tailboard_signalling_pcb:CAPC2012X135N" H 11088 4500 50  0001 C CNN
F 3 "https://www.digikey.com/en/products/detail/samsung-electro-mechanics/CL21B105KOFNNNE/3886684" H 11050 4650 50  0001 C CNN
	1    11050 4650
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR04
U 1 1 617CED72
P 7100 6350
F 0 "#PWR04" H 7100 6100 50  0001 C CNN
F 1 "GND" V 7105 6222 50  0000 R CNN
F 2 "" H 7100 6350 50  0001 C CNN
F 3 "" H 7100 6350 50  0001 C CNN
	1    7100 6350
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR03
U 1 1 617C1759
P 11050 4800
F 0 "#PWR03" H 11050 4550 50  0001 C CNN
F 1 "GND" H 11250 4700 50  0000 R CNN
F 2 "" H 11050 4800 50  0001 C CNN
F 3 "" H 11050 4800 50  0001 C CNN
	1    11050 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9700 6800 9700 6500
Text Label 10900 6500 0    50   ~ 0
ethernet_D-
Wire Wire Line
	10000 8200 10000 6700
Text Label 11250 6700 0    50   ~ 0
R-
Text Label 11250 6600 0    50   ~ 0
R+
Connection ~ 9200 6400
Wire Wire Line
	9200 6400 9200 6800
Connection ~ 9700 6500
Wire Wire Line
	9700 6150 9700 6500
Wire Wire Line
	10350 6600 10350 8300
$Comp
L power:GND #PWR012
U 1 1 61993EEB
P 10800 7050
F 0 "#PWR012" H 10800 6800 50  0001 C CNN
F 1 "GND" V 10805 6922 50  0000 R CNN
F 2 "" H 10800 7050 50  0001 C CNN
F 3 "" H 10800 7050 50  0001 C CNN
	1    10800 7050
	1    0    0    -1  
$EndComp
Text Label 11200 6300 0    50   ~ 0
GND
NoConn ~ 7400 8000
$Comp
L power:GND #PWR014
U 1 1 619F8A9C
P 7100 8200
F 0 "#PWR014" H 7100 7950 50  0001 C CNN
F 1 "GND" V 7105 8072 50  0000 R CNN
F 2 "" H 7100 8200 50  0001 C CNN
F 3 "" H 7100 8200 50  0001 C CNN
	1    7100 8200
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG02
U 1 1 61A04A2C
P 11500 9950
F 0 "#FLG02" H 11500 10025 50  0001 C CNN
F 1 "PWR_FLAG" H 11500 10123 50  0000 C CNN
F 2 "" H 11500 9950 50  0001 C CNN
F 3 "~" H 11500 9950 50  0001 C CNN
	1    11500 9950
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x07 J1
U 1 1 61A12320
P 11600 6500
F 0 "J1" H 11680 6542 50  0000 L CNN
F 1 "Conn_01x07" H 11680 6451 50  0000 L CNN
F 2 "tailboard_signalling_pcb:171856-1007" H 11600 6500 50  0001 C CNN
F 3 "https://www.digikey.com/en/products/detail/molex/1718561007/4424977?s=N4IgTCBcDaIIwHY4A4CsA2OAGLCQF0BfIA" H 11600 6500 50  0001 C CNN
	1    11600 6500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 8300 7300 8300
Text Label 11200 6800 0    50   ~ 0
Rout
Wire Wire Line
	10350 8850 10350 8750
$Comp
L power:GND #PWR019
U 1 1 61A3E21D
P 10350 8850
F 0 "#PWR019" H 10350 8600 50  0001 C CNN
F 1 "GND" V 10355 8722 50  0000 R CNN
F 2 "" H 10350 8850 50  0001 C CNN
F 3 "" H 10350 8850 50  0001 C CNN
	1    10350 8850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 61A3E227
P 10350 8600
F 0 "R2" H 10450 8750 50  0000 C CNN
F 1 "51" H 10450 8450 50  0000 C CNN
F 2 "tailboard_signalling_pcb:RESC2012X65N" V 10280 8600 50  0001 C CNN
F 3 "https://www.digikey.com/en/products/detail/te-connectivity-passive-product/CRG0805F51R/2380916" H 10350 8600 50  0001 C CNN
	1    10350 8600
	1    0    0    -1  
$EndComp
Wire Wire Line
	10350 8450 10350 8300
Wire Wire Line
	10000 8850 10000 8750
$Comp
L power:GND #PWR018
U 1 1 61A42BE4
P 10000 8850
F 0 "#PWR018" H 10000 8600 50  0001 C CNN
F 1 "GND" V 10005 8722 50  0000 R CNN
F 2 "" H 10000 8850 50  0001 C CNN
F 3 "" H 10000 8850 50  0001 C CNN
	1    10000 8850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 61A42BEE
P 10000 8600
F 0 "R1" H 10100 8750 50  0000 C CNN
F 1 "51" H 10100 8450 50  0000 C CNN
F 2 "tailboard_signalling_pcb:RESC2012X65N" V 9930 8600 50  0001 C CNN
F 3 "https://www.digikey.com/en/products/detail/te-connectivity-passive-product/CRG0805F51R/2380916" H 10000 8600 50  0001 C CNN
	1    10000 8600
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C8
U 1 1 61A47387
P 5900 6900
F 0 "C8" H 5992 6946 50  0000 L CNN
F 1 "N/S" H 5992 6855 50  0000 L CNN
F 2 "tailboard_signalling_pcb:CAPC2012X135N" H 5900 6900 50  0001 C CNN
F 3 "~" H 5900 6900 50  0001 C CNN
	1    5900 6900
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C7
U 1 1 61A48EF8
P 6600 6900
F 0 "C7" H 6692 6946 50  0000 L CNN
F 1 "N/S" H 6692 6855 50  0000 L CNN
F 2 "tailboard_signalling_pcb:CAPC2012X135N" H 6600 6900 50  0001 C CNN
F 3 "~" H 6600 6900 50  0001 C CNN
	1    6600 6900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR015
U 1 1 61A491F6
P 6600 7000
F 0 "#PWR015" H 6600 6750 50  0001 C CNN
F 1 "GND" H 6605 6827 50  0000 C CNN
F 2 "" H 6600 7000 50  0001 C CNN
F 3 "" H 6600 7000 50  0001 C CNN
	1    6600 7000
	1    0    0    -1  
$EndComp
Text Notes 6050 7250 0    50   ~ 0
0603 size
$Comp
L tailboard_signalling_pcb-rescue:MIC5219-3.3YM5-TR-tailboard_signalling_pcb U3
U 1 1 61A528F1
P 10650 4600
F 0 "U3" H 11750 4988 60  0000 C CNN
F 1 "MIC5219-3.3YM5-TR" H 11750 4882 60  0000 C CNN
F 2 "tailboard_signalling_pcb:SOT95P280X145-5N" H 11750 4840 60  0001 C CNN
F 3 "https://www.digikey.com/en/products/detail/microchip-technology/MIC5219-3-3YM5-TR/771902" H 11750 4882 60  0001 C CNN
	1    10650 4600
	-1   0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 617C07B7
P 6850 5700
F 0 "C1" V 6598 5700 50  0000 C CNN
F 1 "1uF" V 6689 5700 50  0000 C CNN
F 2 "tailboard_signalling_pcb:CAPC2012X135N" H 6888 5550 50  0001 C CNN
F 3 "https://www.digikey.com/en/products/detail/samsung-electro-mechanics/CL21B105KOFNNNE/3886684" H 6850 5700 50  0001 C CNN
	1    6850 5700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR011
U 1 1 617C433B
P 6850 5850
F 0 "#PWR011" H 6850 5600 50  0001 C CNN
F 1 "GND" V 6750 5850 50  0000 R CNN
F 2 "" H 6850 5850 50  0001 C CNN
F 3 "" H 6850 5850 50  0001 C CNN
	1    6850 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 6400 11400 6400
Wire Wire Line
	9700 6500 11400 6500
Wire Wire Line
	10350 6600 11400 6600
Wire Wire Line
	10000 6700 11400 6700
Text Label 8200 4700 0    50   ~ 0
+3V3
Wire Wire Line
	7300 8300 7300 9250
$Comp
L power:GND #PWR06
U 1 1 618DD86C
P 5900 7000
F 0 "#PWR06" H 5900 6750 50  0001 C CNN
F 1 "GND" V 5905 6872 50  0000 R CNN
F 2 "" H 5900 7000 50  0001 C CNN
F 3 "" H 5900 7000 50  0001 C CNN
	1    5900 7000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 6450 5900 6450
Wire Wire Line
	5900 6450 5900 6750
Wire Wire Line
	6100 6750 5900 6750
Connection ~ 5900 6750
Wire Wire Line
	5900 6750 5900 6800
Wire Wire Line
	6850 6750 6600 6750
Wire Wire Line
	6600 6800 6600 6750
Connection ~ 6600 6750
Wire Wire Line
	6600 6750 6500 6750
Wire Wire Line
	6850 6550 7200 6550
Wire Wire Line
	6850 6550 6850 6750
Wire Wire Line
	4900 6400 4900 6550
Connection ~ 10350 8300
Wire Wire Line
	8000 8200 10000 8200
Wire Wire Line
	8000 8300 10350 8300
Wire Wire Line
	10000 8200 10000 8450
Connection ~ 10000 8200
$Comp
L power:+5V #PWR020
U 1 1 619E0C76
P 7700 7350
F 0 "#PWR020" H 7700 7200 50  0001 C CNN
F 1 "+5V" H 7715 7523 50  0000 C CNN
F 2 "" H 7700 7350 50  0001 C CNN
F 3 "" H 7700 7350 50  0001 C CNN
	1    7700 7350
	1    0    0    -1  
$EndComp
Connection ~ 7100 8200
Wire Wire Line
	7100 7900 7100 8200
Wire Wire Line
	7700 7350 7700 7500
$Comp
L tailboard_signalling_pcb-rescue:C-Device-tail-rescue C51
U 1 1 619F231B
P 8150 7650
F 0 "C51" H 8265 7696 50  0000 L CNN
F 1 "1uF" H 8265 7605 50  0000 L CNN
F 2 "tailboard_signalling_pcb:CAPC2012X135N" H 8188 7500 50  0001 C CNN
F 3 "https://www.digikey.com/en/products/detail/samsung-electro-mechanics/CL21B105KOFNNNE/3886684" H 8150 7650 50  0001 C CNN
	1    8150 7650
	1    0    0    -1  
$EndComp
$Comp
L tailboard_signalling_pcb-rescue:C-Device-tail-rescue C50
U 1 1 619F5EF8
P 8550 7650
F 0 "C50" H 8665 7696 50  0000 L CNN
F 1 "0.01uF" H 8665 7605 50  0000 L CNN
F 2 "tailboard_signalling_pcb:CAPC2012X140N" H 8588 7500 50  0001 C CNN
F 3 "https://www.digikey.com/en/products/detail/samsung-electro-mechanics/CL21B103KCANNNC/3886907" H 8550 7650 50  0001 C CNN
	1    8550 7650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR022
U 1 1 61A0BE58
P 8350 7800
F 0 "#PWR022" H 8350 7550 50  0001 C CNN
F 1 "GND" V 8355 7672 50  0000 R CNN
F 2 "" H 8350 7800 50  0001 C CNN
F 3 "" H 8350 7800 50  0001 C CNN
	1    8350 7800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8150 7800 8350 7800
Connection ~ 8350 7800
Wire Wire Line
	8350 7800 8550 7800
Wire Wire Line
	8150 7500 8550 7500
Wire Wire Line
	8150 7500 7700 7500
Connection ~ 8150 7500
Connection ~ 7700 7500
Wire Wire Line
	7700 7500 7700 7700
$Comp
L tailboard_signalling_pcb-rescue:UIC4102CP-tailboard_signalling_pcb D2
U 1 1 617B73BB
P 7600 6250
F 0 "D2" H 7600 6800 50  0000 C CNN
F 1 "UIC4102CP" H 7600 6700 50  0000 C CNN
F 2 "tailboard_signalling_pcb:SOP14_small" H 7600 6850 50  0001 C CNN
F 3 "" H 7600 6750 50  0001 C CNN
	1    7600 6250
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 617CF8D8
P 8300 6450
F 0 "R6" V 8100 6450 50  0000 C CNN
F 1 "9.1k" V 8200 6450 50  0000 C CNN
F 2 "tailboard_signalling_pcb:RESC2012X65N" V 8230 6450 50  0001 C CNN
F 3 "https://www.digikey.com/en/products/detail/yageo/RC0201FR-079K1L/5280717" H 8300 6450 50  0001 C CNN
	1    8300 6450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8000 6050 8500 6050
Wire Wire Line
	8500 6050 8500 6350
Wire Wire Line
	8000 6350 8500 6350
Connection ~ 8500 6350
Wire Wire Line
	8500 6350 8500 6450
Wire Wire Line
	8450 6450 8500 6450
Connection ~ 8500 6450
Wire Wire Line
	8500 6450 8500 6600
Wire Wire Line
	8000 5950 8000 5550
Wire Wire Line
	8000 5550 7100 5550
Wire Wire Line
	7100 5550 7100 5950
Connection ~ 7100 5550
Wire Wire Line
	6850 5550 7100 5550
$Comp
L power:GND #PWR021
U 1 1 61ADC135
P 7900 5000
F 0 "#PWR021" H 7900 4750 50  0001 C CNN
F 1 "GND" V 7905 4872 50  0000 R CNN
F 2 "" H 7900 5000 50  0001 C CNN
F 3 "" H 7900 5000 50  0001 C CNN
	1    7900 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	10800 7050 10800 6300
Wire Wire Line
	10800 6300 11400 6300
Wire Wire Line
	11100 6800 11100 9250
Wire Wire Line
	7300 9250 8600 9250
Wire Wire Line
	11100 6800 11400 6800
Wire Wire Line
	10650 4600 10900 4600
Wire Wire Line
	10900 4600 10900 4500
Wire Wire Line
	10900 4500 11050 4500
Wire Wire Line
	11400 4500 11400 5050
Connection ~ 11050 4500
Wire Wire Line
	11050 4500 11400 4500
Wire Wire Line
	10650 4700 10900 4700
Wire Wire Line
	10900 4700 10900 4800
Wire Wire Line
	10900 4800 11050 4800
Connection ~ 11400 4500
Wire Wire Line
	11400 4250 11400 4500
$Comp
L power:+8V #PWR026
U 1 1 61A2A285
P 11400 4250
F 0 "#PWR026" H 11400 4100 50  0001 C CNN
F 1 "+8V" H 11415 4423 50  0000 C CNN
F 2 "" H 11400 4250 50  0001 C CNN
F 3 "" H 11400 4250 50  0001 C CNN
	1    11400 4250
	1    0    0    -1  
$EndComp
Connection ~ 11050 4800
Wire Wire Line
	10650 4800 10650 5050
Wire Wire Line
	10650 5050 11400 5050
Connection ~ 11400 5050
Wire Wire Line
	11400 5050 11400 6200
$Comp
L power:PWR_FLAG #FLG04
U 1 1 61B92DCE
P 10950 9950
F 0 "#FLG04" H 10950 10025 50  0001 C CNN
F 1 "PWR_FLAG" H 10950 10123 50  0000 C CNN
F 2 "" H 10950 9950 50  0001 C CNN
F 3 "~" H 10950 9950 50  0001 C CNN
	1    10950 9950
	-1   0    0    1   
$EndComp
$Comp
L power:PWR_FLAG #FLG01
U 1 1 61B99C98
P 9850 10100
F 0 "#FLG01" H 9850 10175 50  0001 C CNN
F 1 "PWR_FLAG" H 9850 10273 50  0000 C CNN
F 2 "" H 9850 10100 50  0001 C CNN
F 3 "~" H 9850 10100 50  0001 C CNN
	1    9850 10100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR023
U 1 1 61B9D7DA
P 9850 10100
F 0 "#PWR023" H 9850 9850 50  0001 C CNN
F 1 "GND" V 9855 9972 50  0000 R CNN
F 2 "" H 9850 10100 50  0001 C CNN
F 3 "" H 9850 10100 50  0001 C CNN
	1    9850 10100
	1    0    0    -1  
$EndComp
$Comp
L power:+8V #PWR027
U 1 1 61BA10BA
P 11500 9950
F 0 "#PWR027" H 11500 9800 50  0001 C CNN
F 1 "+8V" H 11515 10123 50  0000 C CNN
F 2 "" H 11500 9950 50  0001 C CNN
F 3 "" H 11500 9950 50  0001 C CNN
	1    11500 9950
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR025
U 1 1 61BA5E6B
P 10950 9950
F 0 "#PWR025" H 10950 9800 50  0001 C CNN
F 1 "+5V" H 10965 10123 50  0000 C CNN
F 2 "" H 10950 9950 50  0001 C CNN
F 3 "" H 10950 9950 50  0001 C CNN
	1    10950 9950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR030
U 1 1 618539A5
P 3800 8850
F 0 "#PWR030" H 3800 8600 50  0001 C CNN
F 1 "GND" H 3805 8677 50  0000 C CNN
F 2 "" H 3800 8850 50  0001 C CNN
F 3 "" H 3800 8850 50  0001 C CNN
	1    3800 8850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C10
U 1 1 61857CDA
P 5050 8900
F 0 "C10" H 4950 8800 50  0000 C CNN
F 1 "2.2uF" H 4950 9000 50  0000 C CNN
F 2 "tailboard_signalling_pcb:CAPC2012X135N" H 5088 8750 50  0001 C CNN
F 3 "https://www.digikey.com/en/products/detail/samsung-electro-mechanics/CL21B105KOFNNNE/3886684" H 5050 8900 50  0001 C CNN
	1    5050 8900
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR032
U 1 1 61859DBB
P 5050 9050
F 0 "#PWR032" H 5050 8800 50  0001 C CNN
F 1 "GND" H 5055 8877 50  0000 C CNN
F 2 "" H 5050 9050 50  0001 C CNN
F 3 "" H 5050 9050 50  0001 C CNN
	1    5050 9050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C9
U 1 1 6185D2F9
P 3300 8900
F 0 "C9" H 3200 8800 50  0000 C CNN
F 1 "0.33uF" H 3200 9000 50  0000 C CNN
F 2 "tailboard_signalling_pcb:CAPC2012X135N" H 3338 8750 50  0001 C CNN
F 3 "https://www.digikey.com/en/products/detail/samsung-electro-mechanics/CL21B105KOFNNNE/3886684" H 3300 8900 50  0001 C CNN
	1    3300 8900
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR029
U 1 1 6185D727
P 3300 9050
F 0 "#PWR029" H 3300 8800 50  0001 C CNN
F 1 "GND" H 3305 8877 50  0000 C CNN
F 2 "" H 3300 9050 50  0001 C CNN
F 3 "" H 3300 9050 50  0001 C CNN
	1    3300 9050
	1    0    0    -1  
$EndComp
$Comp
L power:+8V #PWR028
U 1 1 61862993
P 3300 8750
F 0 "#PWR028" H 3300 8600 50  0001 C CNN
F 1 "+8V" H 3315 8923 50  0000 C CNN
F 2 "" H 3300 8750 50  0001 C CNN
F 3 "" H 3300 8750 50  0001 C CNN
	1    3300 8750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 8750 3800 8750
$Comp
L power:+5V #PWR031
U 1 1 61872A95
P 5050 8750
F 0 "#PWR031" H 5050 8600 50  0001 C CNN
F 1 "+5V" H 5065 8923 50  0000 C CNN
F 2 "" H 5050 8750 50  0001 C CNN
F 3 "" H 5050 8750 50  0001 C CNN
	1    5050 8750
	1    0    0    -1  
$EndComp
$Comp
L tailboard_signalling_pcb-rescue:NJM2845DL1-05-TE1-tailboard_signalling_pcb IC1
U 1 1 6187E413
P 3800 8750
F 0 "IC1" H 4350 9015 50  0000 C CNN
F 1 "NJM2845DL1-05-TE1" H 4350 8924 50  0000 C CNN
F 2 "TO-252-3-L1" H 4750 8850 50  0001 L CNN
F 3 "https://datasheet.datasheetarchive.com/originals/distributors/Datasheets_SAMA/34641714fd14802a23a049b754208e87.pdf" H 4750 8750 50  0001 L CNN
F 4 "LDO Voltage Regulators LDO w/On/Off Cntrl" H 4750 8650 50  0001 L CNN "Description"
F 5 "" H 4750 8550 50  0001 L CNN "Height"
F 6 "513-NJM2845DL105-TE1" H 4750 8450 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/NJR/NJM2845DL1-05-TE1?qs=Vf9KeiGtj%252BGbfCGSGlajaA%3D%3D" H 4750 8350 50  0001 L CNN "Mouser Price/Stock"
F 8 "New Japan Radio" H 4750 8250 50  0001 L CNN "Manufacturer_Name"
F 9 "NJM2845DL1-05-TE1" H 4750 8150 50  0001 L CNN "Manufacturer_Part_Number"
	1    3800 8750
	1    0    0    -1  
$EndComp
Connection ~ 5050 8750
Wire Wire Line
	4900 8750 5050 8750
Connection ~ 3300 8750
Wire Wire Line
	7100 4550 7100 4700
Wire Wire Line
	7100 4700 7500 4700
Connection ~ 7500 4700
Wire Wire Line
	7500 4700 7900 4700
Connection ~ 7900 4700
Wire Wire Line
	7900 4700 8450 4700
Wire Wire Line
	7100 4700 7100 5550
Connection ~ 7100 4700
$Comp
L power:PWR_FLAG #FLG03
U 1 1 6187C31D
P 10400 9950
F 0 "#FLG03" H 10400 10025 50  0001 C CNN
F 1 "PWR_FLAG" H 10400 10123 50  0000 C CNN
F 2 "" H 10400 9950 50  0001 C CNN
F 3 "~" H 10400 9950 50  0001 C CNN
	1    10400 9950
	-1   0    0    1   
$EndComp
$Comp
L power:+3V3 #PWR016
U 1 1 6187FB5E
P 10400 9950
F 0 "#PWR016" H 10400 9800 50  0001 C CNN
F 1 "+3V3" H 10415 10123 50  0000 C CNN
F 2 "" H 10400 9950 50  0001 C CNN
F 3 "" H 10400 9950 50  0001 C CNN
	1    10400 9950
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR017
U 1 1 6188337D
P 7100 4550
F 0 "#PWR017" H 7100 4400 50  0001 C CNN
F 1 "+3V3" H 7115 4723 50  0000 C CNN
F 2 "" H 7100 4550 50  0001 C CNN
F 3 "" H 7100 4550 50  0001 C CNN
	1    7100 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 618D1798
P 8600 9100
F 0 "R3" H 8700 9250 50  0000 C CNN
F 1 "5k" H 8700 8950 50  0000 C CNN
F 2 "tailboard_signalling_pcb:RESC2012X50N" V 8530 9100 50  0001 C CNN
F 3 "https://www.digikey.com/en/products/detail/vishay-dale/CRCW08055K00JNTA/5075662" H 8600 9100 50  0001 C CNN
	1    8600 9100
	1    0    0    -1  
$EndComp
Connection ~ 8600 9250
Wire Wire Line
	8600 9250 11100 9250
$Comp
L power:+5V #PWR024
U 1 1 618D2901
P 8600 8950
F 0 "#PWR024" H 8600 8800 50  0001 C CNN
F 1 "+5V" H 8615 9123 50  0000 C CNN
F 2 "" H 8600 8950 50  0001 C CNN
F 3 "" H 8600 8950 50  0001 C CNN
	1    8600 8950
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J2
U 1 1 6199E0A9
P 4700 6200
F 0 "J2" H 4780 6192 50  0000 L CNN
F 1 "Conn_01x04" H 4780 6101 50  0000 L CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x04_P2.00mm_Vertical" H 4700 6200 50  0001 C CNN
F 3 "~" H 4700 6200 50  0001 C CNN
	1    4700 6200
	-1   0    0    -1  
$EndComp
Text GLabel 4900 6200 2    50   Input ~ 0
USB_D-
Text GLabel 4900 6300 2    50   Input ~ 0
USB_D+
Text GLabel 7200 6150 0    50   Input ~ 0
USB_D-
Text GLabel 7200 6250 0    50   Input ~ 0
USB_D+
Wire Wire Line
	4900 5400 4550 5400
Connection ~ 4900 5400
Wire Wire Line
	4900 5400 4900 6100
Wire Wire Line
	4900 5300 4900 5400
$Comp
L power:+5V #PWR01
U 1 1 619AEC0A
P 4900 5300
F 0 "#PWR01" H 4900 5150 50  0001 C CNN
F 1 "+5V" H 4915 5473 50  0000 C CNN
F 2 "" H 4900 5300 50  0001 C CNN
F 3 "" H 4900 5300 50  0001 C CNN
	1    4900 5300
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 619AEC14
P 4550 5700
F 0 "#PWR02" H 4550 5450 50  0001 C CNN
F 1 "GND" V 4555 5572 50  0000 R CNN
F 2 "" H 4550 5700 50  0001 C CNN
F 3 "" H 4550 5700 50  0001 C CNN
	1    4550 5700
	-1   0    0    -1  
$EndComp
$Comp
L Device:CP C5
U 1 1 619AEC1E
P 4550 5550
F 0 "C5" V 4805 5550 50  0000 C CNN
F 1 "100uF" V 4714 5550 50  0000 C CNN
F 2 "tailboard_signalling_pcb:A765EB107M1CLAE025" H 4588 5400 50  0001 C CNN
F 3 "https://www.digikey.com/en/products/detail/kemet/A765EB107M1CLAE025/6196340" H 4550 5550 50  0001 C CNN
	1    4550 5550
	-1   0    0    -1  
$EndComp
$EndSCHEMATC
