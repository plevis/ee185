# Assignment 4: Taking Shape 

**Due: Monday, October 28, 2019 at 10:00 AM**

In this assignment, we will continue the work we started in class on 
Monday, October 21 so that we have working prototypes to use on the 28th.
We will also start thinking about artistic elements on the wings and
finish up our mechanical prototypes.

There are three items due.

## Part 1: Etching Shapes and Designs

Bring to class 3 (or more) printouts of designs, shapes, or graphics that 
you think
might look good etched on the dichroic wings of the shapes. These 
can be inspired by beauty, engineering, your own interests, the diversity
of engineering, the concept of flight, anything that you
think might look beautiful, or anything that makes a statement you
find valuable.

## Part 2: Finish your dichroic coaster/shape

Finish the dichroic coaster:
  1. Cut a small shape out of 1/4" acrylic
  1. Attach dichroic film to it
  1. Attach it to your motor arm with screws
  1. Attach an LED strip to it
  1. Write a simple CircuitPython program that animates the LEDs and moves the coaster so we can see different colors and effects.

In Monday (10/28) class we will look at the coasters in the sunlight and 
in the dark,
to see how the dichroic reflects, shadows and interacts with LEDs. Program
your board with your animation/control before class. We will have USB
power packs so we can power your setup without needing your laptop.

## Part 3: Finish making the body of your shape

Cut and assemble **two** instances of your body shape. It should be strong 
enough to attach
dichroic acrylic wings to it. You can make it out of Duron or acrylic or
some other material. In the weekly workshop we cut several wing
designs, which we will use in class on Monday. These designs are not
final by any means, but they will give us some designs to attach and
see what they look like in the stairwell in class on 10/28.

### Handing in 

Bring your completed items to class, where we will check you off.


