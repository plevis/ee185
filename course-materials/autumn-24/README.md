# Course Materials for Autumn 2024

This directory stores the course materials for the Winter 2024
offering of EE285/CS241. Directory structure:
  - board: The printed circuit board (PCB) within each flyer
  - plate: the aluminum plate that defines the profile of the flyer and which everything attaches to
  - shell: vacuum forming and cutting the acrylic body shell
  - signaling: communicating USB differential signaling over an Ethernet twisted pair
  - software: firmware and GUI for controlling the installation


