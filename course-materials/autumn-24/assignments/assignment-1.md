# Assignment 1: The Big Picture and Picking a Group

*Written by Philip Levis*

**Due: Wednesday, September 25, 2024 at 7PM**

This quarter, you're each going to contribute to FLIGHT.  FLIGHT is a
complex system that has a lot of engineering and insight behind
it. For example, to pick the acrylic we should use for the wings,
students ran controlled experiments in a vacuum chamber over 2 weeks.
The Python firmware on a Fractal Flyer involved students writing and
testing interrupt handling code for a PID
(proportional-integrative-derivative) control loop for the wings over
six weeks.

There are five major projects in FLIGHT this quarter: software, 
boards, signaling, body shell and top plate. In this assignment, you 
will learn about these projects and submit your preferences for which
interest you the most.

## Goals

When you're done with this assignment, you should
  - Understand all of the parts of FLIGHT at a high level and how
    they fit together.
  - Have read about the projects this quarter and submitted your
    preferences for which ones you'd like to work on.

## 1 Read the FLIGHT Design Document

Go to the [course repository](https://code.stanford.edu/plevis/ee185).
If you are comfortable with the `git` tool, you can clone it and
browse the files locally. If you're not familiar with `git`, don't
worry about it: just browse the files through the web interface. You
will eventually need to use git to access the repository, but don't
worry about it for now.

Poke around the repository a bit to see what's in there.  Not all of
the project materials are there. In particular, the CAD for a Fractal
Flyer is in an online CAD program called OnShape. There's a link to
the CAD model in the README for the project, which is rendered as a
web page at the top level of the repository when viewing it through
the web interface.  You should be able to view the CAD but not modify
it -- if you want to play with it, make a copy for yourself and do
so. As you get further along in projects, we'll open up write
permissions to the CAD.

Once you have looked around a bit, read the following documents:
  - The Flyer Construction Manual. This is a detailed, step-by-step
  instruction manual on how to assemble a Fractal Flyer. It is from
  September 2021 so is slightly out of date. Reading this will give
  you a good sense of the physicality of a Flyer and all of the
  dependencies in it.
  - The FLIGHT design document. This gives an overview of the entire system,
  talking about issues in each component, how they've arisen, and how they
  are solved. 

As you poke around the repository, find out the answers to these questions:
 1. Where do you find materials/documents for this quarter's class?
 2. Where do you find Python scripts that run on a Fractal Flyer?
 3. Where do you find architectural drawings of the Packard building?
 4. Why did we choose acrylic pre-treated with dichroic film as the
 wing material (hint: find and skim the wing materials report from
 winter 2020)?
 5. How is power delivered to a Fractal Flyer?
 6. Why do we use rivets when possible?

There will be a short quiz in class on Thursday on the FLIGHT design.
Students who do not pass the quiz will write a 2-page report on an aspect 
of FLIGHT's design.

## 2 Project Preferences

Now that you have a sense of the overall FLIGHT project, read through 
the 5 project descriptions:
  - [The acrylic body shell](../shell/shell-description.pdf)
  - [Software](../software/software-description.pdf)
  - [The Flyer top plate](../plate/plate-description.pdf)
  - [Flyer PCB](../board/board-description.pdf)
  - [Signaling USB over Ethernet](../signaling/signaling-description.pdf)

After reading them, fill out this short [survey](https://forms.gle/wqwRhG4tr36bcq116).

You're done!


