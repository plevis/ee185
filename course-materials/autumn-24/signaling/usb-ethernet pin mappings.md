# USB and Ethernet Pinout and Mapping

## Ethernet (T568B) Pinout

Striped color has a white/color wire, solid color has a color wire. E.g. striped
orange is white/orange, solid orange is orange.

| Pin Number | Wire color     |
| ---------- | -------------- |
| 1          | striped orange |
| 2          | solid orange   |
| 3          | striped green  |
| 4          | solid blue     |
| 5          | striped blue   |
| 6          | solid green    |
| 7          | striped brown  |
| 8          | solid brown    |

## USB Pinout

| Pin number | Function |
| ---------- | -------- |
| 1          | Vbus     |
| 2          | Data-    |
| 3          | Data+    |
| 4          | Gnd      |

## USB/Reset - Ethernet mapping

Power and ground are routed along multiple wires to split the power.

| USB Pin Number | Function | Ethernet Pin Number | T568B Color          |
| -------------- | -------- | ------------------- | -------------------- |
| 1              | Vbus     | 7, 8                | striped, solid brown |
| 2              | Data-    | 1                   | solid orange         |
| 3              | Data+    | 2                   | striped orange       |
| 4              | Gnd      | 4, 5                | solid, striped blue  |
| -              | Reset-   | 6                   | solid green          |
| -              | Reset+   | 3                   | striped green        |