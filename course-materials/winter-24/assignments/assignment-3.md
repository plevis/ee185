# Assignment 3: The Final Countdown

*Written by Philip Levis*

**Final Presentations Due: March 12 and 14, in class**

**Final Report First Draft: Friday, March 15, 5PM**

**Final Report Minor Revision: Friday, March 22, 5PM**

You've been working hard all quarter. You've had setbacks.
You've had victories. We're now at the point when you write
it all up, so the next group of students can build on your
work and learn from your experience.

## Goals

When you're done with this assignment, you should have

- documented and explained the artifacts and processes 
you've built this quarter,

- committed all of your materials to the course repository
for future contributors of the project to benefit from,

- be proud of all you've accomplished!

## 1 Final Reports

You need to, as a group, write up a final report that documents
the results of your work this quarter.  Be sure to include clear, 
high-quality figures, which you write clear captions for.

### Structure

Your final report should include:
  - An introduction (1 page). This should explain what your report
is and give a high-level summary of its contents.
  - A table of contents, with page numbers.
  - A problem statement, that describes the part of FLIGHT you
worked on and its requirements. Note that this is distinct from
your tasks for the quarter, which might only encompass a subset
of the requirements. 
  - Your work results, broken down in a structured way into smaller
parts. For example, the shell group might break its technical sections
into the buck, acrylic selection and sourcing, and vacuum forming.
  - An open questions/future work section. If we were teaching
this class again in 3 weeks, where should people pick up?

### Style and Focus

The final report differs
from the mid-term report in that it should document the **results**
of your work, not the work itself. Rather than describing everything
you did, it should describe the end result. This is important because
it's the hand-off point for future students. While it's useful to us
as instructors to know that you tried 7 things that didn't work, the 
space and time you use to describe this is much less useful to future 
students than describing in detail the final approach you took.

Of course, you want to some degree also explain the *why* of the design.
This allows future students to understand your decisions, and the issues
you dealt with to get there. However, there's a subtle difference between
explaining the why and the how. To show you the distinction, I'll repeat
the example I have in class, of work in the software group to uniquely
identify Flyers over USB. The group spent several weeks learning and
understanding the firmware, how it sets serial IDs, and how you can
change code compilation to change the ID, compiling a unique firmware
image for each Flyer. However, it turns out there's a unique identifier
burned into every processor, which you can easily use, so all of this work
wasn't needed (although doing it is what let them realize there was a hardware
ID). The final report should explain that the hardware ID is used, and 
how that works, but doesn't need to document all of the effort spent to
understand the USB stack and compilation path. 

Put another way, you want to document where you ended up, not the path
you took to get there. You're documenting your work product, not your
work process.

In addition to explaining the end result, you want to explain the reasons
behind design decisions. To take a second example, from the boards team,
documenting that a particular H-bridge chip was used because its large footprint
makes it easy to probe, hand-solder, and debug is great. The next students
will know both that this is important as well as it wasn't a deep technical
subtlety hidden in the datasheet. But you shouldn't go into the fact that
you had a short when soldering the previous, smaller chip.

Imagine, for example, that you needed to go away for a year then come
back to your work. You'd want to explain the exact state you're in, and
the design tradeoffs and decisions that got you there, but not necessarily
every thing you did.

### Revision

Because your grade is based a lot on your final report, but you likely
have not written many reports like this before, you'll submit a first
draft and a revision. We will provide comments on the first draft, with
concrete suggestions on what you should do to improve it for the final
hand-in. Assuming you've taken care with your first draft and it's something
you're proud of, this revision should be relatively minor (3-4 hours of work total).

## 2 Final Presentations

Your final presentions will be in class on Week 10. Like the mid-term reports,
plan on presenting for 20 minutes. Unlike the mid-term reports, we will keep you 
to this and cut you off if you go over. So practice your timing, and keep a timer.

Your final presentation should summarize your effort over the quarter: it can document
things you tried, and lessons learned. Your final presentation should:
  - Remind everyone of the tasks of your project.
  - Summarize the completion status of each.
  - Explain how completed tasks were completed.
  - Explain what disrupted not completed tasks. This doesnt need to be detailed if it's
  simple, e.g., "The vacuum former was not operational until week 7."
  - Give a high-level summary of your end work product.
  - Articulate "lessons learned" that you'd want to pass on to the next group.
  - Call out major victories and mistakes.
  - Reflect on how, with the knowledge you have now, you might have gone about
  the process differently.

Presentation schedule:

 - *Tuesday:* Shell, Software, and Mounting
 - *Thursday:* Signaling and Electronics

## 3 Handing In

You should put your report and slides in your directory in the course repository.
This constitutes your handin. Don't forget to push!

